//
//  CameraSDKManager.m
//  Camera
//
//  Created by Ｃhun-Yang Lin on 2020/5/9.
//  Copyright © 2020 Jarvish. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CameraSDKManager.h"
#import "WifiCamControl.h"
#include "PreviewSDKEventListener.h"
#import "DBHelper.h"
#import "MediaFile.h"
#import "SettingViewSDKEventListener.h"
#import "ICatchWificamSession.h"

#define CONNECT_RETRY_COUNT 3
#define POLLING_TIME_CAMERA_STATUS 60   // 每分鐘拿一次電量與剩餘容量
#define POLLING_TIME_SENSOR_DATA 10     // 每兩秒拿一次sensor data

@interface CameraSDKManager()
@property (nonatomic, strong) NSMutableArray *cameraObserver;
@property (nonatomic, strong) NSMutableArray *valueObserver;
@property (nonatomic, strong) NSMutableDictionary *deviceDictionary;

@property (nonatomic, assign) CamMode currentMode;
@property (nonatomic, assign) BOOL isCameraConnected;

//// orig SDK wapper params
//@property (nonatomic, strong) WifiCamCamera *camera;
//@property (nonatomic, strong) WifiCamControlCenter *ctrl;
//@property (nonatomic, strong) WifiCamStaticData *staticData;

@property (nonatomic) ICatchWificamSession *theSession;

@property (nonatomic) dispatch_group_t previewGroup;
@property (nonatomic) dispatch_queue_t audioQueue;
@property (nonatomic) dispatch_queue_t videoQueue;
@property (nonatomic) dispatch_semaphore_t previewSemaphore;

@property (nonatomic) NSMutableArray *alertTableArray;
@property(nonatomic) WifiCamAlertTable *tbDelayCaptureTimeArray;
@property(nonatomic) WifiCamAlertTable *tbVideoSizeArray;
@property(nonatomic) WifiCamAlertTable *tbPhotoSizeArray;

@property(nonatomic) NSTimer *videoCaptureTimer;
@property(nonatomic) uint movieRecordElapsedTimeInSeconds;

@property(nonatomic, getter = isPVRun) BOOL PVRun;
@property(nonatomic, getter = isPVRunning) BOOL PVRunning;


// camera values
@property (nonatomic, strong) CameraStatus *cameraStatus;
@property (nonatomic, strong) SensorData *sensorData;
@end

@implementation CameraSDKManager {
    StillCaptureDoneListener *stillCaptureDoneListener;
    VideoRecOffListener *videoRecOffListener;
    VideoRecOnListener *videoRecOnListener;
    VideoRecPostTimeListener *videoRecPostTimeListener;
    SettingViewSDKEventListener *FWUpdateCheckEventListener;
    SettingViewSDKEventListener *udpateFWCompleteEventListener;
    SettingViewSDKEventListener *updateFWPowerOffEventListener;
    SettingViewSDKEventListener *updateFWCksumEventListener;
    SettingViewSDKEventListener *helmetDisconnectListener;
//    SettingViewSDKEventListener *wifiDisconnectedListener;
}

+(CameraSDKManager*)sharedManager {
    static CameraSDKManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

-(id)init {
    self = [super init];
    if (self) {
//        [self WifiCamInit];
        self.cameraObserver = [[NSMutableArray alloc] init];
        self.valueObserver = [[NSMutableArray alloc] init];
        self.isCameraConnected = NO;
    }
    
    return self;
}

#pragma mark - camera observer
-(void)addCameraObserver:(id<ICameraObserver>)observer {
    @synchronized (self.cameraObserver) {
        if (![self.cameraObserver containsObject:observer]) {
            [self.cameraObserver addObject:observer];
        }
    }
}

-(void)removeCameraObserver:(id<ICameraObserver>)observer {
    @synchronized (self.cameraObserver) {
        [self.cameraObserver removeObject:observer];
    }
}

-(void)updatePreviewImg:(UIImage*)img {
    @synchronized (self.cameraObserver) {
        for (id<ICameraObserver> obs in self.cameraObserver) {
            [obs onReceivedPreviewImg:img];
        }
    }
}

-(void)actionCompleted:(CamAction)action {
    @synchronized (self.cameraObserver) {
        for (id<ICameraObserver> obs in self.cameraObserver) {
            [obs onActionCompleted:action];
        }
    }
}

-(void)updateRecordTime:(NSString*)timeStr {
    @synchronized (self.cameraObserver) {
        for (id<ICameraObserver> obs in self.cameraObserver) {
            [obs onMovieRecordElapsedTime:timeStr];
        }
    }
}



#pragma mark - value observer
-(void)addValueObserver:(id<IValueObserver>)observer {
    @synchronized (self.valueObserver) {
        id<IValueObserver> needRemove = nil;
        for (id<IValueObserver> obs in self.valueObserver) {
            if ([obs isKindOfClass:[observer class]]) {
                needRemove = obs;
                break;
            }
        }
        
        if (needRemove) {
            [self removeValueObserver:needRemove];
        }
        
        [self.valueObserver addObject:observer];
    }
}

-(void)removeValueObserver:(id<IValueObserver>)observer {
    @synchronized (self.valueObserver) {
        [self.valueObserver removeObject:observer];
    }
}

-(void)updateValue:(id)value withType:(ValueType)type {
    @synchronized (self.valueObserver) {
        for (id<IValueObserver> obs in self.valueObserver) {
            [obs onValueUpdate:value forValueType:type];
        }
    }
}

#pragma mark - camera operation
-(void)connectToCamera {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if ([WifiCamControl initSDK]) {
            NSLog(@"SDK init success");
            [WifiCamControl scan];
            WifiCamManager *app = [WifiCamManager instance];
            WifiCam *wifiCam = [app.wifiCams objectAtIndex:0];
            wifiCam.camera = [WifiCamControl createOneCamera];
            
            self.camera = wifiCam.camera;
            self.ctrl = wifiCam.controler;
            
            
            self.staticData = [WifiCamStaticData instance];
            [self constructPreviewData];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self getDevicePlist];
                [self getVersionAndDeviceID];
                self.isCameraConnected = YES;
                [self actionCompleted:CamActionConnectedToCamera];
                
            });
            //註冊 disconnect event
            
                helmetDisconnectListener = new HelmetDisconnectListener(self);
                [_ctrl.comCtrl addObserver:ICATCH_EVENT_CONNECTION_DISCONNECTED
                                  listener:helmetDisconnectListener
                               isCustomize:NO];
            
//            if( ! wifiDisconnectedListener ){
//                wifiDisconnectedListener = new WifiDisconnectedListener(self);
//                [_ctrl.comCtrl addObserver:ICATCH_EVENT_CONNECTION_DISCONNECTED
//                                  listener:wifiDisconnectedListener
//                               isCustomize:NO];
//            }else{
//                [_ctrl.comCtrl addObserver:ICATCH_EVENT_CONNECTION_DISCONNECTED
//                                  listener:wifiDisconnectedListener
//                               isCustomize:NO];
//            }
            [self retrieveCameraDataLoop];
            
        } else {
            NSLog(@"SDK init error");
            dispatch_async(dispatch_get_main_queue(), ^{
                self.isCameraConnected = NO;
                [self actionCompleted:CamActionCameraDisconnected];
            });
        }
    });
}

-(void)shot {
    dispatch_async(dispatch_get_main_queue(), ^{
        // Check preview is still running
        if (!self.PVRunning) {
            AppLog(@"PV is already dead..!");
            [self actionCompleted:CamActionPreviewDead];
            return;
        }
        
        // Check existence of SD card
        if (![_ctrl.propCtrl checkSDExist]) {
            NSLog(@"NoCard");
            [self actionCompleted:CamActionNoCard];
            return;
        }
        
        AppLog(@"camera.previewMode: %lu", (unsigned long)_camera.previewMode);
        [self stillCapture];
    });
}

-(void)startRec {
    // 先call stop
    // 直接搬stop code過來
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        if ([self capableOf:WifiCamAbilityGetMovieRecordedTime]) {
            [_ctrl.comCtrl removeObserver:(ICatchEventID)0x5001 listener:videoRecPostTimeListener
                              isCustomize:YES];
            if (videoRecPostTimeListener) {
                delete videoRecPostTimeListener;
                videoRecPostTimeListener = NULL;
            }
        }
        
        BOOL ret = [_ctrl.actCtrl stopMovieRecord];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([_videoCaptureTimer isValid]) {
                [_videoCaptureTimer invalidate];
                self.movieRecordElapsedTimeInSeconds = 0;
            }
            
            if (ret) {
                [self remMovieRecListener];
                
                // 原本start rec的code
                dispatch_async(dispatch_get_main_queue(), ^{
                    AppLog(@"_camera.storageSpaceForVideo: %d", _camera.storageSpaceForVideo);
                    if (_camera.storageSpaceForVideo==0 && [_ctrl.propCtrl connected]) {
                        NSLog(@"card full");
                        [self actionCompleted:CamActionCardFull];
                        return;
                    }
                    
                    _camera.previewMode = WifiCamPreviewModeVideoOn;
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        if ([self capableOf:WifiCamAbilityGetMovieRecordedTime]) {
                            videoRecPostTimeListener = new VideoRecPostTimeListener(self);
                            [_ctrl.comCtrl addObserver:(ICatchEventID)0x5001 listener:videoRecPostTimeListener
                                           isCustomize:YES];
                        }
                        
                        BOOL ret = [_ctrl.actCtrl startMovieRecord];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (ret) {
                                
                                [self actionCompleted:CamActionRecordingStart];
                                
                                [self addMovieRecListener];
                                
                                if (![self capableOf:WifiCamAbilityGetMovieRecordedTime]) {
                                    if (![_videoCaptureTimer isValid]) {
                                        self.videoCaptureTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                                                target  :self
                                                                                                selector:@selector(movieRecordingTimerCallback:)
                                                                                                userInfo:nil
                                                                                                repeats :YES];
                                    }
                                }
                                
                            } else {
                                NSLog(@"Failed to begin movie recording.");
                                [self actionCompleted:CamActionMovieRecordingFailed];
                            }
                            
                        });
                    });
                });
                
            } else {
                NSLog(@"Failed to stop movie recording.");
                [self actionCompleted:CamActionMovieRecordingFailed];
            }
        });
    });
}

-(void)stopRec {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        if ([self capableOf:WifiCamAbilityGetMovieRecordedTime]) {
            [_ctrl.comCtrl removeObserver:(ICatchEventID)0x5001 listener:videoRecPostTimeListener
                              isCustomize:YES];
            if (videoRecPostTimeListener) {
                delete videoRecPostTimeListener;
                videoRecPostTimeListener = NULL;
            }
        }
        
        BOOL ret = [_ctrl.actCtrl stopMovieRecord];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([_videoCaptureTimer isValid]) {
                [_videoCaptureTimer invalidate];
                self.movieRecordElapsedTimeInSeconds = 0;
            }
            
            if (ret) {
                [self remMovieRecListener];
                [self actionCompleted:CamActionRecordingStop];
                
            } else {
                NSLog(@"Failed to stop movie recording.");
                [self actionCompleted:CamActionMovieRecordingFailed];
            }
            
            _camera.previewMode = WifiCamPreviewModeVideoOff;
        });
    });
}

#pragma mark - camera status
-(void)updateCamMode:(CamMode)mode {
    if (self.currentMode == mode) {
        return;
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        (mode == CamModePhoto) ? [self changeToCameraState] : [self changeToVideoState];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            self.currentMode = mode;
            [NSThread sleepForTimeInterval:2.0f];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self actionCompleted:CamActionChangeCameraState];
            });
        });
    });
}

-(CamMode)currentCamMode {
    return self.currentMode;
}

-(BOOL)isRecording {
    return _camera.previewMode == WifiCamPreviewModeVideoOn;
}

-(BOOL)isConnected {
    return [[SDK instance] isConnected];
//    return self.isCameraConnected;
}

-(void)startPreview {
    NSLog(@"startPreview");
    [self showPreview];
}

-(void)stopPreview {
    NSLog(@"stopPreview");
    if (self.PVRun) {
        self.PVRun = NO;
        [NSThread sleepForTimeInterval:0.2];
    }
}

#pragma mark - private methods for camera preview
-(void)constructPreviewData {
    NSLog(@"constructPreviewData");
    BOOL onlyStillFunction = YES;
    
    self.previewGroup = dispatch_group_create();
    self.audioQueue = dispatch_queue_create("WifiCam.GCD.Queue.Preview.Audio", 0);
    self.videoQueue = dispatch_queue_create("WifiCam.GCD.Queue.Preview.Video", 0);
    
    self.previewSemaphore = dispatch_semaphore_create(1);
    
    self.alertTableArray = [[NSMutableArray alloc] init];
    
    
    if ([self capableOf:WifiCamAbilityMovieRecord]) {
        if ([self capableOf:WifiCamAbilityVideoSize]) {
            self.tbVideoSizeArray = [self.ctrl.propCtrl prepareDataForVideoSize:self.camera.curVideoSize];
        }
        
        [self constructMovieRec];
        onlyStillFunction = NO;
    }
    
    if ([self capableOf:WifiCamAbilityStillCapture]){
        if ([self capableOf:WifiCamAbilityImageSize]) {
            self.tbPhotoSizeArray = [self.ctrl.propCtrl prepareDataForImageSize:self.camera.curImageSize];
        }
        
        if ([self capableOf:WifiCamAbilityDelayCapture]) {
            self.tbDelayCaptureTimeArray = [self.ctrl.propCtrl prepareDataForDelayCapture:self.camera.curCaptureDelay];
        }
        
        if (onlyStillFunction) {
            self.camera.previewMode = WifiCamPreviewModeCameraOff;
        }
    }
    
    NSLog(@"cameraMode: %d", self.camera.cameraMode);
    switch (self.camera.cameraMode) {
        case MODE_VIDEO_OFF: {
            self.camera.previewMode = WifiCamPreviewModeVideoOff;
            
            // Manager param
            self.currentMode = CamModeVideo;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self actionCompleted:CamActionChangeCameraState];
            });
            break;
        }
            
            
        case MODE_CAMERA: {
            self.camera.previewMode = WifiCamPreviewModeCameraOff;
            
            // Manager param
            self.currentMode = CamModePhoto;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self actionCompleted:CamActionChangeCameraState];
            });
            break;
        }
            
        case MODE_IDLE:
            break;
            
        case MODE_SHARED:
            break;
            
        case MODE_TIMELAPSE_STILL:
            self.camera.previewMode = WifiCamPreviewModeTimelapseOn;
            self.camera.timelapseType = WifiCamTimelapseTypeStill;
            break;
            
        case MODE_TIMELAPSE_VIDEO:
            self.camera.previewMode = WifiCamPreviewModeTimelapseOn;
            self.camera.timelapseType = WifiCamTimelapseTypeVideo;
            break;
            
        case MODE_VIDEO_ON: {
            self.camera.previewMode = WifiCamPreviewModeVideoOn;
            
            // Manager param
            self.currentMode = CamModeVideo;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self actionCompleted:CamActionChangeCameraState];
            });
            break;
        }
            
            
        case MODE_UNDEFINED:
        default:
            break;
    }
    
    // Movie rec
    if ([self capableOf:WifiCamAbilityMovieRecord]) {
        videoRecOnListener = new VideoRecOnListener(self);
        [_ctrl.comCtrl addObserver:ICATCH_EVENT_VIDEO_ON listener:videoRecOnListener
                       isCustomize:NO];
    }
    
}


-(void)constructMovieRec {
    NSLog(@"constructMovieRec");
    if (self.camera.movieRecording) {
        [self addMovieRecListener];
        if (![self.videoCaptureTimer isValid]) {
            self.videoCaptureTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                    target  :self
                                                                    selector:@selector(movieRecordingTimerCallback:)
                                                                    userInfo:nil
                                                                    repeats :YES];
            
            if ([self capableOf:WifiCamAbilityGetMovieRecordedTime]) {
                self.movieRecordElapsedTimeInSeconds = [self.ctrl.propCtrl retrieveCurrentMoviceRecordElapsedTime];
                NSLog(@"elapsedTimeInSeconds: %d", self.movieRecordElapsedTimeInSeconds);
            }
            
        }
        
        self.camera.previewMode = WifiCamPreviewModeVideoOn;
    }
}

-(void)showPreview { // SDK DEMO's view did appear
    
    // Check SD card
    if (![_ctrl.propCtrl checkSDExist]) {
        NSLog(@"NoCard");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self actionCompleted:CamActionNoCard];
        });
    } else if ((_camera.previewMode == WifiCamPreviewModeCameraOff && _camera.storageSpaceForImage <= 0) || (_camera.previewMode == WifiCamPreviewModeCameraOff && _camera.storageSpaceForVideo==0)) {
        NSLog(@"CARD_FULL");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self actionCompleted:CamActionCardFull];
        });
    }
    
    // Prepare preview
    self.PVRun = YES;
    switch (_camera.previewMode) {
        case WifiCamPreviewModeCameraOff:
        case WifiCamPreviewModeCameraOn:
            [self runPreview:ICATCH_STILL_PREVIEW_MODE];
            break;
            
        case WifiCamPreviewModeTimelapseOff:
        case WifiCamPreviewModeTimelapseOn:
            if (_camera.timelapseType == WifiCamTimelapseTypeVideo) {
                [self runPreview:ICATCH_VIDEO_PREVIEW_MODE];
            } else {
                [self runPreview:ICATCH_STILL_PREVIEW_MODE];
            }
            break;
            
        case WifiCamPreviewModeVideoOff:
        case WifiCamPreviewModeVideoOn:
            [self runPreview:ICATCH_VIDEO_PREVIEW_MODE];
            break;
            
        default:
            break;
    }
}

- (void)runPreview:(ICatchPreviewMode)mode
{
    AppLog(@"%s start(%d)", __func__, mode);
    
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_time_t timeOutCount = dispatch_time(DISPATCH_TIME_NOW, 5ull * NSEC_PER_SEC);
    
    dispatch_async(globalQueue, ^{
        if (dispatch_semaphore_wait(_previewSemaphore, timeOutCount) != 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
            return;
        }
        
        if (![_ctrl.actCtrl startPreview:mode withAudioEnabled:false ]) {
            AppLog(@"Failed to start media stream.");
            dispatch_semaphore_signal(_previewSemaphore);
            dispatch_async(dispatch_get_main_queue(), ^{
            });
            return;
        }
        
        self.PVRunning = YES;
        
        if ([_ctrl.propCtrl videoStreamEnabled]) {
            dispatch_group_async(_previewGroup, _videoQueue, ^{[self playbackVideo];});
        } else {
            AppLog(@"No Video");
        }
        
        dispatch_group_notify(_previewGroup, globalQueue, ^{
            [_ctrl.actCtrl stopPreview];
            self.PVRunning = NO;
            dispatch_semaphore_signal(_previewSemaphore);
        });
    });
}

- (void)movieRecordingTimerCallback:(NSTimer *)sender {
    ++self.movieRecordElapsedTimeInSeconds;
    NSLog(@"movieRecordingTimerCallback %d", self.movieRecordElapsedTimeInSeconds);
    
    [self updateRecordTime:[Tool translateSecsToString:self.movieRecordElapsedTimeInSeconds]];
}

- (void)playbackVideo {
    NSMutableData *videoFrameData = nil;
    UIImage *receivedImage = nil;
    
    while (self.PVRun) {
        videoFrameData = [[self.ctrl.propCtrl prepareDataForVideoFrame] data];
        if (videoFrameData) {
            if (![self isJPEGValid:videoFrameData]) {
                AppLog(@"Invalid JPEG.");
                continue;
            }
            receivedImage = [[UIImage alloc] initWithData:videoFrameData];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (self.PVRun && receivedImage) {
                    [self updatePreviewImg:receivedImage];
                }
            });
            
            videoFrameData = nil;
            receivedImage = nil;
            
        } else {
            AppLog(@"videoFrameData is nil ...Check connection...");
            if (![self.ctrl.propCtrl connected]) {
                AppLog(@"[%s]Ooops...disconnected.", __func__);
                
                self.PVRun = NO;
                
                if ([self.videoCaptureTimer isValid]) {
                    [self.videoCaptureTimer invalidate];
                    self.movieRecordElapsedTimeInSeconds = 0;
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self actionCompleted:CamActionCameraDisconnected];
                });
                
                
            } else {
                AppLog(@"It's still connected!");
            }
        }
    }
}

- (void)changeToCameraState {
    if (_camera.previewMode != WifiCamPreviewModeCameraOff) {
        if (![_ctrl.propCtrl checkSDExist]) {
            NSLog(@"NoCard");
            [self actionCompleted:CamActionNoCard];
            return;
        }
        
        //[self updatePreviewSceneByMode:WifiCamPreviewModeVideoOn];
        self.PVRun = NO;
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 10ull * NSEC_PER_SEC);
        if (dispatch_semaphore_wait(_previewSemaphore, time) != 0) {
        } else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_semaphore_signal(_previewSemaphore);
                self.PVRun = YES;
                [self runPreview:ICATCH_STILL_PREVIEW_MODE]; // TODO:Make certain its' over
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    _camera.previewMode = WifiCamPreviewModeCameraOff;
                });
                
            });
            
            
            if (_camera.storageSpaceForImage <= 0 && [_ctrl.propCtrl connected]) {
                AppLog(@"card full");
                [self actionCompleted:CamActionCardFull];
            }
        }
    }
}

-(void)changeToVideoState {
    if (_camera.previewMode != WifiCamPreviewModeVideoOff) {
        if (![_ctrl.propCtrl checkSDExist]) {
            NSLog(@"NoCard");
            [self actionCompleted:CamActionNoCard];
            return;
        }
        
        self.PVRun = NO;
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 10ull * NSEC_PER_SEC);
        if (dispatch_semaphore_wait(_previewSemaphore, time) != 0) {
        } else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_semaphore_signal(_previewSemaphore);
                self.PVRun = YES;
                [self runPreview:ICATCH_VIDEO_PREVIEW_MODE];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    _camera.previewMode = WifiCamPreviewModeVideoOff;
                });
            });
            if (_camera.storageSpaceForVideo==0 && [_ctrl.propCtrl connected]) {
                AppLog(@"card full");
                [self actionCompleted:CamActionCardFull];
            }
        }
    }
}

- (void)stillCapture {
    // Check whether sd card is full
    if (self.camera.storageSpaceForImage <= 0 && [self.ctrl.propCtrl connected]) {
        NSLog(@"CARD_FULL");
        [self actionCompleted:CamActionCardFull];
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (![self capableOf:WifiCamAbilityDelayCapture]
            || self.camera.curCaptureDelay == 0
            || ![self capableOf:WifiCamAbilityLatestDelayCapture]) {
            
            AppLog(@"Stop PV");
            self.PVRun = NO;
            dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 5ull * NSEC_PER_SEC); //
            if (dispatch_semaphore_wait(_previewSemaphore, time) != 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _camera.previewMode = WifiCamPreviewModeCameraOff;
                });
                return;
            }
        }
        
        stillCaptureDoneListener = new StillCaptureDoneListener(self);
        [_ctrl.comCtrl addObserver:ICATCH_EVENT_CAPTURE_COMPLETE
                          listener:stillCaptureDoneListener
                       isCustomize:NO];
        
        // Capture
        [self.ctrl.actCtrl triggerCapturePhoto];
    });
}

#pragma mark - SD card file operate
-(NSArray*)getSDFileListByType:(WCFileType)type {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    [self stopPreview];
    
    vector<ICatchFile> list;
    list = [[SDK instance] requestFileListOfType:type];
    
    for(vector<ICatchFile>::iterator it = list.begin(); it != list.end(); ++it) {
        ICatchFile *f = &(*it);
        MediaFile *m = [[MediaFile alloc] initWithICatchFile:f];
        UIImage *img = [[SDK instance] requestThumbnail:f];
        if(img) {
            m.thumbnail = img;
        }
        
        [result addObject:m];

    }
    
    return [result copy];
}

-(BOOL)downloadFile:(ICatchFile*)file cb:(void (^)(float progress))cb {
    BOOL result = NO;
    
    [self stopPreview];
    
    // get download precent
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        while (YES) {
            float progress = [self requestDownloadedPercent:file];
            dispatch_async(dispatch_get_main_queue(), ^{
                cb(progress);
            });
            
            [NSThread sleepForTimeInterval:0.2];
            if(progress == 1) {
                break;
            }
        }
    });
    
    result = [[SDK instance] downloadFile:file];
    return result;
}

-(BOOL)deleteFile:(ICatchFile*)file {
    BOOL result = NO;
    
    [self stopPreview];
    
    result = [[SDK instance] deleteFile:file];
    return result;
}



-(void)requestCameraStatus {
    if (![self isConnected]) {
        return;
    }
    
    // 這邊放main thread如果斷線會導致ui停掉
    // 因此放在背景跑。
    // 另外有一個同步的method requestCameraStatusSync. 給loop用的，不給前端用
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        unsigned int battery = [[SDK instance] retrieveBatteryLevel];
        NSInteger time = [self requestTimeRamin];
        
        @synchronized (self.cameraStatus) {
            self.cameraStatus = [[CameraStatus alloc] initWithBattery:battery andTime:time];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateValue:self.cameraStatus withType:ValueTypeCameraStatus];
        });
    });
}

-(void)requestSensorData {
    if (![self isConnected]) {
        return;
    }
    
    NSString *rawData = [[SDK instance] getCustomizePropertyStringValue:0xD802];
    if (!rawData) {
        self.isCameraConnected = NO;
        [self stopPreview];
        return;
    }
    
    if ([rawData isEqualToString:@""]) {
        self.isCameraConnected = NO;
        [self stopPreview];
        return;
    }
    
    self.sensorData = [[SensorData alloc] initWithRawData:rawData];
    [self updateValue:self.sensorData withType:ValueTypeSensorData];
}

-(UIImage*)requestImage:(ICatchFile*)file {
    return [[SDK instance] requestImage:file];
}

-(BOOL)formatSD {
    return [[SDK instance] formatSD];
}

- (void) updataBinFileToSDCard:(NSString *)binPath
{
    std::string tabkePath = [binPath cStringUsingEncoding:[NSString defaultCStringEncoding]];
    
    NSString *remoteMainPath = [NSString stringWithFormat:@"/%@",[[binPath componentsSeparatedByString:@"/"] lastObject]];
    
    std::string remotePath = [remoteMainPath cStringUsingEncoding:[NSString defaultCStringEncoding]];
    
    [[SDK instance] updateBin:tabkePath
                   remotePath:remotePath];
}

- (void) updataToSDCard:(NSString *)fwPath
{
    if( ! FWUpdateCheckEventListener ){
        FWUpdateCheckEventListener = new UpdateFWCheckListener(self);
        [_ctrl.comCtrl addObserver:ICATCH_EVENT_FW_UPDATE_CHECK
                          listener:FWUpdateCheckEventListener
                       isCustomize:NO];
    }
    if( !udpateFWCompleteEventListener ){
        udpateFWCompleteEventListener = new UpdateFWCompleteListener(self);
        [_ctrl.comCtrl addObserver:ICATCH_EVENT_FW_UPDATE_COMPLETED
                          listener:udpateFWCompleteEventListener
                       isCustomize:NO];
    }
    if( ! updateFWPowerOffEventListener ){
        updateFWPowerOffEventListener = new UpdateFWCompletePowerOffListener(self);
        [_ctrl.comCtrl addObserver:ICATCH_EVENT_FW_UPDATE_POWEROFF
                          listener:updateFWPowerOffEventListener
                       isCustomize:NO];
    }
    if( ! updateFWCksumEventListener ){
        updateFWCksumEventListener = new UpdateFWCKSUMListener(self);
        [_ctrl.comCtrl addObserver:ICATCH_EVENT_FW_UPDATE_CHKSUMERR
                          listener:updateFWCksumEventListener
                       isCustomize:NO];
    }
    //    [self showProgressHUDWithMessage:@"Updating..."];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        FILE *fileHandle = fopen([fwPath UTF8String],"rb");
        void *buf = malloc(30*1024*1024);
        if (buf != NULL)
        {
            size_t n = fread(buf, sizeof(char), 30*1024*1024, fileHandle);
            
            FILE *toFileHandle = fopen([fwPath cStringUsingEncoding:NSASCIIStringEncoding], "w+");
            fwrite(buf, sizeof(char), n, toFileHandle);
            free(buf);
            fclose(fileHandle);
            fclose(toFileHandle);
            AppLog(@"FW : %@", fwPath);
            std::string path = [fwPath cStringUsingEncoding:[NSString defaultCStringEncoding]];
            NSLog(@"*** time stamp (SDKManager updataToSDCard) = %@",[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]);
            [_ctrl.comCtrl updateFW:path];
            
        }
        else
        {
            
        }
    });
}

- (ICatchEventID) feedBackStatus:(ICatchEventID)status
{
    return status;
}

-(void)updateFWCompleted {
    TRACE();
    NSLog(@"*** time stamp (notification updateFWCompleted) = %@",[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]);
    dispatch_async(dispatch_get_main_queue(), ^{
        //        [self showProgressHUDNotice:@"Update Done" showTime:1.0];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EventStatus"
                                                            object:@"UpdateDone"];
        
//        [self feedBackStatus:ICATCH_EVENT_FW_UPDATE_COMPLETED];
        
        [_ctrl.comCtrl removeObserver:ICATCH_EVENT_FW_UPDATE_COMPLETED
                             listener:FWUpdateCheckEventListener
                          isCustomize:NO];
        if (FWUpdateCheckEventListener) {
            delete FWUpdateCheckEventListener;
            FWUpdateCheckEventListener = NULL;
        }
    });
    
}

-(void)updateFWPowerOff {
    TRACE();
    NSLog(@"*** time stamp (notification updateFWPowerOff) = %@",[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]);
    dispatch_async(dispatch_get_main_queue(), ^{
        //        [self showProgressHUDNotice:@"PowerOff" showTime:1.5];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EventStatus"
                                                            object:@"PowerOff"];
        
        [_ctrl.comCtrl removeObserver:ICATCH_EVENT_FW_UPDATE_POWEROFF
                             listener:udpateFWCompleteEventListener
                          isCustomize:NO];
        if (udpateFWCompleteEventListener) {
            delete udpateFWCompleteEventListener;
            udpateFWCompleteEventListener = NULL;
        }
    });
    
}

-(void)udpateFWCheck:(int)retValue {
    TRACE();
    NSLog(@"*** time stamp (notification udpateFWCheck) = %@",[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]);
    NSString *msg = [NSString stringWithFormat:@"Check ret:%d",retValue];
    AppLog(@"Check ret:%d",retValue);
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self showProgressHUDNotice:msg showTime:1.5];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EventStatus"
                                                            object:@"StartUpdate"];
        
    });
    
}
-(void)updateFWCKSUM:(int)retValue{
    NSString *msg = [NSString stringWithFormat:@"CheckCKSUM ret:%d",retValue];
    AppLog(@"%@",msg);
    NSLog(@"*** time stamp (notification updateFWCKSUM) = %@",[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]);
    dispatch_async(dispatch_get_main_queue(), ^{
        //        [self showProgressHUDNotice:msg showTime:1.5];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EventStatus"
                                                            object:msg];
    });
}
-(void)updateHelmetDisconnect{
    TRACE();
    NSLog(@"*** time stamp (notification helmet disconnect) = %@",[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]);
    [[NSNotificationCenter defaultCenter]postNotificationName:DISCONNECT_HELMET_NOTIFICATION object:nil];
   dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    [[SDK instance]destroySDK];
   });
}
//-(void)wifiDisconnected:(int)retValue{
//    NSString *msg = [NSString stringWithFormat:@"WifiDisconnect ret:%d",retValue];
//    AppLog(@"%@",msg);
//    NSLog(@"*** time stamp (notification wifiDisconnected) = %@",[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]);
//    [[SDK instance]destroySDK];
////    dispatch_async(dispatch_get_main_queue(), ^{
////                [self showProgressHUDNotice:msg showTime:1.5];
////
////        [[NSNotificationCenter defaultCenter] postNotificationName:@"EventStatus"
////                                                            object:msg];
////    });
//}
#pragma mark - local file operate
-(NSArray*)getLocalFileListByType:(WCFileType)type {
    DBHelper *db = [[DBHelper alloc] init];
    return [[db getfileListByType:type] copy];
}

#pragma mark - other
-(void)changeResolution:(NSString*)resStr {
    NSLog(@"[CameraSDKManager changeResolution] str: %@", resStr);
    
    string cstr = [resStr UTF8String];
    int res = [[SDK instance] changeVideoSize:cstr];
    NSLog(@"[CameraSDKManager changeResolution] res: %d", res);
}

-(BOOL)setVol:(NSInteger)vol {
    return [[SDK instance] setCustomizeIntProperty:0xD704 value:(uint)vol];
}

-(BOOL)setLanguage:(NSInteger)language {
    return [[SDK instance] setCustomizeIntProperty:0xD703 value:(uint)language];
}

-(NSInteger)loadVol {
    return [[SDK instance] getCustomizePropertyIntValue:0xD704];
}

-(NSInteger)loadLanguage {
    return [[SDK instance] getCustomizePropertyIntValue:0xD703];
}

-(NSString*)loadVer {
    return [[SDK instance] getCustomizePropertyStringValue:0xD803];
}

-(NSString*)loadDeviceID {
    return [[SDK instance] getCustomizePropertyStringValue:0xD804];
}

-(void)shutdown {
    
    if ([self capableOf:WifiCamAbilityMovieRecord] && videoRecOnListener) {
        [_ctrl.comCtrl removeObserver:ICATCH_EVENT_VIDEO_ON
                             listener:videoRecOnListener
                          isCustomize:NO];
        delete videoRecOnListener;
        videoRecOnListener = NULL;
    }
    
    self.isCameraConnected = NO;
    [self stopPreview];
    [[SDK instance] destroySDK];
}

#pragma mark - ICameraHandler
- (void)stopStillCapture {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"stop still capture");
        [self.ctrl.comCtrl removeObserver:ICATCH_EVENT_CAPTURE_COMPLETE
                                 listener:stillCaptureDoneListener
                              isCustomize:NO];
        
        if (stillCaptureDoneListener) {
            delete stillCaptureDoneListener;
            stillCaptureDoneListener = NULL;
        }
        
        self.PVRun = YES;
        dispatch_semaphore_signal(_previewSemaphore);
        [self runPreview:ICATCH_STILL_PREVIEW_MODE];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _camera.previewMode = WifiCamPreviewModeCameraOff;
            [self actionCompleted:CamActionStillCaptureDone];
        });
    });
}

- (void)updateMovieRecState:(MovieRecState)state {
    NSLog(@"updateMovieRecState %lu", (unsigned long)state);
}

- (void)postMovieRecordTime {
    NSLog(@"postMovieRecordTime");
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![self.videoCaptureTimer isValid]) {
            self.videoCaptureTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                    target  :self
                                                                    selector:@selector(movieRecordingTimerCallback:)
                                                                    userInfo:nil
                                                                    repeats :YES];
        }
    });
}

// 似乎沒用到
- (void)postMovieRecordFileAddedEvent {
    self.movieRecordElapsedTimeInSeconds = 0;
}

#pragma mark - add, remove movie record listener
- (void)addMovieRecListener {
    videoRecOffListener = new VideoRecOffListener(self);
    [self.ctrl.comCtrl addObserver:ICATCH_EVENT_VIDEO_OFF
                          listener:videoRecOffListener
                       isCustomize:NO];
    
    
    if ([self capableOf:WifiCamAbilityGetMovieRecordedTime]) {
        [self addObserver:self forKeyPath:@"movieRecordElapsedTimeInSeconds" options:0 context:nil];
    }
}

- (void)remMovieRecListener {
    [self.ctrl.comCtrl removeObserver:ICATCH_EVENT_VIDEO_OFF
                             listener:videoRecOffListener
                          isCustomize:NO];
    if (videoRecOffListener) {
        delete videoRecOffListener;
        videoRecOffListener = NULL;
    }
    
    if ([self capableOf:WifiCamAbilityGetMovieRecordedTime]) {
        [self removeObserver:self forKeyPath:@"movieRecordElapsedTimeInSeconds"];
    }
}

#pragma mark - private
-(NSInteger)requestTimeRamin {
    if (![self isConnected]) {
        return 0;
    }
    
    NSInteger result = [[SDK instance] getCustomizePropertyIntValue:0xD705];
    NSLog(@"requestTimeRamin %ld", (long)result);
    if (result > 240) {
        result = 240;
    }
    
    return result;
}

- (BOOL)capableOf:(WifiCamAbility)ability {
    return (self.camera.ability & ability) == ability ? YES : NO;
}

- (BOOL)isJPEGValid:(NSData *)jpeg {
    if ([jpeg length] < 4) return NO;
    const unsigned char * bytes = (const unsigned char *)[jpeg bytes];
    if (bytes[0] != 0xFF || bytes[1] != 0xD8) return NO;
    if (bytes[[jpeg length] - 2] != 0xFF ||
        bytes[[jpeg length] - 1] != 0xD9) return NO;
    return YES;
}

- (float)requestDownloadedPercent:(ICatchFile *)f {
    float progress = 0;
    NSString *locatePath = nil;
    NSString *fileName = nil;
    NSDictionary *attrs = nil;
    unsigned long long downloadedBytes;
    
    if (f != NULL) {
        fileName = [NSString stringWithUTF8String:f->getFileName().c_str()];
        locatePath = [NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), fileName];
        attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:locatePath error:nil];
        downloadedBytes = [[attrs objectForKey:@"NSFileSize"] longLongValue];
        if (f->getFileSize() > 0) {
            progress = (float)downloadedBytes / (float)f->getFileSize();
        }
    }
    
    return MAX(0, MIN(1, progress*1));
}

-(void)retrieveCameraDataLoop {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        while (self.isCameraConnected) {
            [self requestCameraStatusSync];
            [NSThread sleepForTimeInterval:POLLING_TIME_CAMERA_STATUS];
        }
    });
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        while (self.isCameraConnected) {
            [self requestSensorData];
            [NSThread sleepForTimeInterval:POLLING_TIME_SENSOR_DATA];
        }
    });
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        [self getDevicePlist];
//        [self getVersionAndDeviceID];
//    });
}

-(void)requestCameraStatusSync {
    if (![self isConnected]) {
        return;
    }
    
    unsigned int battery = [[SDK instance] retrieveBatteryLevel];
    NSInteger time = [self requestTimeRamin];
    
    @synchronized (self.cameraStatus) {
        self.cameraStatus = [[CameraStatus alloc] initWithBattery:battery andTime:time];
    }
    
    [self updateValue:self.cameraStatus withType:ValueTypeCameraStatus];
}

-(BOOL)WifiCamInit{
    // open log
    ICatchWificamLog* log = ICatchWificamLog::getInstance();
    log->setSystemLogOutput(true);
    log->setRtpLog(true); // turn on RTP log
    log->setPtpLog(true); // turn on PTP log
    log->setRtpLogLevel(LOG_LEVEL_CONNECT); // LOG_LEVEL_CONNECT, LOG_LEVEL_INFO, LOG_LEVEL_WARN, LOG_LEVEL_ERROR
    log->setPtpLogLevel(LOG_LEVEL_CONNECT); // LOG_LEVEL_CONNECT, LOG_LEVEL_INFO, LOG_LEVEL_WARN, LOG_LEVEL_ERROR
    
    if(![self wifiCamSession])
        return false;
    
    return true;
}
// nil fail, theSession success
-(ICatchWificamSession *)wifiCamSession
{
    if( ! _theSession){
        _theSession = new ICatchWificamSession();
        if( _theSession->prepareSession("192.168.1.1","anonymous", "anonymous@icatchtek.com")<0)
            return nil;
    }
    return _theSession;
}

#pragma mark - Device Plist
- (void)getDevicePlist {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *deviceSetPath = [documentsDirectory stringByAppendingPathComponent:@"DeviceSet.plist"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:deviceSetPath]) {
        self.deviceDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:deviceSetPath];
    }
}

- (void)setDevicePlist {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *deviceSetPath = [documentsDirectory stringByAppendingPathComponent:@"DeviceSet.plist"];
    
    if (self.deviceDictionary) {
        [self.deviceDictionary writeToFile:deviceSetPath atomically:YES];
    }
}

- (void)getVersionAndDeviceID {
    NSString *version = [self loadVer];
    NSString *deviceID = [self loadDeviceID];
#ifdef DEBUG
    if (deviceID.length == 0) {
        deviceID = @"6886135666667";
    }
#endif
    [self.deviceDictionary setObject:version forKey:@"NowVersion"];
    [self.deviceDictionary setObject:deviceID forKey:@"DeviceID"];
    
    [self setDevicePlist];
}

@end
