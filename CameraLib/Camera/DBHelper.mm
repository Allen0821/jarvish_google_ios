//
//  DB.m
//  Camera
//
//  Created by Ｃhun-Yang Lin on 2020/5/9.
//  Copyright © 2020 Jarvish. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBHelper.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#define DB_NAME @"storage.sqlite"

@implementation DBHelper
-(id)init {
    self = [super init];
    if (self) {
        [self createDBIfNotExist];
    }
    return self;
}

-(void)addRecord:(MediaFile*)mf {
    if ([self checkRecordExist:mf]) {
        return;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:[self dbPath]];
    if(![db open]) {
        NSLog(@"[DBHelper][addRecord] db open failure");
        return;
    }
    
    [db executeUpdate:@"INSERT INTO localFile (name, date, size, type, thumbnail) VALUES (?,?,?,?,?)",
     mf.name, mf.date, [NSNumber numberWithUnsignedLongLong:mf.fileSize],
     [NSNumber numberWithInteger:[self formatType:mf.type]], UIImagePNGRepresentation(mf.thumbnail)];
    
    [db close];
}

-(NSArray*)getfileListByType:(WCFileType)type {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[self dbPath]];
    if(![db open]) {
        NSLog(@"[DBHelper][getfileList] db open failure");
        return result;
    }
    

    
    FMResultSet *rs = [db executeQuery:@"SELECT * FROM localFile WHERE type = ? ORDER BY date DESC",
                       [NSNumber numberWithInteger:[self formatType:type]]];
    while ([rs next]) {
        MediaFile *mf = [[MediaFile alloc] initWithName:[rs stringForColumn:@"name"]
                                                   date:[rs stringForColumn:@"date"]
                                                   size:[rs unsignedLongLongIntForColumn:@"size"]
                                                   type:type
                                              thumbnail:[rs dataForColumn:@"thumbnail"]];
        [result addObject:mf];
    }
    
    [db close];
    return result;
}

-(void)deleteRecord:(MediaFile*)mf {
    FMDatabase *db = [FMDatabase databaseWithPath:[self dbPath]];
    if(![db open]) {
        NSLog(@"[DBHelper][deleteRecord] db open failure");
        return;
    }
    
    [db executeUpdate:@"DELETE FROM localFile WHERE name = ? AND date = ?", mf.name, mf.date];

    [db close];
}

#pragma mark - private
-(void)createDBIfNotExist {
    if ([self isDBExist]) {
        return;
    }
    
    FMDatabase *db = [FMDatabase databaseWithPath:[self dbPath]];
    if(![db open]) {
        NSLog(@"[DBHelper][createDBIfNotExist] db open failure");
        return;
    }
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS localFile (name TEXT, date TEXT, size UNSIGNED BIG INT, type INTEGER, thumbnail blob)"];
    [db close];
}

-(NSString*)dbPath {
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask , YES);
    NSString *path = [[searchPaths objectAtIndex:0] stringByAppendingString:[NSString stringWithFormat:@"/%@", DB_NAME]];
    return  path;
}

-(BOOL)isDBExist {
    return [[NSFileManager defaultManager] fileExistsAtPath:[self dbPath]];
}

-(NSInteger)formatType:(WCFileType)type {
    if (type == WCFileTypeImage) {
        return 0;
    } else if (type == WCFileTypeVideo) {
        return 1;
    }
    
    return -1;
}

-(BOOL)checkRecordExist:(MediaFile*)mf {
    BOOL result = NO;
    FMDatabase *db = [FMDatabase databaseWithPath:[self dbPath]];
    if(![db open]) {
        NSLog(@"[DBHelper][addRecord] db open failure");
        return result;
    }
    
    NSInteger count = [db intForQuery:@"SELECT COUNT (name) FROM localFile WHERE name = ? AND date = ?", mf.name, mf.date];
    if (count > 0) {
        result = YES;
    }
    
    return result;
}
@end
