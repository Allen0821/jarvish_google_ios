//
//  CameraStatus.h
//  Camera
//
//  Created by VampireBear on 2016/8/26.
//  Copyright © 2016年 Jarvish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CameraStatus : NSObject
@property (nonatomic, assign) unsigned int battery;
@property (nonatomic, assign) NSInteger time;
-(id)initWithBattery:(unsigned int)bt andTime:(NSInteger)time;
@end
