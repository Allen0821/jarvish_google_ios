//
//  CameraStatus1.m
//  Camera
//
//  Created by Ｃhun-Yang Lin on 2020/5/9.
//  Copyright © 2020 Jarvish. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CameraStatus.h"

@implementation CameraStatus
-(id)initWithBattery:(unsigned int)bt andTime:(NSInteger)time {
    self = [super init];
    if (self) {
        self.battery = bt;
        self.time = time;
    }
    
    return self;
}
@end
