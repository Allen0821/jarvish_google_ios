//
//  MediaFile.h
//  Raiden
//
//  Created by VampireBear on 2016/8/26.
//  Copyright © 2016年 CloudNest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CameraSDKManager.h"

typedef enum : NSUInteger {
    FileSourceSD,
    FileSourceLocal,
    FileSourceCloud
} FileSource;

@interface MediaFile : NSObject
@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *day;
@property (nonatomic, assign) unsigned long long fileSize;
@property (nonatomic, assign) WCFileType type; // image or video
@property (nonatomic, assign) FileSource fileSource;
@property (nonatomic, strong) NSString *fileID;
@property (nonatomic, assign) BOOL Corrected;
@property (nonatomic, assign) NSString *status;

-(id)initWithICatchFile:(ICatchFile*)file;
-(id)initWithName:(NSString*)name date:(NSString*)date size:(unsigned long long)size type:(WCFileType)type thumbnail:(NSData*)thumbnail;
// TODO: init with cloud

-(NSString*)getDateForUI;
-(NSString*)getSizeForUI;

-(BOOL)downloadFromSD:(void (^)(float progress))cb;
-(void)downloadFromCloudWhithData:(NSData *)fileData;
-(BOOL)deleteFromSD;
-(BOOL)deleteFromLocal;
-(void)deleteFileRecordInDB;

//-(NSString*)getPath;
-(NSString *)getPathInDocumentsDirectory;
-(BOOL)localFileExist;
-(BOOL)localFileExistAtDocumentsDirectory;
-(UIImage*)getSDImage;
-(UIImage*)getLocalImage;

-(ICatchFile*)getICatchFileRef;
@end
