//
//  MediaFile.m
//  Raiden
//
//  Created by VampireBear on 2016/8/26.
//  Copyright © 2016年 CloudNest. All rights reserved.
//

#import "MediaFile.h"
#import "DBHelper.h"
@interface MediaFile () {
    ICatchFile *mFile;
}
@end

@implementation MediaFile
-(id)initWithICatchFile:(ICatchFile*)file {
    self = [super init];
    if (self) {
        mFile = new ICatchFile(file->getFileHandle(), file->getFileType(), file->getFilePath(), file->getFileSize(), file->getFileDate(), file->getFileFrameRate(), file->getFileWidth(), file->getFileHeight());
        
        ICatchFileType type = mFile->getFileType();
        switch (type) {
            case TYPE_IMAGE:
                self.type = WCFileTypeImage;
                break;
                
            case TYPE_VIDEO:
                self.type = WCFileTypeVideo;
                break;
            default:
                break;
        }
        
        self.name = [NSString stringWithCString:mFile->getFileName().c_str() encoding:NSUTF8StringEncoding];
        self.date = [NSString stringWithCString:mFile->getFileDate().c_str() encoding:NSUTF8StringEncoding];
        self.fileSize = mFile->getFileSize();
        self.fileSource = FileSourceSD;
    }
    
    return self;
}

-(id)initWithName:(NSString*)name date:(NSString*)date size:(unsigned long long)size type:(WCFileType)type thumbnail:(NSData*)thumbnail {
    self = [super init];
    if (self) {
        self.name = name;
        self.date = date;
        self.fileSize = size;
        self.type = type;
        self.thumbnail = [UIImage imageWithData:thumbnail];
        self.fileSource = FileSourceLocal;
    }
    
    return self;
}

-(void)dealloc {
    if (mFile != NULL) {
        delete(mFile);
        mFile = NULL;
    }
}

-(NSString*)getDateForUI {
    NSArray *dateTime = [self.date componentsSeparatedByString:@"T"];
    NSMutableString *date = [NSMutableString stringWithString:[dateTime objectAtIndex:0]];
    NSMutableString *time = [NSMutableString stringWithString:[dateTime objectAtIndex:1]];
    
    [date insertString:@"/" atIndex:4];
    [date insertString:@"/" atIndex:7];
    
    [time insertString:@":" atIndex:2];
    [time insertString:@":" atIndex:5];
    
    NSString *result = [NSString stringWithFormat:@"%@ %@", date, time];
    return result;
}

-(NSString*)getSizeForUI {
    int kb = (int) (self.fileSize / 1024);
    int mb = (int) (kb / 1024);
    NSString *result = [NSString stringWithFormat:@"%d MB", mb];
    return result;
}

-(BOOL)downloadFromSD:(void (^)(float progress))cb {
    BOOL result = [[CameraSDKManager sharedManager] downloadFile:mFile cb:^(float progress) {
        cb(progress);
    }];
    
    if (!result) {
        return result;
    }
    
    if (![self localFileExist]) {
        return result;
    }
    
    DBHelper *db = [[DBHelper alloc] init];
    [db addRecord:self];
    
    if(result){
        [self moveFileToDocumentsDirectory];
    }
    return result;
}

-(void)downloadFromCloudWhithData:(NSData *)fileData {
    DBHelper *db = [[DBHelper alloc] init];
    [db addRecord:self];
    [self moveFileToDocumentsDirectory];
}

-(BOOL)deleteFromSD {
    return [[CameraSDKManager sharedManager] deleteFile:mFile];
}

-(BOOL)deleteFromLocal {
    DBHelper *db = [[DBHelper alloc] init];
    [db deleteRecord:self];
    return [[NSFileManager defaultManager] removeItemAtPath:[self getPathInDocumentsDirectory] error:nil];
}
-(void)deleteFileRecordInDB{
    DBHelper *db = [[DBHelper alloc] init];
    [db deleteRecord:self];
}
-(NSString*)getPath {
    NSString *path = [NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), self.name];
    NSLog(@"temp path : %@",path);
    return path;
}
    
-(NSString *)getPathInDocumentsDirectory
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * path = [[documentPaths objectAtIndex:0] stringByAppendingPathComponent:self.name];
    NSLog(@"document path %@",path);
    return path;
}
-(BOOL)moveFileToDocumentsDirectory
{
    NSString *fromPath = [self getPath];
    NSString *toPath = [self getPathInDocumentsDirectory];
    NSError *Error = nil;
    BOOL success = NO;
    if([[NSFileManager defaultManager]fileExistsAtPath:toPath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:toPath error:NULL];
    }
    success = [[NSFileManager defaultManager]copyItemAtPath:fromPath toPath:toPath error:&Error];
    [[NSFileManager defaultManager] removeItemAtPath:fromPath error:NULL];
    
    return success;
}
-(BOOL)localFileExist {
    NSString *path = [self getPath];
    if (!path) {
        return NO;
    }
    
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}
-(BOOL)localFileExistAtDocumentsDirectory {
    NSString *path = [self getPathInDocumentsDirectory];
    if (!path) {
        return NO;
    }
    
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(UIImage*)getSDImage {
    return [[CameraSDKManager sharedManager] requestImage:mFile];
}

-(UIImage*)getLocalImage {
    return [UIImage imageWithContentsOfFile:[self getPath]];
}

-(ICatchFile*)getICatchFileRef {
    return mFile;
}
@end
