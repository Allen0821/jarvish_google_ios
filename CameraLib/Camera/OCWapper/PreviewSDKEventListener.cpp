//
//  PreviewSDKEventListener.cpp
//  WifiCamMobileApp
//
//  Created by Sunmedia Apple on 14-6-13.
//  Copyright (c) 2014年 iCatchTech. All rights reserved.
//

#include "PreviewSDKEventListener.h"

StillCaptureDoneListener::StillCaptureDoneListener (id<ICameraHandler> handler) {
    _handler = handler;
}

void StillCaptureDoneListener::eventNotify(ICatchEvent *icatchEvt) {
    [_handler stopStillCapture];
}


VideoRecOnListener::VideoRecOnListener (id<ICameraHandler> handler) {
    _handler = handler;
}

void VideoRecOnListener::eventNotify(ICatchEvent *icatchEvt) {
    [_handler updateMovieRecState:MovieRecStarted];
}


VideoRecOffListener::VideoRecOffListener (id<ICameraHandler> handler) {
    _handler = handler;
}

void VideoRecOffListener::eventNotify(ICatchEvent *icatchEvt) {
    [_handler updateMovieRecState:MovieRecStoped];
}

VideoRecPostTimeListener::VideoRecPostTimeListener (id<ICameraHandler> handler) {
    _handler = handler;
}

void VideoRecPostTimeListener::eventNotify(ICatchEvent *icatchEvt) {
    [_handler postMovieRecordTime];
}