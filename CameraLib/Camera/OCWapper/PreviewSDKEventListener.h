//
//  PreviewSDKEventListener.h
//  WifiCamMobileApp
//
//  Created by Sunmedia Apple on 14-6-13.
//  Copyright (c) 2014年 iCatchTech. All rights reserved.
//

#ifndef WifiCamMobileApp_PreviewSDKEventListener_h
#define WifiCamMobileApp_PreviewSDKEventListener_h
#import "ICameraHandler.h"

class StillCaptureDoneListener : public ICatchWificamListener {
    private:
        id<ICameraHandler> _handler;
        void eventNotify(ICatchEvent *icatchEvt) ;
    public:
        StillCaptureDoneListener (id<ICameraHandler> handler);
};

class VideoRecOffListener : public ICatchWificamListener {
    private:
        id<ICameraHandler> _handler;
        void eventNotify(ICatchEvent *icatchEvt) ;
    public:
        VideoRecOffListener (id<ICameraHandler> handler);
};

class VideoRecOnListener : public ICatchWificamListener {
    private:
        id<ICameraHandler> _handler;
        void eventNotify(ICatchEvent *icatchEvt) ;
    public:
        VideoRecOnListener (id<ICameraHandler> handler);
};

class VideoRecPostTimeListener : public ICatchWificamListener {
private:
    id<ICameraHandler> _handler;
    void eventNotify(ICatchEvent *icatchEvt) ;
public:
    VideoRecPostTimeListener (id<ICameraHandler> handler);
};
#endif
