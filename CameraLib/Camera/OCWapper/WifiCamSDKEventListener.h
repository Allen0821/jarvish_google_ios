//
//  ddd.h
//  Camera
//
//  Created by Ｃhun-Yang Lin on 2020/5/9.
//  Copyright © 2020 Jarvish. All rights reserved.
//

#ifndef ddd_h
#define ddd_h

#import <Foundation/Foundation.h>

class WifiCamSDKEventListener : public ICatchWificamListener {
    private:
        id object;
        SEL callback;
        void eventNotify(ICatchEvent *icatchEvt);
    public:
        WifiCamSDKEventListener(id object, SEL callback);
}

#endif /* ddd_h */
