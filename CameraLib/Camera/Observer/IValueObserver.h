//
//  IValueObserver.h
//  Camera
//
//  Created by VampireBear on 2016/9/1.
//  Copyright © 2016年 Jarvish. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CameraStatus.h"

typedef NS_ENUM(NSInteger, ValueType) {
    ValueTypeCameraStatus,
    ValueTypeSensorData
};

@protocol IValueObserver <NSObject>
-(void)onValueUpdate:(id)value forValueType:(ValueType)vt;
@end
