#ifndef __ICATCH_WIFICAM_H__
#define __ICATCH_WIFICAM_H__

#include "ICatchWificamSession.h"
#include "ICatchWificamControl.h"
#include "ICatchWificamPreview.h"
#include "ICatchWificamPlayback.h"
#include "ICatchWificamVideoPlayback.h"
#include "ICatchWificamProperty.h"
#include "ICatchWificamState.h"
#include "ICatchWificamInfo.h"
#include "ICatchWificamUtil.h"
#include "ICatchWificamLog.h"
#include "ICatchWificamListener.h"

#include "ICatchWificamAssist.h"

#include "ICatchFileType.h"
#include "ICatchCameraProperty.h"
#include "ICatchCaptureDelay.h"
#include "ICatchFrameBuffer.h"
#include "ICatchEvent.h"
#include "ICatchEventID.h"
#include "ICatchFile.h"
#include "ICatchFileType.h"
#include "ICatchFrameSize.h"
#include "ICatchLightFrequency.h"
#include "ICatchVideoSize.h"
#include "ICatchWhiteBalance.h"
#include "ICatchBurstNumber.h"
#include "ICatchLogLevel.h"
#include "ICatchStreamParam.h"
#include "ICatchMJPGStreamParam.h"
#include "ICatchH264StreamParam.h"
#include "ICatchMode.h"
#include "ICatchPreviewMode.h"
#include "ICatchError.h"

#endif

