//
//  JarvishApi.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/24.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import Moya

let JarvishProvider = MoyaProvider<Jarvish>()


public enum Jarvish {
    case loginWithEmail(email: String, password: String)
    case loginWithFacebook(token: String)
    case loginWithApple(token: String)
    case registerWithEmail(name: String, email: String, password: String)
    case registerWithFacebook(token: String, email: String, name: String)
    case registerWithApple(token: String, email: String, name: String)
    case forgetPassword(email: String)
    case resetPassword(verificationCode: String, email: String, password: String)
    case getUserProfile(session: String, uid: String)
    case updateUserProfile(session: String, uid: String, data: Data)
    case registerHelmet(session: String, data: Data)
}



extension Jarvish: TargetType {
    public var baseURL: URL {
        return URL(string: "https://developer.jarvish.com")!
    }
    
    public var path: String {
        switch self {
        case .loginWithEmail(_,_):
            return "/apis/v1/login"
        case .loginWithFacebook(_):
            return "/apis/v1/login"
        case .loginWithApple(_):
            return "/apis/v1/login"
        case .registerWithEmail(_,_,_):
            return "/apis/v1/registration"
        case .registerWithFacebook(_,_,_):
            return "/apis/v1/registration"
        case .registerWithApple(_,_,_):
            return "/apis/v1/registration"
        case .forgetPassword(let email):
            return "/apis/v1/member/\(email)/verificationcode"
        case .resetPassword(_,_,_):
            return "/apis/v1/member/password/reset"
        case .getUserProfile(_, let uid):
            return "/apis/v1/profile/\(uid)"
        case .updateUserProfile(_, let uid,_):
            return "/apis/v1/profile/\(uid)"
        case .registerHelmet(_,_):
            return "/apis/v1/product"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .loginWithEmail(_,_):
            return .post
        case .loginWithFacebook(_):
            return .post
        case .loginWithApple(_):
            return .post
        case .registerWithEmail(_,_,_):
            return .post
        case .registerWithFacebook(_,_,_):
            return .post
        case .registerWithApple(_,_,_):
            return .post
        case .forgetPassword(_):
            return .post
        case .resetPassword(_,_,_):
            return .put
        case .getUserProfile(_,_):
            return .get
        case .updateUserProfile(_,_,_):
            return .patch
        case .registerHelmet(_,_):
            return .post
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .loginWithEmail(let email, let password):
            return .requestParameters(parameters: [
                "email" : email,
                "password" : password],
                encoding: JSONEncoding.default)
        case .loginWithFacebook(let token):
            return .requestParameters(parameters: [
            "provider" : "facebook",
            "token" : token],
            encoding: JSONEncoding.default)
        case .loginWithApple(let token):
            return .requestParameters(parameters: [
            "provider" : "apple",
            "token" : token],
            encoding: JSONEncoding.default)
        case .registerWithEmail(let name, let email, let password):
            return .requestParameters(parameters: [
                "first_name" : name,
                "last_name" : name,
                "email" : email,
                "password" : password],
                encoding: JSONEncoding.default)
        case .registerWithFacebook(let token, let email, let name):
            return .requestParameters(parameters: [
                "token" : token,
                "email" : email,
                "first_name" : name],
                encoding: JSONEncoding.default)
        case .registerWithApple(let token, let email, let name):
            return .requestParameters(parameters: [
                "token" : token,
                "email" : email,
                "first_name" : name],
                encoding: JSONEncoding.default)
        case .forgetPassword:
            return .requestPlain
        case .resetPassword(let verificationCode, let email, let password):
            return .requestParameters(parameters: [
                "verificationCode" : verificationCode,
                "email" : email,
                "password" : password],
                encoding: JSONEncoding.default)
        case .getUserProfile(_,_):
            return .requestPlain
        case .updateUserProfile(_,_, let data):
            return .requestData(data)
        case .registerHelmet(_, let data):
            return .requestData(data)
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        case .getUserProfile(let session,_):
            return [
                "Content-type": "application/json",
                "APIKey": "JarvishAPIKey",
                "session": session
            ]
        case .updateUserProfile(let session,_,_):
            return [
                "Content-type": "application/json",
                "APIKey": "JarvishAPIKey",
                "session": session
            ]
        case .registerHelmet(let session,_):
            return [
                "Content-type": "application/json",
                "APIKey": "JarvishAPIKey",
                "session": session
            ]
        default:
            return [
                "Content-type": "application/json",
                "APIKey": "JarvishAPIKey"
            ]
        }
    }
    
}
