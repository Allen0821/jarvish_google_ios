//
//  EditProfileItem.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/1.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct EditProfileItem {
    var op: String = "replace"
    var fieldType: String = "String"
    var field: String
    var value: String
    
   
    init(field: String, value: String) {
        self.op = "replace"
        self.fieldType = "String"
        self.field = field
        self.value = value
    }
    
    func toDictionary() -> [String : String] {
        return [
            "op": op,
            "fieldType": fieldType,
            "field": field,
            "value": value
        ]
    }
    
}
