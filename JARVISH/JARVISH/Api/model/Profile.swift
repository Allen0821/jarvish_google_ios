//
//  Profile.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/1.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct Profile {
    var id: String?
    var user_name: String?
    var first_name: String?
    var last_name: String?
    var middle_name: String?
    var birthday: String?
    var email: String?
    var gender: String?
    var country_code: String?
    var zip_code: String?
    var address: String?
    var phone1: String?
    var phone2: String?
    var line_id: String?
    var wechat_id: String?
    var emergency_contact: String?
    var emergency_contact_phone: String?
    var created: String?
    var city: String?
    var head_shot: String?
    var head_hsize: String?
    var head_vsize: String?
    
    
    var dictionary: [String: Any?] {
        return [
            "id": id,
            "user_name": user_name,
            "first_name": first_name,
            "last_name": last_name,
            "middle_name": middle_name,
            "birthday": birthday,
            "email": email,
            "gender": gender,
            "country_code": country_code,
            "zip_code": zip_code,
            "address": address,
            "phone1": phone1,
            "phone2": phone2,
            "line_id": line_id,
            "wechat_id": wechat_id,
            "emergency_contact": emergency_contact,
            "emergency_contact_phone": emergency_contact_phone,
            "created": created,
            "city": city,
            "head_shot": head_shot,
            "head_hsize": head_shot,
            "head_vsize": head_shot
        ]
    }
}


extension Profile: Serializable {
    init?(dictionary: [String : Any?]) {
        let id = dictionary["id"] as? String ?? ""
        let user_name = dictionary["user_name"] as? String ?? ""
        let first_name = dictionary["first_name"] as? String ?? ""
        let last_name = dictionary["last_name"] as? String ?? ""
        let middle_name = dictionary["middle_name"] as? String ?? ""
        let birthday = dictionary["birthday"] as? String ?? ""
        let email = dictionary["email"] as? String ?? ""
        let gender = dictionary["gender"] as? String ?? ""
        let country_code = dictionary["country_code"] as? String ?? ""
        let zip_code = dictionary["zip_code"] as? String ?? ""
        let address = dictionary["address"] as? String ?? ""
        let phone1 = dictionary["phone1"] as? String ?? ""
        let phone2 = dictionary["phone2"] as? String ?? ""
        let line_id = dictionary["line_id"] as? String ?? ""
        let wechat_id = dictionary["wechat_id"] as? String ?? ""
        let emergency_contact = dictionary["emergency_contact"] as? String ?? ""
        let emergency_contact_phone = dictionary["emergency_contact_phone"] as? String ?? ""
        let created = dictionary["created"] as? String ?? ""
        let city = dictionary["city"] as? String ?? ""
        let head_shot = dictionary["head_shot"] as? String ?? ""
        let head_hsize = dictionary["head_hsize"] as? String ?? ""
        let head_vsize = dictionary["head_shot"] as? String ?? ""
    
        
        self.init(
            id: id,
            user_name: user_name,
            first_name: first_name,
            last_name: last_name,
            middle_name: middle_name,
            birthday: birthday,
            email: email,
            gender: gender,
            country_code: country_code,
            zip_code: zip_code,
            address: address,
            phone1: phone1,
            phone2: phone2,
            line_id: line_id,
            wechat_id: wechat_id,
            emergency_contact: emergency_contact,
            emergency_contact_phone: emergency_contact_phone,
            created: created,
            city: city,
            head_shot: head_shot,
            head_hsize: head_hsize,
            head_vsize: head_vsize
        )
    }
}
