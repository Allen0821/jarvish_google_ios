//
//  RegisteHelmet.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/5.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct RegisteHelmet {
    var memberId: Int?
    var serialNumber: String?
    
    init(memberId: Int, serialNumber: String) {
        self.memberId = memberId
        self.serialNumber = serialNumber
    }
    
    func toDictionary() -> [String : Any] {
        return [
            "memberId": memberId ?? 0,
            "serialNumber": serialNumber ?? ""
        ]
    }
}
