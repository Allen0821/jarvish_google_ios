//
//  Session.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/25.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct Session {
    var memberId: String
    var session: String
    
    var dictionary: [String: Any] {
        return [
            "memberId": memberId,
            "session": session
        ]
    }
    
}


extension Session: Serializable {
    init?(dictionary: [String : Any?]) {
        let memberId = dictionary["memberId"] as? String ?? ""
        let session = dictionary["session"] as? String ?? ""
        
        self.init(memberId: memberId,
                  session: session
        )
    }
}
