//
//  UserManual.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/18.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct UserManual {
    var name: String?
    var link: String?
    
    init(name: String, link: String) {
        self.name = name
        self.link = link
    }
}
