//
//  HelmetBleManager.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/2/25.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import CoreBluetooth

public class HelmetBleManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var delgate: HelmetBleManagerDelegate?
    
    var centralManager: CBCentralManager!
    
    var peripherals: [CBPeripheral] = []
    var connectPeripheral: CBPeripheral!
    var characteristic: CBCharacteristic!
   
    
    var discovering: Bool = false
   //var jProduct: JProduct?
  
    var fetchTimer: Timer?
    var batteryWatchTimer: Timer?
    var connectionTimer: Timer?
    var fetchIndex: Int = 0

    var helmet: Helmet = Helmet.init()
    
    //Initializer access level change now
    public override init(){
        super.init()
        print("HelmetBleManager init")
        let centralQueue = DispatchQueue.global()
        centralManager = CBCentralManager(delegate: self, queue: centralQueue)
    }
        
   
    public func destrory() {
        stopFetchHelmetSettings()
        stopBatteryWatch()
        clearConnectionTimer()
    }
    
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("centralManagerDidUpdateState")
        switch central.state {
        case .unknown, .resetting:
            print(".unknown, .resetting")
        case .unsupported, .unauthorized, .poweredOff:
            print(".unsupported, .unauthorized, .poweredOff")
            
        case .poweredOn:
            print(".poweredOn")
        @unknown default:
            break
        }
    }
    
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let manufacturer = advertisementData[CBAdvertisementDataManufacturerDataKey] as? Data
            
        if manufacturer != nil {
            let id = manufacturer?.hexEncodedString()
            print("scan helmet \(String(describing: peripheral.name)) \(String(describing: id))")
            if (id?.elementsEqual("556600f1"))! {
                print("helmet \(String(describing: peripheral.name))")
                peripherals.append(peripheral)
                self.delgate?.discoverResult(name: peripheral.name ?? "unknown")
            }
        }
    }
    
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("did connect to \(String(describing: peripheral.name))")
        print("did connect uuid to \(peripheral.identifier.uuidString)")
        peripheral.discoverServices(nil)
    }
    
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        
    }
    
    
    public func centralManager(_ central: CBCentralManager, connectionEventDidOccur event: CBConnectionEvent, for peripheral: CBPeripheral) {
        
    }
    
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("did disconnect to \(peripheral)")
        stopBatteryWatch()
        DispatchQueue.main.async {
            self.delgate?.didDisconnected()
        }
        
    }
    
    
    

    //connection
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("didDiscoverServices")
        guard let services = peripheral.services else {
            print("no services")
            DispatchQueue.main.async {
                self.delgate?.onConnectionFailure()
            }
            return
        }
        for service in services {
            print(service)
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("didDiscoverCharacteristicsFor")
        print("did discover characteristics  \(service.uuid)")
        guard let characteristics = service.characteristics else {
            print("no characteristics")
            return
        }
        for characteristic in characteristics {
            print("characteristic: \(characteristic)")
            if characteristic.uuid.uuidString.isEqual("F102") {
                print("find characteristics F102")
                peripheral.setNotifyValue(true, for: characteristic)
            }
            if characteristic.uuid.uuidString.isEqual("F103") {
                print("find characteristics F103")
                self.characteristic = characteristic
            }
        }
        clearConnectionTimer()
        DispatchQueue.main.async {
            self.delgate?.didConnected()
        }
        startFetchHelmetSettings()
    }
    
    
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        print("fdid update notification state")
    }
    
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
        print("fdid update descriptor")
    }
    
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        let data = characteristic.value! as NSData
        print("did update characteristic: \(data)")
        self.parseData(data: data)
    }
    

    private func writeData(data: NSData) {
        if self.characteristic == nil {
            return
        }
        self.connectPeripheral.writeValue(data as Data, for: self.characteristic, type: CBCharacteristicWriteType.withResponse)
    }
    
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if error != nil {
            print("write value error \(String(describing: error))")
        }
    }
    
    
    private func parseData(data: NSData) {
        let header = HelmetBleMessage.parseHeader(data: data)
        //print("parse hearder  \(String(describing: header))")
        switch header {
        case HelmetBleMessage.MSG_ID_MODEL_NUMBER:
            self.helmet.serialNumber = HelmetBleMessage.parseHelmetStringValue(data: data)
            //self.helmet.serialNumber = "78861jjjiikk"
            self.helmet.product = ParseSerialNumber.parse(serial: self.helmet.serialNumber)
            if self.helmet.product != nil && !self.helmet.product.productType.isEmpty {
                self.helmet.style = self.helmet.product.productType
                self.helmet.name = JCandy.getHelmetName(type: self.helmet.style)
                self.helmet.image = JCandy.getPhoto(type: self.helmet.style, color: "")
            }
            break
        case HelmetBleMessage.MSG_ID_FIRMWARE_VERSION:
            self.helmet.firmwareVersion = HelmetBleMessage.parseHelmetStringValue(data: data)
            break
        case HelmetBleMessage.MSG_ID_TAIL_LIGHT:
            self.helmet.tailLight = HelmetBleMessage.parseSwitchSetting(data: data)
            break
        case HelmetBleMessage.MSG_ID_GET_VIDEO_SETTINGS:
            self.helmet.videoTimestamp = HelmetBleMessage.parseVideoTimestamp(data: data)
            self.helmet.videoResolution = HelmetBleMessage.parseVideoResolution(data: data)
            self.helmet.videoRecordingTime = HelmetBleMessage.parseVideoRecordingTime(data: data)
            break
        case HelmetBleMessage.MSG_ID_BATTERY_EFFICIENCY:
            self.helmet.batteryEfficiency = HelmetBleMessage.parseBatteryEfficiency(data: data)
            break
        case HelmetBleMessage.MSG_ID_BATTERY_LEVEL:
            self.helmet.batteryLevel = HelmetBleMessage.parseBatteryLevel(data: data)
            DispatchQueue.main.async {
                print("MSG_ID_BATTERY_LEVEL! \(String(describing: self.helmet.batteryLevel))")
                self.delgate?.didBatteryLevel(level: self.helmet.batteryLevel)
            }
            break
        case HelmetBleMessage.MSG_ID_ACK:
            parseAck(index: HelmetBleMessage.parseAckIndex(data: data), data: data)
            break
        default:
            print("unknown header!")
            break
        }
    }
    
    
    private func parseAck(index: UInt16, data: NSData) {
        let ack = HelmetBleMessage.parseAckValue(data: data)
        switch index {
        case HelmetBleMessage.MSG_ID_TAIL_LIGHT:
            DispatchQueue.main.async {
                self.delgate?.didTailLightAck(ack: ack)
            }
            break
        case HelmetBleMessage.MSG_ID_VIDEO_TIMESTAMP:
            DispatchQueue.main.async {
                self.delgate?.didTimestampAck(ack: ack)
            }
            break
        case HelmetBleMessage.MSG_ID_VIDEO_RESOLUTION:
            DispatchQueue.main.async {
                self.delgate?.didVideoResolutionAck(ack: ack)
            }
            break
        case HelmetBleMessage.MSG_ID_VIDEO_RECORDING_TIME:
            DispatchQueue.main.async {
                self.delgate?.didVideoRecordingTimeAck(ack: ack)
            }
            break
        case HelmetBleMessage.MSG_ID_BATTERY_EFFICIENCY:
            DispatchQueue.main.async {
                self.delgate?.didBatteryEfficiencyAck(ack: ack)
            }
            break
        case HelmetBleMessage.MSG_ID_RESET_DEFAULT:
            DispatchQueue.main.async {
                self.delgate?.didResetDefaultAck(ack: ack)
            }
            break
        case HelmetBleMessage.MSG_ID_SYS_DATE_TIME:
            DispatchQueue.main.async {
                self.delgate?.didSetSystemDateTimeAck(ack: ack)
            }
            break
        default:
            print("unknown ack!")
            break
        }
    }
    
    
    /*private func parseHelmetModel(data: NSData?) {
        print("parse helmet model")
        var number: String
        if data == nil {
            number = UserDefaults.standard.string(forKey: "SERIAL_NUMBER") ?? ""
        } else {
            number = HelmetBleMessage.parseHelmetNumber(data: data!)
        }
        print("parse helmet number = \(number)")
        
        self.jProduct = JCandy.identifyProduct(serial: number)
        if self.jProduct == nil {
            DispatchQueue.main.async {
                self.delgate?.didNotIdentifyHelmet()
            }
            return
        }
        
        self.jHelmet = JHelmet.init(product: self.jProduct!)
        
        self.jHelmet?.name = JCandy.getHelmetName(type: self.jProduct!.productType)
        self.jHelmet?.photo = JCandy.getPhoto(
            type: self.jProduct!.productType,
            color: self.jProduct!.productColor)
        print("show helmet info = \(String(describing: self.jHelmet?.product))")
        
        UserDefaults.standard.set(self.jHelmet?.photo, forKey: "YOUR_HELMET")
        
        DispatchQueue.main.async {
            self.delgate?.didIdentifyHelmet()
        }
        self.startFetchHelmetSettings()
    }*/
    
    
    private func startFetchHelmetSettings() {
        print("fetch helmet settings")
        self.fetchIndex = 0
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.fetchTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (Timer) in
                print("fetchTimer \(self.fetchIndex)")
                self.fetchHelmetSettings()
            })
        }
    }
    
    
    private func stopFetchHelmetSettings() {
        if self.fetchTimer != nil {
            self.fetchTimer?.invalidate()
        }
    }
    
    
    private func batteryWatch() {
        stopBatteryWatch()
        DispatchQueue.main.async {
            self.batteryWatchTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { (Timer) in
                self.queryBatteryLevel()
            })
        }
    }
    
    
    private func stopBatteryWatch() {
        if self.batteryWatchTimer != nil {
            self.batteryWatchTimer?.invalidate()
        }
    }
    
    
    
    private func fetchHelmetSettings() {
        switch self.fetchIndex {
        case 0:
            self.queryModelNumber()
            break
        case 1:
            self.queryModelNumber()
            break
        case 2:
            self.queryFirmwareVersion()
            break
        case 3:
            self.queryTailLight()
            break
        case 4:
            self.queryBatteryEfficiency()
            break
        case 5:
            self.queryVideoSettings()
            break
        case 6:
            DispatchQueue.main.async {
                self.delgate?.didFetchHelmet(helmet: self.helmet)
            }
            stopFetchHelmetSettings()
            self.fetchIndex = 0
            self.queryBatteryLevel()
            self.batteryWatch()
            return
        default:
            break
        }
        self.fetchIndex += 1
    }
    
}


extension HelmetBleManager {
    
    public func startDiscovery() {
        print("start discovery")
        if self.discovering {
            return
        }
        self.discovering = true
        peripherals.removeAll()
        centralManager.scanForPeripherals(withServices: nil, options:nil)
    }
    
    
    public func stopDiscovery() {
        print("stop discovery")
        centralManager.stopScan()
        self.discovering = false
    }
    
    
    

    private func setConnectionTimer() {
        clearConnectionTimer()
        DispatchQueue.main.async {
            self.connectionTimer = Timer.scheduledTimer(withTimeInterval: 15, repeats: false, block: { (Timer) in
                print("BLE connection timeout")
                self.delgate?.onConnectionFailure()
            })
        }
    }
    
    
    private func clearConnectionTimer() {
        if self.connectionTimer != nil {
            print("clear BLE connection timeout")
            self.connectionTimer?.invalidate()
            connectionTimer = nil
        }
    }
    
    
    public func connectDirect(identify: String) {
        var exist = false
        let uuid = UUID(uuidString: identify)
        print("uuid => \(String(describing: uuid))")
        if uuid != nil {
            let peripherals = centralManager.retrievePeripherals(withIdentifiers: [uuid!])
            peripherals.forEach { peripheral in
                if peripheral.identifier == uuid {
                    self.connectPeripheral = peripheral
                    self.connectPeripheral.delegate = self
                    centralManager.connect(self.connectPeripheral, options: nil)
                    exist = true
                    setConnectionTimer()
                    return
                }
            }
        }
        if !exist {
            self.delgate?.onConnectionFailure()
        }
    }
    
    
    public func connect(name: String) {
        self.stopDiscovery()
        for peripheral in peripherals {
            print("device => \(peripheral)")
            if peripheral.name?.elementsEqual(name) ?? false {
                print("device name => \(peripheral.name ?? "nil")")
                self.connectPeripheral = peripheral
                self.connectPeripheral.delegate = self
                centralManager.connect(self.connectPeripheral, options: nil)
                setConnectionTimer()
                return
            }
        }
        self.delgate?.onConnectionFailure()
    }
    
    
    public func disconnect() {
        self.centralManager.cancelPeripheralConnection(self.connectPeripheral)
    }
    
    
    public func queryBatteryLevel() {
        let data = NSData(bytes: [0x2A,0x0c,0x00] as [UInt8], length: 3)
        self.writeData(data: data)
    }
    
    
    public func queryModelNumber() {
        print("query model number")
        self.writeData(data: HelmetBleMessage.getModelNumber())
    }
    
    
    public func queryFirmwareVersion() {
        print("query firmware version")
        self.writeData(data: HelmetBleMessage.getFirmwareVersion())
    }
    
    
    /*public func queryProximity() {
        print("query proximity")
        self.writeData(data: HelmetBleMessage.getProximity())
    }
    
    public func setProximity(value: Bool) {
        print("set proximity")
        self.writeData(data: HelmetBleMessage.setProximity(value: value))
    }*/
    
    
    public func queryTailLight() {
        print("query tail light")
        self.writeData(data: HelmetBleMessage.getTailLight())
    }
    
    
    public func setTailLight(value: Bool) {
        print("set tail light \(value)")
        self.writeData(data: HelmetBleMessage.setTailLight(value: value))
    }
    
    
    public func queryBatteryEfficiency() {
        self.writeData(data: HelmetBleMessage.getBatteryEfficiency())
    }
    
    
    public func setBatteryEfficiency(value: Int) {
        print("set battery efficiency")
        self.writeData(data: HelmetBleMessage.setBatteryEfficiency(value: value))
    }
    
    
    public func queryVideoSettings() {
        print("query video settings")
        self.writeData(data: HelmetBleMessage.getVideoSettings())
    }
    
    
    public func setTimestamp(value: Bool) {
        print("set timestamp")
        self.writeData(data: HelmetBleMessage.setTimestamp(value: value))
    }
    
    
    public func setVideoResolution(value: Int) {
        print("set video resolution")
        self.writeData(data: HelmetBleMessage.setVideoResolution(value: value))
    }
    
    
    public func setVideoRcordingTime(value: Int) {
        print("set video rcording time")
        self.writeData(data: HelmetBleMessage.setVideoRecordingTime(value: value))
    }
    
    
    public func resetDefault() {
        print("reset default")
        self.writeData(data: HelmetBleMessage.resetDefault())
    }
    
    
    public func setSystemDateTime() {
        print("set system date time")
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year,.month,.day,.hour,.minute,.second], from: Date())
        print("year: \(String(describing: components.year))")
        print("month: \(String(describing: components.month))")
        print("day: \(String(describing: components.day))")
        print("hour: \(String(describing: components.hour))")
        print("minute: \(String(describing: components.minute))")
        print("second: \(String(describing: components.second))")
        
        self.writeData(data: HelmetBleMessage.setSystemTime(
            year: components.year!,
            month: components.month!,
            day: components.day!,
            hour: components.hour!,
            minute: components.minute!,
            second: components.second!))
    }
}


extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) ->
        String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}
