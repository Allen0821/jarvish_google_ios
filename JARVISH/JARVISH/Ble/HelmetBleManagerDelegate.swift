//
//  HelmetBleManagerDelegate.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/2/25.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol HelmetBleManagerDelegate {
    func didUpdateState()
    func didConnected()
    func onConnectionFailure()
    func didDisconnected()
    func didConnectedPeripheral(deviceUuid: String)
    func discoverResult(name: String)
    func didFetchHelmet(helmet: Helmet)
    
    func didTailLight(value: Bool)
    func didTailLightAck(ack: Bool)
    func didTimestamp(value: Bool)
    func didTimestampAck(ack: Bool)
    func didVideoRecordingTime(value: Int)
    func didVideoRecordingTimeAck(ack: Bool)
    func didVideoResolution(value: Int)
    func didVideoResolutionAck(ack: Bool)
    func didBatteryEfficiency(value: Int)
    func didBatteryEfficiencyAck(ack: Bool)
    func didResetDefaultAck(ack: Bool)
    func didSetSystemDateTimeAck(ack: Bool)
    func didBatteryLevel(level: Int)
    func didFirmwareVersion(version: String)
}

