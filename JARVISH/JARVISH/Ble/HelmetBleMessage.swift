//
//  HelmetBleMessage.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/2/27.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation


class HelmetBleMessage {
    static let MSG_ID_WAKE_ON_EVENT: UInt16 = 0x2A00;
    
    // WIFI related
    // WIFI outgoing message
    static let MSG_ID_WIFI_ON_OFF_REQUEST: UInt16 = 0x2A04;
    static let MSG_ID_WIFI_AP_SSID: UInt16 = 0x2A08;
    // WIFI incoming message
    static let MSG_ID_WIFI_READY_NOTIFY: UInt16 = 0x2A05;
    static let MSG_ID_WIFI_AP_SSID_NOTIFY: UInt16 = 0x2A09;
    
    
    static let MSG_ID_LOCATION_AND_SPEED: UInt16 = 0x2A0B;
    // incoming message
    static let MSG_ID_GYROSCOPE: UInt16 = 0x2A0D;
    static let MSG_ID_ACCELEROMETER: UInt16 = 0x2A0E;
    static let MSG_ID_MAGNETOMETER: UInt16 = 0x2A1E;
    static let MSG_ID_SENSOR_FUSION: UInt16 = 0x2A1D;
    // IO message
    static let MSG_ID_BATTERY_LEVEL: UInt16 = 0x2A0C;
    static let MSG_ID_MODEL_NUMBER: UInt16 = 0x2A02;
    static let MSG_ID_FIRMWARE_VERSION: UInt16 = 0x2A03;
    
    static let MSG_ID_HELMET_NAME: UInt16 = 0x2A1B;
    
    static let MSG_ID_TAIL_LIGHT: UInt16 = 0x2A13;
  
    
    static let MSG_ID_VIDEO_TIMESTAMP: UInt16 = 0x2A20;
    static let MSG_ID_VIDEO_RESOLUTION: UInt16 = 0x2A14;
    static let MSG_ID_VIDEO_RECORDING_TIME: UInt16 = 0x2A21;
    
    static let MSG_ID_GET_VIDEO_SETTINGS: UInt16 = 0x2A22;
    
    static let MSG_ID_BATTERY_EFFICIENCY: UInt16 = 0x2A23;
    
    static let MSG_ID_SYS_DATE_TIME: UInt16  = 0x2A01;
    
    // ICP related
    // ICP outgoing
    static let MSG_ID_ICP_SERVICE_REQUEST: UInt16 = 0x2A17;
    static let MSG_ID_ICP_CONNECT_REQUEST: UInt16 = 0x2A19;
    // ICP incoming
    static let MSG_ID_ICP_PAIRING_LIST: UInt16 = 0x2A18;
    static let MSG_ID_ICP_CONNECT_COMPLETED: UInt16 = 0x2A1A;
    // ICP IO message
    static let MSG_ID_ICP_REMOTE_NAME: UInt16 = 0x2A1B;
    
    static let MSG_ID_RESET_DEFAULT: UInt16 = 0x2A11;
    
    static let MSG_ID_ALEXA_WAKE_ON_EVENT: UInt16 = 0x2A1C;
    static let MSG_ID_DIRECTION: UInt16 = 0x2A20;
    static let MSG_ID_ACK: UInt16 = 0x2AFF;

    
    static func parseHeader(data: NSData) -> UInt16 {
        print("parse header bytes: \(data.bytes)")
        print("parse header length: \(data.length)")
        var header: UInt16 = 0
        data.getBytes(&header, range: NSRange(location: 0, length: 2))
        header = UInt16(bigEndian: header)
        print("parse value: \(header)")
        return header
    }
    
    
    static func parseAckIndex(data: NSData) -> UInt16 {
        print("parse ack bytes: \(data.bytes)")
        print("parse ack length: \(data.length)")
        var ack: UInt16 = 0
        data.getBytes(&ack, range: NSRange(location: 3, length: 2))
        ack = UInt16(bigEndian: ack)
        print("parse ack index: \(ack)")
        return ack
    }
    
    
    static func parseAckValue(data: NSData) -> Bool {
        var value: UInt8 = 0
        data.getBytes(&value, range: NSRange(location: 5, length: 1))
        value = UInt8(bigEndian: value)
        return value == 0x01
    }
    
    
    static func parseHelmetNumber(data: NSData) -> String {
        if data.length != 16 {
            return ""
        }
        let d = data.subdata(with: NSRange(location: 3, length: 13))
        let number = String.init(data: d, encoding: .utf8)
        print("number= \(String(describing: number))")
        if number?.count != 13 {
            return ""
        }
        return number!;
        //return self.fakeHelmetNumber();
    }
    
    
    private static func fakeHelmetNumber() -> String {
        return "7860701205000"
    }
    
    
    static func parseSwitchSetting(data: NSData) -> Bool {
        if data.length != 4 {
            return false
        }
        var value: UInt16 = 0
        data.getBytes(&value, range: NSRange(location: 3, length: 1))
        value = UInt16(bigEndian: value)
        print("parse value: \(value)")
        return value == 0x00
    }
    
    

    /*static func identifyHelmet(number: String) -> HelmetIdentify {
        let helmet = HelmetIdentify.init(number: number)
        print("helmet = \(helmet)")
        return helmet
    }*/
    
    
    static func getModelNumber() -> NSData {
        print("get model number")
        return NSData(bytes: [0x2A,0x02,0x00] as [UInt8], length: 3)
    }
    
    
    static func getFirmwareVersion() -> NSData {
        print("get firmware version")
        return NSData(bytes: [0x2A,0x03,0x00] as [UInt8], length: 3)
    }
    
    
    
    static func getTailLight() -> NSData {
        return NSData(bytes: [0x2A,0x13,0x02,0x00] as [UInt8], length: 4)
    }
    
    
    static func setTailLight(value: Bool) -> NSData {
        var b: UInt8
        if value {
            b = 0x00
        } else {
            b = 0x01
        }
        return NSData(bytes: [0x2A,0x13,0x02,0x01,b] as [UInt8], length: 5)
    }
    
    
    static func getBatteryEfficiency() -> NSData {
        return NSData(bytes: [0x2A,0x23,0x02,0x00] as [UInt8], length: 4)
    }
    
    
    static func setBatteryEfficiency(value: Int) -> NSData {
        var b: UInt8
        if value == 0 {
            b = 0x00
        } else {
            b = 0x01
        }
        return NSData(bytes: [0x2A,0x23,0x02,0x01,b] as [UInt8], length: 5)
    }
    
    
    static func getVideoSettings() -> NSData {
        return NSData(bytes: [0x2A,0x22,0x02,0x00] as [UInt8], length: 4)
    }
    
    
    static func setTimestamp(value: Bool) -> NSData {
        var b: UInt8
        if value {
            b = 0x00
        } else {
            b = 0x01
        }
        return NSData(bytes: [0x2A,0x20,0x01,b] as [UInt8], length: 4)
    }
    
    
    static func setVideoResolution(value: Int) -> NSData {
        var b: UInt8
        switch value {
        case 1:
            b = 0x01
            break;
        case 2:
            b = 0x02
            break;
        default:
            b = 0x00
            break;
        }
        return NSData(bytes: [0x2A,0x14,0x01,b] as [UInt8], length: 4)
    }
    
    
    static func setVideoRecordingTime(value: Int) -> NSData {
        var b: UInt8
        switch value {
        case 1:
            b = 0x01
            break;
        case 2:
            b = 0x02
            break;
        default:
            b = 0x00
            break;
        }
        return NSData(bytes: [0x2A,0x21,0x01,b] as [UInt8], length: 4)
    }
    
    
    static func resetDefault() -> NSData {
        return NSData(bytes: [0x2A,0x11,0x01,0x01] as [UInt8], length: 4)
    }
    
    
    static func setSystemTime(year: Int, month: Int, day: Int, hour: Int, minute: Int, second: Int) -> NSData {
        let y = UInt16(year)
        let y1: UInt8 = UInt8(y >> 8)
        let y2: UInt8 = UInt8(y & 0x00ff)
        let mon: UInt8 = UInt8(month)
        let d: UInt8 = UInt8(day)
        let hr: UInt8 = UInt8(hour)
        let min: UInt8 = UInt8(minute)
        let sec: UInt8 = UInt8(second)
        return NSData(bytes: [0x2A,0x01,0x01,0x01,y1,y2,mon,d,hr,min,sec] as [UInt8], length: 11)
    }
    
    
    static func parseHelmetStringValue(data: NSData) -> String {
        if data.length <= 3 {
            return ""
        }
        let d = data.subdata(with: NSRange(location: 3, length: data.length - 3))
        let value = String.init(data: d, encoding: .utf8)
        print("parse string value= \(value ?? "")")
        return value ?? "";
    }
    
    
    static func parseProximity(data: NSData) -> Bool {
        var value: UInt8 = 0
        data.getBytes(&value, range: NSRange(location: 3, length: 1))
        value = UInt8(bigEndian: value)
        print("parse proximity \(value)")
        return value == 0x00
    }
    
    
    static func parseTailLight(data: NSData) -> Bool {
        var value: UInt8 = 0
        data.getBytes(&value, range: NSRange(location: 3, length: 1))
        value = UInt8(bigEndian: value)
        print("parse tail light \(value)")
        return value == 0x00
    }
    
    
    static func parseVideoTimestamp(data: NSData) -> Bool {
        var value: UInt8 = 0
        data.getBytes(&value, range: NSRange(location: 4, length: 1))
        value = UInt8(bigEndian: value)
        print("parse video timestamp \(value)")
        return value == 0x00
    }
    
    
    static func parseVideoResolution(data: NSData) -> Int {
        var value: UInt8 = 0
        data.getBytes(&value, range: NSRange(location: 3, length: 1))
        value = UInt8(bigEndian: value)
        print("parse video resolution \(value)")
        return Int(value)
    }
    
    
    static func parseVideoRecordingTime(data: NSData) -> Int {
        var value: UInt8 = 0
        data.getBytes(&value, range: NSRange(location: 5, length: 1))
        value = UInt8(bigEndian: value)
        print("parse video recording time \(value)")
        return Int(value)
    }
    
    
    static func parseBatteryEfficiency(data: NSData) -> Int {
        var value: UInt8 = 0
        data.getBytes(&value, range: NSRange(location: 3, length: 1))
        value = UInt8(bigEndian: value)
        print("parse battery efficiency \(value)")
        return Int(value)
    }
    
    
    static func parseBatteryLevel(data: NSData) -> Int {
        var value: UInt8 = 0
        data.getBytes(&value, range: NSRange(location: 3, length: 1))
        value = UInt8(bigEndian: value)
        return Int(value)
    }
}



