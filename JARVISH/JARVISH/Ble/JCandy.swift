//
//  JCandy.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/10/7.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

class JCandy {
    public static let HELMET_RAIDEN_EVOS = "01";
    public static let HELMET_FLASH_1 = "02";
    public static let HELMET_F2_NEW_MIC = "03";
    public static let HELMET_RAIDEN_R1_NEW_MIC = "04";
    public static let HELMET_XTREME_X1_FOR_NEW_MIC = "05";
    public static let HELMET_MONACO_EVO_S2 = "06";
    public static let HELMET_A2 = "07";
    public static let HELMET_X = "08";
    public static let HELMET_X_AR = "09";
    public static let HELMET_AT5 = "10";
    public static let HELMET_VINTAGE = "11";
    public static let HELMET_ANC_MIC_X_M26 = "12";
    public static let HELMET_ANC_MIC_FX_M26 = "13";


    public static let HELMET_NAME_RAIDEN_EVOS = "Monaco Evo S";
    public static let HELMET_NAME_FLASH_1 = "FLASH F1";
    public static let HELMET_NAME_F2_NEW_MIC = "FLASH F2";
    public static let HELMET_NAME_RAIDEN_R1_NEW_MIC = "RAIDEN R1";
    public static let HELMET_NAME_XTREME_X1_FOR_NEW_MIC = "XTREME X1";
    public static let HELMET_NAME_MONACO_EVO_S2 = "Monaco Evo S2";
    public static let HELMET_NAME_A2 = "A2";
    public static let HELMET_NAME_X = "JARVISH X";
    public static let HELMET_NAME_X_AR = "JARVISH X-AR";
    public static let HELMET_NAME_AT5 = "AT-KIT";
    public static let HELMET_NAME_VINTAGE = "AN-KIT";
    public static let HELMET_NAME_ANC_MIC_X_M26 = "JARVISH X";
    public static let HELMET_NAME_ANC_MIC_FX_M26 = "FLASH X";


    public static let RESOLUTION_M1 = 0;
    public static let RESOLUTION_M2526 = 1;
    
    
    public static func getPhoto(type: String, color: String) -> String {
        switch (type) {
        case JCandy.HELMET_RAIDEN_EVOS,
             JCandy.HELMET_MONACO_EVO_S2:
            switch (color) {
                case "01":
                    return "mes01"
                case "02":
                    return "mes02"
                case "03":
                    return "mes03"
                case "04":
                    return "mes04"
                case "05":
                    return "mes05"
                case "06":
                    return "mes06"
                default:
                    return "mes01"
            }
        case JCandy.HELMET_FLASH_1:
            switch (color) {
                case "07":
                    return "f107"
                case "08":
                    return "f108"
                case "09":
                    return "f109"
                case "10":
                    return "f110"
                case "11":
                    return "f111"
                case "12":
                    return "f112"
                case "13":
                    return "f113"
                case "14":
                    return "f114"
                case "15":
                    return "f115"
                default:
                    return "f107"
            }
        case JCandy.HELMET_F2_NEW_MIC:
            switch (color) {
                case "07":
                    return "f107"
                case "14":
                    return "f114"
                default:
                    return "f107"
            }
        case JCandy.HELMET_RAIDEN_R1_NEW_MIC:
            switch (color) {
                case "19":
                    return "r119"
                case "21":
                    return "r121"
                default:
                    return "r119"
            }
        case JCandy.HELMET_XTREME_X1_FOR_NEW_MIC:
            return "x120"
        case JCandy.HELMET_A2:
            return "a299"
        case JCandy.HELMET_X:
            switch (color) {
                case "01":
                    return "x01"
                case "02":
                    return "x02"
                default:
                    return "x01"
            }
        case JCandy.HELMET_AT5:
            return "at599"
        case JCandy.HELMET_X_AR:
            return "xar01"
        case JCandy.HELMET_VINTAGE:
            return "vintage99"
        case JCandy.HELMET_ANC_MIC_X_M26:
            switch (color) {
                case "01":
                    return "x01"
                case "02":
                    return "x02"
                default:
                    return "x01"
            }
        case JCandy.HELMET_ANC_MIC_FX_M26:
            switch (color) {
                case "07":
                    return "f107"
                case "14":
                    return "f114"
                default:
                    return "f107"
            }
        default:
            return "x01"
        }
    }


    public static func getHelmetName(type: String) -> String {
        switch (type) {
            case JCandy.HELMET_A2:
                return JCandy.HELMET_NAME_A2;
            case JCandy.HELMET_AT5:
                return JCandy.HELMET_NAME_AT5;
            case JCandy.HELMET_F2_NEW_MIC:
                return JCandy.HELMET_NAME_F2_NEW_MIC;
            case JCandy.HELMET_FLASH_1:
                return JCandy.HELMET_NAME_FLASH_1;
            case JCandy.HELMET_MONACO_EVO_S2:
                return JCandy.HELMET_NAME_MONACO_EVO_S2;
            case JCandy.HELMET_RAIDEN_EVOS:
                return JCandy.HELMET_NAME_RAIDEN_EVOS;
            case JCandy.HELMET_RAIDEN_R1_NEW_MIC:
                return JCandy.HELMET_NAME_RAIDEN_R1_NEW_MIC;
            case JCandy.HELMET_XTREME_X1_FOR_NEW_MIC:
                return JCandy.HELMET_NAME_XTREME_X1_FOR_NEW_MIC;
            case JCandy.HELMET_X:
                return JCandy.HELMET_NAME_X;
            case JCandy.HELMET_X_AR:
                return JCandy.HELMET_NAME_X_AR;
            case JCandy.HELMET_VINTAGE:
                return JCandy.HELMET_NAME_VINTAGE;
            case JCandy.HELMET_ANC_MIC_X_M26:
                return JCandy.HELMET_NAME_ANC_MIC_X_M26;
            case JCandy.HELMET_ANC_MIC_FX_M26:
                return JCandy.HELMET_NAME_ANC_MIC_FX_M26;
            default:
                return "";
        }
    }


        
    public static func identifyProduct(serial: String?) -> JProduct? {
        var number = serial;
        print("parse helmet number = \(String(describing: number))")
        if number == nil || number!.isEmpty || number!.count != 13 {
            number = UserDefaults.standard.string(forKey: "SERIAL_NUMBER") ?? ""
        }

        if number == nil || number!.isEmpty || number!.count != 13 {
            return nil
        }

        return JProduct.init(serial: number!)
    }

    
    public static func getBatteryLevel(type: String, level: Int) -> Int {
        if type == HELMET_AT5 ||
            type == HELMET_VINTAGE {
            switch level {
            case 4:
                return 100
            case 3:
                return 80
            case 2:
                return 60
            case 1:
                return 40
            case 0:
                return 20
            default:
                return 0
            }
        } else {
            return level
        }
    }
    
    
    public static func getVideoResolutionCapacity(productZza: String) -> Int {
        if productZza.elementsEqual(HELMET_RAIDEN_EVOS) ||
                productZza.elementsEqual(HELMET_FLASH_1) ||
                productZza.elementsEqual(HELMET_F2_NEW_MIC) ||
                productZza.elementsEqual(HELMET_RAIDEN_R1_NEW_MIC) ||
                productZza.elementsEqual(HELMET_XTREME_X1_FOR_NEW_MIC) ||
                productZza.elementsEqual(HELMET_MONACO_EVO_S2) {
            return RESOLUTION_M1;
        } else {
            return RESOLUTION_M2526;
        }
    }
}
