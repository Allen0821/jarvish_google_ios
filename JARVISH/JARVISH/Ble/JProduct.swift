//
//  JProduct.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/10/7.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct JProduct {
    var serial: String
    var year: String
    var language: String
    var productType: String
    var productColor: String
    var productSize: String
    var manufacturerNumber: String
    var verifyCode: String
    
    
    init(serial: String) {
        self.serial = serial
        print("JProduct: \(serial)")
        self.year = String(self.serial[..<self.serial.index(self.serial.startIndex, offsetBy: 1)])
        self.language = String(self.serial[self.serial.index(self.serial.startIndex, offsetBy: 1)..<self.serial.index(self.serial.startIndex, offsetBy: 3)])
        self.productType = String(self.serial[self.serial.index(self.serial.startIndex, offsetBy: 3)..<self.serial.index(self.serial.startIndex, offsetBy: 5)])
        self.productColor = String(self.serial[self.serial.index(self.serial.startIndex, offsetBy: 5)..<self.serial.index(self.serial.startIndex, offsetBy: 7)])
        self.productSize = String(self.serial[self.serial.index(self.serial.startIndex, offsetBy: 7)..<self.serial.index(self.serial.startIndex, offsetBy: 8)])
        self.manufacturerNumber = String(self.serial[self.serial.index(self.serial.startIndex, offsetBy: 8)..<self.serial.index(self.serial.startIndex, offsetBy: 12)])
        self.verifyCode = String(self.serial[self.serial.index(self.serial.startIndex, offsetBy: 12)..<self.serial.index(self.serial.startIndex, offsetBy: 13)])
        print("JProduct: \(self.year) \(self.language) \(self.productType) \(self.productColor) \(self.productSize) \(self.manufacturerNumber) \(self.verifyCode)")
        
    }
}
