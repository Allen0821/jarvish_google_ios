//
//  CustomPaddingTextField.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/26.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import UIKit

class CustomPaddingTextField: UITextField {

    let padding = UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16)
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
