//
//  CustomPaddingUIImageView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/6.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import UIKit

class CustomPaddingUIImageView: UIImageView {
    
    override var alignmentRectInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
    }
    
    func setRounded() {
        let radius = self.frame.width / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}
