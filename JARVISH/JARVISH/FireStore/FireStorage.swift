//
//  FireStorage.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/22.
//  Copyright © 2021 jarvish. All rights reserved.
//

import Firebase

class FireStorage {
    
    func uploadAvatar(uid: String, localFile: URL, completed: @escaping ((URL?)->())) {
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let path = "user/\(uid)/avatar.jgp"
        let avatarRef = storageRef.child(path)
        
        let uploadTask = avatarRef.putFile(from: localFile, metadata: nil) { metadata, error in          
            guard let metadata = metadata else {
                print("upload error: \(String(describing: error?.localizedDescription))")
                completed(nil)
                return
          
            }
            print("upload size: \(metadata.size)")
            // You can also access to download URL after upload.
            avatarRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    print("upload error: \(String(describing: error?.localizedDescription))")
                    completed(nil)
                    return
                }
                completed(downloadURL)
            }
            
        }
    }
}
