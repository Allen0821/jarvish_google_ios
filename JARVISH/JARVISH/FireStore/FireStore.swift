//
//  FireStore.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/7.
//  Copyright © 2021 jarvish. All rights reserved.
//

import Firebase

class FireStore {
    let db = Firestore.firestore()
    
    let accountPath = "account"
    let profilePath = "profile"
    let registerPath = "register"
    let newsPath = "news"
    
    fileprivate var someProperty: Any?
    
    private func getDocumentWithID(path: String, documentID: String, completed: @escaping ((DocumentSnapshot?)->())) {
        let docRef = db.collection(path).document(documentID)
        docRef.getDocument { (document, error) in
            completed(document)
        }
    }
    
}

