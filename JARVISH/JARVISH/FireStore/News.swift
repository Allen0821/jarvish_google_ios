//
//  News.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/15.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseFirestore

struct News: Identifiable, Codable {
    
    var id: String
    var title: String
    var subTitle: String
    var content: String
    var link: String?
    var photo: [String]?
    var video: String?
    var type: Int = 0
    var date: Timestamp?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case subTitle
        case content
        case link
        case photo
        case video
        case type
        case date
    }
}
