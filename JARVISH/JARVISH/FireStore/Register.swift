//
//  Register.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/6.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseFirestore
import FirebaseFirestoreSwift

struct Register: Identifiable, Codable {
    var id: String = UUID().uuidString
    
    var serial: String
    var uid: String
    var name: String?
    var category: Int = 0
    var expired: Bool = false
    var date: Timestamp?
    
    enum CodingKeys: String, CodingKey {
        case serial
        case uid
        case name
        case category
        case expired
        case date
    }
    
    func toDictionary() -> [String : Any] {
        let data: [String: Any] = [
            "serial": self.serial,
            "uid": self.uid,
            "name": self.name ?? "",
            "category": self.category,
            "expired": self.expired,
            "date": FieldValue.serverTimestamp()
        ]
        return data
    }
}

