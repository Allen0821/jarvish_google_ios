//
//  StoreAccount.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/15.
//  Copyright © 2021 jarvish. All rights reserved.
//

import Firebase

class StoreAccount: FireStore {
    //let db = Firestore.firestore()
    //private let path = "account"
   
    func getDocument(documentID: String, completed: @escaping ((Account?)->())) {
        let docRef = db.collection(accountPath).document(documentID)
        docRef.getDocument { (document, error) in
            let result = Result {
                try document?.data(as: Account.self)
            }
            switch result {
            case .success(let account):
                if let account = account {
                    print("[Account]: Get=>\(account)")
                    completed(account)
                } else {
                    print("[Account]: Document does not exist")
                    completed(nil)
                }
            case .failure(let error):
                print("[Account]: Error decoding city: \(error)")
                completed(nil)
            }
        }
    }
    
    func updateDocument(documentID: String, data: [String : Any], completed: @escaping ((Bool)->())) {
        let docRef = db.collection(accountPath).document(documentID)
        docRef.updateData(data) { error in
            if let error = error {
                print("[Account]: Error updating document: \(error)")
                completed(false)
            } else {
                print("Document successfully updated")
                completed(true)
            }
        }
    }
    
}
