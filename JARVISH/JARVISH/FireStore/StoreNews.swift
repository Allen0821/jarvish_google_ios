//
//  StoreNews.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/16.
//  Copyright © 2021 jarvish. All rights reserved.
//

import Foundation

class StoreNews: FireStore {
    
    func queryNewsList(completed: @escaping (([News]?)->())) {
        db.collection(newsPath)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("[News]: Error get documents: \(err)")
                    completed(nil)
                } else {
                    var newsList: [News] = []
                    for document in querySnapshot!.documents {
                        //print("[News]:  \(document.documentID) => \(document.data())")
                        let result = Result {
                            try document.data(as: News.self)
                        }
                        switch result {
                        case .success(let news):
                            if let news = news {
                                newsList.append(news)
                            }
                        case .failure(let error):
                            print("[News]: Error decoding: \(error)")
                        }
                    }
                    completed(newsList)
                }
        }
    }
}
