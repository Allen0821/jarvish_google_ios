//
//  StoreProfile.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/15.
//  Copyright © 2021 jarvish. All rights reserved.
//

import Firebase

class StoreProfile {
    let db = Firestore.firestore()
    private let path = "profile"
    
    func getDocument(documentID: String, completed: @escaping ((UserProfile?)->())) {
        let docRef = db.collection(path).document(documentID)
        docRef.getDocument { (document, error) in
            let result = Result {
                try document?.data(as: UserProfile.self)
            }
            switch result {
            case .success(let profile):
                if let profile = profile {
                    print("[Profile]: Get=>\(profile)")
                    completed(profile)
                } else {
                    print("[Profile]: Document does not exist")
                    completed(nil)
                }
            case .failure(let error):
                print("[Profile]: Error decoding city: \(error)")
                completed(nil)
            }
        }
    }
    
    func updateDocument(documentID: String, data: [String : Any], completed: @escaping ((Bool)->())) {
        let docRef = db.collection(path).document(documentID)
        docRef.updateData(data) { error in
            if let error = error {
                print("[Account]: Error updating document: \(error)")
                completed(false)
            } else {
                print("Document successfully updated")
                completed(true)
            }
        }
    }
}
