//
//  StoreRegister.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/7.
//  Copyright © 2021 jarvish. All rights reserved.
//


class StoreRegister: FireStore {
   
   
    func getRegisterWithID(documentID: String,  completed: @escaping ((Register?)->())) {
        let docRef = db.collection(registerPath).document(documentID)
        docRef.getDocument { (document, error) in
            let result = Result {
                try document?.data(as: Register.self)
            }
            switch result {
            case .success(let register):
                if let register = register {
                    print("[Register]: Get=>\(register)")
                    completed(register)
                } else {
                    print("[Register]: Document does not exist")
                    completed(nil)
                }
            case .failure(let error):
                print("[Register]: Error decoding register: \(error)")
                completed(nil)
            }
        }
    }
    
    
    func createRegister(documentID: String, register: Register, completed: @escaping ((Bool)->())) {
        let batch = db.batch()

        let regRef = db.collection(registerPath).document(register.serial)
        batch.setData(register.toDictionary(), forDocument: regRef)

        let proRef = db.collection(accountPath).document(register.uid)
        batch.updateData(["haveProduct": true ], forDocument: proRef)
        
        batch.commit() { err in
            if let err = err {
                print("[Register]: Error writing batch \(err)")
                completed(false)
            } else {
                print("[Register]: Batch write succeeded.")
                completed(true)
            }
        }
    }
    
    
    func queryRegisteredProducts(uid: String, completed: @escaping (([Register]?)->())) {
        db.collection(registerPath)
            .whereField("uid", isEqualTo: uid)
            .order(by: "date", descending: true)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("[Register]: Error get documents: \(err)")
                    completed(nil)
                } else {
                    var registers: [Register] = []
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        let result = Result {
                            try document.data(as: Register.self)
                        }
                        switch result {
                        case .success(let register):
                            if let register = register {
                                registers.append(register)
                            }
                        case .failure(let error):
                            print("[Register]: Error decoding register: \(error)")
                        }
                    }
                    completed(registers)
                }
        }
    }
    
    
    func isValidSerial(serial: String?) -> Bool {
        print("input serial: \(String(describing: serial))")
        if (serial == nil || serial!.isEmpty || serial!.count != 13) {
            return false
        }
        
        return (Int(serial!) != nil)
    }
}
