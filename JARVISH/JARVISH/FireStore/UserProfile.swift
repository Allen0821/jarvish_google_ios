//
//  UserProfile.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/15.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseFirestore
import FirebaseFirestoreSwift

struct UserProfile: Codable {
    var uid: String
    var name: String
    var avatarUrl: String?
    var level: Int = 0
    var created: Timestamp
    
    enum CodingKeys: String, CodingKey {
        case uid
        case name
        case avatarUrl
        case level
        case created
    }
}
