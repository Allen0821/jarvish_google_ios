//
//  AuthMethod.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/4.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseAuth

class AuthMethod {
    
    func sendSignInLink(email: String, completion: @escaping (AuthErrorCode)->()) {
        
        let actionCodeSettings = ActionCodeSettings()
        actionCodeSettings.url = URL(string: "https://jarvish-24e27.firebaseapp.com")
        // The sign-in operation has to always be completed in the app.
        actionCodeSettings.handleCodeInApp = true
        actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)
           
        Auth.auth().languageCode = Locale.current.languageCode
        Auth.auth().sendSignInLink(toEmail: email,
                                   actionCodeSettings: actionCodeSettings) { error in
            if let error = error {
                print("[Auth]: \(error.localizedDescription)")
                completion(AuthErrorCode.ERROR_SEND_SIGN_IN_LINK)
                return
            }
            UserDefaults.standard.set(email, forKey: "AuthEmail")
            completion(AuthErrorCode.NONE)
            
        }
    }
}
