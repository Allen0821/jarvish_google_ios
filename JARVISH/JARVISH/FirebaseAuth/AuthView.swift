//
//  AuthView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/2.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI
import GoogleSignIn

struct ProgressView: View {
    @State private var isLoading = false
    
    var body: some View {
        ZStack() {
            Color.black.opacity(0.35)
            Circle()
                .trim(from: 0.2, to: 1.0)
                    .stroke(style: StrokeStyle(lineWidth: 6.0, lineCap: .round, lineJoin: .round))
                    .foregroundColor(Color("colorSecondary"))
                .frame(width: 48, height: 48)
                .rotationEffect(.degrees(self.isLoading ? 360 : 0))
                .animation(
                    Animation.linear(duration: 1)
                    .repeatForever(autoreverses: false))
        }
        .edgesIgnoringSafeArea(.all)
        .onAppear(perform: {
            self.isLoading = true
        })
        .onDisappear(perform: {
            self.isLoading = false
        })
    }
}

struct AuthView: View {
    var dismiss: (() -> Void)!
    var signInDismiss: ((Bool) -> Void)!
    var googleSignIn: (() -> Void)!
    @ObservedObject var viewModel = AuthViewModel()
    
    var body: some View {
        ZStack {
            if !viewModel.isPresentedEmailLinkAuth {
                VStack {
                    Spacer()
                    Button(action: {
                        viewModel.isPresentedEmailLinkAuth.toggle()
                    }) {
                        HStack {
                            Image("mail")
                                .resizable()
                                .frame(width: 24, height: 24)
                                .padding(.horizontal, 12.0)
                            Text(LocalizedStringKey("auth_method_sign_in_with_email_link"))
                                .font(.body)
                                .fontWeight(.medium)
                            Spacer()
                        }
                        .padding(.horizontal)
                        .padding(.vertical, 12.0)
                        .foregroundColor(.white)
                        .background(Color("colorPrimary"))
                        .cornerRadius(40)
                    }
                    .padding(.horizontal, 44.0)
                    
                    Button(action: {
                        viewModel.requestAppleLogin()
                    }) {
                        HStack {
                            Image("appleLogo")
                                .resizable()
                                .frame(width: 24, height: 24)
                                .padding(.horizontal, 12.0)
                            Text("auth_method_sign_in_with_apple")
                                .font(.body)
                                .fontWeight(.medium)
                            Spacer()
                        }
                        .padding(.horizontal)
                        .padding(.vertical, 12.0)
                        .foregroundColor(Color("colorAppleLogo"))
                        .background(Color("colorApple"))
                        .cornerRadius(40)
                    }
                    .padding(.horizontal, 44.0)
                    
                    Button(action: {
                        viewModel.facebookLogin()
                    }) {
                        HStack {
                            Image("facebooklogo")
                                .resizable()
                                .frame(width: 24, height: 24)
                                .padding(.horizontal, 12.0)
                            Text("auth_method_sign_in_with_facebook")
                                .font(.body)
                                .fontWeight(.medium)
                            Spacer()
                        }
                        .padding(.horizontal)
                        .padding(.vertical, 12.0)
                        .foregroundColor(.white)
                        .background(Color("colorFacebook"))
                        .cornerRadius(40)
                    }
                    .padding(.horizontal, 44.0)
                    
                    Button(action: googleSignIn) {
                        HStack {
                            Image("googleLogo")
                                .resizable()
                                .frame(width: 24, height: 24)
                                .padding(.horizontal, 12.0)
                            Text("auth_method_sign_in_with_google")
                                .font(.body)
                                .fontWeight(.medium)
                                .foregroundColor(.black)
                            Spacer()
                        }
                        .padding(.horizontal)
                        .padding(.vertical, 12.0)
                        .background(Color.white)
                        .cornerRadius(40)
                        .overlay(RoundedRectangle(cornerRadius: 40)
                                    .stroke(Color("colorApple"), lineWidth: 1))
                    }
                    .padding(.horizontal, 44.0)
                    Spacer()
                    Button(action: dismiss) {
                        Text("Skip")
                            .font(.body)
                            .fontWeight(.regular)
                            .foregroundColor(Color("colorPrimary"))
                            .padding(.vertical, 8.0)
                            .padding(.horizontal,24.0)
                            .overlay(RoundedRectangle(cornerRadius: 40)
                                        .stroke(Color("colorPrimary"), lineWidth: 2))
                    }
                    .padding(.bottom, 24.0)
                }
            } else {
                EmailLinkAuthView(viewModel: viewModel)
            }
            if viewModel.isProgress {
                ProgressView()
            }
        }
        .disabled(viewModel.isProgress)
        .onReceive(NotificationCenter.default.publisher(for: Notification.Name(rawValue: "EMAILLINKAUTH")), perform: { _ in
            print("[Auth]: onReceive email link auth")
            self.viewModel.isEmailLinkSent = false
            if let link = UserDefaults.standard.string(forKey: "AuthLink") {
                self.viewModel.signInWithEmailLink(link: link)
                
            }
        })
        .onReceive(NotificationCenter.default.publisher(for: Notification.Name(rawValue: "GOOGLEAUTH")), perform: { _ in
            print("[Auth]: onReceive google auth")
            self.viewModel.signInWithGoogle()
        })
        .onChange(of: viewModel.isSignIn, perform: signInDismiss)
        .alert(isPresented: $viewModel.isShowAlert, content: {
            return Alert(title: Text(viewModel.authCode.rawValue))
        })
    }
    
}

struct AuthView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
    }
}
