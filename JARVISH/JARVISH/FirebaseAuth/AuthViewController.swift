//
//  AuthViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/12.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI
import GoogleSignIn

class AuthViewController: UIHostingController<AuthView> {
    init() {
        super.init(rootView: AuthView())
        rootView.dismiss = dismiss
        rootView.signInDismiss = signInDismiss
        rootView.googleSignIn = googleSignIn
    }
    
    @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    func signInDismiss(value: Bool) {
        if value {
            dismiss(animated: true, completion: nil)
        }
    }
    
    func googleSignIn() {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
}
