//
//  AuthViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/3.
//  Copyright © 2021 jarvish. All rights reserved.
//

class AuthViewModel: ObservableObject {
    @Published var isDisplayMainView: Bool = false
    
    @Published var isPresentedEmailLinkAuth = false
    @Published var isEmailLinkSent = false
    @Published var isSentDisabled: Bool = true
    
    let authMethod = AuthMethod()
    
    @Published var email: String = "" {
        didSet {
            self.isSentDisabled = !isValidEmail(email: self.email)
            print("is  email valid? \(!self.isSentDisabled)")
        }
    }
    
    
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    
    func sendEmailLink() {
        authMethod.sendSignInLink(email: email) { errorCode in
            if errorCode == AuthErrorCode.NONE {
                self.isSent = true
            }
        }
    }
}
