//
//  EmailLinkAuthView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/4.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

struct EmailLinkSentView: View {
    @ObservedObject var viewModel = AuthViewModel()
    
    init(viewModel: AuthViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(LocalizedStringKey("auth_method_email_link_sent"))
                .font(.title)
                .fontWeight(.bold)
                .multilineTextAlignment(.leading)
                .padding(.horizontal, 8.0)
                
            Text(LocalizedStringKey("auth_method_email_link_sent_des"))
                .font(.body)
                .fontWeight(.regular)
                .multilineTextAlignment(.leading)
                .padding(.top, 4.0)
                .padding(.horizontal, 8.0)
            Button(action: {
                self.viewModel.openEmail()
            }) {
                Spacer()
                Text(LocalizedStringKey("auth_action_open_email"))
                    .font(.body)
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                Spacer()
            }
            .padding(.vertical, 12.0)
            .background(Color("colorPrimary"))
            .cornerRadius(40.0)
            .padding(.top, 24.0)
            
            Button(action: {
                self.viewModel.sendEmailLink()
            }) {
                Spacer()
                Text(LocalizedStringKey("auth_action_resend_email_link"))
                    .font(.footnote)
                    .fontWeight(.regular)
                    .foregroundColor(Color("colorSecondary"))
                Spacer()
            }
            .padding(.top, 4.0)
        }
    }
}

struct SendEmailLinkView: View {
    @ObservedObject var viewModel = AuthViewModel()
    
    init(viewModel: AuthViewModel) {
        self.viewModel = viewModel
    }
    
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(LocalizedStringKey("auth_method_sign_in_with_email_link"))
                .font(.title)
                .fontWeight(.bold)
                .multilineTextAlignment(.leading)
                .padding(.horizontal, 8.0)
                
            Text(LocalizedStringKey("auth_method_email_link_des"))
                .font(.body)
                .fontWeight(.regular)
                .multilineTextAlignment(.leading)
                .padding(.top, 4.0)
                .padding(.horizontal, 8.0)

            HStack {
                Image(systemName: "envelope")
                    .resizable()
                    .frame(width: 20, height: 16)
                    .foregroundColor(.white)
                TextField("", text: $viewModel.email)
                    .lineLimit(1)
                    .textContentType(/*@START_MENU_TOKEN@*/.emailAddress/*@END_MENU_TOKEN@*/)
                    .keyboardType(.emailAddress)
                    .foregroundColor(.white)
                    .padding(.leading, 4.0)
            }
            .padding(.all, 12.0)
            .background(Color("colorTextFiledBG"))
            .cornerRadius(40.0)
            .padding(.top, 24.0)
            
            Button(action: {
                self.viewModel.sendEmailLink()
            }) {
                Spacer()
                Text(LocalizedStringKey("auth_action_continue"))
                    .font(.body)
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                Spacer()
            }
            .disabled(viewModel.isSentDisabled)
            .padding(.vertical, 12.0)
            .background(buttonBackgroundColor())
            .cornerRadius(40.0)
            .padding(.top, 4.0)
        }
    }
    
    func buttonBackgroundColor() -> Color {
        if viewModel.isSentDisabled {
            return Color.gray
        } else {
            return Color("colorPrimary")
        }
    }
}

struct EmailLinkAuthView: View {
    @ObservedObject var viewModel = AuthViewModel()
    
    init(viewModel: AuthViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ZStack {
            VStack {
                HStack {
                    Button(action: {
                        self.viewModel.isPresentedEmailLinkAuth = false
                        self.viewModel.email = ""
                        self.viewModel.isEmailLinkSent = false
                    }) {
                        Image(systemName: "xmark.circle.fill")
                            .resizable()
                            .frame(width: 32.0, height: 32.0)
                    }
                    .foregroundColor(Color("colorSecondary"))
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                    .shadow(radius: 1.0)
                    .padding(.leading, 12.0)
                    Spacer()
                }
                if viewModel.isEmailLinkSent {
                    EmailLinkSentView(viewModel: viewModel)
                        .padding(.horizontal, 40.0)
                        .padding(.top, 48.0)
                } else {
                    SendEmailLinkView(viewModel: viewModel)
                        .padding(.horizontal, 40.0)
                        .padding(.top, 48.0)
                }
                Spacer()
            }
            
            if viewModel.isProgress {
                ProgressView()
            }
        }
        .disabled(viewModel.isProgress)
    }
}

struct EmailLinkAuthView_Previews: PreviewProvider {
    static var previews: some View {
        EmailLinkAuthView(viewModel: AuthViewModel())
    }
}
