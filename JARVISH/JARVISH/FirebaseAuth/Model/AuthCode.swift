//
//  AuthCode.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/4.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

enum AuthCode: LocalizedStringKey {
    case NONE = "error_none"
    case INVALID_EMAIL_ADDRESS = "error_invalid_email_address"
    case ERROR_SEND_SIGN_IN_LINK = "error_send_email_link"
    case ERROR_EMAIL_LINK_AUTH = "error_email_link_auth"
    case ERROR_SIGN_IN = "error_sign_in"
    case ERROR_COLLISION_AUTH = "error_collision_auth"
    
    var message: LocalizedStringKey {
        return rawValue
    }
    

}

