//
//  AuthMethod.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/4.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseAuth
import FacebookLogin
import CryptoKit
import Firebase

class AuthMethod {
    
    func sendSignInLink(email: String, completion: @escaping (AuthCode)->()) {
        
        let actionCodeSettings = ActionCodeSettings()
        actionCodeSettings.url = URL(string: "https://jarvishpremium.page.link/helmet")
        // The sign-in operation has to always be completed in the app.
        actionCodeSettings.handleCodeInApp = true
        actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)
           
        Auth.auth().languageCode = Locale.preferredLanguages[0]
        print("languageCode: \(String(describing: Auth.auth().languageCode))")
        Auth.auth().sendSignInLink(toEmail: email,
                                   actionCodeSettings: actionCodeSettings) { error in
            if let error = error {
                print("[Auth]: \(error.localizedDescription)")
                completion(AuthCode.ERROR_SEND_SIGN_IN_LINK)
                return
            }
            UserDefaults.standard.set(email, forKey: "AuthEmail")
            completion(AuthCode.NONE)
            
        }
    }
    
    
    func signInWithEmailLink(email: String, link: String, completion: @escaping (AuthCode)->()) {
        print("[Auth]: signInWithEmailLink=>\(link)")
        Auth.auth().signIn(withEmail: email, link: link) { (authResult, error) in
            UserDefaults.standard.set(nil, forKey: "AuthEmail")
            UserDefaults.standard.set(nil, forKey: "AuthLink")
            if let error = error {
                print("[Auth]: \(error.localizedDescription)")
                let errCode = AuthErrorCode(rawValue: error._code)
                print("[Auth]: error code: \(errCode!)")
                completion(AuthCode.ERROR_EMAIL_LINK_AUTH)
                return
            }
            if authResult!.additionalUserInfo!.isNewUser {
                print("[Auth]: new user")
                self.createNewUser(user: authResult!.user) {
                    completion(AuthCode.NONE)
                }
            } else {
                completion(AuthCode.NONE)
            }
                
        }
    }
    
    func signInWithFacebook(accessToken: AccessToken, completion: @escaping (AuthCode)->()) {
        let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
        signInUser(authCredential: credential) { authCode in
            completion(authCode)
        }
    }
    
    
    func signInWithGoogle(completion: @escaping (AuthCode)->()) {
        let idToken = UserDefaults.standard.string(forKey: "GIDToken")
        let accessToken = UserDefaults.standard.string(forKey: "GAccessToken")
        let credential = GoogleAuthProvider.credential(withIDToken: idToken!, accessToken: accessToken!)
        signInUser(authCredential: credential) { authCode in
            UserDefaults.standard.set(nil, forKey: "GIDToken")
            UserDefaults.standard.set(nil, forKey: "GAccessToken")
            completion(authCode)
        }
    }
    
    func signInWithApple(idToken: String, rawNonce: String, completion: @escaping (AuthCode)->()) {
        let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                  idToken: idToken,
                                                        rawNonce: rawNonce)
        signInUser(authCredential: credential) { authCode in
            completion(authCode)
        }
    }
    
    private func signInUser(authCredential: AuthCredential, completion: @escaping (AuthCode)->()) {
        Auth.auth().signIn(with: authCredential) { authResult, error in
            if let error = error {
                print("[Auth]: \(error.localizedDescription)")
                let errCode = AuthErrorCode(rawValue: error._code)
                switch errCode {
                case .accountExistsWithDifferentCredential:
                    completion(AuthCode.ERROR_COLLISION_AUTH)
                default:
                    completion(AuthCode.ERROR_SIGN_IN)
                }
                return
            }
            
            print("[Auth]: sign in user: \(String(describing: authResult?.user.displayName))")
            if authResult!.additionalUserInfo!.isNewUser {
                print("[Auth]: new user")
                self.createNewUser(user: authResult!.user) {
                    completion(AuthCode.NONE)
                }
            } else {
                completion(AuthCode.NONE)
            }
        }
    }
    
    
    /*private func mergeUser(user: User, completion: @escaping ()->()) {
        let storeProfile = StoreProfile()
        storeProfile.getDocument(documentID: user.uid) { result in
            if let profile = result {
                print("[Auth]: user have already created")
                if !profile.name.elementsEqual(user.displayName ?? "") {
                    
                }
            }
            completion()
        }
    }*/
    
    
    private func createNewUser(user: User, completion: @escaping ()->()) {
        print("[Auth]: create new user")
        let db = Firestore.firestore()
        let batch = db.batch()
        
        let profile: [String: Any] = [
            "uid": user.uid,
            "name": user.displayName ?? "",
            "avatarUrl": user.photoURL?.absoluteString ?? "",
            "level": 0,
            "created": FieldValue.serverTimestamp()
        ]
        let profileRef = db.collection("profile").document(user.uid)
        batch.setData(profile, forDocument: profileRef)
        
        let account: [String: Any] = [
            "uid": user.uid,
            "email": user.email ?? "",
            "emailVerified": false,
            "elderId": "",
            "elder": false,
            "haveProduct": false,
            "created": FieldValue.serverTimestamp()
        ]
        
        let accountRef = db.collection("account").document(user.uid)
        batch.setData(account, forDocument: accountRef)
        
        // Commit the batch
        batch.commit() { err in
            if let err = err {
                print("Error writing batch \(err)")
            } else {
                print("Batch write succeeded.")
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: "PROFILEUPDATED"), object: nil)
            completion()
        }
       
    }
    
    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> = Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length

        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }

            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }

                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        return result
    }
    
    func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()

        return hashString
    }
}
