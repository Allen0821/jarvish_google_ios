//
//  AuthViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/3.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FacebookLogin
import FacebookCore
import AuthenticationServices

class AuthViewModel: NSObject, ObservableObject, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    @Published var isSignIn: Bool = false
    
    @Published var isPresentedEmailLinkAuth = false
    @Published var isEmailLinkSent = false
    @Published var isSentDisabled: Bool = true
    
    @Published var isProgress: Bool = false
    @Published var isShowAlert: Bool = false
    var authCode: AuthCode = AuthCode.NONE
    
    let authMethod = AuthMethod()
    let loginManager = LoginManager()
    
    fileprivate var currentNonce: String?
    
    @Published var email: String = "" {
        didSet {
            self.isSentDisabled = !isValidEmail(email: self.email)
            print("is  email valid? \(!self.isSentDisabled)")
        }
    }
    
    
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    
    func sendEmailLink() {
        self.isProgress = true
        authMethod.sendSignInLink(email: email) { errorCode in
            self.isProgress = false
            if errorCode == AuthCode.NONE {
                self.isEmailLinkSent = true
            } else {
                self.isShowAlert = true
                self.authCode = errorCode
            }
        }
    }
    
    func signInWithEmailLink(link: String) {
        isPresentedEmailLinkAuth = false
        self.isProgress = true
        if let email = UserDefaults.standard.string(forKey: "AuthEmail") {
            authMethod.signInWithEmailLink(email: email, link: link) { errorCode in
                self.isProgress = false
                if errorCode == AuthCode.NONE {
                    
                    self.isSignIn = true
                } else {
                    self.isShowAlert = true
                    self.authCode = errorCode
                }
            }
        }
    }
    
    func openEmail() {
        let url = URL(string: "message://")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func facebookLogin() {
        loginManager.logOut()
        loginManager.logIn(permissions: [.email, .publicProfile], viewController: nil) { result in
            switch result {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let granted, let declined, let token):
                print("Logged in! \(granted) \(declined) \(token)")
                self.isProgress = true
                self.authMethod.signInWithFacebook(accessToken: token) { errorCode in
                    self.isProgress = false
                    if errorCode == AuthCode.NONE {
                       
                        self.isSignIn = true
                    } else {
                        self.isShowAlert = true
                        self.authCode = errorCode
                    }
                }
            }
            
        }
    }
    
    func signInWithGoogle() {
        self.isProgress = true
        authMethod.signInWithGoogle() { errorCode in
            self.isProgress = false
            if errorCode == AuthCode.NONE {
               
                self.isSignIn = true
            } else {
                self.isShowAlert = true
                self.authCode = errorCode
            }
        }
    }
    
    func requestAppleLogin() {
        let nonce = authMethod.randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = authMethod.sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
    }
    
    
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return ASPresentationAnchor()
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            self.isProgress = true
            authMethod.signInWithApple(idToken: idTokenString, rawNonce: nonce) { errorCode in
                self.isProgress = false
                if errorCode == AuthCode.NONE {
                  
                    self.isSignIn = true
                } else {
                    self.isShowAlert = true
                    self.authCode = errorCode
                }
            }
        }
    }
    
    
}
