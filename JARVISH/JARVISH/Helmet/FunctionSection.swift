//
//  FunctionSection.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/9.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct FunctionSection {
    var item: [String]
    var index: Int
    
    
    init(item: [String], index: Int) {
        self.item = item
        self.index = index
    }
}
