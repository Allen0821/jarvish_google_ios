//
//  HelmetVideo.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/19.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import UIKit

struct HelmetVideo {
    var thumbnail: UIImage
    var name: String
    var date: String
    var day: String
    var fileSize: Int
    var type: Int
    var fileSource: Int
    var fileID: String
    var corrected: Bool
    var status: String
    
    
    var dictionary: [String: Any] {
       return [
           "thumbnail": thumbnail,
           "name": name,
           "date": date,
           "day": day,
           "fileSize": fileSize,
           "type": type,
           "fileSource": fileSource,
           "fileID": fileID,
           "corrected": corrected,
           "status": status
       ]
    }
    
   
    
    init() {
        self.thumbnail = UIImage(systemName: "video")!
        self.name = ""
        self.date = ""
        self.day = ""
        self.fileSize = 0
        self.type = 1
        self.fileSource = 1
        self.fileID = ""
        self.corrected = false
        self.status = ""
    }
}
