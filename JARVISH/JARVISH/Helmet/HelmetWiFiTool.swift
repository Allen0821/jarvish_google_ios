//
//  HelmetWiFiTool.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/10.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import PlainPing

class HelmetWiFiTool {
    static func pingHelmet(completion: @escaping (_ result: Bool)->()) {
        PlainPing.ping("192.168.1.1", withTimeout: 1.0, completionBlock: { (timeElapsed:Double?, error:Error?) in
            if let latency = timeElapsed {
                print("HelmetWiFiTool latency (ms): \(latency)")
                completion(true)
            } else {
                print("HelmetWiFiTool error")
                completion(false)
            }
        })
    }
}
