//
//  PreviewViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/5.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController, ICatchPreviewDelegate {
    @IBOutlet weak var closeImageView: UIImageView!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    
    var iCatchManager: ICatchManager?
    //var c: CameraSDKManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        closeImageView.isUserInteractionEnabled = true
        let close = UITapGestureRecognizer(target: self, action: #selector(closePreview))
        close.numberOfTapsRequired = 1
        closeImageView.addGestureRecognizer(close)
        
        self.indicator.isHidden = false
        
        self.iCatchManager = ICatchManager.shared()!
        self.iCatchManager?.add(self)
        
        //self.icatchManager?.initializeIcatchManager()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if (iCatchManager?.isICatchConnected())! {
            self.iCatchManager?.startPreview()
            self.indicator.isHidden = true
        } else {
            self.iCatchManager?.connectToICatchCamera()
        }
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        print("xxx viewWillDisappear")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func closePreview() {
        //self.icatchManager?.destroy()
        self.iCatchManager?.stopPreview()
        self.iCatchManager?.remove(self)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func onReceivedPreviewImage(_ image: UIImage!) {
        self.previewImageView.image = image
    }
    
    private func iCatchInitFailureAlert() {
        let alert = UIAlertController(
            title: NSLocalizedString("helmet_icatch_connection_fail_title", comment: ""),
            message: NSLocalizedString("helmet_icatch_connection_fail_content", comment: ""),
            preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    func onActionCompleted(_ action: ICatchAction) {
        switch action {
        case ActionConnectedToCamera:
            print("xxx Connected")
            self.iCatchManager?.startPreview()
            self.indicator.isHidden = true
            break
        case ActionCameraDisconnected:
            print("xxx Disconnected")
            self.iCatchManager?.stopPreview()
            self.iCatchManager?.remove(self)
            self.iCatchInitFailureAlert()
            break
        default:
            print("unknown")
            break
        }
    }
    
    func onMovieRecordElapsedTime(_ timeStr: String!) {
        print("xxx timeStr \(String(describing: timeStr))")
    }
}
