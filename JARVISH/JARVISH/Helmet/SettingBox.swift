//
//  SettingBox.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/10/5.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

class SettingBox {
    public static let FUNC_TAIL_LIGHT = "FUNC_TAIL_LIGHT"
    public static let FUNC_VIDEO_TIMESTAMP = "FUNC_VIDEO_TIMESTAMP"
    public static let FUNC_VIDEO_RESOLUTION = "FUNC_VIDEO_RESOLUTION"
    public static let FUNC_RECORDING_TIME = "FUNC_RECORDING_TIME"
    public static let FUNC_BATTERY_EFFICIENCY = "FUNC_BATTERY_EFFICIENCY"
    public static let FUNC_SYNC_SYSTEM_TIME = "FUNC_SYNC_SYSTEM_TIME"
    public static let FUNC_RESET_DEFAULT = "FUNC_RESET_DEFAULT"
    public static let FUNC_CAMERA_PREVIEW = "FUNC_CAMERA_PREVIEW"
    public static let FUNC_HELMET_VIDEO = "FUNC_HELMET_VIDEO"
    public static let FUNC_BOOKMARK_VIDEO = "FUNC_BOOKMARK_VIDEO"
    public static let FUNC_FORMAT = "FUNC_FORMAT"
    public static let FUNC_FW_UPDATE = "FUNC_FW_UPDATE"
    public static let FUNC_SERIAL_NUMBER = "FUNC_SERIAL_NUMBER"
    public static let FUNC_FW_VERSION = "FUNC_FW_VERSION"
    
    
    public static func buildSettingBox(_ helmet: Helmet) -> [FunctionSection] {
        var box: [FunctionSection] = []
        switch helmet.style {
        case JCandy.HELMET_RAIDEN_EVOS,
             JCandy.HELMET_MONACO_EVO_S2:
            
            box.append(addSettingTitle())
            //box[0].item.append(addTailLight())
            box[0].item.append(addVideoTimestamp())
            box[0].item.append(addVideoResolution())
            box[0].item.append(addRecordingTime())
            box[0].item.append(contentsOf: addCommonSettingsView())
            box.append(addWifiSettingTitle())
            box[1].item.append(contentsOf: addWifiFunctions())
            box.append(addInformationTitle())
            box[2].item.append(addSerialNumber())
            box[2].item.append(addFirmwareVersion())
            
            break
            
        case JCandy.HELMET_A2,
             JCandy.HELMET_X,
             JCandy.HELMET_X_AR,
             JCandy.HELMET_ANC_MIC_X_M26,
             JCandy.HELMET_RAIDEN_R1_NEW_MIC,
             JCandy.HELMET_F2_NEW_MIC,
             JCandy.HELMET_FLASH_1,
             JCandy.HELMET_XTREME_X1_FOR_NEW_MIC,
             JCandy.HELMET_ANC_MIC_FX_M26:
            
            box.append(addSettingTitle())
            box[0].item.append(addTailLight())
            box[0].item.append(addVideoTimestamp())
            box[0].item.append(addVideoResolution())
            box[0].item.append(addRecordingTime())
            box[0].item.append(contentsOf: addCommonSettingsView())
            box.append(addWifiSettingTitle())
            box[1].item.append(contentsOf: addWifiFunctions())
            box.append(addInformationTitle())
            box[2].item.append(addSerialNumber())
            box[2].item.append(addFirmwareVersion())
            
            break
        case JCandy.HELMET_AT5,
             JCandy.HELMET_VINTAGE:
            box.append(addInformationTitle())
            box[0].item.append(addSerialNumber())
            box[0].item.append(addFirmwareVersion())
            break
        default:
            print("unknown helmet")
            break
        }
        return box
    }
    
    
    private static func addSettingTitle() -> FunctionSection {
        return FunctionSection.init(item: [], index: 0)
    }
    
    private static func addWifiSettingTitle() -> FunctionSection {
        return FunctionSection.init(item: [], index: 1)
    }
    
    private static func addInformationTitle() -> FunctionSection {
        return FunctionSection.init(item: [], index: 2)
    }
    
    
    private static func addCommonSettingsView() -> [String] {
        var common: [String] = []
        common.append(addBatteryEfficiency())
        common.append(addSyncSystemTime())
        common.append(addResetDefault())
        return common
    }
    
    
    private static func addWifiFunctions() -> [String] {
        var wifi: [String] = []
        wifi.append(addCameraPreview())
        wifi.append(addHelmetVideo())
        wifi.append(addBookmarkVideo())
        wifi.append(addFormat())
        wifi.append(addFirmwareUpdate())
        return wifi
    }
    
    
    private static func addTailLight() -> String {
        return self.FUNC_TAIL_LIGHT
    }
    
    private static func addVideoTimestamp() -> String {
        return self.FUNC_VIDEO_TIMESTAMP
    }
    
    private static func addVideoResolution() -> String {
        return self.FUNC_VIDEO_RESOLUTION
    }
    
    private static func addRecordingTime() -> String {
        return self.FUNC_RECORDING_TIME
    }
    
    private static func addBatteryEfficiency() -> String {
        return self.FUNC_BATTERY_EFFICIENCY
    }
    
    private static func addSyncSystemTime() -> String {
        return self.FUNC_SYNC_SYSTEM_TIME
    }
    
    private static func addResetDefault() -> String {
        return self.FUNC_RESET_DEFAULT
    }
    
    private static func addCameraPreview() -> String {
        return self.FUNC_CAMERA_PREVIEW
    }
    
    private static func addHelmetVideo() -> String {
        return self.FUNC_HELMET_VIDEO
    }
    
    private static func addBookmarkVideo() -> String {
        return self.FUNC_BOOKMARK_VIDEO
    }
    
    private static func addFormat() -> String {
        return self.FUNC_FORMAT
    }
    
    private static func addFirmwareUpdate() -> String {
        return self.FUNC_FW_UPDATE
    }
    
    private static func addSerialNumber() -> String {
        return self.FUNC_SERIAL_NUMBER
    }
    
    private static func addFirmwareVersion() -> String {
        return self.FUNC_FW_VERSION
    }
    
    
}
