//
//  VideoSection.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/20.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import UIKit

struct VideoSection {
    var videos: [HelmetVideo]
    var header: String
   
    
    var dictionary: [String: Any] {
       return [
           "videos": videos,
           "header": header
       ]
    }
    
   
    
    init(videos: [HelmetVideo], header: String) {
        self.videos = videos
        self.header = header
    }
}
