//
//  DevicesTableViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/2/26.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class DevicesTableViewCell: UITableViewCell {
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var deviceImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    var name: String! {
        didSet {
            self.updateUI()
        }
    }
    
    
    func updateUI() {
        nameLabel.text = name
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        itemView.layer.cornerRadius = itemView.frame.size.height/2
        itemView.clipsToBounds = true
        itemView.layer.borderWidth = 1
        itemView.layer.borderColor = UIColor.white.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
