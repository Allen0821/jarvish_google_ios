//
//  FunctionViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/6.
//  Copyright © 2021 jarvish. All rights reserved.
//

import UIKit

class FunctionViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if #available(iOS 14.0, *) {
            backgroundConfiguration?.backgroundColor = UIColor.clear
        } else {
            // Fallback on earlier versions
            contentView.backgroundColor = UIColor.clear
        }
    }

}
