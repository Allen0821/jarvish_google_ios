//
//  HelmetStyleViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/5.
//  Copyright © 2021 jarvish. All rights reserved.
//

import UIKit

class HelmetStyleViewCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var itemView: UIView!
    
    var style: HelmetStyle! {
        didSet {
            nameLabel.text = style.name
            photoImageView.image = UIImage(named: style.image!)
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //itemView.layer.cornerRadius = 8
        //itemView.clipsToBounds = true
    }

}
