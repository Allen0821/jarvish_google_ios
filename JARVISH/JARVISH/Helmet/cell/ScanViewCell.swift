//
//  ScanViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/5.
//  Copyright © 2021 jarvish. All rights reserved.
//

import UIKit

class ScanViewCell: UITableViewCell {
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var name: String! {
        didSet {
            nameLabel.text = name
            //itemView.layer.borderColor = UIColor(named: "colorText01")?.cgColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        itemView.layer.cornerRadius = itemView.frame.size.height / 2
        itemView.clipsToBounds = true
        itemView.layer.borderWidth = 0.8
        itemView.layer.borderColor = UIColor(named: "colorText01")?.cgColor
    }

}
