//
//  VideoReusableView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/20.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class VideoReusableView: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
    
    var title: String! {
        didSet {
            titleLabel.text = title
        }
    }
}
