//
//  VideoViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/19.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class VideoViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var playImage: UIImageView!
    
    var image: UIImage! {
        didSet {
            thumbnail.image = image
        }
    }
    
}
