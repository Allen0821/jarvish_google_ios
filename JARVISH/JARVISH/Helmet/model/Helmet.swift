//
//  Helmet.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/6.
//  Copyright © 2021 jarvish. All rights reserved.
//


struct Helmet {
    var name: String!
    var style: String!
    var image: String!
    var serialNumber: String!
    var firmwareVersion: String!

    /* Helmet settings */
    var tailLight: Bool!
    var batteryEfficiency: Int!
    var videoResolution: Int!
    var videoRecordingTime: Int!
    var videoTimestamp: Bool!
    var batteryLevel: Int!
    
    /* product identification */
    var product: Product!
    
    init() {
        self.name = ""
        self.style = ""
        self.image = ""
        self.tailLight = false
        self.batteryEfficiency = 0
        self.videoResolution = 0
        self.videoRecordingTime = 0
        self.videoTimestamp = false
        self.batteryLevel = 0
        self.product = Product.init()
    }
}

struct Product {
    var year: String!
    var language: String!
    var productType: String!
    var productColor: String!
    var productSize: String!
    var manufacturerNumber: String!
    var verifyCode: String!
    
    init() {
        self.year = ""
        self.language = ""
        self.productType = ""
        self.productColor = ""
        self.productSize = ""
        self.manufacturerNumber = ""
        self.verifyCode = ""
    }
}
