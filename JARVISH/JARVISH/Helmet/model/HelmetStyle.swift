//
//  HelmetStyle.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/5.
//  Copyright © 2021 jarvish. All rights reserved.
//


struct HelmetStyle: Codable {
    var name: String?
    var information: String?
    var image: String?
    var index: String?
    
    init(name: String, information: String, image: String, index: String) {
        self.name = name
        self.information = information
        self.image = image
        self.index = index
    }
}
