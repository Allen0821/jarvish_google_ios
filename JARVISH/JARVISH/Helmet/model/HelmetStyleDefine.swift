//
//  HelmetStyleDefine.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/5.
//  Copyright © 2021 jarvish. All rights reserved.
//


class HelmetStyleDefine {
    public static let HELMET_NAME_RAIDEN_EVOS: String = "Monaco Evo S"
    public static let HELMET_NAME_FLASH_1: String = "FLASH F1"
    public static let HELMET_NAME_F2_NEW_MIC: String = "FLASH F2"
    public static let HELMET_NAME_RAIDEN_R1_NEW_MIC: String = "RAIDEN R1"
    public static let HELMET_NAME_XTREME_X1_FOR_NEW_MIC: String = "XTREME X1"
    public static let HELMET_NAME_MONACO_EVO_S2: String = "Monaco Evo S2"
    public static let HELMET_NAME_A2: String = "A2"
    public static let HELMET_NAME_X: String = "JARVISH X"
    public static let HELMET_NAME_X_AR: String = "JARVISH X-AR"
    public static let HELMET_NAME_AT5: String = "AT-KIT"
    public static let HELMET_NAME_VINTAGE: String = "AN-KIT"
    public static let HELMET_NAME_ANC_MIC_X_M26: String = "JARVISH X"
    public static let HELMET_NAME_ANC_MIC_FX_M26: String = "FLASH X"
    
    private static func getMonacoEvoS() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_RAIDEN_EVOS, information: "", image: "mes01", index: "01")
    }

    private static func getFlashF1() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_FLASH_1, information: "", image: "f107", index: "02")
    }

    private static func getFlashF2() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_F2_NEW_MIC, information: "", image: "f107", index: "03")
    }

    private static func getRaidenR1() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_RAIDEN_R1_NEW_MIC, information: "", image: "r119", index: "04")
    }

    private static func getXtremeX1() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_XTREME_X1_FOR_NEW_MIC, information: "", image: "x120", index: "05")
    }

    private static func getMonacoEvoS2() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_MONACO_EVO_S2, information: "", image: "mes01", index: "06")
    }

    private static func getA2() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_A2, information: "", image: "a299", index: "07")
    }

    private static func getJarvishX() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_X, information: "", image: "x01", index: "08")
    }

    private static func getJarvishXAR() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_X_AR, information: "", image: "xar01", index: "09")
    }

    private static func getAT5() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_AT5, information: "", image: "at599", index: "10")
    }

    private static func getVintage() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_VINTAGE, information: "", image: "vintage99", index: "11")
    }

    private static func getJarvishX2() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_ANC_MIC_X_M26, information: "", image: "x01", index: "12")
    }

    private static func getFlashX() -> HelmetStyle {
        return HelmetStyle.init(name: HelmetStyleDefine.HELMET_NAME_ANC_MIC_FX_M26, information: "", image: "f107", index: "13")
    }


    public static func buildHelmetStyleList() -> [HelmetStyle] {
        var styles: [HelmetStyle] = []
        styles.append(self.getMonacoEvoS())
        styles.append(self.getFlashF1())
        styles.append(self.getFlashF2())
        styles.append(self.getRaidenR1())
        styles.append(self.getXtremeX1())
        styles.append(self.getMonacoEvoS2())
        styles.append(self.getA2())
        styles.append(self.getJarvishX())
        styles.append(self.getJarvishXAR())
        styles.append(self.getAT5())
        styles.append(self.getVintage())
        styles.append(self.getFlashX())
        return styles
    }
}
