//
//  ParseSerialNumber.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/3/4.
//  Copyright © 2021 jarvish. All rights reserved.
//

class ParseSerialNumber {
    
    static func parse(serial: String?) -> Product? {
        var product: Product
        
        if serial == nil {
            return nil
        }
        
        if serial!.hasPrefix("78861") {
            product = parseFirstProduct(serial: serial!)
        } else {
            if serial!.isEmpty || serial!.count != 13 {
                return nil
            }
            product = parseRegularProduct(serial: serial!)
        }
        
        return product
    }
    
    private static func parseFirstProduct(serial: String) -> Product {
        var product = Product.init()
        product.productType = "01"
        
        return product
    }
    
    private static func parseRegularProduct(serial: String) -> Product {
        var product = Product.init()
        
        product.year = String(serial[..<serial.index(serial.startIndex, offsetBy: 1)])
        product.language = String(serial[serial.index(serial.startIndex, offsetBy: 1)..<serial.index(serial.startIndex, offsetBy: 3)])
        product.productType = String(serial[serial.index(serial.startIndex, offsetBy: 3)..<serial.index(serial.startIndex, offsetBy: 5)])
        product.productColor = String(serial[serial.index(serial.startIndex, offsetBy: 5)..<serial.index(serial.startIndex, offsetBy: 7)])
        product.productSize = String(serial[serial.index(serial.startIndex, offsetBy: 7)..<serial.index(serial.startIndex, offsetBy: 8)])
        product.manufacturerNumber = String(serial[serial.index(serial.startIndex, offsetBy: 8)..<serial.index(serial.startIndex, offsetBy: 12)])
        product.verifyCode = String(serial[serial.index(serial.startIndex, offsetBy: 12)..<serial.index(serial.startIndex, offsetBy: 13)])
        print("JProduct: \(product)")
        
        return product
    }
}
