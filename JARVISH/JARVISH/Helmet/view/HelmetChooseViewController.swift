//
//  HelmetChooseViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/5.
//  Copyright © 2021 jarvish. All rights reserved.
//

import UIKit

protocol HelmetChooseViewControllerDelegate {
    func onSelected(style: HelmetStyle)
    func onClose()
}

class HelmetChooseViewController: UIViewController {
    @IBOutlet weak var topBar: UINavigationBar!
    @IBOutlet weak var styleTableView: UITableView!
    
    var delegate: HelmetChooseViewControllerDelegate?
    
    var styles: [HelmetStyle] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        topBar.topItem?.title = NSLocalizedString("helmet_choose", comment: "")
        
        styleTableView.dataSource = self
        styleTableView.delegate = self
        styles = HelmetStyleDefine.buildHelmetStyleList()
        styleTableView.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onClickClose(_ sender: UIBarButtonItem) {
        self.delegate?.onClose()
    }
    
}


extension HelmetChooseViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return styles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelmetStyleViewCell", for: indexPath) as! HelmetStyleViewCell
        cell.style = styles[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.onSelected(style: styles[indexPath.row])
    }
    
}
