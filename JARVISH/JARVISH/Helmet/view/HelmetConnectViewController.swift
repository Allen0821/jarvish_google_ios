//
//  HelmetConnectViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/5.
//  Copyright © 2021 jarvish. All rights reserved.
//

import UIKit
import Pulsator

protocol HelmetConnectViewControllerDelegate {
    func onFindHelmet()
    func onStopFindHelmet()
    func onConnectHelmet(name: String)
    func onAddHelmet()
}

class HelmetConnectViewController: UIViewController {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var scanTableView: UITableView!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    
    var delegate: HelmetConnectViewControllerDelegate?
    var style: HelmetStyle?
    
    var devices: [String] = []
    var discoverTimer: Timer?
    
    let pulsator1 = Pulsator()
    let pulsator2 = Pulsator()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        findButton.layer.cornerRadius = findButton.frame.height / 2
        findButton.clipsToBounds = true
        findButton.setTitle(NSLocalizedString("helmet_find_my_helmet", comment: ""), for: .normal)
        
        contentLabel.text = NSLocalizedString("helmet_connect_content", comment: "")
        
        scanTableView.dataSource = self
        scanTableView.delegate = self
        scanTableView.reloadData()
        
        pulsator1.numPulse = 5
        pulsator1.radius = 320.0
        pulsator1.backgroundColor = UIColor(named: "colorPrimary")?.cgColor
        animationView.layer.addSublayer(pulsator1)
        
        pulsator2.numPulse = 5
        pulsator2.radius = 320.0
        pulsator2.backgroundColor = UIColor(named: "colorSecondary")?.cgColor
        animationView.layer.addSublayer(pulsator2)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if discoverTimer != nil {
            discoverTimer?.invalidate()
        }
        delegate?.onStopFindHelmet()
        findButton.isEnabled = true
        addButton.isEnabled = true
        self.view.isUserInteractionEnabled = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onClickFind(_ sender: UIButton) {
        findButton.isEnabled = false
        addButton.isEnabled = false
        devices.removeAll()
        scanTableView.reloadData()
        delegate?.onFindHelmet()
        discoverTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { (Timer) in
            print("discover timeout!")
            self.stopFindHelmet()
        })
        
        self.pulsator1.start()
    }
    
    @IBAction func onClickAdd(_ sender: UIButton) {
        delegate?.onAddHelmet()
    }
    
    
    func stopFindHelmet() {
        self.delegate?.onStopFindHelmet()
        findButton.isEnabled = true
        addButton.isEnabled = true
        self.pulsator1.stop()
    }
    
    func setConnectView(style: HelmetStyle) {
        self.style = style
        photoImageView.image = UIImage(named: self.style!.image!)
        nameLabel.text = self.style!.name!
        scanTableView.isHidden = false
        findButton.isHidden = false
        contentLabel.isHidden = false
        addButton.isEnabled = true
        statusLabel.isHidden = true
    }
    
    func clearConnectView() {
        pulsator1.stop()
        pulsator2.stop()
    }

    func feedDiscoverResult(_ name: String) {
        self.devices.append(name)
        DispatchQueue.main.async {
            self.scanTableView.beginUpdates()
            self.scanTableView.insertRows(at: [
                (NSIndexPath(row: self.devices.count - 1, section: 0) as IndexPath)], with: .bottom)
            self.scanTableView.endUpdates()
            if self.scanTableView.isHidden {
                self.scanTableView.isHidden = false
            }
        }
    }
    
    func connectHelmet(_ name: String) {
        if discoverTimer != nil {
            discoverTimer?.invalidate()
        }
        stopFindHelmet()
        pulsator2.start()
        delegate?.onConnectHelmet(name: name)
        statusLabel.text = NSLocalizedString("helmet_connecting", comment: "")
        statusLabel.isHidden = false
        findButton.isHidden = true
        contentLabel.isHidden = true
        addButton.isEnabled = false
        devices.removeAll()
        scanTableView.reloadData()
        scanTableView.isHidden = true
    }
    
    func onFetchSettings() {
        self.statusLabel.text = NSLocalizedString("helmet_fetching_settings", comment: "")
    }
    
    func onConnectionFailure() {
        pulsator2.stop()
        statusLabel.isHidden = true
        self.view.isUserInteractionEnabled = true
        findButton.isHidden = false
        contentLabel.isHidden = false
        addButton.isEnabled = true
      
        let alert = UIAlertController(title: NSLocalizedString("helmet_connect_failed_title", comment: ""), message: NSLocalizedString("helmet_connect_failed_content", comment: ""), preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
           print("")
        })
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
}


extension HelmetConnectViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScanViewCell", for: indexPath) as! ScanViewCell
        cell.name = devices[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        connectHelmet(devices[indexPath.row])
    }
}
