//
//  HelmetSettingViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/6.
//  Copyright © 2021 jarvish. All rights reserved.
//

import UIKit
import UICircularProgressRing

protocol HelmetSettingViewControllerDelegate {
    func setTailLight(value: Bool)
    func setTimestamp(value: Bool)
    func setBatteryEfficiency(value: Int)
    func setVideoResolution(value: Int)
    func setVideoRcordingTime(value: Int)
    func resetDefault()
    func setSystemTime()
    func onUserDisconnectHelmet()
}

class HelmetSettingViewController: UIViewController, OtaViewControllerDelegate, ICatchFormatDelegate, OtaUpdateViewControllerDelegate {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var batteryRing: UICircularProgressRing!
    @IBOutlet weak var batteryLabel: UILabel!
    @IBOutlet weak var settingTableView: UITableView!
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var progressView: UIImageView!
    @IBOutlet weak var snackLabel: CustomPaddingLabel!
    
    var delegate: HelmetSettingViewControllerDelegate?
    
    var settings: [FunctionSection] = []
    
    var helmet: Helmet!
    
    var progressTimer: Timer?
    var snackTimer: Timer?
    
    var tempValue: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        disconnectButton.layer.cornerRadius = disconnectButton.frame.height / 2
        disconnectButton.clipsToBounds = true
        disconnectButton.setTitle(NSLocalizedString("helmet_disconnect", comment: ""), for: .normal) 
        
        settingTableView.delegate = self
        settingTableView.dataSource = self
        
        batteryRing.style = .ontop
        batteryRing.innerCapStyle = .round
        batteryRing.isHidden = true
        batteryRing.isHidden = true
        
        progressView.isHidden = true
        
        snackLabel.padding(12, 12, 16, 16)
        snackLabel.layer.cornerRadius = 5
        snackLabel.layer.masksToBounds = true
        snackLabel.isHidden = true
    }
    
    private func showIndicator(value: Bool, type: Int) {
        if (value) {
            if progressTimer != nil {
                progressTimer?.invalidate()
                progressTimer = nil
            }
            progressTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { result in
                if type == 0 {
                    self.setFunctionFailedAlert()
                } else {
                    self.connectHelmetWiFiAlert()
                }
                self.showIndicator(value: false, type: type)
            })
            self.view.isUserInteractionEnabled = false
            startLoadingRotate()
        } else {
            if progressTimer != nil {
                progressTimer?.invalidate()
                progressTimer = nil
            }
            self.view.isUserInteractionEnabled = true
            stopLoadingRotate()
        }
    }
    
    
    func startLoadingRotate() {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.toValue = Double.pi * 2
        rotateAnimation.duration = 1
        rotateAnimation.repeatCount = .infinity
        progressView.layer.add(rotateAnimation, forKey: nil)
        self.view.isUserInteractionEnabled = false
        progressView.isHidden = false
    }
    
    
    func stopLoadingRotate() {
        progressView.layer.removeAllAnimations()
        self.view.isUserInteractionEnabled = true
        progressView.isHidden = true
    }

    private func setFunctionFailedAlert() {
        let alert = UIAlertController(title: NSLocalizedString("helmet_set_failed_title", comment: ""), message: NSLocalizedString("helmet_set_failed_content", comment: ""), preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("confirm")})
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }

    
    private func connectHelmetWiFiAlert() {
        let alert = UIAlertController(title: NSLocalizedString("helmet_wifi_connection_hint_title", comment: ""), message: NSLocalizedString("helmet_wifi_connection_hint_message", comment: ""), preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
           print("")
        })
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func snackBar(message: String) {
        snackLabel.isHidden = false
        snackLabel.text = message
        if snackTimer != nil {
            snackTimer?.invalidate()
            snackTimer = nil
        }
        snackTimer = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (Timer) in
            self.snackLabel.isHidden = true
        }
    }
    
    @IBAction func onClickDisconnect(_ sender: UIButton) {
        DispatchQueue.main.async() {
            let alert = UIAlertController(
                title: NSLocalizedString("helmet_disconnect_title", comment: ""),
                message: nil,
                preferredStyle: .alert)
            let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: {(alert: UIAlertAction!) in
                print("disconnect helmet")
                self.delegate?.onUserDisconnectHelmet()
            })
            alert.addAction(confirm)
            let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: nil)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func removeSettingView() {
        batteryRing.isHidden = true
        batteryRing.isHidden = true
        progressView.isHidden = true
        self.settings.removeAll()
        self.settingTableView.reloadData()
    }
    
    
    func feedHelmet(helmet: Helmet) {
        self.helmet = helmet
        nameLabel.text = self.helmet.name
        photoImageView.image = UIImage(named: self.helmet.image)
        self.settings.removeAll()
        self.settings.append(contentsOf: SettingBox.buildSettingBox(self.helmet))
        self.settingTableView.reloadData()
    }
    

    @objc private func switchTailLight(_ sender: UISwitch) {
        if sender.tag == 1001 {
            self.showIndicator(value: true, type: 0)
            self.helmet.tailLight = sender.isOn
            print("set tail light \(String(describing: self.helmet.tailLight)) tag \(sender.tag)")
            self.delegate?.setTailLight(value: self.helmet.tailLight)
        }
    }
    
    @objc private func switchTimestamp(_ sender: UISwitch) {
        if sender.tag == 1002 {
            self.showIndicator(value: true, type: 0)
            self.helmet.videoTimestamp = sender.isOn
            print("switch timestamp \(String(describing: self.helmet.videoTimestamp)) tag \(sender.tag)")
            self.delegate?.setTimestamp(value: self.helmet.videoTimestamp)
        }
    }
    
    private func setVideoResolution(_ value: Int) {
        self.showIndicator(value: true, type: 0)
        self.tempValue = value
        self.delegate?.setVideoResolution(value: self.tempValue)
    }
    
    private func openVideoResolution() {
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(Utility.detAlertTitleAttributed(title: NSLocalizedString("helmet_chose_camera_resolution", comment: "")), forKey: "attributedTitle")
        
        let capa = JCandy.getVideoResolutionCapacity(productZza: self.helmet.style)

        if capa == JCandy.RESOLUTION_M1 {
            let action1 = UIAlertAction(title: NSLocalizedString("helmet_chose_camera_resolution_1", comment: ""), style: .default) {(_) in
                self.setVideoResolution(0)
            }
            let action2 = UIAlertAction(title: NSLocalizedString("helmet_chose_camera_resolution_3", comment: ""), style: .default) {(_) in
                self.setVideoResolution(1)
            }
            
            switch self.helmet.videoResolution {
            case 0:
                action1.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
                break
            case 1:
                action2.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
                break
            default:
                action1.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
                break
            }
            
            alert.addAction(action1)
            alert.addAction(action2)
        } else {
            let action1 = UIAlertAction(title: NSLocalizedString("helmet_chose_camera_resolution_1", comment: ""), style: .default) {(_) in
                self.setVideoResolution(0)
            }
            let action2 = UIAlertAction(title: NSLocalizedString("helmet_chose_camera_resolution_2", comment: ""), style: .default) {(_) in
                self.setVideoResolution(1)
            }
            let action3 = UIAlertAction(title: NSLocalizedString("helmet_chose_camera_resolution_3", comment: ""), style: .default) {(_) in
                self.setVideoResolution(2)
            }
            
            switch self.helmet.videoResolution {
            case 0:
                action1.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
                break
            case 1:
                action2.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
                break
            case 2:
                action3.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
                break
            default:
                action1.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
                break
            }
            
            alert.addAction(action1)
            alert.addAction(action2)
            alert.addAction(action3)
        }
        
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("Cancel")})
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setRecordingTime(_ value: Int) {
        self.showIndicator(value: true, type: 0)
        self.tempValue = value
        self.delegate?.setVideoRcordingTime(value: self.tempValue)
    }
    
    private func openRecordingTime() {
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(Utility.detAlertTitleAttributed(title: NSLocalizedString("helmet_chose_recording_time", comment: "")), forKey: "attributedTitle")

        let action1 = UIAlertAction(title: NSLocalizedString("helmet_chose_recording_time_1", comment: ""), style: .default) {(_) in
            self.setRecordingTime(0)
        }
        let action2 = UIAlertAction(title: NSLocalizedString("helmet_chose_recording_time_2", comment: ""), style: .default) {(_) in
            self.setRecordingTime(1)
        }
        let action3 = UIAlertAction(title: NSLocalizedString("helmet_chose_recording_time_3", comment: ""), style: .default) {(_) in
            self.setRecordingTime(2)
        }
        switch self.helmet.videoRecordingTime {
        case 0:
            action1.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
            break
        case 1:
            action2.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
            break
        case 2:
            action3.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
            break
        default:
            action1.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
            break
        }
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("Cancel")})
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setBatteryEfficiency(_ value: Int) {
        self.showIndicator(value: true, type: 0)
        self.tempValue = value
        self.delegate?.setBatteryEfficiency(value: self.tempValue)
    }
    
    private func openBatteryEfficiency() {
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.setValue(Utility.detAlertTitleAttributed(title: NSLocalizedString("helmet_chose_battery_efficiency", comment: "")), forKey: "attributedTitle")
        let action1 = UIAlertAction(title: NSLocalizedString("helmet_chose_battery_efficiency_1", comment: ""), style: .default) {(_) in
            self.setBatteryEfficiency(0)
        }
        let action2 = UIAlertAction(title: NSLocalizedString("helmet_chose_battery_efficiency_2", comment: ""), style: .default) {(_) in
            self.setBatteryEfficiency(1)
        }
        if (self.helmet.batteryEfficiency == 0) {
            action1.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
        } else {
           action2.setValue(UIColor(named: "colorPrimary"), forKey: "titleTextColor")
        }
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("Cancel")})
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openPreview() {
        self.showIndicator(value: true, type: 1)
        HelmetWiFiTool.pingHelmet { result in
            self.showIndicator(value: false, type: 1)
            if result {
                DispatchQueue.main.async(){
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreviewViewController") as! PreviewViewController
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                self.connectHelmetWiFiAlert()
            }
        }
    }
    
    func openHelmetVideo(index: Int) {
        self.showIndicator(value: true, type: 1)
        HelmetWiFiTool.pingHelmet { result in
            self.showIndicator(value: false, type: 1)
            if result {
                DispatchQueue.main.async(){
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HelmetVideoViewController") as! HelmetVideoViewController
                    vc.index = index
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                self.connectHelmetWiFiAlert()
            }
        }
    }
    
    func openOta() {
        if OtaRoles.checkBatteryLevel(level: self.helmet.batteryLevel) {
            DispatchQueue.main.async() {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtaViewController") as! OtaViewController
                vc.modalPresentationStyle = .fullScreen
                vc.fwVersion = self.helmet.firmwareVersion
                vc.language = "86"
                vc.model = self.helmet.style
                vc.delegate = self
                //vc.style = self.helmet.style
                //vc.language = self.helmet.product.language
                //vc.firmwareVersion = self.helmet.firmwareVersion
                //vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            DispatchQueue.main.async() {
                let alert = UIAlertController(title: nil, message: NSLocalizedString("ota_battery_warning", comment: ""), preferredStyle: .alert)
                let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("confirm")})
                alert.addAction(confirm)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func format() {
        self.showIndicator(value: true, type: 1)
        HelmetWiFiTool.pingHelmet { result in
            self.showIndicator(value: false, type: 1)
            if result {
                self.showIndicator(value: true, type: 0)
                self.formatHelmetAlert()
            } else {
                self.connectHelmetWiFiAlert()
            }
        }
    }
    
    func onOtaDisconnectHelmet() {
        self.delegate?.onUserDisconnectHelmet()
    }
    
    func onUpdateCompleted() {
        
        self.delegate?.onUserDisconnectHelmet()
    }
    
    func updateBatteryLevel(level: Int) {
        self.batteryRing.maxValue = 100.0
        self.batteryRing.minValue = 0.0
        let v = JCandy.getBatteryLevel(type: self.helmet.style, level: level)
        self.helmet.batteryLevel = v
        self.batteryRing.value = CGFloat( self.helmet.batteryLevel)
        let text = Int(CGFloat(self.helmet.batteryLevel))
        self.batteryLabel.text = text.description
        if self.batteryRing.isHidden {
            self.batteryRing.isHidden = false
            self.batteryLabel.isHidden = false
        }
    }
    
    func setTailLightResult(_ value: Bool) {
        self.showIndicator(value: false, type: 0)
        if (!value) {
            self.setFunctionFailedAlert()
            let value = self.helmet.tailLight
            self.helmet.tailLight = !value!
            self.settingTableView.reloadData()
        }
    }
    
    func setVideoTimestampResult(_ value: Bool) {
        self.showIndicator(value: false, type: 0)
        if (!value) {
            self.setFunctionFailedAlert()
            let value = self.helmet.videoTimestamp
            self.helmet.videoTimestamp = !value!
            self.settingTableView.reloadData()
        } else {
            snackBar(message: NSLocalizedString("helmet_reboot_apply", comment: ""))
        }
    }
    
    func setVideoResolutionResult(_ value: Bool) {
        self.showIndicator(value: false, type: 0)
        if (value) {
            self.helmet.videoResolution = self.tempValue
            snackBar(message: NSLocalizedString("helmet_reboot_apply", comment: ""))
        } else {
            self.setFunctionFailedAlert()
        }
    }
    
    func setVideoRecordingTimeResult(_ value: Bool) {
        self.showIndicator(value: false, type: 0)
        if (value) {
            self.helmet.videoRecordingTime = self.tempValue
            snackBar(message: NSLocalizedString("helmet_reboot_apply", comment: ""))
        } else {
            self.setFunctionFailedAlert()
        }
    }
    
    func setBatteryEfficiencyResult(_ value: Bool) {
        self.showIndicator(value: false, type: 0)
        if (value) {
            self.helmet.batteryEfficiency = self.tempValue
            snackBar(message: NSLocalizedString("helmet_reboot_apply", comment: ""))
        } else {
            self.setFunctionFailedAlert()
        }
    }
    
    func setResetDefaultResult(_ value: Bool) {
        self.showIndicator(value: false, type: 0)
        if (value) {
            let alert = UIAlertController(title: NSLocalizedString("helmet_reset_default_title", comment: ""), message: NSLocalizedString("helmet_reset_default_content", comment: ""), preferredStyle: .alert)
            alert.overrideUserInterfaceStyle = .dark
            let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in self.delegate?.onUserDisconnectHelmet()})
            alert.addAction(confirm)
            self.present(alert, animated: true, completion: nil)
        } else {
            self.setFunctionFailedAlert()
        }
    }
    
    private func formatHelmetAlert() {
        let alert = UIAlertController(
            title: NSLocalizedString("helmet_format_title", comment: ""),
            message: NSLocalizedString("helmet_format_message", comment: ""),
            preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: {(alert: UIAlertAction!) in
            self.formatHelmetStorage()
        })
        alert.addAction(confirm)
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func formatHelmetStorage() {
        if ICatchManager.init().isICatchConnected() {
            if !ICatchManager.init().formatHelmet() {
                self.setFunctionFailedAlert()
            } else {
                snackBar(message: NSLocalizedString("helmet_format_success", comment: ""))
            }
            self.showIndicator(value: false, type: 0)
        } else {
            ICatchManager.init().add(self)
            ICatchManager.init().connectToICatchCamera()
        }
    }
    
    func onActionCompleted(_ action: ICatchAction) {
        switch action {
        case ActionConnectedToCamera:
            formatHelmetStorage();
            ICatchManager.init().remove(self)
            break
        default:
            print("unknown")
            break
        }
    }
    
    func setSyncSystemTimeResult(_ value: Bool) {
        self.showIndicator(value: false, type: 0)
        if (value) {
            snackBar(message: NSLocalizedString("helmet_reboot_apply", comment: ""))
        } else {
            self.setFunctionFailedAlert()
        }
    }
}


extension HelmetSettingViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.settings.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.settings[section].item.count
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let view = view as? UITableViewHeaderFooterView {
            view.textLabel?.textColor = UIColor(named: "colorPrimary")
            
            if #available(iOS 14.0, *) {
                view.backgroundConfiguration?.backgroundColor = UIColor.clear
            } else {
                // Fallback on earlier versions
                view.contentView.backgroundColor = UIColor.clear
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.helmet.style == JCandy.HELMET_AT5 ||
            self.helmet.style ==  JCandy.HELMET_VINTAGE {
            return NSLocalizedString("helmet_setting_information_title", comment: "")
        }
        if section == 0 {
            return NSLocalizedString("helmet_setting_control_title", comment: "")
        } else if section == 1 {
            return NSLocalizedString("helmet_setting_need_wifi_title", comment: "")
        } else {
           return NSLocalizedString("helmet_setting_information_title", comment: "")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: String = self.settings[indexPath.section].item[indexPath.item]
        
        if item.elementsEqual(SettingBox.FUNC_TAIL_LIGHT) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchViewCell", for: indexPath) as! SwitchViewCell
            cell.iconImageView.image = UIImage(named: "tailLight")
            cell.titleLabel.text = NSLocalizedString("helmet_setting_tail_light", comment: "")
            cell.enableSwitch.isOn = self.helmet.tailLight
            cell.enableSwitch.tag = 1001
            cell.enableSwitch.addTarget(self, action: #selector(self.switchTailLight), for: .valueChanged)
            return cell
        } else if item.elementsEqual(SettingBox.FUNC_VIDEO_TIMESTAMP) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchViewCell", for: indexPath) as! SwitchViewCell
            cell.iconImageView.image = UIImage(named: "timestamp")
            cell.titleLabel.text = NSLocalizedString("helmet_setting_video_timestamp", comment: "")
            cell.enableSwitch.isOn = self.helmet.videoTimestamp
            cell.enableSwitch.tag = 1002
            cell.enableSwitch.addTarget(self, action: #selector(self.switchTimestamp), for: .valueChanged)
            return cell
        } else if item.elementsEqual(SettingBox.FUNC_VIDEO_RESOLUTION) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_video_resolution", comment: "")
            cell.iconImageView.image = UIImage(named: "resolution")
            return cell
        } else if item.elementsEqual(SettingBox.FUNC_RECORDING_TIME) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_recording_time", comment: "")
            cell.iconImageView.image = UIImage(named: "recordingTime")
            return cell
        } else if item.elementsEqual(SettingBox.FUNC_BATTERY_EFFICIENCY) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_battery_efficiency", comment: "")
            cell.iconImageView.image = UIImage(named: "batteryEff")
            return cell
        } else if item.elementsEqual(SettingBox.FUNC_SYNC_SYSTEM_TIME) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_sync_system_time", comment: "")
            cell.iconImageView.image = UIImage(named: "syncSystem")
            return cell
        } else if item.elementsEqual(SettingBox.FUNC_RESET_DEFAULT) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_reset_default", comment: "")
            cell.iconImageView.image = UIImage(named: "reset")
            return cell
        } else if item.elementsEqual(SettingBox.FUNC_CAMERA_PREVIEW) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_camera_preview", comment: "")
            cell.iconImageView.image = UIImage(named: "preview")
            return cell
         } else if item.elementsEqual(SettingBox.FUNC_HELMET_VIDEO) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_helmet_video", comment: "")
            cell.iconImageView.image = UIImage(named: "viewVideo")
            return cell
         } else if item.elementsEqual(SettingBox.FUNC_BOOKMARK_VIDEO) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_helmet_bookmark_video", comment: "")
            cell.iconImageView.image = UIImage(named: "bookmark")
            return cell
         } else if item.elementsEqual(SettingBox.FUNC_FORMAT) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_format", comment: "")
            cell.iconImageView.image = UIImage(named: "format")
            return cell
         } else if item.elementsEqual(SettingBox.FUNC_FW_UPDATE) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FunctionViewCell", for: indexPath) as! FunctionViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_fw_update", comment: "")
            cell.iconImageView.image = UIImage(named: "update")
            return cell
         } else if item.elementsEqual(SettingBox.FUNC_SERIAL_NUMBER) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InformationViewCell", for: indexPath) as! InformationViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_serial_number", comment: "")
            cell.infoLabel.text = self.helmet.serialNumber
            cell.iconImageView.image = UIImage(named: "serialNumber")
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InformationViewCell", for: indexPath) as! InformationViewCell
            cell.titleLabel.text = NSLocalizedString("helmet_setting_firmware_version", comment: "")
            cell.infoLabel.text = self.helmet.firmwareVersion
            cell.iconImageView.image = UIImage(named: "fwVersion")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item: String = self.settings[indexPath.section].item[indexPath.item]
        print("didSelectRowAt: \(item)")
        switch item {
        case SettingBox.FUNC_VIDEO_RESOLUTION:
            openVideoResolution()
            break
        case SettingBox.FUNC_RECORDING_TIME:
            openRecordingTime()
            break
        case SettingBox.FUNC_BATTERY_EFFICIENCY:
            openBatteryEfficiency()
            break
        case SettingBox.FUNC_SYNC_SYSTEM_TIME:
            self.showIndicator(value: true, type: 0)
            self.delegate?.setSystemTime()
            break
        case SettingBox.FUNC_RESET_DEFAULT:
            self.showIndicator(value: true, type: 0)
            self.delegate?.resetDefault()
            break
        case SettingBox.FUNC_CAMERA_PREVIEW:
            openPreview()
            break
        case SettingBox.FUNC_HELMET_VIDEO:
            openHelmetVideo(index: 0)
            break
        case SettingBox.FUNC_BOOKMARK_VIDEO:
            openHelmetVideo(index: 1)
            break
        case SettingBox.FUNC_FORMAT:
            format()
            break
        case SettingBox.FUNC_FW_UPDATE:
            openOta()
            break
        default:
            print("unknown")
            break
        }
    }
}
