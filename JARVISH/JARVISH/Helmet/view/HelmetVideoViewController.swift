//
//  HelmetVideoViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/18.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import JGProgressHUD

class HelmetVideoViewController: UIViewController, ICatchVideoDelegate,  VideoAlertViewDelegate {
    @IBOutlet weak var topBar: UINavigationBar!
    
   
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var numberTitleLable: UILabel!
    
    @IBOutlet weak var usedView: UIView!
    @IBOutlet weak var usedLabel: UILabel!
    @IBOutlet weak var usedTitleLabel: UILabel!
    
    @IBOutlet weak var freeView: UIView!
    @IBOutlet weak var freeLabel: UILabel!
    @IBOutlet weak var freeTitleLabel: UILabel!
    
    @IBOutlet weak var videoCollevtion: UICollectionView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    var index: Int?
    var jhud: JGProgressHUD?
    var downloadHud: JGProgressHUD?
    var deleteHud: JGProgressHUD?
    var iCatchManager: ICatchManager?
    var videoSections: [VideoSection] = []
    var indexPath: IndexPath?
    var numberOfVideos = 0
    //var
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        numberView.isHidden = true
        numberTitleLable.text = NSLocalizedString("helmet_number_of_videos", comment: "")
        usedView.isHidden = true
        usedTitleLabel.text = NSLocalizedString("helmet_total_videos_size", comment: "")
        freeView.isHidden = true
        freeTitleLabel.text = NSLocalizedString("helmet_free_space", comment: "")
        
        self.iCatchManager = ICatchManager.shared()!
        self.iCatchManager?.add(self)
        if (iCatchManager?.isICatchConnected())! {
            print("free: xxxx")
            self.iCatchManager?.getICatchMedia()
        } else {
            print("free: xzzzz")
            self.iCatchManager?.connectToICatchCamera()
        }
        self.videoCollevtion.dataSource = self
        self.videoCollevtion.delegate = self
        if index == 0 {
            topBar.topItem?.title = NSLocalizedString("helmet_video_title", comment: "")
        } else {
            topBar.topItem?.title = NSLocalizedString("helmet_bookmark_video_title", comment: "")
        }
        jhud = JGProgressHUD(style: .light)
        jhud!.textLabel.text = NSLocalizedString("action_loading", comment: "")
        jhud!.show(in: self.view)
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            for fileURL in fileURLs {
                print("xxx fileURL \(fileURL)")
                if fileURL.pathExtension == "MOV" {
                    try FileManager.default.removeItem(at: fileURL)
                }
            }
        } catch  { print(error) }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func clickClose(_ sender: UIBarButtonItem) {
        jhud!.dismiss()
        self.iCatchManager?.remove(self)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func onReceivedVideoList(_ list: NSMutableArray!) {
        self.self.videoSections.removeAll()
        list.forEach { video in
            let f: ICatchFileModel = video as! ICatchFileModel
            if index == 0 && f.name.contains("M.M") {
                return
            }
            if index == 1 && !f.name.contains("M.M") {
                return
            }
            var file = HelmetVideo.init()
            if f.thumbnail != nil {
                file.thumbnail = f.thumbnail
            }
            file.name = f.name
            file.date = f.date
            if f.day != nil {
                file.day = f.day
            }
            file.fileSize = Int(f.fileSize)
            file.type = 1
            file.fileSource = 1
            if f.fileID != nil {
                file.fileID = f.fileID
            }
            file.corrected = f.corrected
            if f.status != nil {
                file.status = f.status! as String
            }
            self.numberOfVideos+=1
            var exist = false
            for section in self.videoSections.enumerated() {
                let header = String(file.date.prefix(8))
                if section.element.header.elementsEqual(header) {
                    self.videoSections[section.offset].videos.append(file)
                    exist = true
                    break
                }
            }
            if !exist {
                let h = String(file.date.prefix(8))
                var videos: [HelmetVideo] = []
                videos.append(file)
                let section = VideoSection.init(videos: videos, header: h)
                self.videoSections.append(section)
            }
        }
        self.displayNumberOfVideos()
        self.videoCollevtion.reloadData()
        jhud!.dismiss()
    }
    
    
    private func displayNumberOfVideos() {
        /*if self.numberOfVideos == 0 {
            self.titleLabel.text = NSLocalizedString("helmet_no_video", comment: "")
        } else if self.numberOfVideos == 1 {
            self.titleLabel.text = NSLocalizedString("helmet_single_video", comment: "")
        } else {
            self.titleLabel.text = String(format: NSLocalizedString("helmet_multiple_video", comment: ""), self.numberOfVideos)
        }
        titleLabel.isHidden = false*/
        numberLabel.text = String(self.numberOfVideos)
        var size = 0;
        for section in self.videoSections {
            for video in section.videos {
                size = size + video.fileSize
            }
        }
        usedLabel.text = getVideoSizeStringFormat(bytes: size)
        let free = self.iCatchManager?.getFreeSpaceInImages()
        let x = Int(free ?? 0)
        freeLabel.text = getVideoSizeStringFormat(bytes: (x * 1024 * 1024))
        if numberView.isHidden {
            numberView.isHidden = false
            usedView.isHidden = false
            freeView.isHidden = false
        }
    }
    
    private func getVideoSizeStringFormat(bytes: Int) -> String {
        if bytes == 0 {
            return "0"
        }
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useKB, .useMB, .useGB]
        bcf.countStyle = .memory
        return bcf.string(fromByteCount: Int64(bytes))
    }
    
    private func iCatchInitFailureAlert() {
        let alert = UIAlertController(
            title: NSLocalizedString("helmet_icatch_connection_fail_title", comment: ""),
            message: NSLocalizedString("helmet_icatch_connection_fail_content", comment: ""),
            preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func onActionCompleted(_ action: ICatchAction) {
        switch action {
        case ActionConnectedToCamera:
            self.iCatchManager?.getICatchMedia()
            break
        case ActionCameraDisconnected:
            self.iCatchManager?.remove(self)
            self.iCatchInitFailureAlert()
            break
        default:
            print("unknown")
            break
        }
    }
    
    
    func onDownloadProgress(_ progress: Float) {
        if progress > 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(40)) {
             self.incrementHUD(self.jhud!, progress: progress)
            }
        }
    }
    
    
    func onDownloadCompleted() {
        print("xxx onDownload completed")
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            UIView.animate(withDuration: 0.1, animations: {
                self.downloadHud!.textLabel.text = "Success"
                self.downloadHud!.detailTextLabel.text = nil
                self.downloadHud!.indicatorView = JGProgressHUDSuccessIndicatorView()
            })
            self.downloadHud!.dismiss(afterDelay: 1.0)
        }
    }
    
    
    func onDownloadFailed() {
        print("xxx onDownload failed")
    }
    
    
    func onDeleteResult(_ result: Bool, videoName name: String!) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            UIView.animate(withDuration: 0.1, animations: {
                self.deleteHud!.detailTextLabel.text = nil
                if (result) {
                    self.deleteHud!.textLabel.text = "Success"
                    self.deleteHud!.indicatorView = JGProgressHUDSuccessIndicatorView()
                    self.videoSections[self.indexPath!.section].videos.remove(at: self.indexPath!.item)
                    self.numberOfVideos-=1
                    self.displayNumberOfVideos()
                    self.videoCollevtion.reloadData()
                } else {
                    self.deleteHud!.textLabel.text = "failure"
                    self.deleteHud!.indicatorView = JGProgressHUDErrorIndicatorView()
                }
            })
            self.deleteHud!.dismiss(afterDelay: 1.0)
        }
    }
   
    
    func openVideoAlertView(video: HelmetVideo) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let alert = storyboard.instantiateViewController(withIdentifier: "VideoAlertViewController") as! VideoAlertViewController
        alert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        alert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        alert.video = video
        alert.delegate = self
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func downloadVideo(videoName: String) {
        print("xxx download video: \(videoName)")
        showLoadingHUD()
        self.iCatchManager?.downloadVideo(videoName)
    }
    
    func deleteVideo(videoName: String) {
        print("xxx delete video: \(videoName)")
        self.deleteHud = JGProgressHUD(style: .dark)
        self.deleteHud!.textLabel.text = "Deleting"
        self.deleteHud!.show(in: self.view)
        self.iCatchManager?.deleteVideo(videoName)
    }
    
    func playVideo(videoName: String) {
        print("xxx play video: \(videoName)")
        DispatchQueue.main.async() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlayVideoViewController") as! PlayVideoViewController
            vc.video = self.videoSections[self.indexPath!.section].videos[self.indexPath!.item]
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    func showLoadingHUD() {
        self.downloadHud = JGProgressHUD(style: .light)
        self.downloadHud!.vibrancyEnabled = true
        if arc4random_uniform(2) == 0 {
            self.downloadHud!.indicatorView = JGProgressHUDPieIndicatorView()
        }
        else {
            self.downloadHud!.indicatorView = JGProgressHUDRingIndicatorView()
        }
        self.downloadHud!.detailTextLabel.text = "0%"
        self.downloadHud!.textLabel.text = "Downloading"
        self.downloadHud!.show(in: self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10)) {
            self.incrementHUD(self.downloadHud!, progress: 0)
        }
    }
    
    
    func incrementHUD(_ hud: JGProgressHUD, progress: Float) {
        self.downloadHud!.progress = progress
        let p = String(format: "%.f", progress * 100)
        self.downloadHud!.detailTextLabel.text = "\(p)%"
    }
}


/*extension HelmetVideoViewController: ICatchDownloadDelegate, VideoAlertViewDelegate {
    func onProgress(_ progress: Float) {
        
    }
    
    func downloadVideo(videoName: String) {
        
    }
    
    func deleteVideo(videoName: String) {
        
    }
    
    func playVideoname(videoName: String) {
        
    }
}*/


extension HelmetVideoViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        //print("xxx numberOfSections: \(self.videoSections.count)")
        return self.videoSections.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //print("xxx \(section) numberOfItemsInSection: \(self.videoSections[section].videos.count)")
        return self.videoSections[section].videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoViewCell", for: indexPath) as! VideoViewCell
        cell.image = self.videoSections[indexPath.section].videos[indexPath.item].thumbnail
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "VideoReusableView", for: indexPath) as! VideoReusableView
        let title = Utility.getVideoDateFormat(dateString: self.videoSections[indexPath.section].header)
        view.title = title
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let cellSize = (width - 8) / 3
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let video = self.videoSections[indexPath.section].videos[indexPath.item]
        self.indexPath = indexPath
        self.openVideoAlertView(video: video)
    }
    

}
