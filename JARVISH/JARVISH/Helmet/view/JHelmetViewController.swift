//
//  JHelmetViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/1/5.
//  Copyright © 2021 jarvish. All rights reserved.
//

import UIKit

class JHelmetViewController: UIViewController {
    @IBOutlet weak var helmetAddView: UIView!
    @IBOutlet weak var helmetChooseView: UIView!
    @IBOutlet weak var helmetConnectView: UIView!
    @IBOutlet weak var helmetSettingView: UIView!
    
    var helmetAddViewController: HelmetAddViewController?
    var helmetChooseViewController: HelmetChooseViewController?
    var helmetConnectViewController: HelmetConnectViewController?
    var helmetSettingViewController: HelmetSettingViewController?
    
    private var helmetBleManager: HelmetBleManager!
    
    private var helmetStyle: HelmetStyle!
    
    private var isConnected: Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let data = UserDefaults.standard.value(forKey: "HelmetStyle") as? Data,
            let style = try? JSONDecoder().decode(HelmetStyle.self, from: data) {
            print("helmet style: \(style)")
            helmetStyle = style
            helmetConnectViewController?.setConnectView(style: helmetStyle)
            helmetAddView.isHidden = true
            helmetChooseView.isHidden = true
            helmetConnectView.isHidden = false
            helmetSettingView.isHidden = true
        } else {
            helmetAddView.isHidden = false
            helmetChooseView.isHidden = true
            helmetConnectView.isHidden = true
            helmetSettingView.isHidden = true
        }

        helmetBleManager = HelmetBleManager()
        helmetBleManager.delgate = self
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "HelmetAddSegue") {
            helmetAddViewController = segue.destination as? HelmetAddViewController
            helmetAddViewController?.delegate = self
        }
        
        if (segue.identifier == "HelmetChooseSegue") {
            helmetChooseViewController = segue.destination as? HelmetChooseViewController
            helmetChooseViewController?.delegate = self
        }
        
        if (segue.identifier == "HelmetConnectSegue") {
            helmetConnectViewController = segue.destination as? HelmetConnectViewController
            helmetConnectViewController?.delegate = self
        }
        
        if (segue.identifier == "HelmetSettingSegue") {
            helmetSettingViewController = segue.destination as? HelmetSettingViewController
            helmetSettingViewController?.delegate = self
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setConnectionView() {
        helmetConnectViewController?.setConnectView(style: helmetStyle)
        helmetAddView.isHidden = true
        helmetChooseView.isHidden = true
        helmetConnectView.isHidden = false
        helmetSettingView.isHidden = true
        helmetSettingViewController?.removeSettingView()
    }

    func setSettingView() {
        helmetConnectViewController?.clearConnectView()
        helmetAddView.isHidden = true
        helmetChooseView.isHidden = true
        helmetConnectView.isHidden = true
        helmetSettingView.isHidden = false
    }
}


extension JHelmetViewController: HelmetBleManagerDelegate {
    func didConnectedPeripheral(deviceUuid: String) {
        
    }
    
    func didUpdateState() {
        
    }
    
    func didConnected() {
        helmetConnectViewController?.onFetchSettings()
    }
    
    func onConnectionFailure() {
        helmetConnectViewController?.onConnectionFailure()
    }
    
    func didDisconnected() {
        if isConnected {
            setConnectionView()
            isConnected = false
        }
    }
    
    func discoverResult(name: String) {
        helmetConnectViewController?.feedDiscoverResult(name)
    }
    
    func didFetchHelmet(helmet: Helmet) {
        print("didFetchHelmet: \(helmet)")
        var h = helmet
        if h.style.isEmpty || h.style.elementsEqual(helmetStyle.index!) {
            h.name = helmetStyle.name
            h.style = helmetStyle.index
            h.image = helmetStyle.image
        } else {
            helmetStyle.name = h.name
            helmetStyle.index = h.style
            helmetStyle.image = h.image
            if let data = try? JSONEncoder().encode(self.helmetStyle) {
                UserDefaults.standard.set(data, forKey: "HelmetStyle")
            }
        }
        helmetSettingViewController?.feedHelmet(helmet: h)
        setSettingView()
        isConnected = true
    }
    
    func didTailLight(value: Bool) {

    }
    
    func didTailLightAck(ack: Bool) {
        print("set tail light ack: \(ack)")
        helmetSettingViewController?.setTailLightResult(ack)
    }
    
    func didTimestamp(value: Bool) {
        
    }
    
    func didTimestampAck(ack: Bool) {
        print("switch timestamp ack: \(ack)")
        helmetSettingViewController?.setVideoTimestampResult(ack)
    }
    
    func didVideoRecordingTime(value: Int) {
        
    }
    
    func didVideoRecordingTimeAck(ack: Bool) {
        helmetSettingViewController?.setVideoRecordingTimeResult(ack)
    }
    
    func didVideoResolution(value: Int) {
        
    }
    
    func didVideoResolutionAck(ack: Bool) {
        helmetSettingViewController?.setVideoResolutionResult(ack)
    }
    
    func didBatteryEfficiency(value: Int) {
        
    }
    
    func didBatteryEfficiencyAck(ack: Bool) {
        helmetSettingViewController?.setBatteryEfficiencyResult(ack)
    }
    
    func didResetDefaultAck(ack: Bool) {
        helmetSettingViewController?.setResetDefaultResult(ack)
    }
    
    func didSetSystemDateTimeAck(ack: Bool) {
        helmetSettingViewController?.setSyncSystemTimeResult(ack)
    }
    
    func didBatteryLevel(level: Int) {
        helmetSettingViewController?.updateBatteryLevel(level: level)
    }
    
    func didFirmwareVersion(version: String) {
        
    }
    
    
}


extension JHelmetViewController: HelmetAddViewControllerDelegate, HelmetChooseViewControllerDelegate, HelmetConnectViewControllerDelegate, HelmetSettingViewControllerDelegate {
    
    func setTailLight(value: Bool) {
        print("set tail light: \(value)")
        helmetBleManager.setTailLight(value: value)
    }
    
    func setTimestamp(value: Bool) {
        print("switch timestamp: \(value)")
        helmetBleManager.setTimestamp(value: value)
    }
    
    func setBatteryEfficiency(value: Int) {
        helmetBleManager.setBatteryEfficiency(value: value)
    }
    
    func setVideoResolution(value: Int) {
        helmetBleManager.setVideoResolution(value: value)
    }
    
    func setVideoRcordingTime(value: Int) {
        helmetBleManager.setVideoRcordingTime(value: value)
    }
    
    func resetDefault() {
        helmetBleManager.resetDefault()
    }
    
    func setSystemTime() {
        helmetBleManager.setSystemDateTime()
    }
    
    func onUserDisconnectHelmet() {
        setConnectionView()
        helmetBleManager.disconnect()
        isConnected = false
    }
    
    
    func onSelect() {
        helmetAddView.isHidden = true
        helmetChooseView.isHidden = false
        helmetConnectView.isHidden = true
        helmetSettingView.isHidden = true
    }
    
    func onSelected(style: HelmetStyle) {
        self.helmetStyle = style
        helmetConnectViewController?.setConnectView(style: self.helmetStyle)
        helmetAddView.isHidden = true
        helmetChooseView.isHidden = true
        helmetConnectView.isHidden = false
        helmetSettingView.isHidden = true
        if let data = try? JSONEncoder().encode(self.helmetStyle) {
            UserDefaults.standard.set(data, forKey: "HelmetStyle")
        }
    }
    
    func onClose() {
        helmetAddView.isHidden = false
        helmetChooseView.isHidden = true
        helmetConnectView.isHidden = true
        helmetSettingView.isHidden = true
    }
    
    func onFindHelmet() {
        helmetBleManager.startDiscovery()
    }
    
    func onStopFindHelmet() {
        helmetBleManager.stopDiscovery()
    }
    
    func onConnectHelmet(name: String) {
        helmetBleManager.connect(name: name)
    }
    
    func onAddHelmet() {
        UserDefaults.standard.removeObject(forKey: "HelmetStyle")
        helmetAddView.isHidden = false
        helmetChooseView.isHidden = true
        helmetConnectView.isHidden = true
        helmetSettingView.isHidden = true
        helmetConnectViewController?.clearConnectView()
    }
    
}
