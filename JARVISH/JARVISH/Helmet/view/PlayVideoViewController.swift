//
//  PlayVideoViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/26.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import AVFoundation

class PlayVideoViewController: UIViewController, ICatchPlayVideoDelegate {
    
    @IBOutlet weak var closeImageView: UIImageView!
    @IBOutlet weak var videoNameLabel: UILabel!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var playingTimeLabel: UILabel!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var playSeek: UISlider!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playImageView: UIImageView!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    
    var iCatchManager: ICatchManager?
    var video: HelmetVideo?
    var controlTimer: Timer?
    
    var isPaused = false
    var isPlaying = false
    var isSeeking = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapClose = UITapGestureRecognizer(target: self, action: #selector(self.clickClose))
        self.closeImageView.isUserInteractionEnabled = true
        self.closeImageView.addGestureRecognizer(tapClose)
        
        let tapVideo = UITapGestureRecognizer(target: self, action: #selector(self.displayControlItem))
        self.videoImageView.isUserInteractionEnabled = true
        self.videoImageView.addGestureRecognizer(tapVideo)
        
        let tapPlay = UITapGestureRecognizer(target: self, action: #selector(self.touchPlayImageView))
        self.playImageView.isUserInteractionEnabled = true
        self.playImageView.addGestureRecognizer(tapPlay)
        
        videoNameLabel.text = video?.name
        iCatchManager = ICatchManager.shared()!
        playSeek.isContinuous = false
        playSeek.value = 0
        playSeek.minimumValue = 0
        playSeek.maximumValue = 100
        playingTimeLabel.text = "00:00:00"
        totalTimeLabel.text = "00:00:00"
        showIndicator(value: true)
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .moviePlayback, options: [])
        } catch {
            print("Failed to set audio session category.")
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appEnterForeground),
            name: UIScene.willEnterForegroundNotification , object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(appEnterBackground),
            name:UIScene.willDeactivateNotification , object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(helmetDisconnect),
            name:NSNotification.Name(rawValue: "HELME_DISCONNECT") , object: nil)
        
        self.iCatchManager?.add(self)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        print("[Play]: viewDidAppear")
        self.iCatchManager?.add(self)
        if (iCatchManager?.isICatchConnected())! {
            self.iCatchManager?.startPlayVideo(self.video?.name)
        } else {
            self.iCatchManager?.connectToICatchCamera()
        }
        self.setControlTimer()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        print("[Play]: viewWillDisappear")
        self.iCatchManager?.remove(self)
        self.iCatchManager?.stopPlayVideo()
        if self.controlTimer != nil {
            self.controlTimer?.invalidate()
            self.controlTimer = nil
        }
    }
    
    @objc private func appEnterForeground() {
        print("[Play]: play video Will Enter Foreground")
        HelmetWiFiTool.pingHelmet { result in
            if result {
                self.displayControlItem()
            } else {
                self.iCatchManager?.stopPlayVideo()
                self.iCatchInitFailureAlert()
            }
        }
        
    }
    
    @objc private func appEnterBackground() {
        print("[Play]: play video Will Enter Background")
        pausePlay()
    }
    
    @objc private func helmetDisconnect() {
        print("[Play]: helmet disconnect")
        DispatchQueue.main.async {
            self.iCatchManager?.stopPlayVideo()
            self.iCatchInitFailureAlert()
        }
    }
    
    private func setControlTimer() {
        if self.controlTimer != nil {
            self.controlTimer?.invalidate()
            self.controlTimer = nil
        }
        self.controlTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(hideControlItem), userInfo: nil, repeats: false)
    }

    
    @objc private func displayControlItem() {
        if !self.isPlaying {
            return
        }
        closeImageView.isHidden = false
        videoNameLabel.isHidden = false
        playingTimeLabel.isHidden = false
        totalTimeLabel.isHidden = false
        playImageView.isHidden = false
        playSeek.isHidden = false
        self.setControlTimer()
    }
    
    
    @objc private func hideControlItem() {
        closeImageView.isHidden = true
        videoNameLabel.isHidden = true
        playingTimeLabel.isHidden = true
        totalTimeLabel.isHidden = true
        playImageView.isHidden = true
        playSeek.isHidden = true
    }
    
    private func resumePlay() {
        self.showIndicator(value: true)
        self.iCatchManager?.resumePlayVideo()
        self.playImageView.image = UIImage(systemName: "pause.fill")
        self.isPaused = false
    }
    
    private func pausePlay() {
        self.iCatchManager?.pausePlayVideo()
        self.playImageView.image = UIImage(systemName: "play.fill")
        self.isPaused = true
    }
    
    
    @objc private func touchPlayImageView() {
        if self.isPaused {
            resumePlay()
        } else {
            pausePlay()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func clickClose() {
        self.iCatchManager?.stopPlayVideo()
        self.isPlaying = false
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func touchSlider(_ sender: UISlider) {
        let value = sender.value
        print("[Play]: seek Video to \(value)")
        playingTimeLabel.text = translateSecondsToString(seconds: Double(value))
        self.isSeeking = true
        showIndicator(value: true)
        playSeek.isEnabled = false
        playImageView.isHidden = true
        self.iCatchManager?.seekVideo(value)
    }
    
    
    func onReceivedImage(_ image: UIImage!) {
        showIndicator(value: false)
        self.isPlaying = true
        self.videoImageView.image = image
    }
    
    func onVideoTotalSeconds(_ seconds: Double) {
        totalTimeLabel.text = translateSecondsToString(seconds: seconds)
        playSeek.minimumValue = 0
        playSeek.maximumValue = Float(seconds)
    }
    
    
    func onVideoPlayingSeconds(_ seconds: Double) {
        if !self.isPlaying {
            return
        }
        playingTimeLabel.text = translateSecondsToString(seconds: seconds)
        playSeek.value = Float(seconds)
    }
    
    
    
    func onPauseResult(_ result: Bool) {
        print("[Play]: onPauseResult \(result)")
    }
    
    
    func onResumeResult(_ result: Bool) {
        print("[Play]: onResumeResult \(result)")
        if result {
            self.showIndicator(value: false)
        }
    }
    
    
    func onSeekVideoResult(_ result: Bool) {
        self.isSeeking = false
    }
    
    private func iCatchInitFailureAlert() {
        let alert = UIAlertController(
            title: NSLocalizedString("helmet_icatch_connection_fail_title", comment: ""),
            message: NSLocalizedString("helmet_icatch_connection_fail_content", comment: ""),
            preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    func onActionCompleted(_ action: ICatchAction) {
        print("[Play]: onActionCompleted")
        switch action {
        case ActionConnectedToCamera:
            self.iCatchManager?.startPlayVideo(self.video?.name)
            break
        case ActionCameraDisconnected:
            self.iCatchManager?.stopPlayVideo()
            self.iCatchInitFailureAlert()
            break
        default:
            print("unknown")
            break
        }
    }
    
    
    private func translateSecondsToString(seconds: Double) -> String {
        let tempHour = Int((seconds / 3600));
        let tempMinute = (Int(seconds) / 60 - tempHour * 60);
        let tempSecond = (Int(seconds) - (tempHour * 3600 + tempMinute * 60));
        let hour = String(format:"%02d", tempHour)
        let minute = String(format:"%02d", tempMinute)
        let second = String(format:"%02d", tempSecond)
        return String(format:"%@:%@:%@", hour, minute, second)
    }
    
    
    private func showIndicator(value: Bool) {
        if isSeeking {
            return
        }
        if (value) {
            self.loadingIndicator.isHidden = false
            playSeek.isEnabled = false
            playImageView.isUserInteractionEnabled = false
        } else {
            self.loadingIndicator.isHidden = true
            playSeek.isEnabled = true
            playImageView.isUserInteractionEnabled = true
        }
    }
}
