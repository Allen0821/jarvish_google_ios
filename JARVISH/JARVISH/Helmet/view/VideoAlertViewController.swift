//
//  VideoAlertViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/21.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import JGProgressHUD

protocol VideoAlertViewDelegate {
    func downloadVideo(videoName: String)
    func deleteVideo(videoName: String)
    func playVideo(videoName: String)
}


class VideoAlertViewController: UIViewController {
    @IBOutlet weak var colseImageView: UIImageView!
    @IBOutlet weak var playImageView: UIImageView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var videoNameLabel: UILabel!
    @IBOutlet weak var videoDateLabel: UILabel!
    @IBOutlet weak var videoSizeLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    var delegate: VideoAlertViewDelegate?
    var video: HelmetVideo?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapClose = UITapGestureRecognizer(target: self, action: #selector(self.clickClose))
        self.colseImageView.isUserInteractionEnabled = true
        self.colseImageView.addGestureRecognizer(tapClose)
        
        let tapPlay = UITapGestureRecognizer(target: self, action: #selector(self.clickPlay))
        self.playImageView.isUserInteractionEnabled = true
        self.playImageView.addGestureRecognizer(tapPlay)
        
        self.alertView.layer.cornerRadius = 12;
        self.alertView.layer.masksToBounds = true;
        
        if video != nil {
            thumbnailImageView.image = video!.thumbnail
            videoNameLabel.text = video!.name
            videoDateLabel.text = getVideoDateStringFormat(dateString: (video!.date))
            videoSizeLabel.text = getVideoSizeStringFormat(size: Int(video!.fileSize))
        }
    }
    
    
    private func getVideoDateStringFormat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd'T'HHmmss"
        guard let date = dateFormatter.date(from: dateString) else {
            return ""
        }
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy H:mm:ss"
        return dateFormatter.string(from: date)
    }
    
    
    private func getVideoSizeStringFormat(size: Int) -> String {
        let mb: Double = Double(size/1024/1024)
        return String(format:"%.fMB", mb)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @objc func clickClose() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func clickPlay() {
        if self.delegate != nil {
            self.delegate?.playVideo(videoName: self.video!.name)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func clickDownload(_ sender: UIButton) {
        if self.delegate != nil {
            self.delegate?.downloadVideo(videoName: self.video!.name)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func clickDelete(_ sender: UIButton) {
        if self.delegate != nil {
            self.delegate?.deleteVideo(videoName: self.video!.name)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func onActionCompleted(_ action: ICatchAction) {
        
    }
}
