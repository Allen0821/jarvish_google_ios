//
//  CameraSDKManager.h
//  CameraSDKDemo
//
//  Created by VampireBear on 2016/6/14.
//  Copyright © 2016年 CloudNest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ICameraHandler.h"
#import "ICameraObserver.h"
#import "IValueObserver.h"
#import "SDK.h"
#import "CameraStatus.h"
#import "SensorData.h"
#import "WifiCamCamera.h"
#import "WifiCamControlCenter.h"
#import "WifiCamStaticData.h"
#define WIFI_CONNECT_NOTIFICATION @"wifiConnected"
#define WIFI_DISCONNECT_NOTIFICATION @"wifiDisconnected"
#define DISCONNECT_HELMET_NOTIFICATION @"HelmetDisconnected"
typedef NS_ENUM(NSInteger, CamMode) {
    CamModePhoto,
    CamModeVideo
};

@interface CameraSDKManager : NSObject <ICameraHandler>
+(CameraSDKManager*)sharedManager;

// orig SDK wapper params
@property (nonatomic, strong) WifiCamCamera *camera;
@property (nonatomic, strong) WifiCamControlCenter *ctrl;
@property (nonatomic, strong) WifiCamStaticData *staticData;
// observer
-(void)addCameraObserver:(id<ICameraObserver>)observer;
-(void)removeCameraObserver:(id<ICameraObserver>)observer;
-(void)addValueObserver:(id<IValueObserver>)observer;
-(void)removeValueObserver:(id<IValueObserver>)observer;

// camera operation
-(void)connectToCamera;
-(void)shot;


// camera status
-(void)updateCamMode:(CamMode)mode;
-(CamMode)currentCamMode;
-(BOOL)isRecording;
-(BOOL)isConnected;

-(void)startPreview;
-(void)stopPreview;

// SD card file operate
-(NSArray*)getSDFileListByType:(WCFileType)type;
-(BOOL)downloadFile:(ICatchFile*)file cb:(void (^)(float progress))cb;
-(BOOL)deleteFile:(ICatchFile*)file;

-(void)requestCameraStatus;
-(void)requestSensorData;
-(UIImage*)requestImage:(ICatchFile*)file;
-(BOOL)formatSD;
-(int)getFreeSpaceInImages;

// local file operate
-(NSArray*)getLocalFileListByType:(WCFileType)type;

// other
-(void)changeResolution:(NSString*)resStr;
-(BOOL)setVol:(NSInteger)vol;
-(BOOL)setLanguage:(NSInteger)language;
-(NSInteger)loadVol;
-(NSInteger)loadLanguage;
-(NSString*)loadVer;
-(NSString*)loadDeviceID;
-(void)shutdown;

- (void) updataToSDCard:(NSString *)fwPath;
-(void)updateFWCompleted;
-(void)updateFWPowerOff;
-(void)udpateFWCheck:(int)retValue;
-(void)updateFWCKSUM:(int)retValue;
-(void)updateHelmetDisconnect;
//aaaaa
//-(void)wifiDisconnected:(int)retValue;
//Log run

-(BOOL)WifiCamInit;


- (void) updataBinFileToSDCard:(NSString *)binPath;

- (ICatchEventID) feedBackStatus:(ICatchEventID)status;

@end
