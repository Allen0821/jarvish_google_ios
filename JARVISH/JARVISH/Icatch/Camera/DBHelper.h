//
//  DBHelper.h
//  Camera
//
//  Created by VampireBear on 2016/9/5.
//  Copyright © 2016年 Jarvish. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MediaFile.h"
@interface DBHelper : NSObject
-(id)init;
-(void)addRecord:(MediaFile*)mf;
-(NSArray*)getfileListByType:(WCFileType)type;
-(void)deleteRecord:(MediaFile*)mf;
@end
