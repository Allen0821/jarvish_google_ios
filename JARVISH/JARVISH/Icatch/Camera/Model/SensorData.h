//
//  SensorData.h
//  Camera
//
//  Created by VampireBear on 2016/9/1.
//  Copyright © 2016年 Jarvish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SensorData : NSObject
@property (nonatomic, strong) NSString *rawData;

@property (nonatomic, strong) NSString *time;
@property (nonatomic, assign) float acc_x;
@property (nonatomic, assign) float acc_y;
@property (nonatomic, assign) float acc_z;
@property (nonatomic, assign) float gyro_x;
@property (nonatomic, assign) float gyro_y;
@property (nonatomic, assign) float gyro_z;
@property (nonatomic, assign) float mag_x;
@property (nonatomic, assign) float mag_y;
@property (nonatomic, assign) float mag_z;
@property (nonatomic, assign) float pressure;

-(id)initWithRawData:(NSString*)rawData;
-(NSString*)jsonStr;
@end
