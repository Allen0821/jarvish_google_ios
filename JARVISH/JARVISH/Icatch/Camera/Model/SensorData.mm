//
//  SensorData.m
//  Camera
//
//  Created by VampireBear on 2016/9/1.
//  Copyright © 2016年 Jarvish. All rights reserved.
//

#import "SensorData.h"

@implementation SensorData
-(id)initWithRawData:(NSString*)rawData {
    self = [super init];
    if (self) {
        self.rawData = rawData;
        [self parseRawData];
    }
    
    return self;
}

-(NSString*)jsonStr {
    
    NSDictionary *dict = @{@"time": self.time,
                           @"acc_x": [NSNumber numberWithFloat:self.acc_x],
                           @"acc_y": [NSNumber numberWithFloat:self.acc_y],
                           @"acc_z": [NSNumber numberWithFloat:self.acc_z],
                           @"gyro_x": [NSNumber numberWithFloat:self.gyro_x],
                           @"gyro_y": [NSNumber numberWithFloat:self.gyro_y],
                           @"gyro_z": [NSNumber numberWithFloat:self.gyro_z],
                           @"mag_x": [NSNumber numberWithFloat:self.mag_x],
                           @"mag_y": [NSNumber numberWithFloat:self.mag_y],
                           @"mag_z": [NSNumber numberWithFloat:self.mag_z],
                           @"pressure": [NSNumber numberWithFloat:self.pressure]
                           };
    
 
    return [self jsonStrFromObj:dict];
}

#pragma mark - private
-(void)parseRawData {
    
    NSArray *arr = [self.rawData componentsSeparatedByString:@","];
    
    NSMutableString *timeStr = [NSMutableString stringWithString:[arr objectAtIndex:0]];
    [timeStr insertString:@"/" atIndex:4];
    [timeStr insertString:@"/" atIndex:7];
    [timeStr insertString:@" " atIndex:10];
    [timeStr insertString:@":" atIndex:13];
    [timeStr insertString:@":" atIndex:16];
    
    
    NSString *pressureStr = [NSString stringWithFormat:@"%@.%@", [arr objectAtIndex:10], [arr objectAtIndex:11]];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    self.time = [NSString stringWithString:timeStr];
    
    self.acc_x = [[f numberFromString:[arr objectAtIndex:1]] floatValue];
    self.acc_x = [[f numberFromString:[arr objectAtIndex:1]] floatValue];
    self.acc_y = [[f numberFromString:[arr objectAtIndex:2]] floatValue];
    self.acc_z = [[f numberFromString:[arr objectAtIndex:3]] floatValue];
    self.gyro_x = [[f numberFromString:[arr objectAtIndex:4]] floatValue];
    self.gyro_y = [[f numberFromString:[arr objectAtIndex:5]] floatValue];
    self.gyro_z = [[f numberFromString:[arr objectAtIndex:6]] floatValue];
    self.mag_x = [[f numberFromString:[arr objectAtIndex:7]] floatValue];
    self.mag_y = [[f numberFromString:[arr objectAtIndex:8]] floatValue];
    self.mag_z = [[f numberFromString:[arr objectAtIndex:9]] floatValue];
    self.pressure = [[f numberFromString:pressureStr] floatValue];
    
    /*
     2016/08/24 15:34:59, -315,-132,-981,1680,4130,1470,46,55,926,1007,87
     時間, 加速計x, 加速計y, 加速計z, 陀螺儀x, 陀螺儀y, 陀螺儀z, 磁力計x, 磁力計y, 磁力計z, 壓力計整數部分, 壓力計小數部分.
     */
}

-(NSString*)jsonStrFromObj:(id)obj {
    NSError *error = nil;
    NSString *result = nil;
    if ([NSJSONSerialization isValidJSONObject:obj]) {
        NSData *strData = [NSJSONSerialization dataWithJSONObject:obj
                                                          options:NSJSONWritingPrettyPrinted
                                                            error:&error];
        if (error) {
            NSLog(@"[ERROR][JSON Parser] %@", error);
        } else {
            result = [[NSString alloc] initWithData:strData encoding:NSUTF8StringEncoding];
        }
    }
    result = [result stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return result;
}
@end
