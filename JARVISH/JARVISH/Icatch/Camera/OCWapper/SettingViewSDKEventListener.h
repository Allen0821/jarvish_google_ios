//
//  SettingViewSDKEventListener.h
//  Camera
//
//  Created by 馬秀燁 on 2017/2/18.
//  Copyright © 2017年 Jarvish. All rights reserved.
//

#ifndef SettingViewSDKEventListener_h
#define SettingViewSDKEventListener_h

#import "CameraSDKManager.h"


class SettingViewSDKEventListener : public ICatchWificamListener {
private:
    CameraSDKManager *controller;
protected:
    void eventNotify(ICatchEvent *icatchEvt);
    SettingViewSDKEventListener(CameraSDKManager *controller);
    void udpateFWCompleted(ICatchEvent *icatchEvt);
    void udpateFWPowerOff(ICatchEvent *icatchEvt);
    void udpateFWCheck(ICatchEvent *icatchEvt);
    void updateFWCKSUM(ICatchEvent *icatchEvt);
    void helmetDisconnect(ICatchEvent *icatchEvt);
    //aaaaa
    //void wifiDisconnected(ICatchEvent *icatchEvt);
};
class HelmetDisconnectListener : public SettingViewSDKEventListener {
private:
    void eventNotify(ICatchEvent *iCatchEvt) {
        NSLog(@"Helmet disconnect Event Received.");
        helmetDisconnect(iCatchEvt);
    }
public:
    HelmetDisconnectListener(CameraSDKManager *controller) : SettingViewSDKEventListener(controller) {}
};

class UpdateFWCompleteListener : public SettingViewSDKEventListener {
private:
    void eventNotify(ICatchEvent *iCatchEvt) {
        NSLog(@"Update FW Completed Event Received.");
        udpateFWCompleted(iCatchEvt);
    }
public:
    UpdateFWCompleteListener(CameraSDKManager *controller) : SettingViewSDKEventListener(controller) {}
};

class UpdateFWCompletePowerOffListener : public SettingViewSDKEventListener {
private:
    void eventNotify(ICatchEvent *iCatchEvt) {
        NSLog(@"Update FW Power Off Event Received.");
        udpateFWPowerOff(iCatchEvt);
    }
public:
    UpdateFWCompletePowerOffListener(CameraSDKManager *controller) : SettingViewSDKEventListener(controller) {}
};

class UpdateFWCheckListener : public SettingViewSDKEventListener {
private:
    void eventNotify(ICatchEvent *iCatchEvt) {
        NSLog(@"Update FW Check Event Received.");
        udpateFWCheck(iCatchEvt);
    }
public:
    UpdateFWCheckListener(CameraSDKManager *controller) : SettingViewSDKEventListener(controller) {}
};
class UpdateFWCKSUMListener : public SettingViewSDKEventListener {
private:
    void eventNotify(ICatchEvent *iCatchEvt) {
        NSLog(@"Update FW Check Event Received.");
        updateFWCKSUM(iCatchEvt);
    }
public:
    UpdateFWCKSUMListener(CameraSDKManager *controller) : SettingViewSDKEventListener(controller) {}
};

//aaaaaaa
/*class WifiDisconnectedListener : public SettingViewSDKEventListener {
private:
    void eventNotify(ICatchEvent *iCatchEvt) {
        NSLog(@"Wifi Disconnected Event Received.");
        wifiDisconnected(iCatchEvt);
    }
public:
    WifiDisconnectedListener(CameraSDKManager *controller) : SettingViewSDKEventListener(controller) {}
};*/

#endif /* SettingViewSDKEventListener_h */
