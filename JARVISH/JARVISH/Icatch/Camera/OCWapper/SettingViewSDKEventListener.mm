//
//  SettingViewSDKEventListener.cpp
//  Camera
//
//  Created by 馬秀燁 on 2017/2/18.
//  Copyright © 2017年 Jarvish. All rights reserved.
//

#include "SettingViewSDKEventListener.h"

void SettingViewSDKEventListener::eventNotify(ICatchEvent *icatchEvt) {}

SettingViewSDKEventListener::SettingViewSDKEventListener(CameraSDKManager *controller) {
    this->controller = controller;
}

void SettingViewSDKEventListener::udpateFWCompleted(ICatchEvent *icatchEvt) {
    [controller updateFWCompleted];
}

void SettingViewSDKEventListener::udpateFWPowerOff(ICatchEvent *icatchEvt) {
    [controller updateFWPowerOff];
}

void SettingViewSDKEventListener::udpateFWCheck(ICatchEvent *icatchEvt) {
    [controller udpateFWCheck:icatchEvt->getIntValue1()];
}

void SettingViewSDKEventListener::updateFWCKSUM(ICatchEvent *icatchEvt) {
    [controller updateFWCKSUM:icatchEvt->getIntValue1()];
}
void SettingViewSDKEventListener::helmetDisconnect(ICatchEvent *icatchEvt){
    [controller updateHelmetDisconnect];
}

//aaaaa
/*void SettingViewSDKEventListener::wifiDisconnected(ICatchEvent *icatchEvt) {
    [controller wifiDisconnected:icatchEvt->getIntValue1()];
}*/

