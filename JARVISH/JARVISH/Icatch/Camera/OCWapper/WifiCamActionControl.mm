//
//  WifiCamActionControl.m
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/14.
//  Copyright © 2020 jarvish. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDK.h"
#import "WifiCamActionControl.h"

@implementation WifiCamActionControl

-(BOOL)startPreview:(ICatchPreviewMode)mode withAudioEnabled:(BOOL)enableAudio
{
    return [[SDK instance] startMediaStream:mode enableAudio:enableAudio];
}


-(BOOL)stopPreview
{
  BOOL retVal = NO;
  retVal = [[SDK instance] stopMediaStream];
  return retVal;
}

-(void)capturePhoto
{
  [[SDK instance] capturePhoto];
}
-(void)triggerCapturePhoto
{
  [[SDK instance] triggerCapturePhoto];
}

- (BOOL)formatSD
{
  return [[SDK instance] formatSD];
}

- (int)getFreeSpaceInImages
{
  return [[SDK instance] getFreeSpaceInImages];
}

- (BOOL)startMovieRecord
{
  return [[SDK instance] startMovieRecord];
}

- (BOOL)stopMovieRecord
{
  return [[SDK instance] stopMovieRecord];
}

- (void)cleanUpDownloadDirectory
{
  [[SDK instance] cleanUpDownloadDirectory];
}

-(BOOL)startTimelapseRecord {
  return [[SDK instance] startTimelapseRecord];
}

-(BOOL)stopTimelapseRecord {
  return [[SDK instance] stopTimelapseRecord];
}

-(BOOL)zoomIn {
  return [[SDK instance] zoomIn];
}

-(BOOL)zoomOut {
  return [[SDK instance] zoomOut];
}
@end
