//
//  ICameraHandler.h
//  CameraSDKDemo
//
//  Created by VampireBear on 2016/5/15.
//  Copyright © 2016年 CloudNest. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum : NSUInteger {
    MovieRecStarted,
    MovieRecStoped,
} MovieRecState;


@protocol ICameraHandler <NSObject>
- (void)updateMovieRecState:(MovieRecState)state;
- (void)stopStillCapture;
- (void)postMovieRecordTime;
- (void)postMovieRecordFileAddedEvent;
@end
