//
//  ICameraObserver.h
//  CameraSDKDemo
//
//  Created by VampireBear on 2016/6/14.
//  Copyright © 2016年 CloudNest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CamAction) {
    CamActionChangeCameraState,
    CamActionStillCaptureDone,
    CamActionConnectedToCamera,
    CamActionCameraDisconnected,
    CamActionCardFull,
    CamActionNoCard,
    CamActionMovieRecordingFailed,
    CamActionPreviewDead,
    CamActionRecordingStart,
    CamActionRecordingStop
};

@protocol ICameraObserver <NSObject>
-(void)onReceivedPreviewImg:(UIImage*)img;
-(void)onActionCompleted:(CamAction)action;
-(void)onMovieRecordElapsedTime:(NSString*)timeStr;
@end
