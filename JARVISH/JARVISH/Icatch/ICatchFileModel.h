//
//  ICatchFileModel.h
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/19.
//  Copyright © 2020 jarvish. All rights reserved.
//

#ifndef ICatchFile_h
#define ICatchFile_h
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ICatchFileModel : NSObject

@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *day;
@property (nonatomic, assign) unsigned long long fileSize;
@property (nonatomic, assign) int type; // image or video
@property (nonatomic, assign) int fileSource;
@property (nonatomic, strong) NSString *fileID;
@property (nonatomic, assign) BOOL Corrected;
@property (nonatomic, assign) NSString *status;

//-(id)initIMediaFile:(MediaFile*)file;

@end

#endif /* ICatchFile_h */
