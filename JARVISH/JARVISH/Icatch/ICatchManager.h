//
//  ICatchManager.h
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/14.
//  Copyright © 2020 jarvish. All rights reserved.
//

#ifndef ICatchManager_h
#define ICatchManager_h
#import <Foundation/Foundation.h>
#import "ICameraObserver.h"
#import "ICatchFileModel.h"

typedef enum: NSInteger {
    ActionChangeCameraState,
    ActionStillCaptureDone,
    ActionConnectedToCamera,
    ActionCameraDisconnected,
    ActionCardFull,
    ActionNoCard,
    ActionMovieRecordingFailed,
    ActionPreviewDead,
    ActionRecordingStart,
    ActionRecordingStop
}ICatchAction;


@class ICatchManager;
@protocol ICatchPreviewDelegate <NSObject>
-(void)onReceivedPreviewImage: (UIImage*)image;
-(void)onActionCompleted:(ICatchAction)action;
-(void)onMovieRecordElapsedTime:(NSString*)timeStr;
@end

@protocol ICatchVideoDelegate <NSObject>
-(void)onDownloadProgress: (float)progress;
-(void)onDownloadCompleted;
-(void)onDownloadFailed;
-(void)onDeleteResult: (BOOL)result videoName: (NSString*)name;
-(void)onReceivedVideoList: (NSMutableArray*)videos;
-(void)onActionCompleted:(ICatchAction)action;
@end


@protocol ICatchPlayVideoDelegate <NSObject>
-(void)onReceivedImage: (UIImage*)image;
-(void)onVideoTotalSeconds: (double)seconds;
-(void)onVideoPlayingSeconds: (double)seconds;
-(void)onSeekVideoResult: (BOOL)result;
-(void)onPauseResult: (BOOL)result;
-(void)onResumeResult: (BOOL)result;
-(void)onActionCompleted:(ICatchAction)action;
@end


@protocol ICatchUpdateDelegate <NSObject>
-(void)onActionCompleted:(ICatchAction)action;
-(void)onUploadCompleted;
-(void)onUploadFailure;
@end


@protocol ICatchFormatDelegate <NSObject>
-(void)onActionCompleted:(ICatchAction)action;
@end


@interface ICatchManager: NSObject <ICameraObserver>

+ (ICatchManager*)sharedManager;
@property (nonatomic, strong) NSMutableArray *iCatchPreviewDelegate;
@property (nonatomic, strong) NSMutableArray *iCatchVideoDelegate;
@property (nonatomic, strong) NSMutableArray *iCatchPlayVideoDelegate;
@property (nonatomic, strong) NSMutableArray *iCatchUpdateDelegate;
@property (nonatomic, strong) NSMutableArray *iCatchFormatDelegate;
@property (nonatomic, strong) NSArray *mediaFiles;

-(BOOL)isICatchConnected;
-(void)connectToICatchCamera;

-(void)addICatchPreviewDelegate: (id<ICatchPreviewDelegate>) delegate;
-(void)removeICatchPreviewDelegate: (id<ICatchPreviewDelegate>) delegate;
-(void)addICatchVideoDelegate: (id<ICatchVideoDelegate>) delegate;
-(void)removeICatchVideoDelegate: (id<ICatchVideoDelegate>) delegate;
-(void)addICatchPlayVideoDelegate: (id<ICatchPlayVideoDelegate>) delegate;
-(void)removeICatchPlayVideoDelegate: (id<ICatchPlayVideoDelegate>) delegate;
-(void)addICatchUpdateDelegate: (id<ICatchUpdateDelegate>) delegate;
-(void)removeICatchUpdateDelegate: (id<ICatchUpdateDelegate>) delegate;
-(void)addICatchFormatDelegate: (id<ICatchFormatDelegate>) delegate;
-(void)removeICatchFormatDelegate: (id<ICatchFormatDelegate>) delegate;

-(void)startPreview;
-(void)stopPreview;

-(void)getICatchMedia;

-(void)downloadVideo: (NSString*)name;
-(void)deleteVideo: (NSString*)name;
-(void)startPlayVideo: (NSString*)name;
-(void)stopPlayVideo;
-(void)pausePlayVideo;
-(void)resumePlayVideo;
-(void)seekVideo: (float)value;

-(void)updataFirmware:(NSString *)path;
-(void)uploadFileWtihPath:(NSString *)path;
-(void)deleteFileWtihPath:(NSString *)path;

-(BOOL)formatHelmet;

-(int)getFreeSpaceInImages;

-(NSString*)getFirmwareVersion;

-(void)destory;
@end

#endif /* ICatchManager_h */
