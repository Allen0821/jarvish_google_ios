//
//  ICatchManager.m
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/14.
//  Copyright © 2020 jarvish. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <string>
#import "ICameraObserver.h"
#import "ICatchManager.h"
#import "ICatchFileModel.h"
#import "CameraSDKManager.h"
#import "MediaFile.h"
#import "PCMDataPlayer.h"
#import "LxFTPRequest.h"
#import "NetworkManager.h"

@interface ICatchManager()
@property(nonatomic) MediaFile* playFile;
@property(nonatomic) BOOL isPlaying;
@property(nonatomic, getter = isPlayed) BOOL played;
@property(nonatomic, getter = isPaused) BOOL paused;
@property(nonatomic) double totalSecs;
@property(nonatomic) double playedSecs;
@property(nonatomic) BOOL seeking;
@property(nonatomic) BOOL exceptionHappen;
@property(nonatomic) dispatch_semaphore_t semaphore;
@property(nonatomic) dispatch_group_t playbackGroup;
@property(nonatomic) dispatch_queue_t videoPlaybackQ;
@property(nonatomic) dispatch_queue_t audioQueue;
@property(nonatomic) dispatch_queue_t videoQueue;

@property(nonatomic) WifiCamCamera *camera;
@property(nonatomic) WifiCamControlCenter *controlCenter;
@property(nonatomic) PCMDataPlayer *pcmPl;
@property(nonatomic) NSTimer *pbTimer;
@property(nonatomic) double curVideoPTS;
@property(nonatomic) int times;
@property(nonatomic) int times1;
@property(nonatomic) float totalElapse;
@property(nonatomic) float totalElapse1;
@property(nonatomic) float totalDuration;
@end

@implementation ICatchManager

+ (ICatchManager*)sharedManager {
    static ICatchManager *iCatchManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        iCatchManager = [[self alloc] init];
    });
    return iCatchManager;
}


- (id)init {
  if (self = [super init]) {
      if (self != nil) {
          [self.iCatchPreviewDelegate removeAllObjects];
          self.iCatchPreviewDelegate = nil;
          [self.iCatchVideoDelegate removeAllObjects];
          self.iCatchVideoDelegate = nil;
          [self.iCatchPlayVideoDelegate removeAllObjects];
          self.iCatchPlayVideoDelegate = nil;
          [self.iCatchUpdateDelegate removeAllObjects];
          self.iCatchUpdateDelegate = nil;
          [self.iCatchFormatDelegate removeAllObjects];
          self.iCatchFormatDelegate = nil;
      }
      self.iCatchPreviewDelegate = [NSMutableArray new];
      self.iCatchVideoDelegate = [NSMutableArray new];
      self.iCatchPlayVideoDelegate = [NSMutableArray new];
      self.iCatchUpdateDelegate = [NSMutableArray new];
      self.iCatchFormatDelegate = [NSMutableArray new];
      [[CameraSDKManager sharedManager] addCameraObserver: self];
  }
  return self;
}


-(void)updataFirmware:(NSString *)path {
    NSLog(@"qaz updataFirmware %@", path);
    [[CameraSDKManager sharedManager] updataToSDCard: path];
}


-(void)uploadFileWtihPath:(NSString *)path {
    NSLog(@"qaz uploadFileWtihPath %@", path);
    NSURL *url = [[NetworkManager sharedInstance] smartURLForString: @"192.168.1.1"];
    url = CFBridgingRelease(CFURLCreateCopyAppendingPathComponent(NULL, (__bridge CFURLRef) url, (__bridge CFStringRef) [path lastPathComponent], false));
    
    LxFTPRequest * request = [LxFTPRequest uploadRequest];
    request.serverURL = url;
    request.localFileURL = [NSURL fileURLWithPath:path];
    request.username = @"anonymous";
    request.password = @"anonymous%40icatchtek.com";
    
    request.progressAction = ^(NSInteger totalSize, NSInteger finishedSize, CGFloat finishedPercent) {
        NSLog(@"qaz upload progress : %1.f%%",((float)finishedSize / (float)totalSize) * 100);
    };
    request.successAction = ^(Class resultClass, id result) {
        NSLog(@"qaz successAction xxx");
        @synchronized (self.iCatchUpdateDelegate) {
            for (id<ICatchUpdateDelegate> obs in self.iCatchUpdateDelegate) {
                [obs onUploadCompleted];
            }
        }
        
    };
    request.failAction = ^(CFStreamErrorDomain domain, NSInteger error, NSString *errorMessage) {
        NSLog(@"qaz failAction xxx %@", errorMessage);
        @synchronized (self.iCatchUpdateDelegate) {
            for (id<ICatchUpdateDelegate> obs in self.iCatchUpdateDelegate) {
                [obs onUploadFailure];
            }
        }
    };
    
    [request start];
}


-(void)deleteFileWtihPath:(NSString *)path {
    NSLog(@"qaz deleteFTPDataWtihPath %@", path);
    NSURL *url = [[NetworkManager sharedInstance] smartURLForString: @"192.168.1.1"];
    url = CFBridgingRelease(CFURLCreateCopyAppendingPathComponent(NULL, (__bridge CFURLRef) url, (__bridge CFStringRef) [path lastPathComponent], false));
    
    LxFTPRequest * request = [LxFTPRequest destoryResourceRequest];
    request.serverURL = url;
    request.username = @"anonymous";
    request.password = @"anonymous%40icatchtek.com";
    
    request.successAction = ^(Class resultClass, id result) {
        NSLog(@"qaz resultClass = %@, result = %@", resultClass, result);
        [self uploadFileWtihPath: path];
    };
    request.failAction = ^(CFStreamErrorDomain domain, NSInteger error, NSString *errorMessage) {
        NSLog(@"qaz errorMessage = %@", errorMessage);
        /*@synchronized (self.iCatchUpdateDelegate) {
            for (id<ICatchUpdateDelegate> obs in self.iCatchUpdateDelegate) {
                [obs onUploadFailure];
            }
        }*/
        NSLog(@"qaz kkkkkkkkkkk2 %@", path);
        [self uploadFileWtihPath: path];
    };
    
    [request start];
}


-(BOOL)formatHelmet {
    if ([[CameraSDKManager sharedManager] isConnected]) {
        return [[CameraSDKManager sharedManager] formatSD];
    } else {
        return false;
    }
}

-(int)getFreeSpaceInImages {
    if ([[CameraSDKManager sharedManager] isConnected]) {
        return [[CameraSDKManager sharedManager] getFreeSpaceInImages];
    } else {
        return 0;
    }
}


-(NSString*)getFirmwareVersion {
    return [[CameraSDKManager sharedManager] loadVer];
}


- (void)destory {
    [[CameraSDKManager sharedManager] removeCameraObserver: self];
    if (self.iCatchPreviewDelegate != nil) {
        [self.iCatchPreviewDelegate removeAllObjects];
        self.iCatchPreviewDelegate = nil;
    }
    if (self.iCatchVideoDelegate != nil) {
        [self.iCatchVideoDelegate removeAllObjects];
        self.iCatchVideoDelegate = nil;
    }
    if (self.iCatchPlayVideoDelegate != nil) {
        [self.iCatchPlayVideoDelegate removeAllObjects];
        self.iCatchPlayVideoDelegate = nil;
    }
    if (self.iCatchUpdateDelegate != nil) {
        [self.iCatchUpdateDelegate removeAllObjects];
        self.iCatchUpdateDelegate = nil;
    }
    if (self.iCatchFormatDelegate != nil) {
        [self.iCatchFormatDelegate removeAllObjects];
        self.iCatchFormatDelegate = nil;
    }
    [[CameraSDKManager sharedManager] shutdown];
}


-(BOOL)isICatchConnected {
    return [[CameraSDKManager sharedManager] isConnected];
}


-(void)connectToICatchCamera {
    NSLog(@"xxx connect to icatch camera");
    if (![[CameraSDKManager sharedManager] isConnected]) {
        [[CameraSDKManager sharedManager] connectToCamera];        
    }
}


-(void)addICatchPreviewDelegate: (id<ICatchPreviewDelegate>) delegate {
    NSLog(@"xxx add iCatch delegate");
    @synchronized (self.iCatchPreviewDelegate) {
        if (![self.iCatchPreviewDelegate containsObject: delegate]) {
            NSLog(@"xxx add iCatch delegate ###");
            [self.iCatchPreviewDelegate addObject: delegate];
        }
    }
}


-(void)removeICatchPreviewDelegate: (id<ICatchPreviewDelegate>) delegate {
    NSLog(@"xxx remove iCatch delegate");
    @synchronized (self.iCatchPreviewDelegate) {
        NSLog(@"xxx remove iCatch delegate ###");
        [self.iCatchPreviewDelegate removeObject: delegate];
    }
}


-(void)addICatchVideoDelegate: (id<ICatchVideoDelegate>) delegate {
    NSLog(@"xxx add iCatch video delegate");
    //[[CameraSDKManager sharedManager] addCameraObserver: self];
    @synchronized (self.iCatchVideoDelegate) {
        if (![self.iCatchVideoDelegate containsObject: delegate]) {
            NSLog(@"xxx add iCatch video delegate ###");
            [self.iCatchVideoDelegate addObject: delegate];
        }
    }
}


-(void)removeICatchVideoDelegate: (id<ICatchVideoDelegate>) delegate {
    @synchronized (self.iCatchVideoDelegate) {
        [self.iCatchVideoDelegate removeObject: delegate];
    }
}



-(void)addICatchPlayVideoDelegate: (id<ICatchPlayVideoDelegate>) delegate {
    NSLog(@"xxx add iCatch play video delegate");
    //[[CameraSDKManager sharedManager] addCameraObserver: self];
    @synchronized (self.iCatchPlayVideoDelegate) {
        if (![self.iCatchVideoDelegate containsObject: delegate]) {
            NSLog(@"xxx add iCatch play video delegate ###");
            [self.iCatchPlayVideoDelegate addObject: delegate];
        }
    }
}


-(void)removeICatchPlayVideoDelegate: (id<ICatchPlayVideoDelegate>) delegate {
    @synchronized (self.iCatchPlayVideoDelegate) {
        [self.iCatchPlayVideoDelegate removeObject: delegate];
    }
}


-(void)addICatchUpdateDelegate: (id<ICatchUpdateDelegate>) delegate {
    NSLog(@"xxx add iCatch update delegate");
    //[[CameraSDKManager sharedManager] addCameraObserver: self];
    @synchronized (self.iCatchUpdateDelegate) {
        if (![self.iCatchUpdateDelegate containsObject: delegate]) {
            NSLog(@"xxx add iCatch update delegate ###");
            [self.iCatchUpdateDelegate addObject: delegate];
        }
    }
}


-(void)removeICatchUpdateDelegate: (id<ICatchUpdateDelegate>) delegate {
    @synchronized (self.iCatchUpdateDelegate) {
        [self.iCatchUpdateDelegate removeObject: delegate];
    }
}


-(void)addICatchFormatDelegate: (id<ICatchUpdateDelegate>) delegate {
    NSLog(@"xxx add iCatch format delegate");
    //[[CameraSDKManager sharedManager] addCameraObserver: self];
    @synchronized (self.iCatchFormatDelegate) {
        if (![self.iCatchFormatDelegate containsObject: delegate]) {
            NSLog(@"xxx add iCatch update delegate ###");
            [self.iCatchFormatDelegate addObject: delegate];
        }
    }
}


-(void)removeICatchFormatDelegate: (id<ICatchUpdateDelegate>) delegate {
    @synchronized (self.iCatchFormatDelegate) {
        [self.iCatchFormatDelegate removeObject: delegate];
    }
}


-(void)startPreview {
    NSLog(@"xxx start preview");
    [[CameraSDKManager sharedManager] updateCamMode:CamModePhoto];
    [[CameraSDKManager sharedManager] startPreview];
    //[[CameraSDKManager sharedManager] stopPreview];
}


-(void)stopPreview {
    NSLog(@"xxx stop preview");
    [[CameraSDKManager sharedManager] stopPreview];
}


-(void)getICatchMedia {
    if ([[CameraSDKManager sharedManager] isConnected]) {
        NSLog(@"xxx get iCatch videos");
        NSMutableArray *data = [[NSMutableArray alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            self->_mediaFiles = [[CameraSDKManager sharedManager] getSDFileListByType: WCFileTypeVideo];
            for (MediaFile *mf in self->_mediaFiles) {
                NSArray *dateTime = [mf.date componentsSeparatedByString:@"T"];
                mf.day = [NSMutableString stringWithString:[dateTime objectAtIndex:0]];
                //NSLog(@"xxx video day: %@", mf.day);
                ICatchFileModel *file = [[ICatchFileModel alloc] init];
                file.thumbnail = mf.thumbnail;
                file.name = mf.name;
                file.date = mf.date;
                if (mf.type == WCFileTypeVideo) {
                    file.type = 0;
                } else {
                    file.type = 1;
                }
                file.fileSource = 0;
                file.fileID = mf.fileID;
                file.Corrected = mf.Corrected;
                file.status = mf.status;
                file.fileSize = mf.fileSize;
                [data addObject: file];
            }
            
            /*@synchronized (data) {
                [data removeAllObjects];
            }
            
            if (media) {
                @synchronized (data) {
                    [data addObjectsFromArray:media];
                }
            }*/
            
           
            dispatch_async(dispatch_get_main_queue(), ^{
                //[self updateMediaCount:data.count];
                //[self sortingVideoData:data];
                NSLog(@"xxx video count: %lu", (unsigned long)data.count);
                @synchronized (self.iCatchVideoDelegate) {
                    for (id<ICatchVideoDelegate> obs in self.iCatchVideoDelegate) {
                        [obs onReceivedVideoList: data];
                    }
                }
            });
        });
    }
}


-(void)downloadVideo: (NSString*)name {
    for (MediaFile *mf in self->_mediaFiles) {
        if ([mf.name isEqualToString:name]) {
            [UIApplication sharedApplication].idleTimerDisabled = YES;
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                BOOL result = [mf downloadFromSD:^(float progress) {
                    NSLog(@"xxx progress: %f", progress);
                    @synchronized (self.iCatchVideoDelegate) {
                        for (id<ICatchVideoDelegate> obs in self.iCatchVideoDelegate) {
                            [obs onDownloadProgress: progress];
                        }
                    }
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (result) {
                        @synchronized (self.iCatchVideoDelegate) {
                            for (id<ICatchVideoDelegate> obs in self.iCatchVideoDelegate) {
                                [obs onDownloadCompleted];
                            }
                        }
                    } else {
                        @synchronized (self.iCatchVideoDelegate) {
                            for (id<ICatchVideoDelegate> obs in self.iCatchVideoDelegate) {
                                [obs onDownloadFailed];
                            }
                        }
                    }
                });
            });
            break;
        }
    }
}


-(void)deleteVideo: (NSString*)name {
    for (MediaFile *mf in self->_mediaFiles) {
        if ([mf.name isEqualToString:name]) {
            BOOL result = [mf deleteFromSD];
            @synchronized (self.iCatchVideoDelegate) {
                for (id<ICatchVideoDelegate> obs in self.iCatchVideoDelegate) {
                    [obs onDeleteResult: result videoName: name];
                }
            }
            break;
        }
    }
}


-(void)startPlayVideo: (NSString*)name {
    for (MediaFile *mf in self->_mediaFiles) {
        if ([mf.name isEqualToString:name]) {
            self.playFile = mf;
            break;
        }
    }
    
    self.camera = [CameraSDKManager sharedManager].camera;
    self.controlCenter = [CameraSDKManager sharedManager].ctrl;
    
    self.totalSecs = 0;
    self.playedSecs = 0;
    
    self.semaphore = dispatch_semaphore_create(1);
    self.playbackGroup = dispatch_group_create();
    self.videoPlaybackQ = dispatch_queue_create("WifiCam.GCD.Queue.Playback.Q", 0);
    self.audioQueue = dispatch_queue_create("WifiCam.GCD.Queue.Playback.Audio", 0);
    self.videoQueue = dispatch_queue_create("WifiCam.GCD.Queue.Playback.Video", 0);
  
    dispatch_async(self->_videoPlaybackQ, ^{
        ICatchFile file = *[self.playFile getICatchFileRef];
        NSLog(@"xxx play %s", file.getFileName().c_str());
        self.totalSecs = [self.controlCenter.pbCtrl play:&file];
        NSLog(@"xxx totalSecs %f", self.totalSecs);
        if (self.totalSecs <= 0) {
            NSLog(@"xxx Failed to play totalSecs");
            return;
        }
        
        self.isPlaying = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            @synchronized (self.iCatchPlayVideoDelegate) {
                for (id<ICatchPlayVideoDelegate> obs in self.iCatchPlayVideoDelegate) {
                    [obs onVideoTotalSeconds: self.totalSecs];
                }
            }
            @synchronized (self.iCatchPlayVideoDelegate) {
                for (id<ICatchPlayVideoDelegate> obs in self.iCatchPlayVideoDelegate) {
                    [obs onVideoPlayingSeconds: self.playedSecs];
                }
            }
        });
     
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setUpdateTimeInfoTimer: true];
        });
        
        if ([self.controlCenter.pbCtrl audioPlaybackStreamEnabled]) {
            NSLog(@"xxx play Audio");
            dispatch_group_async(self->_playbackGroup, self->_audioQueue, ^{[self playAudio];});
        } else {
            NSLog(@"xxx Playback doesn't contains audio.");
        }
        if ([self.controlCenter.pbCtrl videoPlaybackStreamEnabled]) {
            NSLog(@"xxx play Video");
            dispatch_group_async(self->_playbackGroup,self-> _videoQueue, ^{[self playVideo];});
        } else {
            NSLog(@"xxx Playback doesn't contains video.");
        }
        self.played = true;
    });
}


-(void)pausePlayVideo {
    BOOL res = [self.controlCenter.pbCtrl pause];
    if (res) {
        [self setUpdateTimeInfoTimer: false];
        self.paused = YES;
    }
    @synchronized (self.iCatchPlayVideoDelegate) {
        for (id<ICatchPlayVideoDelegate> obs in self.iCatchPlayVideoDelegate) {
            [obs onPauseResult: res];
        }
    }
}


-(void)resumePlayVideo {
    BOOL res = [self.controlCenter.pbCtrl resume];
    if (res) {
        [self setUpdateTimeInfoTimer: true];
        self.paused = NO;
    }
    @synchronized (self.iCatchPlayVideoDelegate) {
        for (id<ICatchPlayVideoDelegate> obs in self.iCatchPlayVideoDelegate) {
            [obs onResumeResult: res];
        }
    }
}


-(void)stopPlayVideo {
    self.isPlaying = NO;
    [self setUpdateTimeInfoTimer: false];
    self.played = [self.controlCenter.pbCtrl stop];
    self.semaphore = nil;
    self.playbackGroup = nil;
    self.videoPlaybackQ = nil;
    self.audioQueue = nil;
    self.videoQueue = nil;
}



-(void)seekVideo: (float)value {
    if (self.played) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSLog(@"xxx seek ######%%%%%%%%%%");
            self.seeking = YES;
            BOOL retVal = [self.controlCenter.pbCtrl seek:value];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (retVal) {
                    NSLog(@"xxx seek succeed.");
                    self.playedSecs = value;
                    self.curVideoPTS = self.playedSecs;
                } else {
                    NSLog(@"xxx seek failed.");
                }
                @synchronized (self.iCatchPlayVideoDelegate) {
                    for (id<ICatchPlayVideoDelegate> obs in self.iCatchPlayVideoDelegate) {
                        [obs onSeekVideoResult: retVal];
                    }
                }
                [self setUpdateTimeInfoTimer: true];
            });
            self.seeking = NO;
            NSLog(@"xxx seek ####$$$$$$$$");
        });
    }
}


-(void)playVideo {
    UIImage *receivedImage = nil;
    while (self.isPlaying) {
        NSDate *begin = [NSDate date];
        WifiCamAVData *wifiCamData = [self.controlCenter.propCtrl prepareDataForPlaybackVideoFrame];
        //NSLog(@"xxx wifiCamData data length %lu", (unsigned long)wifiCamData.data.length);
        if (wifiCamData.data.length > 0) {
            self.curVideoPTS = wifiCamData.time;
            receivedImage = [[UIImage alloc] initWithData:wifiCamData.data];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (receivedImage) {
                    @synchronized (self.iCatchVideoDelegate) {
                        for (id<ICatchPlayVideoDelegate> obs in self.iCatchPlayVideoDelegate) {
                            [obs onReceivedImage:receivedImage];
                        }
                    }
                }
            });
            receivedImage = nil;
        }
        NSDate *end = [NSDate date];
        NSTimeInterval elapse = [end timeIntervalSinceDate:begin];
        //NSLog(@"xxx [V]Get %lu, elapse: %f", (unsigned long)wifiCamData.data.length, elapse * 1000);
    }
    NSLog(@"xxx quit video");
}


- (void)playAudio {
    NSMutableData *audioBuffer = [[NSMutableData alloc] init];

    ICatchAudioFormat format = [self.controlCenter.propCtrl retrievePlaybackAudioFormat];
    NSLog(@"xxx Codec:%x, freq: %d, chl: %d, bit:%d", format.getCodec(), format.getFrequency(), format.getNChannels(), format.getSampleBits());
    
    _pcmPl = [[PCMDataPlayer alloc] initWithFreq:format.getFrequency() channel:format.getNChannels() sampleBit:format.getSampleBits()];
    if (!_pcmPl) {
        NSLog(@"xxx Init audioQueue failed.");
        return;
    }
    
    while (self.isPlaying) {
        NSDate *begin = [NSDate date];
        [audioBuffer setLength:0];
        for (int i = 0; i < 4; i++) {
            NSDate *begin1 = [NSDate date];
            ICatchFrameBuffer *buff = [self.controlCenter.propCtrl prepareDataForPlaybackAudioTrack1];
            NSDate *end1 = [NSDate date];
            NSTimeInterval elapse1 = [end1 timeIntervalSinceDate:begin1] * 1000;
            //NSLog(@"xxx getNextAudioFrame time: %fms", elapse1);
            _totalElapse1 += elapse1;
            
            if (buff != NULL) {
                [audioBuffer appendBytes:buff->getBuffer() length:buff->getFrameSize()];
                if (audioBuffer.length > MIN_SIZE_PER_FRAME) {
                    //NSLog(@"xxx play audio MIN_SIZE_PER_FRAME");
                    break;
                }
            } else {
                NSLog(@"xxx PlaybackAudioTrack nil");
            }
        }
        
        if (audioBuffer.length > 0) {
            [_pcmPl play:(void *)audioBuffer.bytes length:audioBuffer.length];
        }

        NSDate *end = [NSDate date];
        NSTimeInterval elapse = [end timeIntervalSinceDate:begin] * 1000;
        float duration = audioBuffer.length/4.0/format.getFrequency() * 1000;
        //NSLog(@"xxx [A]Get %lu, elapse: %fms, duration: %fms", (unsigned long)audioBuffer.length, elapse, duration);
        _totalElapse += elapse;
        _totalDuration += duration;
        _times1 ++;
    }
    
    if (_pcmPl) {
        [_pcmPl stop];
    }
    _pcmPl = nil;
    
    NSLog(@"quit audio");
}


- (void)setUpdateTimeInfoTimer:(BOOL)enable {
    if (self.pbTimer != nil) {
        [self.pbTimer invalidate];
        self.pbTimer = nil;
    }
    if (enable) {
        self.pbTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
        target  :self
        selector:@selector(updateTimeInfo:)
        userInfo:nil
        repeats :YES];
    }
}


- (void)updateTimeInfo:(NSTimer *)sender {
    if (!_seeking) {
        self.playedSecs = _curVideoPTS;
        dispatch_async(dispatch_get_main_queue(), ^{
            @synchronized (self.iCatchVideoDelegate) {
                for (id<ICatchPlayVideoDelegate> obs in self.iCatchPlayVideoDelegate) {
                    [obs onVideoPlayingSeconds: self.playedSecs];
                }
            }
        });
    } else {
        NSLog(@"xxx seeking");
    }
    if (++_times == 200) {
        _times = 0;
        _times1 = 0;
        _totalDuration = 0.0;
        _totalElapse = 0.0;
        _totalElapse1 = 0.0;
    }
}


- (void)addPlaybackObserver {
    
}

- (void)removePlaybackObserver {
    
}


#pragma mark - iCatchDelegate
-(void)onReceivedPreviewImg: (UIImage*)img {
    @synchronized (self.iCatchPreviewDelegate) {
        for (id<ICatchPreviewDelegate> obs in self.iCatchPreviewDelegate) {
            NSLog(@"xxx send onReceivedPreviewImg #######");
            [obs onReceivedPreviewImage: img];
        }
    }
}


-(void)onMovieRecordElapsedTime:(NSString*)timeStr {
    @synchronized (self.iCatchPreviewDelegate) {
        for (id<ICatchPreviewDelegate> obs in self.iCatchPreviewDelegate) {
            [obs onMovieRecordElapsedTime: timeStr];
        }
    }
}


-(void)onActionCompleted:(CamAction)action {
    NSLog(@"[UP]: onActionCompleted ####### %ld", (long)action);
    ICatchAction iCatchAction;
    switch (action) {
        case CamActionConnectedToCamera:
            iCatchAction = ActionConnectedToCamera;
            break;
        case CamActionCameraDisconnected:
            iCatchAction = ActionCameraDisconnected;
            break;
        case CamActionPreviewDead:
            iCatchAction = ActionPreviewDead;
            break;
        default:
            return;
    }
    
    @synchronized (self.iCatchPreviewDelegate) {
        for (id<ICatchPreviewDelegate> obs in self.iCatchPreviewDelegate) {
            [obs onActionCompleted: iCatchAction];
        }
    }
    
    @synchronized (self.iCatchVideoDelegate) {
        for (id<ICatchVideoDelegate> obs in self.iCatchVideoDelegate) {
            [obs onActionCompleted: iCatchAction];
        }
    }
    
    @synchronized (self.iCatchPlayVideoDelegate) {
        for (id<ICatchPlayVideoDelegate> obs in self.iCatchPlayVideoDelegate) {
            [obs onActionCompleted: iCatchAction];
        }
    }
    
    @synchronized (self.iCatchUpdateDelegate) {
        for (id<ICatchUpdateDelegate> obs in self.iCatchUpdateDelegate) {
            [obs onActionCompleted: iCatchAction];
        }
    }
    
    @synchronized (self.iCatchFormatDelegate) {
        for (id<ICatchFormatDelegate> obs in self.iCatchFormatDelegate) {
            [obs onActionCompleted: iCatchAction];
        }
    }
}

@end
