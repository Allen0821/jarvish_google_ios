//
//  NetworkManager.h
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/9.
//  Copyright © 2020 jarvish. All rights reserved.
//

#ifndef NetworkManager_h
#define NetworkManager_h

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

+ (NetworkManager *)sharedInstance;

- (NSURL *)smartURLForString:(NSString *)str;
- (BOOL)isImageURL:(NSURL *)url;

- (NSString *)pathForTestImage:(NSUInteger)imageNumber;
- (NSString *)pathForTemporaryFileWithPrefix:(NSString *)prefix;

- (NSString *)pathForTestBinWithPath:(NSString*)filepath;


@property (nonatomic, assign, readonly ) NSUInteger     networkOperationCount;  // observable

- (void)didStartNetworkOperation;
- (void)didStopNetworkOperation;

@end

#endif /* NetworkManager_h */
