//
//  PCMDataPlayer.h
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/5/26.
//  Copyright © 2020 jarvish. All rights reserved.
//

#ifndef PCMDataPlayer_h
#define PCMDataPlayer_h

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

#define QUEUE_BUFFER_SIZE 6 //队列缓冲个数
#define MIN_SIZE_PER_FRAME 5000 //每帧最小数据长度

@interface PCMDataPlayer : NSObject {
    AudioStreamBasicDescription audioDescription; ///音频参数
    AudioQueueRef audioQueue; //音频播放队列
    AudioQueueBufferRef audioQueueBuffers[QUEUE_BUFFER_SIZE]; //音频缓存
    BOOL audioQueueUsed[QUEUE_BUFFER_SIZE];

    NSLock* sysnLock;
}

- (id)initWithFreq:(Float64)freq channel:(UInt32)channel sampleBit:(UInt32)bit;
- (void)reset;
- (void)stop;
- (void)play:(void*)pcmData length:(NSUInteger)length;

@end
#endif /* PCMDataPlayer_h */
