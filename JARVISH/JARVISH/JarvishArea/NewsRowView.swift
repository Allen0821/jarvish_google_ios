//
//  NewsRowView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/16.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

struct NewsRowView: View {
    
    var news: News
    
    var body: some View {
        ZStack {
            GeometryReader { geo in
                Image("nightothers")
                .resizable()
                .scaledToFill()
                .frame(width: geo.size.width, height: geo.size.width * 9 / 16)
                .clipped()
            }
        }
        .padding()
    }
}

struct NewsRowView_Previews: PreviewProvider {
    static var previews: some View {
        let news = News(
            id: "1001",
            title: "uid",
            subTitle: "1",
            content: "1",
            type: 0)
        
        NewsRowView(news: news)
    }
}
