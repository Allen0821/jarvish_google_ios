//
//  NewsView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/15.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI



struct NewsView: View {
    
    @ObservedObject var viewModel = NewsViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.newsList) { news in
                
            }
            
        }
        .onAppear(perform: {
            viewModel.getNewsList()
        })
    }
}

struct NewsView_Previews: PreviewProvider {
    static var previews: some View {
        NewsView()
    }
}
