//
//  NewsViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/16.
//  Copyright © 2021 jarvish. All rights reserved.
//



class NewsViewModel: ObservableObject {
    
    @Published var newsList: [News] = []
    
    
    let storeNews = StoreNews()
    
    func getNewsList() {
        storeNews.queryNewsList() { list in
            self.newsList.removeAll()
            if let list = list {
                self.newsList = list
            }
        }
    }
    
}
