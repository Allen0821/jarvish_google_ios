//
//  MainViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/26.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

protocol MainViewControllerDelegate {
    func onOpenAuth()
    
}

class MainViewController: UITabBarController {
    
    var viewDelegate: MainViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(openAuth), name: NSNotification.Name("OPENAUTH") , object: nil)
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
   
    @objc private func openAuth() {
        print("[auth]: openAuth")
        viewDelegate?.onOpenAuth()
    }

}
