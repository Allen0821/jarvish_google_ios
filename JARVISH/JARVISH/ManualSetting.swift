//
//  ManualSetting.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/10/22.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

public class ManualSetting {
    
    private static func getManualLocaleIndex() -> Int {
        let locale = NSLocale.preferredLanguages[0]
        if locale.contains("zh-Hant") {
            return 0
        } else {
            return 1
        }
    }
    
    
    static func getUserGuideLink() -> [UserManual] {
        var manuals: [UserManual] = []
        let index = getManualLocaleIndex()
        if index == 0 {
            manuals.append(UserManual.init(name: "JARVISH X/ANGELO X 安全帽快速指南", link: "https://drive.google.com/uc?export=view&id=1F7MkaQrEjiIZrz_Lt-numlry30NgLnsu"))
            
            manuals.append(UserManual.init(name: "FLASH F系列 安全帽快速指南", link: "https://drive.google.com/uc?export=view&id=1BZRYc58-WYO5iNN29F9IldOc-Dlso5tp"))
            
            manuals.append(UserManual.init(name: "R1/X1 安全帽快速指南", link: "https://drive.google.com/uc?export=view&id=1Sw1CeX8UtzhESuFCYyKZ1hnKhcUYX7AA"))
            
            manuals.append(UserManual.init(name: "MES系列 安全帽快速指南", link: "https://drive.google.com/uc?export=view&id=1QVtFzatTJi8qvctpj_h46cFKd2rE9jgA"))
            
            manuals.append(UserManual.init(name: "Bluetooth 與 Wi-Fi 連線說明", link: "https://drive.google.com/uc?export=view&id=1vAunveBTnpYDvOIgzUvyR5O5uLLI4CTD"))
            
            manuals.append(UserManual.init(name: "前後對講連線說明", link: "https://drive.google.com/uc?export=view&id=1phclsHfK8jx6J24lArVnNNnXfRN8xmg9"))
            
            manuals.append(UserManual.init(name: "APP 使用說明", link: "https://drive.google.com/uc?export=view&id=1dcGevnEKQELLJn8FT7AfoHlNMt0akEHq"))
        } else {
            manuals.append(UserManual.init(name: "JARVISH X/ANGELO X Quick Start Guide", link: "https://drive.google.com/uc?export=view&id=1czvT2r6y8VHVbKrZ-TSwbwQGIdPsy5cE"))
            
            manuals.append(UserManual.init(name: "JARVISH X/ANGELO X Connection Guide", link: "https://drive.google.com/uc?export=view&id=1Es-LeA5gUHxfwXbjFnwh1CC08TACx0oN"))
            
            manuals.append(UserManual.init(name: "APP User Guide", link: "https://drive.google.com/uc?export=view&id=1C2cDodsPPOHOo_Y_E5t3CAersb3t2PEh"))
        }
        return manuals
    }
    
    
    static func getPrivacyPolicyLink() -> URL {
        if getManualLocaleIndex() == 0 {
            return URL(string: "https://sites.google.com/jarvish.com/jarvishprivacypolicytw/%E9%A6%96%E9%A0%81")!
        } else {
            return URL(string: "https://sites.google.com/jarvish.com/jarvishprivacypolicyen/%E9%A6%96%E9%A0%81")!
        }
    }
}
