//
//  Account.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/15.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseFirestore
import FirebaseFirestoreSwift

struct Account: Codable {
    var uid: String
    var email: String?
    var isEmailVerified: Bool = false
    var elderId: String?
    var isElder: Bool = false
    var haveProduct: Bool = false
    var created: Timestamp?
    
    
    enum CodingKeys: String, CodingKey {
        case uid
        case email
        case isEmailVerified = "emailVerified"
        case elderId
        case isElder = "elder"
        case haveProduct
        case created
    }
    
    
}
