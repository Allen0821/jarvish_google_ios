//
//  OtaApi.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/5.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import Moya


struct FirmwareInfo {
    var model: String?
    var boardInfo: String?
    var fwVersion: String?
    var langCode: String?
    var langSubVersion: String?
    var mcuVersion: String?
    var cmdModel: String?
    
    init(model: String, boardInfo: String, fwVersion: String, langCode: String, langSubVersion: String, mcuVersion: String, cmdModel: String) {
        self.model = model
        self.boardInfo = boardInfo
        self.fwVersion = fwVersion
        self.langCode = langCode
        self.langSubVersion = langSubVersion
        self.mcuVersion = mcuVersion
        self.cmdModel = cmdModel
    }
}


let OtaProvider = MoyaProvider<OtaInterface>(plugins: [NetworkLoggerPlugin()])

public enum OtaInterface {
    case getUpdateInformation(model: String, boardInfo: String, fwVersion: String, langCode: String, langSubVersion: String, mcuVersion: String, cmdModel: String)
    case downloadFirmware(url: URL)
    case downloadMcu(url: URL)
    case downloadCmd(url: URL)
}


extension OtaInterface: TargetType {
    public var baseURL: URL {
        switch self {
        case .getUpdateInformation(_,_,_,_,_,_,_):
            return URL(string: "https://developer.jarvish.com")!
        case .downloadFirmware(let url):
            return url
        case .downloadMcu(let url):
            return url
        case .downloadCmd(let url):
            return url
        }
    }
    
    public var path: String {
        switch self {
        case .getUpdateInformation(let model,let boardInfo,let fwVersion,let langCode,let langSubVersion,_,_):
            return "/api/v2/ota/\(model)/\(boardInfo)/\(fwVersion)/\(langCode)/\(langSubVersion)"
        case .downloadFirmware(_):
            return ""
        case .downloadMcu(_):
            return ""
        case .downloadCmd(_):
            return ""
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .getUpdateInformation(_,_,_,_,_,_,_):
            return .get
        case .downloadFirmware(_):
            return .get
        case .downloadMcu(_):
            return .get
        case .downloadCmd(_):
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .getUpdateInformation(_,_,_,_,_,let mcuVersion,let cmdModel):
            return .requestParameters(parameters: [
                "cmd_model" : cmdModel,
                "mcu_version" : mcuVersion], encoding: URLEncoding.default)
        case .downloadFirmware(_):
            return .downloadDestination(defaultDownloadDestination)
        case .downloadMcu(_):
            return .downloadDestination(defaultDownloadDestination)
        case .downloadCmd(_):
            return .downloadDestination(defaultDownloadDestination)
        }
    }
    
    
    public var headers: [String : String]? {
        switch self {
            case .getUpdateInformation(_,_,_,_,_,_,_):
                return [
                    "Content-type": "application/json"/*,
                    "publishtest": "a373bb72-0472-4461-8c1f-9695187a7de2"*/
                ]
        default:
            return [
                "Content-type": "application/json"
            ]
        }
    }
    
}



private let defaultDownloadDestination: DownloadDestination = { temporaryURL, response in
    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let fileURL = documentsURL.appendingPathComponent(response.suggestedFilename!)
    return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
}
