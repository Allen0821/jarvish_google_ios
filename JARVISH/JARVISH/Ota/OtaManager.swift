//
//  OtaManager.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/5.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import UIKit
import Moya
import CommonCrypto

protocol OtaManagerDelegate: class {
    func onUpdateInformation(updateInfo: UpdateInfo)
    func onNoUpdateInformation()
    func onDownloadUpdateFileCompleted()
    func onDownloadUpdateFileFailure()
    func onUpdateCompleted()
    func onUpdateFailure()
}


class OtaManager: NSObject, ICatchUpdateDelegate {
    
    private static var sharedOtaManager: OtaManager = {
        let otaManager = OtaManager()
        return otaManager
    }()
    
    var updateInfo: UpdateInfo?
    var latest: Latest?
    weak var delegate: OtaManagerDelegate?
    var iCatchManager: ICatchManager?
    var uploadStep = 0
    var triggerUpload = false
    
    
    private override init() {
        
    }
    
    class func shared() -> OtaManager {
        return sharedOtaManager
    }
    
    func destory() {
        if self.iCatchManager != nil {
            self.iCatchManager?.remove(self)
        }
    }
    
    
    func checkHelmetUpdateInformation(model: String, boardInfo: String, fwVersion: String, langCode: String, langSubVersion: String, mcuVersion: String, cmdModel: String) {
        
        OtaProvider.rx.request(.getUpdateInformation(model: model, boardInfo: boardInfo, fwVersion: fwVersion, langCode: langCode, langSubVersion: langSubVersion, mcuVersion: mcuVersion, cmdModel: cmdModel))
            .debug()
        .filterSuccessfulStatusCodes()
        .mapJSON()
            .subscribe(onSuccess: { result in
                print("checkHelmetUpdateInformation result: \(result)")
                if let updateInfo = UpdateInfo(dictionary: result as! [String : Any]) {
                    print("updateInfo: \(updateInfo)")
                    self.updateInfo = updateInfo
                    if let latest = Latest(dictionary: updateInfo.latest) {
                        print("latest: \(latest)")
                        self.latest = latest
                        self.delegate?.onUpdateInformation(updateInfo: self.updateInfo!)
                    } else {
                        self.delegate?.onNoUpdateInformation()
                    }
                }
            }) { error in
                print("checkHelmetUpdateInformation failure: \(error)")
                self.delegate?.onNoUpdateInformation()
        }
    }
    
    
    func downloadUpdateFiles() {
        downloadFirmware()
    }
    
    
    private func downloadFirmware() {
        OtaProvider.rx.request(.downloadFirmware(url: URL(string: (self.latest?.firmwareLink)!)!))
        .filterSuccessfulStatusCodes()
            .subscribe(onSuccess: { result in
                print("downloadFirmware result: \(result)")
                if self.checkFileMD5(fileName: (self.latest?.firmwareFileName)!, md5: (self.latest?.firmwareMD5)!) {
                    self.downloadMcu()
                } else {
                    self.delegate?.onDownloadUpdateFileFailure()
                }
            }) { error in
                print("downloadFirmware result: \(error)")
                self.delegate?.onDownloadUpdateFileFailure()
        }
    }

    
    private func downloadMcu() {
        OtaProvider.rx.request(.downloadMcu(url: URL(string: (self.latest?.mcuLink)!)!))
        .filterSuccessfulStatusCodes()
            .subscribe(onSuccess: { result in
                print("downloadMcu result: \(result)")
                if self.checkFileMD5(fileName: (self.latest?.mcuFileName)!, md5: (self.latest?.mcuMD5)!) {
                    self.downloadCmd()
                } else {
                    self.delegate?.onDownloadUpdateFileFailure()
                }
            }) { error in
                print("downloadMcu result: \(error)")
                self.delegate?.onDownloadUpdateFileFailure()
        }
    }
    
    
    private func downloadCmd() {
        OtaProvider.rx.request(.downloadCmd(url: URL(string: (self.latest?.cmdModelLink)!)!))
        .filterSuccessfulStatusCodes()
            .subscribe(onSuccess: { result in
                print("downloadCmd result: \(result)")
                if self.checkFileMD5(fileName: (self.latest?.cmdModelFileName)!, md5: (self.latest?.cmdModelMD5)!) {
                    self.delegate?.onDownloadUpdateFileCompleted()
                } else {
                    self.delegate?.onDownloadUpdateFileFailure()
                }
            }) { error in
                print("downloadCmd result: \(error)")
                self.delegate?.onDownloadUpdateFileFailure()
        }
    }
    
    
    private func getFileLocalPath(fileName: String) -> URL? {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            for d in directoryContents {
                if d.absoluteString.contains(fileName) {
                    return d
                }
            }
        } catch {
            print("error \(error)")
        }
        return nil
    }
    
    
    func removeFilePrefix(str: String) -> String {
        let p = "file:///private"
        let start = str.index(str.startIndex, offsetBy: p.count)
        let end = str.index(str.endIndex, offsetBy: 0)
        let range = start..<end
        return String(str[range])
    }

    
    
    func checkFileMD5(fileName: String, md5: String) -> Bool {
        let url = getFileLocalPath(fileName: fileName)
        if url == nil {
            return false
        }
        
        let bufferSize = 1024 * 1024
        do {
            // Open file for reading:
            let file = try FileHandle(forReadingFrom: url!)
            defer {
                file.closeFile()
            }

            // Create and initialize MD5 context:
            var context = CC_MD5_CTX()
            CC_MD5_Init(&context)

            // Read up to `bufferSize` bytes, until EOF is reached, and update MD5 context:
            while autoreleasepool(invoking: {
                let data = file.readData(ofLength: bufferSize)
                if data.count > 0 {
                    data.withUnsafeBytes {
                        _ = CC_MD5_Update(&context, $0.baseAddress, numericCast(data.count))
                    }
                    return true // Continue
                } else {
                    return false // End of file
                }
            }) { }

            // Compute the MD5 digest:
            var digest: [UInt8] = Array(repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            _ = CC_MD5_Final(&digest, &context)
            
            let hash = NSMutableString()
            for i in 0..<digest.count{
                hash.appendFormat("%02x", digest[i])
            }
            let result = String(format: hash as String)
            if result.elementsEqual(md5) {
                return true
            }
        } catch {
            print("Cannot open file:", error.localizedDescription)
        }
        return false
    }
    
    
    func createChecksumFile() {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let file = documentsURL.appendingPathComponent("checksum.txt").absoluteURL
        if(!FileManager.default.fileExists(atPath: file.absoluteString)){
            FileManager.default.createFile(atPath: file.absoluteString, contents: nil, attributes: nil)
        }else{
            print("checksum is already created")
        }
        
        do {
            let str = "[SPHOST.BRN] = [\(self.latest!.firmwareMD5!)]\n[STM32F411CE_CMD_MD.bin] = [\(self.latest!.cmdModelMD5!)]\n[STM32F411CE_MCU_FW.bin] = [\(self.latest!.mcuMD5!)]"
            try str.write(to: file, atomically: true, encoding: .utf8)
        } catch {
            print("write checksum failed")
        }
    }
    
    
    func uploadFile() {
        self.uploadStep = 0
        self.iCatchManager = ICatchManager.shared()!
        self.iCatchManager?.add(self)
        if (self.iCatchManager?.isICatchConnected())! {
            let path = self.getFileLocalPath(fileName: self.latest!.cmdModelFileName!)?.absoluteString
            self.iCatchManager?.deleteFileWtihPath(self.removeFilePrefix(str: path!))
        } else {
            self.triggerUpload = true
            self.iCatchManager?.connectToICatchCamera()
        }
    }
    
    
    func onActionCompleted(_ action: ICatchAction) {
        switch action {
        case ActionConnectedToCamera:
            print("xxx Connected")
            if self.triggerUpload {
                self.triggerUpload = false
                let path = self.getFileLocalPath(fileName: self.latest!.cmdModelFileName!)?.absoluteString
                self.iCatchManager?.deleteFileWtihPath(self.removeFilePrefix(str: path!))
            }
            break
        case ActionCameraDisconnected:
            print("xxx Disconnected")
            break
        default:
            print("unknown")
            break
        }
    }
    
    
    func onUploadCompleted() {
        if self.uploadStep == 0 {
            self.uploadStep = 1
            let path = self.getFileLocalPath(fileName: self.latest!.mcuFileName!)?.absoluteString
            self.iCatchManager?.deleteFileWtihPath(self.removeFilePrefix(str: path!))
        } else if self.uploadStep == 1 {
            self.uploadStep = 2
            let path = self.getFileLocalPath(fileName: "checksum.txt")?.absoluteString
            self.iCatchManager?.deleteFileWtihPath(self.removeFilePrefix(str: path!))
        } else {
            let path = self.getFileLocalPath(fileName: self.latest!.firmwareFileName!)?.absoluteString
            self.iCatchManager?.updataFirmware(self.removeFilePrefix(str: path!))
            self.delegate?.onUpdateCompleted()
        }
    }
    
    
    func onUploadFailure() {
        self.delegate?.onUpdateFailure()
    }
}
