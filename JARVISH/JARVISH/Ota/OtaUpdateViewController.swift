//
//  OtaUpdateViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/4/1.
//  Copyright © 2021 jarvish. All rights reserved.
//

import UIKit

protocol OtaUpdateViewControllerDelegate {
    func onUpdateCompleted()
}

class OtaUpdateViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var moreLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var progressView: UIImageView!
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    
    var style: String!
    var language: String?
    var firmwareVersion: String?
    
    private var otaClient: OtaClient!
    private var fwReleaseInfo: FwReleaseInfo?
    
    var step = 0
    var delegate: OtaUpdateViewControllerDelegate?
    
    var checkWifiTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleLabel.text = NSLocalizedString("ota_title", comment: "")
        moreLabel.text = NSLocalizedString("ota_read_release_note", comment: "")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapReadMore))
        moreLabel.isUserInteractionEnabled = true
        moreLabel.addGestureRecognizer(tap)
        
        nextButton.layer.cornerRadius = nextButton.frame.height / 2
        nextButton.clipsToBounds = true
        nextButton.isHidden = true
        
        cancelButton.layer.cornerRadius = cancelButton.frame.height / 2
        cancelButton.layer.borderColor = UIColor(named: "colorPrimary")?.cgColor
        cancelButton.layer.borderWidth = 1
        cancelButton.isHidden = true
        
        moreLabel.isHidden = true
        
        progressView.isHidden = true
        
        otaClient = OtaClient.init(helmetStyle: self.style, languageCode: self.language)
        otaClient.delegate = self
        
        getReleaseInfofromServer()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func touchNext(_ sender: UIButton) {
        switch self.step {
        case 0:
            getReleaseInfofromServer()
            return
        case 1:
            downloadFirmwarefromServer()
            return
        case 2:
            updateHelmet()
            return
        default:
            print("unknown")
        }
    }
    
    @IBAction func touchCancel(_ sender: UIButton) {
        otaClient.stop()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapReadMore(_ sender: Any) {
        print("tapReadMore")
        DispatchQueue.main.async() {
            print("tapReadMore 1")
            if self.fwReleaseInfo?.note != nil &&
                !(self.fwReleaseInfo?.note.isEmpty)! {
                self.readReleaseNote()
            }
        }
    }
    
    
    private func getReleaseInfofromServer() {
        self.contentLabel.text = NSLocalizedString("ota_get_info", comment: "")
        self.nextButton.isHidden = true
        self.cancelButton.isHidden = true
        self.startLoadingRotate()
        otaClient.getReleaseInformation(currVersion: firmwareVersion)
    }
    
    private func downloadFirmwarefromServer() {
        self.contentLabel.text = NSLocalizedString("ota_download", comment: "")
        self.nextButton.isHidden = true
        self.cancelButton.isHidden = true
        self.moreLabel.isHidden = true
        self.startLoadingRotate()
        otaClient.downloadFirmware()
    }
    
    private func updateHelmet() {
        self.contentLabel.text = NSLocalizedString("ota_update_helmet", comment: "")
        self.nextButton.isHidden = true
        self.cancelButton.isHidden = true
        self.startLoadingRotate()
        self.checkWifi()
    }
    
    private func readReleaseNote() {
        print("readReleaseNote")
        let message = self.fwReleaseInfo?.note.replacingOccurrences(of: "\\n", with: "\n")
        let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = NSTextAlignment.left
        
        let attributedMessageText = NSMutableAttributedString(
            string: message!,
            attributes: [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0)
            ]
        )
        
        let alert = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: .alert)
        alert.setValue(attributedMessageText, forKey: "attributedMessage")
        let cancel = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func resetWifiTimer() {
        if checkWifiTimer != nil {
            checkWifiTimer?.invalidate()
            checkWifiTimer = nil
        }
    }
    
    private func checkWifi() {
        resetWifiTimer()
        
        checkWifiTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { result in
            self.contentLabel.text = NSLocalizedString("ota_update_error", comment: "")
            self.nextButton.isHidden = false
            self.cancelButton.isHidden = false
            self.stopLoadingRotate()
            self.step = 2
            self.connectHelmetWiFiAlert()
            })
        
        HelmetWiFiTool.pingHelmet { result in
            if result {
                self.resetWifiTimer()
                self.otaClient.updateHelmet()
            } else {
                self.resetWifiTimer()
                self.contentLabel.text = NSLocalizedString("ota_update_error", comment: "")
                self.nextButton.isHidden = false
                self.cancelButton.isHidden = false
                self.stopLoadingRotate()
                self.step = 2
                self.connectHelmetWiFiAlert()
            }
        }
    }
    
    
    private func connectHelmetWiFiAlert() {
        let alert = UIAlertController(title: NSLocalizedString("helmet_wifi_connection_hint_title", comment: ""), message: NSLocalizedString("helmet_wifi_connection_hint_message", comment: ""), preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: nil)
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func startLoadingRotate() {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.toValue = Double.pi * 2
        rotateAnimation.duration = 1
        rotateAnimation.repeatCount = .infinity
        progressView.layer.add(rotateAnimation, forKey: nil)
        self.view.isUserInteractionEnabled = false
        progressView.isHidden = false
    }
    
    
    func stopLoadingRotate() {
        progressView.layer.removeAllAnimations()
        self.view.isUserInteractionEnabled = true
        progressView.isHidden = true
    }
}

extension OtaUpdateViewController: OtaClientDelegate {
    func onHaveReleaseVersion(fwReleaseInfo: FwReleaseInfo) {
        self.fwReleaseInfo = fwReleaseInfo
        self.contentLabel.text = String(format: NSLocalizedString("ota_have_new_release", comment: ""), self.fwReleaseInfo!.versionName)
        self.nextButton.isHidden = false
        self.cancelButton.isHidden = false
        self.moreLabel.isHidden = false
        self.stopLoadingRotate()
        self.step = 1
    }
    
    func onNoReleaseVersion() {
        self.contentLabel.text = NSLocalizedString("ota_get_info_error", comment: "")
        self.nextButton.isHidden = false
        self.cancelButton.isHidden = false
        self.stopLoadingRotate()
        self.step = 0
    }
    
    func onDownloadCompleted() {
        self.contentLabel.text = NSLocalizedString("ota_download_complete", comment: "")
        self.nextButton.isHidden = false
        self.cancelButton.isHidden = false
        self.stopLoadingRotate()
        self.step = 2
    }
        
    func onDownloadError() {
        self.contentLabel.text = NSLocalizedString("ota_download_error", comment: "")
        self.nextButton.isHidden = false
        self.cancelButton.isHidden = false
        self.stopLoadingRotate()
        self.step = 1
    }
    
    func onUpdateCompleted() {
        print("[UP]: update completed!")
        otaClient.stop()
        self.delegate?.onUpdateCompleted()
        self.dismiss(animated: true, completion: nil)
    }
    
    func onUpdateError() {
        self.contentLabel.text = NSLocalizedString("ota_update_error", comment: "")
        self.nextButton.isHidden = false
        self.cancelButton.isHidden = false
        self.stopLoadingRotate()
        self.step = 2
    }
}
