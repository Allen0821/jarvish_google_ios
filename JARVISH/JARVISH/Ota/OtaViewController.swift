//
//  OtaViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/5.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import JGProgressHUD

protocol OtaViewControllerDelegate {
    func onOtaDisconnectHelmet()
}

class OtaViewController: UIViewController, OtaManagerDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var otaTextView: UITextView!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    var delegate: OtaViewControllerDelegate?
    
    var model: String?
    var fwVersion: String?
    var language: String?
    var updateInfo: UpdateInfo?
    var latest: Latest?
    
    var jhud: JGProgressHUD?
    var status = 0
    var haveNew = false
    
    var setTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleLabel.text = NSLocalizedString("ota_title", comment: "")
        
        updateButton.layer.cornerRadius = updateButton.frame.height / 2
        updateButton.clipsToBounds = true
        updateButton.isHidden = true
        
        cancelButton.layer.cornerRadius = cancelButton.frame.height / 2
        cancelButton.layer.borderColor = UIColor(named: "colorPrimary")?.cgColor
        cancelButton.layer.borderWidth = 1
        cancelButton.isHidden = true
        
        OtaManager.shared().delegate = self
        jhud = JGProgressHUD(style: .dark)
        jhud?.show(in: self.view)
        
        readFwVersion()
    }
    
    
    private func readFwVersion() {
        print("ota update info \(String(describing: self.model)) \(String(describing: self.language)) \(String(describing: self.fwVersion))")
        self.otaTextView.text =  NSLocalizedString("ota_get_info", comment: "")
        if self.language == nil || self.language!.isEmpty {
            self.language = "86"
        }
        if self.fwVersion == nil || self.fwVersion!.isEmpty {
            self.fwVersion = ""
        }
        if model == nil {
            onNoUpdateInformation()
            return
        }
        let version = OtaRoles.getUpdateVersion(id: self.model!, language: self.language!, fwVer: self.fwVersion!)
        if version.isEmpty {
            onNoUpdateInformation()
            return
        }
        print("version: \(version)")
        let fwArray = version.split(separator: ".")
        if fwArray.count == 4 && self.model != nil && !self.model!.isEmpty {
            print("readFwVersion: \(fwArray[0]) \(fwArray[1]) \(fwArray[2]) \(fwArray[3])")
            //fwArray![0] = "2522"
            OtaManager.shared().checkHelmetUpdateInformation(
                model: self.model!,
                boardInfo: "4",
                fwVersion: String(fwArray[0]),
                langCode: String(fwArray[2]),
                langSubVersion: String(fwArray[3]),
                mcuVersion: String(fwArray[1]),
                cmdModel: "12345")
        } else {
            onNoUpdateInformation()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
   
  
    @IBAction func clickCancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchUpdate(_ sender: UIButton) {
        if status == 0 {
            if !self.haveNew {
                self.dismiss(animated: true, completion: nil)
            }
        } else if status == 1 {
            self.otaTextView.text = NSLocalizedString("ota_download", comment: "")
            jhud?.show(in: self.view)
            OtaManager.shared().downloadUpdateFiles()
            updateButton.isHidden = true
            cancelButton.isHidden = true
        } else if status == 2 {
            self.otaTextView.text = NSLocalizedString("ota_update_helmet", comment: "")
            jhud?.show(in: self.view)
            self.checkWiFi()
            updateButton.isHidden = true
            cancelButton.isHidden = true
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    private func checkWiFiTimer() {
        if setTimer != nil {
            setTimer?.invalidate()
            setTimer = nil
        }
    }
    
    
    private func checkWiFi() {
        HelmetWiFiTool.pingHelmet { result in
            if result {
                self.checkWiFiTimer()
                OtaManager.shared().uploadFile()
            } else {
                self.checkWiFiTimer()
                self.jhud?.dismiss()
                self.updateButton.isHidden = false
                self.cancelButton.isHidden = false
                self.connectHelmetWiFiAlert()
            }
        }
        checkWiFiTimer()
        
        setTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { result in
            self.jhud?.dismiss()
            self.updateButton.isHidden = false
            self.cancelButton.isHidden = false
            self.connectHelmetWiFiAlert()
            })
    }
    
    
    private func connectHelmetWiFiAlert() {
        let alert = UIAlertController(title: NSLocalizedString("helmet_wifi_connection_hint_title", comment: ""), message: NSLocalizedString("helmet_wifi_connection_hint_message", comment: ""), preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
           print("")
        })
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func onUpdateInformation(updateInfo: UpdateInfo) {
        print("onUpdateInformation: \(updateInfo)")
        self.updateInfo = updateInfo
        if let latest = Latest(dictionary: updateInfo.latest) {
            self.latest = latest
            if let releaseNote = latest.releaseNote {
                self.otaTextView.text = String(format: NSLocalizedString("ota_get_info_success", comment: ""), releaseNote)
            } else {
                self.otaTextView.text = String(format: NSLocalizedString("ota_get_info_success", comment: ""), "")
            }
            updateButton.isHidden = false
            cancelButton.isHidden = false
        }
        jhud?.dismiss()
        self.status = 1
        self.haveNew = true
    }
    
    
    func onNoUpdateInformation() {
        self.otaTextView.text = NSLocalizedString("ota_get_info_error", comment: "")
        jhud!.dismiss()
        updateButton.isHidden = false
        cancelButton.isHidden = true
        self.status = 0
        self.haveNew = false
    }
    
    
    func onDownloadUpdateFileCompleted() {
        OtaManager.shared().createChecksumFile()
        self.otaTextView.text = NSLocalizedString("ota_download_complete", comment: "")
        jhud!.dismiss()
        updateButton.isHidden = false
        cancelButton.isHidden = false
        self.status = 2
    }
    
    
    func onDownloadUpdateFileFailure() {
        self.otaTextView.text = NSLocalizedString("ota_download_error", comment: "")
        jhud!.dismiss()
        updateButton.isHidden = false
        cancelButton.isHidden = false
        self.status = 1
    }
    
    
    func onUpdateCompleted() {
        self.delegate?.onOtaDisconnectHelmet()
        self.otaTextView.text = NSLocalizedString("ota_updating_disconnect_message", comment: "")
        jhud!.dismiss()
        updateButton.isHidden = false
        cancelButton.isHidden = true
        self.status = 3
    }
    
    
    func onUpdateFailure() {
        self.otaTextView.text = NSLocalizedString("ota_update_error", comment: "")
        jhud!.dismiss()
        updateButton.isHidden = false
        cancelButton.isHidden = false
        self.status = 2
    }
    
}
