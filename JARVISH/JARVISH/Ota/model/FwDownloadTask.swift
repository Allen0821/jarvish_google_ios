//
//  FwDownloadTask.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/4/7.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseStorage

protocol FwDownloadTaskDelegate {
    func onDownloadCompleted()
    func onDownloadfailure()
}

public class FwDownloadTask: NSObject {
    
    let storageRef = Storage.storage().reference()
    var step = 0
    var storagePath: String!
    
    var delegate: FwDownloadTaskDelegate?
    
    var cmdChecksum: String?
    var mcuChecksum: String?
    var fwChecksum: String?
    
    public override init() {
        super.init()
    }
        
    public func startDownload(storagePath: String) {
        print("[DW]: ### start download ###")
        step = 0
        self.storagePath = storagePath
        _startDownload()
    }
    
    private func _startDownload() {
        switch self.step {
        case 0:
            download(fileName: OtaClient.CMD)
            break
        case 1:
            download(fileName: OtaClient.MCU)
            break
        case 2:
            download(fileName: OtaClient.FW)
            break
        case 3:
            createChecksumFile()
            return
        default:
            self.delegate?.onDownloadfailure()
            return
        }
    }
    
    
    private func download(fileName: String) {
        print("[DW]: download \(fileName)")
        let path = self.storagePath + "/" + fileName
        let fwRef = storageRef.child(path)
        
        let defaultPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let localURL = defaultPath.appendingPathComponent(fileName)
        print("[DW]: localURL: \(localURL)")
        
        fwRef.write(toFile: localURL) { url, error in
            if let error = error {
                print("[DW]: download error: \(error)")
                self.delegate?.onDownloadfailure()
            } else {
                print("[DW]: download success")
                let md5 = Md5Utils.getFileMd5(url: localURL)
                self.verifyChecksum(md5: md5, path: path)
            }
        }
    }
    
    private func verifyChecksum(md5: String, path: String) {
        let fwRef = storageRef.child(path)

        fwRef.getMetadata { metadata, error in
            if let error = error {
                print("[DW]: getMetadata error: \(error)")
                self.delegate?.onDownloadfailure()
            } else {
                //print("xxx metadata md5: \(String(describing: metadata?.md5Hash))")
                let metadataMd5 = Md5Utils.covertHexString(hash: (metadata?.md5Hash)!)
                if md5.elementsEqual(metadataMd5) {
                    switch self.step {
                    case 0:
                        self.cmdChecksum = md5
                        self.step = 1
                        break
                    case 1:
                        self.mcuChecksum = md5
                        self.step = 2
                        break
                    case 2:
                        self.fwChecksum = md5
                        self.step = 3
                        break
                    default:
                        self.delegate?.onDownloadfailure()
                        return
                    }
                    self._startDownload()
                } else {
                    self.delegate?.onDownloadfailure()
                }
            }
        }
            
    }
    
    private func createChecksumFile() {
        print("[DW]: cmd md5: \(String(describing: cmdChecksum))")
        print("[DW]: mcu md5: \(String(describing: mcuChecksum))")
        print("[DW]: fw md5: \(String(describing: fwChecksum))")
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let file = documentsURL.appendingPathComponent(OtaClient.CHECKSUM_FILE).absoluteURL
        if !FileManager.default.fileExists(atPath: file.absoluteString) {
            FileManager.default.createFile(atPath: file.absoluteString, contents: nil, attributes: nil)
        } else {
            print("[DW]: checksum is already created")
        }
        
        do {
            let str = "[SPHOST.BRN] = [\(fwChecksum!)]\n[STM32F411CE_CMD_MD.bin] = [\(cmdChecksum!)]\n[STM32F411CE_MCU_FW.bin] = [\(mcuChecksum!)]"
            try str.write(to: file, atomically: true, encoding: .utf8)
            self.delegate?.onDownloadCompleted()
        } catch {
            print("[DW]: write checksum failed")
            self.delegate?.onDownloadfailure()
        }
    }
    
}
