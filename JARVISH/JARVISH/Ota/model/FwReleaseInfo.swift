//
//  FwReleaseInfo.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/4/1.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseFirestore
import FirebaseFirestoreSwift

struct FwReleaseInfo: Codable {
    var helmetStyle: String
    var languageCode: String
    var versionCode: Int
    var versionName: String
    var note: String
    var path: String
    var create: Timestamp
    
    enum CodingKeys: String, CodingKey {
        case helmetStyle
        case languageCode
        case versionCode
        case versionName
        case note
        case path
        case create
      }
}
