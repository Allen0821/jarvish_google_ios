//
//  FwUpdateTask.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/4/8.
//  Copyright © 2021 jarvish. All rights reserved.
//

protocol FwUpdateTaskDelegate {
    func onUploadCompleted()
    func onUploadFailure()
}

public class FwUpdateTask: NSObject, ICatchUpdateDelegate {
    var delegate: FwUpdateTaskDelegate?
    
    var iCatchManager: ICatchManager!
    var isOtaConnectCamera = false
    var step = 0
    
    public override init() {
        super.init()
        iCatchManager = ICatchManager.shared()!
        iCatchManager?.add(self)
        NotificationCenter.default.addObserver(self, selector: #selector(onFirmwareUploadDone), name: NSNotification.Name("UPDATEFW") , object: nil)
    }
    
    public func stop() {
        if iCatchManager != nil {
            iCatchManager.remove(self)
        }
    }

    public func startUpdate() {
        step = 0
        isOtaConnectCamera = true
        if !iCatchManager.isICatchConnected() {
            iCatchManager.connectToICatchCamera()
        } else {
            upload()
        }
    }
    
    private func upload() {
        var fileName: String!
        switch self.step {
        case 0:
            fileName = OtaClient.CMD
            uploadFile(fileName: fileName)
            return
        case 1:
            fileName = OtaClient.MCU
            uploadFile(fileName: fileName)
            return
        case 2:
            fileName = OtaClient.CHECKSUM_FILE
            uploadFile(fileName: fileName)
            return
        case 3:
            uploadFirmware()
            return
        default:
            self.delegate?.onUploadFailure()
            return
        }
    }
    
    private func uploadFile(fileName: String) {
        print("[UP]: upload \(fileName)")
        if let path = self.getFileLocalPath(fileName: fileName)?.absoluteString {
            self.iCatchManager?.deleteFileWtihPath(self.removeFilePrefix(str: path))
        } else {
            self.delegate?.onUploadFailure()
        }
    }
    
    private func uploadFirmware() {
        print("[UP]: upload firmware")
        if let path = self.getFileLocalPath(fileName: OtaClient.FW)?.absoluteString {
            self.iCatchManager?.updataFirmware(self.removeFilePrefix(str: path))
        } else {
            self.delegate?.onUploadFailure()
        }
    }
    
    private func getFileLocalPath(fileName: String) -> URL? {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            for d in directoryContents {
                if d.absoluteString.contains(fileName) {
                    return d
                }
            }
        } catch {
            print("[UP]: error \(error)")
        }
        return nil
    }
    
    private func removeFilePrefix(str: String) -> String {
        let p = "file:///private"
        let start = str.index(str.startIndex, offsetBy: p.count)
        let end = str.index(str.endIndex, offsetBy: 0)
        let range = start..<end
        return String(str[range])
    }
    
    
    public func onActionCompleted(_ action: ICatchAction) {
        switch action {
        case ActionConnectedToCamera:
            print("[UP]: Connected")
            if isOtaConnectCamera {
                isOtaConnectCamera  = false
                upload()
            }
            break
        case ActionCameraDisconnected:
            print("[UP]: Disconnected")
            self.delegate?.onUploadFailure()
            break
        default:
            print("[UP]: unknown")
            break
        }
    }
    
    public func onUploadCompleted() {
        switch self.step {
        case 0:
            self.step = 1
            break
        case 1:
            self.step = 2
            break
        case 2:
            self.step = 3
            break
        default:
            self.delegate?.onUploadFailure()
            return
        }
        self.upload()
    }
    
    public func onUploadFailure() {
        print("[UP]: upload failure")
        self.delegate?.onUploadFailure()
    }
    
    
    @objc func onFirmwareUploadDone() {
        print("[UP]: firmware upload done")
        DispatchQueue.main.async {
            self.delegate?.onUploadCompleted()
        }
    }
}
