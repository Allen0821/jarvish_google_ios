//
//  Md5Utils.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/4/7.
//  Copyright © 2021 jarvish. All rights reserved.
//

import CommonCrypto

public class Md5Utils {
    
    public static func getFileMd5(url: URL) -> String {
        let bufferSize = 1024 * 1024
        do {
            let file = try FileHandle(forReadingFrom: url)
            defer {
                file.closeFile()
            }

            var context = CC_MD5_CTX()
            CC_MD5_Init(&context)

            while autoreleasepool(invoking: {
                let data = file.readData(ofLength: bufferSize)
                if data.count > 0 {
                    data.withUnsafeBytes {
                        _ = CC_MD5_Update(&context, $0.baseAddress, numericCast(data.count))
                    }
                    return true // Continue
                } else {
                    return false // End of file
                }
            }) { }

            var digest: [UInt8] = Array(repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            _ = CC_MD5_Final(&digest, &context)
            
            let hash = NSMutableString()
            for i in 0..<digest.count{
                hash.appendFormat("%02x", digest[i])
            }
            let result = String(format: hash as String)
            //print("MD5: \(result)")
            return result
        } catch {
            print(error.localizedDescription)
        }
        return ""
    }
    
    public static func covertHexString(hash: String) -> String {
        let decodedData = Data(base64Encoded: hash)
        //print("MD5: \(String(describing: decodedData?.hexEncodedString()))")
        return decodedData?.hexEncodedString() ?? ""
    }
}
