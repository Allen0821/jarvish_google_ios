//
//  OtaClient.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/4/1.
//  Copyright © 2021 jarvish. All rights reserved.
//

import Firebase

protocol OtaClientDelegate {
    func onHaveReleaseVersion(fwReleaseInfo: FwReleaseInfo)
    func onNoReleaseVersion()
    func onDownloadCompleted()
    func onDownloadError()
    func onUpdateCompleted()
    func onUpdateError()
}

public class OtaClient {
    
    public static let CMD = "STM32F411CE_CMD_MD.bin"
    public static let FW = "SPHOST.BRN"
    public static let MCU = "STM32F411CE_MCU_FW.bin"
    public static let CHECKSUM_FILE = "checksum.txt"

    
    var delegate: OtaClientDelegate?
    var boardID: String!
    var countryCode: String!
    
    var serverPath: String!
    
    var fwDownloadTask: FwDownloadTask?
    var fwUpdateTask: FwUpdateTask?
    
    public init(helmetStyle: String, languageCode: String?) {
        boardID = helmetStyle
        countryCode = getCountryCode(languageCode: languageCode)
    }
    
    
    public func getReleaseInformation(currVersion: String?) {
        let db = Firestore.firestore()
        let path = "ota/" + self.boardID + "/" + self.countryCode
        db.collection(path).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    let result = Result {
                        try document.data(as: FwReleaseInfo.self)
                    }
                        
                    switch result {
                    case .success(let releaseInfo):
                        if let releaseInfo = releaseInfo {
                            print("ReleaseInfo: \(releaseInfo)")
                            if self.isLatest(currVersion: currVersion, latest: releaseInfo.versionCode) {
                                self.serverPath = releaseInfo.path
                                self.delegate?.onHaveReleaseVersion(fwReleaseInfo: releaseInfo)
                            } else {
                                self.delegate?.onNoReleaseVersion()
                            }
                        } else {
                            print("Document does not exist")
                        }
                    case .failure(let error):
                        print("Error decoding releaseInfo: \(error)")
                    }
                }
            }
        }
    }
    
    
    public func stop() {
        if fwDownloadTask != nil {
            fwDownloadTask = nil
        }
        
        if fwUpdateTask != nil {
            fwUpdateTask?.stop()
            fwUpdateTask = nil
        }
    }
    
    
    public func downloadFirmware() {
        if fwDownloadTask == nil {
            print("[DW]: init download")
            fwDownloadTask = FwDownloadTask.init()
            fwDownloadTask?.delegate = self
        }
        fwDownloadTask?.startDownload(storagePath: self.serverPath)
    }
    
    
    public func updateHelmet() {
        if fwUpdateTask == nil {
            print("[UP]: init update")
            fwUpdateTask = FwUpdateTask.init()
            fwUpdateTask?.delegate = self
        }
        fwUpdateTask?.startUpdate()
    }
    
    
    private func isLatest(currVersion: String?, latest: Int) -> Bool {
        if currVersion == nil || currVersion!.isEmpty {
            return true
        }
       
        let verName = currVersion!.split(separator: ".")
        if verName.count != 4 {
            return true
        }
        
        if let verCode = Int(verName[0]) {
            print("current verCode: \(verCode)")
            return latest > verCode
        } else {
            return true
        }
    }
    
    public func getCountryCode(languageCode: String?) -> String {
        var code: String!
        if languageCode == nil || languageCode!.isEmpty {
            code = getLocaleLanguageCode()
        } else {
            code = languageCode
        }
        if code.elementsEqual("86") {
            return "tw";
        } else {
            return "ww";
        }
    }
    
    private func getLocaleLanguageCode() -> String {
        let locale = NSLocale.preferredLanguages[0]
        if locale.contains("zh-Hant") {
            return "86"
        } else {
            return "01"
        }
    }
}


extension OtaClient: FwDownloadTaskDelegate, FwUpdateTaskDelegate {
    func onDownloadCompleted() {
        self.delegate?.onDownloadCompleted()
    }
    
    func onDownloadfailure() {
        self.delegate?.onDownloadError()
    }
    
    func onUploadCompleted() {
        self.delegate?.onUpdateCompleted()
    }
    
    func onUploadFailure() {
        self.delegate?.onUpdateError()
    }
}
