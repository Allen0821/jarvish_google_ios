//
//  OtaFciRoles.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/9/14.
//  Copyright © 2020 jarvish. All rights reserved.
//


public class OtaRoles {
    
    public static func getUpdateVersion(id: String, language: String, fwVer: String) -> String {
        if id.elementsEqual("08") && language.elementsEqual("86") {
            if fwVer.isEmpty || !fwVer.contains("2540") {
                return "2522.178.86.1"
            }
            if fwVer.contains("2540") {
                return ""
            }
        }
        
        if ((id.elementsEqual("01") || id.elementsEqual("02") || id.elementsEqual("03") || id.elementsEqual("04") || id.elementsEqual("05") || id.elementsEqual("06")) &&
                language.elementsEqual("86")) {
                    
            if (fwVer.isEmpty ||
                    (!fwVer.contains("712") && !fwVer.contains("530"))) {
                return "702.178.86.01"
            } else {
                return ""
            }
        }
        
        return ""
    }
    
    
    public static func checkBatteryLevel(level: Int) -> Bool {
        if level >= 40 {
            return true
        }
        return false
    }
}
