//
//  UpdateInfo.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/8.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation


struct Latest {
    var cmdModelFileName: String?
    var cmdModelLink: String?
    var cmdModelMD5: String?
    var cmdModelVersion: String?
    var firmwareFileName: String?
    var firmwareLgVersion: String?
    var firmwareLink: String?
    var firmwareMD5: String?
    var firmwareVersion: String?
    var langCode: String?
    var mcuFileName: String?
    var mcuLink: String?
    var mcuMD5: String?
    var mcuVersion: String?
    var releaseNote: String?
    
   
    var dictionary: [String: Any?] {
        return [
            "cmdModelFileName": cmdModelFileName,
            "cmdModelLink": cmdModelLink,
            "cmdModelMD5": cmdModelMD5,
            "cmdModelVersion": cmdModelVersion,
            "firmwareFileName": firmwareFileName,
            "firmwareLgVersion": firmwareLgVersion,
            "firmwareLink": firmwareLink,
            "firmwareMD5": firmwareMD5,
            "langCode": langCode,
            "mcuFileName": mcuFileName,
            "mcuLink": mcuLink,
            "mcuMD5": mcuMD5,
            "mcuVersion" : mcuVersion,
            "releaseNote" : releaseNote
        ]
    }
}


extension Latest: Serializable {
    init?(dictionary: [String : Any?]) {
        let cmdModelFileName = dictionary["cmdModelFileName"] as? String ?? ""
        let cmdModelLink = dictionary["cmdModelLink"] as? String ?? ""
        let cmdModelMD5 = dictionary["cmdModelMD5"] as? String ?? ""
        let cmdModelVersion = dictionary["cmdModelVersion"] as? String ?? ""
        let firmwareFileName = dictionary["firmwareFileName"] as?  String ?? ""
        let firmwareLgVersion = dictionary["firmwareLgVersion"] as?  String ?? ""
        let firmwareLink = dictionary["firmwareLink"] as?  String ?? ""
        let firmwareMD5 = dictionary["firmwareMD5"] as?  String ?? ""
        let langCode = dictionary["langCode"] as?  String ?? ""
        let mcuFileName = dictionary["mcuFileName"] as?  String ?? ""
        let mcuLink = dictionary["mcuLink"] as?  String ?? ""
        let mcuMD5 = dictionary["mcuMD5"] as?  String ?? ""
        let mcuVersion = dictionary["mcuVersion"] as?  String ?? ""
        let releaseNote = dictionary["releaseNote"] as?  String ?? ""
    
        
        self.init(
            cmdModelFileName: cmdModelFileName,
            cmdModelLink: cmdModelLink,
            cmdModelMD5: cmdModelMD5,
            cmdModelVersion: cmdModelVersion,
            firmwareFileName: firmwareFileName,
            firmwareLgVersion: firmwareLgVersion,
            firmwareLink: firmwareLink,
            firmwareMD5: firmwareMD5,
            langCode: langCode,
            mcuFileName: mcuFileName,
            mcuLink: mcuLink,
            mcuMD5: mcuMD5,
            mcuVersion: mcuVersion,
            releaseNote: releaseNote
        )
    }
}


struct UpdateInfo {
    var boardInfo: String?
    var helmetModel: String?
    var helmetModelName: String?
    var isLatest: String?
    var latest: [String: Any]
    
    
    var dictionary: [String: Any?] {
        return [
            "boardInfo": boardInfo,
            "boardInfo": boardInfo,
            "helmetModelName": helmetModelName,
            "isLatest": isLatest,
            "latest": latest
        ]
    }
}


extension UpdateInfo: Serializable {
    init?(dictionary: [String : Any?]) {
        let boardInfo = dictionary["boardInfo"] as? String ?? ""
        let helmetModel = dictionary["helmetModel"] as? String ?? ""
        let helmetModelName = dictionary["helmetModelName"] as? String ?? ""
        let isLatest = dictionary["isLatest"] as? String ?? ""
        let latest = dictionary["latest"] as? [String: Any]
    
        
        self.init(
            boardInfo: boardInfo,
            helmetModel: helmetModel,
            helmetModelName: helmetModelName,
            isLatest: isLatest,
            latest: latest!
        )
    }
}
