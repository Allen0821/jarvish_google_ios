//
//  EditProfileViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/1.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import JGProgressHUD

class EditProfileViewController: UIViewController {
    @IBOutlet weak var topBar: UINavigationBar!
    @IBOutlet weak var editTableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    let hud = JGProgressHUD(style: .dark)
    
    var profile: Profile = Profile()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.editTableView.delegate = self
        self.editTableView.dataSource = self
        self.getProfile()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EmailLoginViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    
    private func getProfile() {
        self.hud.show(in: self.view)
        let memberId: String = UserDefaults.standard.object(forKey: "memberId") as! String
        let session: String = UserDefaults.standard.object(forKey: "session") as! String
       print("memberId: \(memberId) session: \(session)")
        JarvishProvider.rx.request(
            .getUserProfile(session: session, uid: memberId))
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .subscribe(onSuccess: { result in
                let r = result as! [String : Any]
                if let profile = Profile(dictionary: result as! [String : Any]) {
                    print("get profile success: \(profile)")
                    self.profile = profile
                    self.profile.head_hsize = "\(r["head_hsize"] ?? "")"
                    self.profile.head_vsize = "\(r["head_vsize"] ?? "")"
                    self.editTableView.reloadData()
                    self.hud.dismiss(afterDelay: 1.0)
                } else {
                                 /*self.failedAlert(title: NSLocalizedString("sign_in_failure", comment: ""), message: nil)*/
                    self.hud.dismiss(afterDelay: 1.0)
                    
                }
            }) { error in
                print("get profile failure: \(error)")
                self.hud.dismiss(afterDelay: 1.0)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickCancel(_ sender: UIBarButtonItem) {
        self.dismissKeyboard()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickConfirm(_ sender: UIBarButtonItem) {
        self.dismissKeyboard()
        var edit: [EditProfileItem] = []
        edit.append(EditProfileItem.init(field: "user_name", value: self.profile.user_name ?? ""))
        edit.append(EditProfileItem.init(field: "phone1", value: self.profile.phone1 ?? ""))
        edit.append(EditProfileItem.init(field: "city", value: self.profile.city ?? ""))
        edit.append(EditProfileItem.init(field: "head_hsize", value: self.profile.head_hsize ?? ""))
        edit.append(EditProfileItem.init(field: "head_vsize", value: self.profile.head_vsize ?? ""))
        let resulst = edit.map { $0.toDictionary()}
        print("update resulst: \(resulst)")
        let data = try! JSONSerialization.data(withJSONObject: resulst, options: [])
        print("update data: \(data)")
        let memberId: String = UserDefaults.standard.object(forKey: "memberId") as! String
        let session: String = UserDefaults.standard.object(forKey: "session") as! String
        self.hud.textLabel.text = "  " + NSLocalizedString("action_update", comment: "") + "  "
        self.hud.show(in: self.view)
        JarvishProvider.rx.request(
            .updateUserProfile(session: session, uid: memberId, data: data))
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .subscribe(onSuccess: { result in
                let r = result as! [String : Any]
                if let profile = Profile(dictionary: result as! [String : Any]) {
                    self.profile = profile
                    self.profile.head_hsize = "\(r["head_hsize"] ?? "")"
                    self.profile.head_vsize = "\(r["head_vsize"] ?? "")"
                    print("update profile success: \(self.profile)")
                    UserDefaults.standard.set(profile.user_name, forKey: "name")
                    self.editTableView.reloadData()
                    self.hud.dismiss(afterDelay: 1.0)
                    let alert = UIAlertController(title: NSLocalizedString("profile_update_success_title", comment: ""), message: nil, preferredStyle: .alert)
                    alert.overrideUserInterfaceStyle = .dark
                    let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
                        self.updateSuccess()})
                    alert.addAction(confirm)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.hud.dismiss(afterDelay: 1.0)
                    self.updateFailed()
                }
            }) { error in
                print("update profile failure: \(error)")
                self.hud.dismiss(afterDelay: 1.0)
                self.updateFailed()
              
        }
    }
    
    
    private func updateSuccess() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "RELOADUSER"), object: nil, userInfo: nil)
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    
    private func updateFailed() {
        let alert = UIAlertController(title: NSLocalizedString("profile_update_failure_title", comment: ""), message: NSLocalizedString("profile_update_failure_content", comment: ""), preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("confirm")})
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @objc private func clickMeasure() {
        DispatchQueue.main.async(){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(self.getMeasureUrl(), options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(self.getMeasureUrl())
            }
        }
    }
    
    private func getMeasureUrl() -> URL {
        let locale = NSLocale.preferredLanguages[0]
        if locale.contains("zh-Hant") {
            return URL(string: "https://drive.google.com/uc?export=view&id=17NyhN27sOaIWX4uGWr7MsxKN0A11fyY9")!
        } else {
            return URL(string: "https://drive.google.com/uc?export=view&id=1U1oq6wt_1pRlVSXAwHHYQiiL5U5W7iot")!
        }
    }
    
}


extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditViewCell", for: indexPath) as! EditViewCell
        cell.profile = self.profile
        cell.nameTextField.delegate = self
        cell.phoneTextField.delegate = self
        cell.cityTextField.delegate = self
        cell.hsizeTextField.delegate = self
        cell.vsizeTextField.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickMeasure))
        cell.measureLabel.isUserInteractionEnabled = true
        cell.measureLabel.addGestureRecognizer(tap)
        return cell
    }
    
}


extension EditProfileViewController: UITextFieldDelegate {
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 31 {
            self.profile.user_name = textField.text
        } else if textField.tag == 32 {
            self.profile.phone1 = textField.text
        } else if textField.tag == 33 {
            self.profile.city = textField.text
        } else if textField.tag == 34 {
            self.profile.head_hsize = textField.text
        } else if textField.tag == 35 {
            self.profile.head_vsize = textField.text
        }
    }
}
