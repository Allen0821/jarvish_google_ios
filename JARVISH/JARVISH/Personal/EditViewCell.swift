//
//  EditViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/1.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class EditViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var nameTextField: CustomPaddingTextField!
    @IBOutlet weak var phoneTextField: CustomPaddingTextField!
    @IBOutlet weak var cityTextField: CustomPaddingTextField!
    @IBOutlet weak var hsizeTextField: CustomPaddingTextField!
    @IBOutlet weak var vsizeTextField: CustomPaddingTextField!
    @IBOutlet weak var measureLabel: UILabel!
    
    
    
    var profile: Profile! {
        didSet {
            self.nameTextField.text = profile.user_name
            self.phoneTextField.text = profile.phone1
            self.cityTextField.text = profile.city
            self.hsizeTextField.text = profile.head_hsize
            self.vsizeTextField.text = profile.head_vsize
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.nameTextField.layer.borderColor = UIColor.white.cgColor
        self.nameTextField.layer.borderWidth = 0.7
        self.phoneTextField.layer.borderColor = UIColor.white.cgColor
        self.phoneTextField.layer.borderWidth = 0.7
        self.cityTextField.layer.borderColor = UIColor.white.cgColor
        self.cityTextField.layer.borderWidth = 0.7
        self.hsizeTextField.layer.borderColor = UIColor.white.cgColor
        self.hsizeTextField.layer.borderWidth = 0.7
        self.vsizeTextField.layer.borderColor = UIColor.white.cgColor
        self.vsizeTextField.layer.borderWidth = 0.7
       
    }

   

}
