//
//  ManualViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/18.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class ManualViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: CustomPaddingLabel!
    
    var name: String! {
        didSet {
            self.nameLabel.text = name
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLabel.padding(20, 20, 16, 16)
    }


}
