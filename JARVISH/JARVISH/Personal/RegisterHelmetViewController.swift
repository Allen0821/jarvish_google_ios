//
//  RegisterHelmetViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/11/6.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import Moya

class RegisterHelmetViewController: UIViewController, QrCodeScanViewControllerDelegate {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    
    @IBOutlet weak var topBar: UINavigationBar!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var SerialTextField: CustomPaddingTextField!
    @IBOutlet weak var qrcodeImageView: UIImageView!
    @IBOutlet weak var loadingImageView: UIImageView!
    @IBOutlet weak var registerButton: UIButton!
    
    var setTimer: Timer?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        topBar.topItem?.title = NSLocalizedString("helmet_register_title", comment: "")
        
        self.loadingImageView.isHidden = true
        self.loadingImageView.tintColor = Utility.getTintColor()
        titleLabel.text = NSLocalizedString("helmet_register_guide", comment: "")
        
        self.SerialTextField.layer.borderColor = UIColor.white.cgColor
        self.SerialTextField.layer.borderWidth = 0.7
        self.SerialTextField.delegate = self
        
        self.registerButton.layer.cornerRadius = self.registerButton.frame.size.height/2
        self.registerButton.clipsToBounds = true
        
        qrcodeImageView.layer.cornerRadius = 5
        qrcodeImageView.layer.masksToBounds = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openScanQrCode))
        qrcodeImageView.isUserInteractionEnabled = true
        qrcodeImageView.addGestureRecognizer(tapGestureRecognizer)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func enterNumberAlert(content: String) {
        let alert = UIAlertController(title: content, message: nil, preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: nil)
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func clickBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickRegister(_ sender: UIButton) {
        if Utility.isValidSerialNumber(number: SerialTextField.text) {
            startLoadingRotate()
            registerHelmet(number: SerialTextField.text!)
        } else {
            enterNumberAlert(content: NSLocalizedString("helmet_enter_serial_number_invalid", comment: ""))
            
        }
    }
    
    @objc func openScanQrCode() {
        DispatchQueue.main.async(){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "QrCodeScanViewController") as! QrCodeScanViewController
            vc.modalPresentationStyle = .fullScreen
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func OnQrCodeContent(content: String) {
        SerialTextField.text = content
        self.view.endEditing(true)
    }
    
    private func registerHelmet(number: String) {
        print("register helmet serial number: \(number)")
        let memberId: String = UserDefaults.standard.object(forKey: "memberId") as! String
        let session: String = UserDefaults.standard.object(forKey: "session") as! String
        
        let helmet = RegisteHelmet.init(memberId: Int(memberId) ?? 0, serialNumber: number)
        let data = try! JSONSerialization.data(withJSONObject: helmet.toDictionary(), options: [])
        print("update data: \(data)")
        
        //self.hud.show(in: self.view)
        JarvishProvider.rx.request(.registerHelmet(session: session, data: data))
        .debug()
        .filterSuccessfulStatusCodes()
        .mapJSON()
            .subscribe(onSuccess: { result in
                print("registerHelmet: \(result)")
                self.stopLoadingRotate()
                self.registerResultAlert(title: NSLocalizedString("helmet_register_success_title", comment: ""), result: true)
            }) { error in
                print("registerHelmet error: \(error)")
                self.stopLoadingRotate()
                do {
                     let errorResponse = error as? Moya.MoyaError
                     if let body = try errorResponse?.response?.mapJSON(){
                        let response = body as! [String : Any]
                        let errorCode = response["errorCode"] as! String
                        if errorCode.elementsEqual("04-301") {
                            self.registerResultAlert(title: NSLocalizedString("helmet_register_fail_invalid", comment: ""), result: false)
                            return
                        } else if errorCode.elementsEqual("04-302") {
                            self.registerResultAlert(title: NSLocalizedString("helmet_register_fail_been_registered", comment: ""), result: false)
                            return
                        }
                     }
                } catch {
                     print(error)
                }
                self.registerResultAlert(title: NSLocalizedString("helmet_register_fail_title", comment: ""), result: false)
        }
    }
   

    private func registerResultAlert(title: String, result: Bool) {
        //self.hud.dismiss()
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
            if result {
                self.dismiss(animated: true, completion: nil)
            }
        })
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func startLoadingRotate() {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.toValue = Double.pi * 2
        rotateAnimation.duration = 1
        rotateAnimation.repeatCount = .infinity
        self.loadingImageView.layer.add(rotateAnimation, forKey: nil)
        self.view.isUserInteractionEnabled = false
        self.loadingImageView.isHidden = false
    }
    
    
    func stopLoadingRotate() {
        self.loadingImageView.layer.removeAllAnimations()
        self.view.isUserInteractionEnabled = true
        self.loadingImageView.isHidden = true
    }

}


extension RegisterHelmetViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
   
}
