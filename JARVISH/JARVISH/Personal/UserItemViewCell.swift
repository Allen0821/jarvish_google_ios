//
//  UserItemViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/19.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class UserItemViewCell: UITableViewCell {
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var funcImageView: UIImageView!
    
    
    var itemImage: UIImage! {
        didSet {
            itemImageView.image = itemImage
        }
    }
    
    
    var itemTitle: String! {
        didSet {
            itemLabel.text = itemTitle
        }
    }
    
    
    var funcImage: UIImage! {
        didSet {
            funcImageView.image = funcImage
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
