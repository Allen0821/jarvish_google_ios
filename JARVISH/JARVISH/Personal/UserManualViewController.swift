//
//  UserManualViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/18.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class UserManualViewController: UIViewController {
    @IBOutlet weak var userManualTableView: UITableView!
    @IBOutlet weak var topBar: UINavigationBar!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           if #available(iOS 13, *) {
               return .lightContent
           } else {
               return .default
           }
       }
       
       override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
           return .portrait
       }
       
       override var shouldAutorotate: Bool {
           return true
       }
    
    var manuals: [UserManual] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        topBar.topItem?.title = NSLocalizedString("personal_user_manual", comment: "")
        manuals.removeAll()
        manuals.append(contentsOf: ManualSetting.getUserGuideLink())
        userManualTableView.delegate = self
        userManualTableView.dataSource = self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func touchBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
 
}



extension UserManualViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        manuals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ManualViewCell", for: indexPath) as! ManualViewCell
        cell.name = manuals[indexPath.row].name
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let url = URL(string: manuals[indexPath.row].link!) {
            DispatchQueue.main.async(){
                if #available(iOS 10.0, *) {
                   UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                   UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
}
