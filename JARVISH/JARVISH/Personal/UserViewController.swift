//
//  UserViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/19.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseAuth
import SwiftUI

class UserViewController: UIViewController {
    @IBOutlet weak var topBar: UINavigationBar!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userTableView: UITableView!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    var isLoaded: Bool = false
    
    
    private let APP_UNITS: String = "APP_UNITS"
    private let USER_MANUAL: String = "USER_MANUAL"
    private let PRIVACY_POLICY: String = "PRIVACY_POLICY"
    //private let VERSION: String = "VERSION"
    private let EDIT_PROFILE: String = "EDIT_PROFILE"
    private let ACCOUNT_INFORMATION: String = "ACCOUNT_INFORMATION"
    private let REGISTER_HELMET: String = "REGISTER_HELMET"
    private let SIGN_OUT: String = "SIGN_OUT"
    
    var items: [String] = []
    var profile: UserProfile?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        /*userView.layer.borderWidth = 0.3
        userView.layer.borderColor = Utility.getTintColor().cgColor
        userView.layer.cornerRadius = 16
        userView.layer.masksToBounds = true*/
        
        userAvatarImageView.layer.borderWidth = 0.5
        userAvatarImageView.layer.masksToBounds = false
        userAvatarImageView.layer.borderColor = Utility.getTintColor().cgColor
        userAvatarImageView.layer.cornerRadius = userAvatarImageView.frame.height/2
        userAvatarImageView.clipsToBounds = true
        
        self.userTableView.dataSource = self
        self.userTableView.delegate = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadLoginFunction),
                                               name: NSNotification.Name("RELOADUSER") , object: nil)
        
        //initViewItem()
        setViewItem()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile), name: NSNotification.Name("PROFILEUPDATED") , object: nil)
        
    }
    
    
    /*private func initViewItem() {
        print("xxx initViewItem")
        items.removeAll()
        items.append(APP_UNITS)
        items.append(USER_MANUAL)
        items.append(PRIVACY_POLICY)
        items.append(VERSION)
        if isLogin() {
            items.append(EDIT_PROFILE)
            items.append(REGISTER_HELMET)
            items.append(SIGN_OUT)
            userNameLabel.text = UserDefaults.standard.string(forKey: "name")
                let avatar = UserDefaults.standard.string(forKey: "avatar")
                userAvatarImageView.sd_setImage(with:
                    URL(string: avatar!),
                    placeholderImage: UIImage(named: "user"))
            userAvatarImageView.isHidden = false
            userNameLabel.isHidden = false
            signinButton.isHidden = true
        } else {
            userAvatarImageView.isHidden = true
            userNameLabel.isHidden = true
            signinButton.isHidden = false
        }
    }*/
    
    @objc private func updateProfile(user: User) {
        Auth.auth().currentUser?.reload(completion: { error in
            if error == nil {
                self.queryProfile(user: Auth.auth().currentUser!)
            }
        })
    }
    
    private func queryProfile(user: User) {
        
        self.userNameLabel.text = user.displayName
        self.userAvatarImageView.sd_setImage(
            with: user.photoURL,
            placeholderImage: UIImage(named: "user"))
        
        let storeProfile = StoreProfile()
        storeProfile.getDocument(documentID: user.uid) { profile in
            if let profile = profile {
                self.profile = profile
                print("queryProfile: \(String(describing: self.profile?.name))")
                self.userNameLabel.text = self.profile!.name
                self.userAvatarImageView.sd_setImage(
                    with: URL(string: self.profile!.avatarUrl ?? ""),
                    placeholderImage: UIImage(named: "user"))
            }
        }
        
    }
    
    /*private func setProfile(user: User) {
        self.userNameLabel.text = user.displayName
        self.userAvatarImageView.sd_setImage(
            with: user.photoURL,
            placeholderImage: UIImage(named: "user"))
    }*/
    
    private func setViewItem() {
        Auth.auth().addStateDidChangeListener { (auth, user) in
            print("Auth user: \(String(describing: user))")
            self.items.removeAll()
            if user != nil {
                self.items.append(self.EDIT_PROFILE)
                self.items.append(self.ACCOUNT_INFORMATION)
                self.items.append(self.REGISTER_HELMET)
                self.items.append(self.APP_UNITS)
                self.items.append(self.USER_MANUAL)
                self.items.append(self.PRIVACY_POLICY)
                //self.items.append(self.VERSION)
                self.items.append(self.SIGN_OUT)
                self.queryProfile(user: user!)
                self.userAvatarImageView.isHidden = false
                self.userNameLabel.isHidden = false
                self.signinButton.isHidden = true
            } else {
                self.userAvatarImageView.isHidden = true
                self.userNameLabel.isHidden = true
                self.signinButton.isHidden = false
                self.items.append(self.APP_UNITS)
                self.items.append(self.USER_MANUAL)
                self.items.append(self.PRIVACY_POLICY)
                //self.items.append(self.VERSION)
            }
            self.userTableView.reloadData()
        }
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        print("version \(version)")
        print("version \(String.localizedStringWithFormat(NSLocalizedString("personal_version", comment: ""), version))")
        self.versionLabel.text = String.localizedStringWithFormat(NSLocalizedString("personal_version", comment: ""), version)
    }
    
    
    /*private func isLogin() -> Bool {
        if UserDefaults.standard.object(forKey: "memberId") == nil ||
            UserDefaults.standard.object(forKey: "session") == nil ||
            UserDefaults.standard.object(forKey: "expired") == nil {
            return false
        }
        let login: Date = UserDefaults.standard.object(forKey: "expired") as! Date
        if login.timeIntervalSince1970.distance(to: Date().timeIntervalSince1970) >
            60*60*24*2 {
            clearLogin()
            return false
        }
        return true
    }*/
    
    
    private func clearLogin() {
        UserDefaults.standard.removeObject(forKey: "memberId")
        UserDefaults.standard.removeObject(forKey: "session")
        UserDefaults.standard.removeObject(forKey: "expired")
    }
    

    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")
        isLoaded = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc private func reloadLoginFunction() {
        //initViewItem()
        self.userTableView.reloadData()
        /*initViewItem()
        if UserDefaults.standard.object(forKey: "memberId") != nil ||
        UserDefaults.standard.object(forKey: "session") != nil {
            userNameLabel.text = UserDefaults.standard.string(forKey: "name")
            let avatar = UserDefaults.standard.string(forKey: "avatar")
            userAvatarImageView.sd_setImage(with: URL(string: avatar!), placeholderImage: UIImage(named: "placeholder.png"))
        }*/
    }
    
    
    @IBAction func clickSignin(_ sender: UIButton) {
        /*DispatchQueue.main.async(){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }*/
        /*let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        print("[auth]: clickSignin")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "OPENAUTH"), object: nil, userInfo: nil)*/
        let viewCtrl = AuthViewController()
        viewCtrl.modalPresentationStyle = .fullScreen
        self.present(viewCtrl, animated: true, completion: nil)
    }
    
    private func editUserProfile() {
        /*if isLogin() {
            DispatchQueue.main.async(){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: NSLocalizedString("sign_in_expired", comment: ""), message: NSLocalizedString("sign_in_again", comment: ""), preferredStyle: .alert)
            alert.overrideUserInterfaceStyle = .dark
            let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: {(alert: UIAlertAction!) in
                self.clearLogin()
                self.reloadLoginFunction()
            })
            alert.addAction(confirm)
            self.present(alert, animated: true, completion: nil)
        }*/
        DispatchQueue.main.async(){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    private func openWebView() {
        DispatchQueue.main.async(){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            vc.titleString = NSLocalizedString("personal_policy", comment: "")
            vc.url = ManualSetting.getPrivacyPolicyLink()
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    private func scanQrCode() {
        DispatchQueue.main.async(){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterHelmetViewController") as! RegisterHelmetViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    private func signoutAccount() {
        let alert = UIAlertController(title: NSLocalizedString("sign_out", comment: ""), message: nil, preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: {(alert: UIAlertAction!) in
            //self.clearLogin()
            //self.reloadLoginFunction()
            do {
                try Auth.auth().signOut()
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
        })
        alert.addAction(confirm)
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func setUnits() {
        let alert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        alert.overrideUserInterfaceStyle = .dark
        alert.setValue(Utility.detAlertTitleAttributed(title: NSLocalizedString("personal_app_units", comment: "")), forKey: "attributedTitle")
        let metric = UIAlertAction(title: NSLocalizedString("personal_app_units_metric", comment: ""), style: .default) {(_) in
            UserDefaults.standard.set(0, forKey: "app_units")
            WeatherManager.shared().setUnits(index: 0)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TRACKCHANGED"), object: nil, userInfo: nil)
        }
        let imperial = UIAlertAction(title: NSLocalizedString("personal_app_units_imperial", comment: ""), style: .default) {(_) in
            UserDefaults.standard.set(1, forKey: "app_units")
            WeatherManager.shared().setUnits(index: 1)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "TRACKCHANGED"), object: nil, userInfo: nil)
        }
        if (UserDefaults.standard.integer(forKey: "app_units") == 0) {
            metric.setValue(Utility.getTintColor(), forKey: "titleTextColor")
        } else {
            imperial.setValue(Utility.getTintColor(), forKey: "titleTextColor")
        }
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("Cancel")})
        
        alert.addAction(metric)
        alert.addAction(imperial)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
}


extension UserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tag: String = self.items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserItemViewCell", for: indexPath) as! UserItemViewCell
    
        if tag.elementsEqual(APP_UNITS) {
            cell.itemTitle = NSLocalizedString("personal_app_units", comment: "")
            cell.itemImageView.image = UIImage(named: "distanceUnits")
        } else if tag.elementsEqual(REGISTER_HELMET) {
            cell.itemTitle = NSLocalizedString("personal_register_helmet", comment: "")
            cell.itemImageView.image = UIImage(named: "registerHelmet")
        } else if tag.elementsEqual(USER_MANUAL) {
            cell.itemTitle = NSLocalizedString("personal_user_manual", comment: "")
            cell.itemImageView.image = UIImage(named: "userManual")
        } else if tag.elementsEqual(EDIT_PROFILE) {
            cell.itemTitle = NSLocalizedString("personal_edit_profile", comment: "")
            cell.itemImageView.image = UIImage(named: "editProfile")
        } else if tag.elementsEqual(ACCOUNT_INFORMATION) {
            cell.itemTitle = NSLocalizedString("personal_account_info", comment: "")
            cell.itemImageView.image = UIImage(named: "version")
        } else if tag.elementsEqual(PRIVACY_POLICY) {
            cell.itemTitle = NSLocalizedString("personal_policy", comment: "")
            cell.itemImageView.image = UIImage(named: "privacyPolicy")
        } /*else if tag.elementsEqual(VERSION) {
            let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            print("version \(version)")
            cell.itemTitle = String.localizedStringWithFormat(NSLocalizedString("personal_version", comment: ""), version)
            cell.itemImageView.image = UIImage(named: "version")
            cell.funcImageView.isHidden = true
        }*/ else {
            cell.itemTitle = NSLocalizedString("personal_sign_out", comment: "")
            cell.itemImageView.image = UIImage(named: "logout")
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tag: String = self.items[indexPath.row]
        if tag.elementsEqual(EDIT_PROFILE) {
            //self.editUserProfile()
            DispatchQueue.main.async(){
                /*let storyboard = UIStoryboard(name: "ProfileEditStoryboard", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ProfileEditViewController") as! ProfileEditViewController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)*/
                let viewCtrl = ProfileEditHostingViewController(
                    avatar: self.userAvatarImageView.image!,
                    name: self.userNameLabel.text ?? "")
                viewCtrl.modalPresentationStyle = .fullScreen
                self.present(viewCtrl, animated: true, completion: nil)
            }
        } else if tag.elementsEqual(ACCOUNT_INFORMATION) {
            DispatchQueue.main.async(){
                let viewCtrl = AccountViewController()
                viewCtrl.modalPresentationStyle = .fullScreen
                self.present(viewCtrl, animated: true, completion: nil)
            }
        } else if tag.elementsEqual(REGISTER_HELMET) {
            //self.scanQrCode()
            DispatchQueue.main.async(){
                let viewCtrl = ProductRegistrationViewController()
                viewCtrl.modalPresentationStyle = .fullScreen
                self.present(viewCtrl, animated: true, completion: nil)
            }
        } else if tag.elementsEqual(USER_MANUAL) {
            DispatchQueue.main.async(){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserManualViewController") as! UserManualViewController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        } else if tag.elementsEqual(APP_UNITS) {
            DispatchQueue.main.async() {
                self.setUnits()
            }
        } else if tag.elementsEqual(PRIVACY_POLICY) {
            self.openWebView()
        } else if tag.elementsEqual(SIGN_OUT) {
            self.signoutAccount()
        }
    }
    
}
