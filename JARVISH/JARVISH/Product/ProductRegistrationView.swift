//
//  ProductRegistration.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/6.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

struct RegistrationItem: View {
    
    var item: Int
    @State var tag: Int? = nil
    
    
    func getTitle() -> LocalizedStringKey {
        if item == 0 {
            return LocalizedStringKey("product_register_new")
        } else {
            return LocalizedStringKey("product_view_registered")
        }
    }
    
    var body: some View {
        HStack {
            Text(getTitle())
            Spacer()
            Image(systemName: "chevron.right")
        }
        .onTapGesture {
            tag = item
        }
        .padding(.vertical, 12.0)
        .background(
            NavigationLink(
            destination: {
                VStack {
                    if item == 0 {
                        RegisterProductView()
                    } else if item == 1 {
                        RegisteredProductsView()
                    } else {
                        EmptyView()
                    }
                }

            }(), tag: item, selection: $tag){
                EmptyView()
           }
           .hidden())        
    }
}

struct ProductRegistrationView: View {
    var dismiss: (() -> Void)!
    let items = [0, 1]
    
    var body: some View {
        NavigationView {
            VStack {
                List(items, id: \.self) { item in
                    RegistrationItem(item: item)
                }
            }
            .listStyle(GroupedListStyle())
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: dismiss) {
                                           Image(systemName: "chevron.left")
                                       }
            )
            .navigationTitle("product_registration")
        
        }
    }
}

struct ProductRegistration_Previews: PreviewProvider {
    static var previews: some View {
        ProductRegistrationView()
    }
}
