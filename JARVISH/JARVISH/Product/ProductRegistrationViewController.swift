//
//  ProductRegistrationViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/6.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

class ProductRegistrationViewController: UIHostingController<ProductRegistrationView> {
    init() {
        super.init(rootView: ProductRegistrationView())
        rootView.dismiss = dismiss
    }
    
    @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func dismiss() {
        dismiss(animated: true, completion: nil)
    }
}
