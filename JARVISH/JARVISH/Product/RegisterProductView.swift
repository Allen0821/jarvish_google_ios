//
//  RegisterProduct.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/6.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI


struct QRcodeScanner: UIViewControllerRepresentable {
    typealias UIViewControllerType = QrCodeScanViewController
    @ObservedObject var viewModel: RegistrationViewModel
    
    init(viewModel: RegistrationViewModel) {
        self.viewModel = viewModel
    }
    
    func makeUIViewController(context: Context) -> QrCodeScanViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QrCodeScanViewController") as! QrCodeScanViewController
        vc.delegate = makeCoordinator()
        return vc
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: QrCodeScanViewControllerDelegate {
        
        let parent: QRcodeScanner

        init(_ parent: QRcodeScanner) {
            self.parent = parent
        }
        
        func OnQrCodeContent(content: String) {
            print("OnQrCodeContent \(content)")
            parent.viewModel.serialNumber = content
        }
        
        
    }
}

struct RegisterProductView: View {
    @Environment(\.presentationMode) private var presentation
    @ObservedObject var viewModel = RegistrationViewModel()
    @State private var isPresented = false

    
    var body: some View {
        ZStack {
            VStack {
                Text("product_register_new_description")
                    .font(.body)
                    .fontWeight(.regular)
                    .multilineTextAlignment(.leading)
                    .padding(.horizontal, 8.0)
                HStack {
                    TextField("", text: $viewModel.serialNumber)
                        .lineLimit(1)
                        .foregroundColor(.white)
                        .padding(.trailing, 8.0)
                    Image(systemName: "qrcode.viewfinder")
                        .resizable()
                        .frame(width: 24, height: 24)
                        .foregroundColor(.white)
                        .onTapGesture {
                            isPresented.toggle()
                        }
                        
                }
                .padding(.all, 12.0)
                .background(Color("colorTextFiledBG"))
                .cornerRadius(40.0)
                .padding(.top, 24.0)
                Button(action: {
                    hideKeyboard()
                    viewModel.onRegister()
                }) {
                    Spacer()
                    Text("product_action_register")
                    .font(.body)
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                    Spacer()
                }
                .padding(.vertical, 12.0)
                .background(Color("colorPrimary"))
                .cornerRadius(40.0)
                .padding(.top, 4.0)
                
                Spacer()
            }
            .padding(.horizontal, 40.0)
            .padding(.top, 48.0)
            
            if viewModel.isProgress {
                ProgressView()
            }
        }
        .navigationTitle("product_register_new")
        .navigationBarTitleDisplayMode(.inline)
        .fullScreenCover(isPresented: $isPresented, content: {
            QRcodeScanner(viewModel: viewModel)
        })
        .alert(isPresented: $viewModel.isShowAlert, content: {
            return Alert(title: Text(LocalizedStringKey(viewModel.alertMessage)))
        })
    }
    
   
}

struct RegisterProduct_Previews: PreviewProvider {
    static var previews: some View {
        RegisterProductView()
    }
}
