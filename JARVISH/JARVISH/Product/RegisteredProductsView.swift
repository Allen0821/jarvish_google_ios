//
//  RegisteredProducts.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/6.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

struct RegisteredProductsView: View {
    @ObservedObject var viewModel = RegistrationViewModel()
    
    var body: some View {
        ZStack {
            List(viewModel.registers) { register in
                HStack {
                    VStack(alignment: .leading, spacing: 8.0) {
                        Text("product_registered_serial_number \(register.serial)")
                            .multilineTextAlignment(.leading)
                        Text("product_registered_date \(viewModel.getRegisterDate(timestamp: register.date))")
                            .multilineTextAlignment(.leading)
                    }
                    Spacer()
                }
                .padding(.all, 8.0)
                .overlay(
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color("colorDivide"), lineWidth: 0.5)
                )
            }
            .listStyle(PlainListStyle())
        }
        .navigationTitle("product_view_registered")
        .navigationBarTitleDisplayMode(.inline)
        .onAppear(perform: {
            viewModel.queryRegisteredProducts()
        })
    }
}

struct RegisteredProducts_Previews: PreviewProvider {
    static var previews: some View {
        RegisteredProductsView()
    }
}
