//
//  RegistrationViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/7/6.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseFirestore
import FirebaseAuth

class RegistrationViewModel: ObservableObject {
    
    @Published var serialNumber: String = ""
    @Published var registers: [Register] = []
    @Published var isProgress: Bool = false
    @Published var isShowAlert: Bool = false
    var alertMessage: String = ""
    
    let storeRegister = StoreRegister()
    
    
    func onRegister() {
        if storeRegister.isValidSerial(serial: serialNumber) {
            isSerialNumberInUse()
        } else {
            alertMessage = "code_invalid_serial_number"
            isShowAlert = true
        }
    }
    
    private func isSerialNumberInUse() {
        self.isProgress = true
        storeRegister.getRegisterWithID(documentID: serialNumber) { data in
            if let register = data {
                print("already in use \(register)")
                self.isProgress = false
                self.alertMessage = "code_serial_number_in_use"
                self.isShowAlert = true
            } else {
                self.uploadRegister()
            }
            
        }
       
    }
    
    private func uploadRegister() {
        let uid = Auth.auth().currentUser?.uid
       
        let register = Register(
        serial: serialNumber,
        uid: uid!,
        name: "",
        category: 1,
        expired: false)
        
        
        storeRegister.createRegister(documentID: serialNumber, register: register) { result in
            self.isProgress = false
            if result {
                self.alertMessage = "code_register_serial_number_ok"
            } else {
                self.alertMessage = "code_register_serial_number_failure"
            }
            self.isShowAlert = true
        }
    }
    
    func queryRegisteredProducts() {
        let uid = Auth.auth().currentUser?.uid
        
        storeRegister.queryRegisteredProducts(uid: uid!) { registers in
            if registers != nil {
                self.registers.removeAll()
                self.registers = registers!
            }
        }
    }
    
    func getRegisterDate(timestamp: Timestamp?) -> String {
        if timestamp == nil {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: timestamp!.dateValue())
    }
}
