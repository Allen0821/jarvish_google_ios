//
//  QrCodeScanViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/19.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import AVFoundation
import SafariServices

protocol QrCodeScanViewControllerDelegate {
    func OnQrCodeContent(content: String)
}


class QrCodeScanViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cameraView: UIView!
    
    var captureSesion: AVCaptureSession?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var delegate: QrCodeScanViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(captureSesion?.isRunning == false){
            captureSesion?.startRunning()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setQRCodeScan()
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if(captureSesion?.isRunning == true){
            captureSesion?.stopRunning()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    private func setQRCodeScan() {
        captureSesion = AVCaptureSession()
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {return}
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        }catch let error {
            print(error)
            return
        }
        if (captureSesion?.canAddInput(videoInput) ?? false ){
            captureSesion?.addInput(videoInput)
        } else {
            return
        }
        
        let metaDataOutput = AVCaptureMetadataOutput()
        if (captureSesion?.canAddOutput(metaDataOutput) ?? false){
            captureSesion?.addOutput(metaDataOutput)
            metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metaDataOutput.metadataObjectTypes = [.qr, .ean8 , .ean13 , .pdf417]
        } else {
            return
        }
           
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSesion!)
        previewLayer!.videoGravity = .resizeAspectFill
        //previewLayer!.frame = cameraView.layer.frame
        previewLayer!.frame = self.cameraView.layer.bounds
        cameraView.layer.addSublayer(previewLayer!)
        
        /*let size = 100
        let sWidth = Int(cameraView.frame.size.width)
        let xPos = (sWidth/2)-(size/2)
        let scanRect = CGRect(x: CGFloat(xPos), y: 100 , width: CGFloat(size) , height: CGFloat(size))
        //設定scan Area window 框框
        let scanAreaView = UIView()
        scanAreaView.layer.borderColor = UIColor.red.cgColor
        scanAreaView.layer.borderWidth = 2
        scanAreaView.frame = scanRect
        cameraView.addSubview(scanAreaView)
        cameraView.bringSubviewToFront(scanAreaView)*/
        
        captureSesion?.startRunning()
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSesion?.startRunning()
        if let metadataObject = metadataObjects.first{
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else {return}
            guard let stringValue = readableObject.stringValue else {return}
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            print("qrcode: \(stringValue)")
            self.delegate?.OnQrCodeContent(content: stringValue)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func touchCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
