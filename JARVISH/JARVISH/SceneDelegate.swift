//
//  SceneDelegate.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/2/21.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import SwiftUI
import Firebase
import FBSDKCoreKit
import GoogleSignIn

class SceneDelegate: UIResponder, UIWindowSceneDelegate, GIDSignInDelegate {
    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        /*guard let _ = (scene as? UIWindowScene) else { return }
        if UserDefaults.standard.object(forKey: "memberId") == nil ||
            UserDefaults.standard.object(forKey: "session") == nil {
            print("User is not signed in")
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.window!.rootViewController = viewController
            self.window!.makeKeyAndVisible()
        } else {
          
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(auth), name: NSNotification.Name("AUTHCHANGED"), object: nil)*/
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        
        /*if Auth.auth().currentUser != nil {
            print("login")
        } else {
            let viewCtrl = UIHostingController(rootView: AuthView())
            self.window!.rootViewController = viewCtrl
            self.window?.makeKeyAndVisible()
        }*/
        
        /*Auth.auth().addStateDidChangeListener { (auth, user) in
          
            if user != nil {
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                self.window!.rootViewController = viewController
                self.window?.makeKeyAndVisible()
            } else {
                let viewCtrl = UIHostingController(rootView: AuthView())
                self.window!.rootViewController = viewCtrl
                self.window?.makeKeyAndVisible()
            }
        }*/

    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        if GIDSignIn.sharedInstance().handle(URLContexts.first?.url) {
            return
        }
        
        guard let url = URLContexts.first?.url else {return}
            ApplicationDelegate.shared.application( UIApplication.shared, open: url, sourceApplication: nil, annotation: [UIApplication.OpenURLOptionsKey.annotation] )
    }
    
    
    /* implements the GIDSignInDelegate protocol */
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("[Auth]: google sign in error: \(error.localizedDescription)")
            return
        }
        guard let authentication = user.authentication else { return }
        UserDefaults.standard.set(authentication.idToken, forKey: "GIDToken")
        UserDefaults.standard.set(authentication.accessToken, forKey: "GAccessToken")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "GOOGLEAUTH"), object: nil)
    }
        
    
    @objc func auth() {
        print("AUTHCHANGED")
        if UserDefaults.standard.object(forKey: "memberId") == nil ||
            UserDefaults.standard.object(forKey: "session") == nil {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.window!.rootViewController = viewController
            self.window!.makeKeyAndVisible()
        }
    }
 

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        if let incomingURL = userActivity.webpageURL {
            print("Incoming URL is \(String(describing: UserDefaults.standard.string(forKey: "AuthLink")))")
            let email = UserDefaults.standard.string(forKey: "AuthEmail")
            let link: String = incomingURL.absoluteString
            if email != nil && link.contains("https://jarvish-24e27.firebaseapp.com") {
                print("[Auth]: email link auth")
                UserDefaults.standard.set(link, forKey: "AuthLink")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EMAILLINKAUTH"), object: nil)
            }
        }
    }
    
    
}

