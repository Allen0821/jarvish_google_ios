//
//  Serializable.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/25.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

protocol Serializable {
    init?(dictionary: [String : Any?])
    
}
