//
//  EmailLoginViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/25.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

protocol EmailLoginViewControllerDelegate {
    func leaveEmailLogin()
    func startEmailLogin(email: String, password: String)
    func forgetPassword()
}

class EmailLoginViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var emaiTextField: CustomPaddingTextField!
    @IBOutlet weak var pwdTextField: CustomPaddingTextField!
    @IBOutlet weak var forgetLabel: UILabel!
    
    
    var delegate: EmailLoginViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.backButton.setTitle(NSLocalizedString("action_back", comment: ""), for: UIControl.State.normal)
        
        self.emaiTextField.layer.borderColor = UIColor.white.cgColor
        self.emaiTextField.layer.borderWidth = 0.7
        self.emaiTextField.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("input_email", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.emaiTextField.delegate = self
        
        self.pwdTextField.layer.borderColor = UIColor.white.cgColor
        self.pwdTextField.layer.borderWidth = 0.7
        self.pwdTextField.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("input_password", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.pwdTextField.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EmailLoginViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        
        let forget = UITapGestureRecognizer(target: self, action: #selector(clickForget))
        self.forgetLabel.isUserInteractionEnabled = true
        self.forgetLabel.addGestureRecognizer(forget)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func clickBack(_ sender: UIButton) {
        self.dismissKeyboard()
        self.delegate?.leaveEmailLogin()
    }
    
   
    @IBAction func clickSignIn(_ sender: UIButton) {
        self.dismissKeyboard()
        if self.checkInput() {
          print("start login")
            self.delegate?.startEmailLogin(email: self.emaiTextField.text!, password: self.pwdTextField.text!)
        }
    }
    
    
    @objc func clickForget() {
        self.delegate?.forgetPassword()
    }
    
   
    private func loginSuccess(session: Session) {
        print(session.memberId)
        print(session.session)
        let userDefaults = UserDefaults.standard
        userDefaults.set(session.memberId, forKey: "memberId")
        userDefaults.set(session.session, forKey: "session")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
    }
    
    
    private func checkInput() -> Bool {
        if self.emaiTextField.text == nil ||
            self.emaiTextField.text!.isEmpty ||
        !Utility.isValidEmail(email: self.emaiTextField.text!) {
            self.invalidInputAlert(title: NSLocalizedString("input_email_invalid", comment: ""))
            return false
        }
        if self.pwdTextField.text == nil ||
            self.pwdTextField.text!.isEmpty {
            self.invalidInputAlert(title: NSLocalizedString("input_password_invalid", comment: ""))
            return false
        }
        return true
    }
    
  
    private func invalidInputAlert(title: String) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("confirm")})
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
}


extension EmailLoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("DidEndEditing \(textField.tag)")
        
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
   
}
