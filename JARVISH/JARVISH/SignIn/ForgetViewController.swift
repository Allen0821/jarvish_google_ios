//
//  ForgetViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/27.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

protocol ForgetViewControllerDelegate {
    func leaveForget()
    func requestReset(email: String)
}


class ForgetViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var emailTextField: CustomPaddingTextField!
    
    var delegate: ForgetViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.backButton.setTitle(NSLocalizedString("action_back", comment: ""), for: UIControl.State.normal)
        
        self.emailTextField.layer.borderColor = UIColor.white.cgColor
        self.emailTextField.layer.borderWidth = 0.7
        self.emailTextField.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("input_email", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.emailTextField.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EmailLoginViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickBack(_ sender: Any) {
        self.dismissKeyboard()
        self.emailTextField.text = ""
        self.delegate?.leaveForget()
    }
    
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        self.dismissKeyboard()
        if self.emailTextField.text == nil ||
            self.emailTextField.text!.isEmpty ||
        !Utility.isValidEmail(email: self.emailTextField.text!) {
            self.invalidInputAlert(title: NSLocalizedString("input_email_invalid", comment: ""))
        } else {
            self.delegate?.requestReset(email: self.emailTextField.text!)
        }
    }
    
    
    private func invalidInputAlert(title: String) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("confirm")})
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
}


extension ForgetViewController: UITextFieldDelegate {
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
}
