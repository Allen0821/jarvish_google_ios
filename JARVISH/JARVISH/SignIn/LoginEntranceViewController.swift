//
//  LoginEntranceViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/26.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

protocol LoginEntranceViewControllerDelegate {
    func appleLogin()
    func emailLogin()
    func facebookLogin()
    func register()
}

class LoginEntranceViewController: UIViewController {
    
    
   
    @IBOutlet weak var registerLabel: UILabel!
    var delegate: LoginEntranceViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickRegister))
        self.registerLabel.isUserInteractionEnabled = true
        self.registerLabel.addGestureRecognizer(tap)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    /*@IBAction func clickApple(_ sender: UIButton) {
        self.delegate?.appleLogin()
    }*/
    
    
    @IBAction func clickEmailLogin(_ sender: UIButton) {
        self.delegate?.emailLogin()
    }
    
    
    
    /*@IBAction func clickFacebookLogin(_ sender: UIButton) {
        self.delegate?.facebookLogin()
    }*/
    
    
    @objc func clickRegister() {
        self.delegate?.register()
    }
    
    @IBAction func clickCancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
