//
//  LoginViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/24.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import JGProgressHUD
import Moya
import FacebookLogin
import FacebookCore
import AuthenticationServices

class LoginViewController: UIViewController, /*ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding,*/ LoginEntranceViewControllerDelegate, EmailLoginViewControllerDelegate,
    RegisterViewControllerDelegate,
    ForgetViewControllerDelegate,
    ResetViewControllerDelegate {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    
    @IBOutlet weak var entranceView: UIView!
    @IBOutlet weak var emailLoginView: UIView!
    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var forgetView: UIView!
    @IBOutlet weak var resetView: UIView!
    
    let hud = JGProgressHUD(style: .dark)
    
    private var loginEntranceViewController: LoginEntranceViewController?
    private var emailLoginViewController: EmailLoginViewController?
    private var registerViewController: RegisterViewController?
    private var forgetViewController: ForgetViewController?
    private var resetViewController: ResetViewController?
    
    private var isFbLogin: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.entranceView.isHidden = false
        self.emailLoginView.isHidden = true
        self.registerView.isHidden = true
        self.forgetView.isHidden = true
        self.resetView.isHidden = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "LoginEntranceViewControllerSegue") {
            self.loginEntranceViewController = segue.destination as? LoginEntranceViewController
            self.loginEntranceViewController?.delegate = self
        }
        if (segue.identifier == "EmailLoginViewControllerSegue") {
            self.emailLoginViewController = segue.destination as? EmailLoginViewController
            self.emailLoginViewController?.delegate = self
        }
        if (segue.identifier == "RegisterViewControllerSegue") {
            self.registerViewController = segue.destination as? RegisterViewController
            self.registerViewController?.delegate = self
        }
        if (segue.identifier == "ForgetViewControllerSegue") {
           self.forgetViewController = segue.destination as? ForgetViewController
           self.forgetViewController?.delegate = self
        }
        if (segue.identifier == "ResetViewControllerSegue") {
           self.resetViewController = segue.destination as? ResetViewController
           self.resetViewController?.delegate = self
        }
    }
    
    
    private func loginSuccess(session: Session) {
        print("login Success! id:\(session.memberId) token: \(session.session)")
        let userDefaults = UserDefaults.standard
        userDefaults.set(session.memberId, forKey: "memberId")
        userDefaults.set(session.session, forKey: "session")
        userDefaults.set(Date(), forKey: "expired")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "RELOADUSER"), object: nil, userInfo: nil)
        self.dismiss(animated: true, completion: nil)
        /*DispatchQueue.main.async() {
            self.performSegue(withIdentifier: "SegueLoginTomain", sender: nil)
        }*/
    }
    
    
    private func failedAlert(title: String, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("confirm")})
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func registerSuccessAlert(message: String?) {
        let alert = UIAlertController(
            title: NSLocalizedString("sign_up_success", comment: ""),
            message: message,
            preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
            self.registerViewController?.clearInput()
            self.leaveRegister()})
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func resetPasswordSuccessAlert() {
        let alert = UIAlertController(
            title: NSLocalizedString("reset_password_success", comment: ""),
            message: nil,
            preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
            self.emailLogin()
            self.resetViewController?.clearInput()
        })
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func resetPasswordFailureAlert() {
        let alert = UIAlertController(
            title: NSLocalizedString("reset_password_failed", comment: ""),
            message: nil,
            preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
        })
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /*private func facebookLoginRequest() {
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: ["email", "public_profile"], from: self) { [weak self] (result, error) in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            guard let result = result, !result.isCancelled else {
                print("User cancelled login")
                return
            }
            /*Profile.loadCurrentProfile { (profile, error) in
                self?.updateMessage(with: Profile.current?.name)
            }*/
            print("fb token \(String(describing: result.token))")
            self!.getFacebookUserProfile(token: result.token!.tokenString)
        }
    }*/
    
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            
            // For the purpose of this demo app, store the `userIdentifier` in the keychain.
            //self.saveUserInKeychain(userIdentifier)
            
            // For the purpose of this demo app, show the Apple ID credential information in the `ResultViewController`.
            //self.showResultViewController(userIdentifier: userIdentifier, fullName: fullName, email: email)
            print("fullName: \(String(describing: fullName))")
            print("email: \(String(describing: email))")
            self.facebookRegisterUser(token: userIdentifier, name: "allen", email: "allen@jarvish.com")
        
        case let passwordCredential as ASPasswordCredential:
        
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            /*DispatchQueue.main.async {
                self.showPasswordCredentialAlert(username: username, password: password)
            }*/
            
        default:
            break
        }
    }
    
    
    private func facebookLoginUser(token: String) {
        JarvishProvider.rx.request(
            .loginWithFacebook(token: token))
        .filterSuccessfulStatusCodes()
        .mapJSON()
        .subscribe(onSuccess: { result in
                print("facebook login success \(result)")
                if let session = Session(dictionary: result as! [String : Any]) {
                    self.hud.dismiss(afterDelay: 1.0)
                    self.loginSuccess(session: session)
                } else {
                  self.failedAlert(title: NSLocalizedString("sign_in_failure", comment: ""), message: nil)
                    self.hud.dismiss(afterDelay: 1.0)
                }
            }) { error in
                print("facebook login error \(error)")
                self.failedAlert(title: NSLocalizedString("sign_in_failure", comment: ""), message: nil)
                self.hud.dismiss(afterDelay: 1.0)
        }
    }
    
    
    private func facebookRegisterUser(token: String, name: String, email: String) {
        JarvishProvider.rx.request(
            .registerWithFacebook(token: token, email: email, name: name))
        .filterSuccessfulStatusCodes()
        .subscribe(onSuccess: { result in
                print("facebook register success \(result)")
                self.hud.dismiss(afterDelay: 1.0)
                self.registerSuccessAlert(message: nil)
            }) { error in
                print("facebook register error \(error)")
                let moyaError: MoyaError? = error as? MoyaError
                let response : Response? = moyaError?.response
                let statusCode: Int? = response?.statusCode
                if statusCode == 403 {
                    self.failedAlert(
                        title: NSLocalizedString("sign_up_failure", comment: ""),
                        message: NSLocalizedString("sign_up_failure_403", comment: ""))
                } else {
                    self.failedAlert(title: NSLocalizedString("sign_up_failure", comment: ""), message: nil)
                }
                self.hud.dismiss(afterDelay: 1.0)
        }
    }
    
    
    /*private func getFacebookUserProfile(token: String) {
        self.hud.textLabel.text = "  " + NSLocalizedString("action_login", comment: "") + "  "
        self.hud.show(in: self.view)
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me", parameters: ["fields" : "name,email,picture.height(500)"])) { httpResponse, result, error   in
            if error != nil {
                NSLog(error.debugDescription)
                return
            }
            if let result = result as? [String: AnyObject] {
                let name = result["name"] as! String
                let email = result["email"] as! String
                let pic = result["picture"] as? [String: AnyObject]
                let data = pic!["data"] as? [String: AnyObject]
                let url = data!["url"] as! String
                print("fb name \(name)")
                print("fb avatar url \(url)")
                UserDefaults.standard.set(name, forKey: "name")
                UserDefaults.standard.set(url, forKey: "avatar")
                if self.isFbLogin {
                    self.facebookLoginUser(token: token)
                } else {
                    self.facebookRegisterUser(token: token, name: name, email: email)
                }
            }

        }
        connection.start()
    }*/
    
    
    private func appleRegisterUser(token: String, name: String, email: String) {
        JarvishProvider.rx.request(
            .registerWithApple(token: token, email: email, name: name))
        .filterSuccessfulStatusCodes()
        .subscribe(onSuccess: { result in
                print("facebook register success \(result)")
                self.hud.dismiss(afterDelay: 1.0)
                self.registerSuccessAlert(message: nil)
            }) { error in
                print("facebook register error \(error)")
                let moyaError: MoyaError? = error as? MoyaError
                let response : Response? = moyaError?.response
                let statusCode: Int? = response?.statusCode
                if statusCode == 403 {
                    self.failedAlert(
                        title: NSLocalizedString("sign_up_failure", comment: ""),
                        message: NSLocalizedString("sign_up_failure_403", comment: ""))
                } else {
                    self.failedAlert(title: NSLocalizedString("sign_up_failure", comment: ""), message: nil)
                }
                self.hud.dismiss(afterDelay: 1.0)
        }
    }
}


extension LoginViewController {
    
    
    func appleLogin() {
        /*let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()*/
    }
    
    
    func emailLogin() {
        print("email login")
        self.entranceView.isHidden = true
        self.emailLoginView.isHidden = false
        self.registerView.isHidden = true
        self.forgetView.isHidden = true
        self.resetView.isHidden = true
    }
        
    func facebookLogin() {
        //isFbLogin = true
        //self.facebookLoginRequest()
    }
                             
    func register() {
        print("register")
        self.entranceView.isHidden = true
        self.emailLoginView.isHidden = true
        self.registerView.isHidden = false
        self.forgetView.isHidden = true
        self.resetView.isHidden = true
    }
    
    func leaveEmailLogin() {
        self.entranceView.isHidden = false
        self.emailLoginView.isHidden = true
        self.registerView.isHidden = true
        self.forgetView.isHidden = true
        self.resetView.isHidden = true
    }
    
    func startEmailLogin(email: String, password: String) {
          self.hud.textLabel.text = "  " + NSLocalizedString("action_login", comment: "") + "  "
          self.hud.show(in: self.view)
          JarvishProvider.rx.request(
              .loginWithEmail(email: email, password: password))
          .filterSuccessfulStatusCodes()
          .mapJSON()
          .subscribe(onSuccess: { result in
              print(result)
              if let session = Session(dictionary: result as! [String : Any]) {
                self.getProfile(session: session)
              } else {
                self.failedAlert(title: NSLocalizedString("sign_in_failure", comment: ""), message: nil)
                  self.hud.dismiss(afterDelay: 1.0)
              }
          }) { error in
              print("failed to signin with email: \(error)")
            self.failedAlert(title: NSLocalizedString("sign_in_failure", comment: ""), message: nil)
              self.hud.dismiss(afterDelay: 1.0)
          }
    }
    
    
    private func getProfile(session: Session) {
        JarvishProvider.rx.request(
            .getUserProfile(session: session.session, uid: session.memberId))
            .filterSuccessfulStatusCodes()
            .mapJSON()
            .subscribe(onSuccess: { result in
                print("get profile success: \(result)")
                if let profile = Profile(dictionary: result as! [String : Any]) {
                    UserDefaults.standard.set(profile.user_name, forKey: "name")
                    UserDefaults.standard.set("", forKey: "avatar")
                    self.hud.dismiss(afterDelay: 1.0)
                    self.loginSuccess(session: session)
                } else {
                    self.hud.dismiss(afterDelay: 1.0)
                    
                }
            }) { error in
                print("get profile failure: \(error)")
                self.hud.dismiss(afterDelay: 1.0)
        }
    }
    
    
    func forgetPassword() {
        self.entranceView.isHidden = true
        self.emailLoginView.isHidden = true
        self.registerView.isHidden = true
        self.forgetView.isHidden = false
        self.resetView.isHidden = true
    }
               
    
    func leaveRegister() {
        print("leaveRegister")
        self.entranceView.isHidden = false
        self.emailLoginView.isHidden = true
        self.registerView.isHidden = true
        self.forgetView.isHidden = true
        self.resetView.isHidden = true
    }
    
    
    func startRegister(name: String, email: String, pwd: String) {
        print("Register: \(name) \(email)")
        self.hud.textLabel.text = "  " + NSLocalizedString("action_register", comment: "") + "  "
        self.hud.show(in: self.view)
        JarvishProvider.rx.request(
            .registerWithEmail(name: name, email: email, password: pwd))
        .filterSuccessfulStatusCodes()
        .subscribe(onSuccess: { result in
            print(result)
            print("success to register with email: \(result)")
            self.hud.dismiss(afterDelay: 1.0)
            self.registerSuccessAlert(message: NSLocalizedString("sign_up_success_confirm", comment: ""))
        }) { error in
            print("failed to register with email: \(error)")
            let moyaError: MoyaError? = error as? MoyaError
            let response : Response? = moyaError?.response
            let statusCode: Int? = response?.statusCode
            if statusCode == 403 {
                self.failedAlert(
                    title: NSLocalizedString("sign_up_failure", comment: ""),
                    message: NSLocalizedString("sign_up_failure_403", comment: ""))
            } else {
                self.failedAlert(title: NSLocalizedString("sign_up_failure", comment: ""), message: nil)
            }
            self.hud.dismiss(afterDelay: 1.0)
        }
    }
    
    
    func startFacebookRegister() {
        //self.isFbLogin = false
        //self.facebookLoginRequest()
    }
    
    
    func leaveForget() {
        self.entranceView.isHidden = true
        self.emailLoginView.isHidden = false
        self.registerView.isHidden = true
        self.forgetView.isHidden = true
        self.resetView.isHidden = true
    }
    
    func requestReset(email: String) {
        print("request reset: \(email)")
        self.hud.textLabel.text = "  " + NSLocalizedString("action_submit", comment: "") + "  "
        self.hud.show(in: self.view)
        JarvishProvider.rx.request(
            .forgetPassword(email: email))
        .filterSuccessfulStatusCodes()
        .subscribe(onSuccess: { result in
            print(result)
            print("success to request reset: \(result)")
            self.hud.dismiss(afterDelay: 1.0)
            self.resetViewController?.email = email
            self.startReset()
        }) { error in
            print("failed to request reset: \(error)")
            self.hud.dismiss(afterDelay: 1.0)
        }
    }
    
    
    func startReset() {
        self.entranceView.isHidden = true
        self.emailLoginView.isHidden = true
        self.registerView.isHidden = true
        self.forgetView.isHidden = true
        self.resetView.isHidden = false
    }
    
    
    func leaveResetPassword() {
        self.entranceView.isHidden = true
        self.emailLoginView.isHidden = true
        self.registerView.isHidden = true
        self.forgetView.isHidden = false
        self.resetView.isHidden = true
    }
    
    
    func submitResetPassword(email: String, password: String, code: String) {
        print("submit reset password: \(email) \(password) \(code)")
        self.hud.textLabel.text = "  " + NSLocalizedString("action_submit", comment: "") + "  "
        self.hud.show(in: self.view)
        let req = JarvishProvider.rx.request(
            .resetPassword(verificationCode: code, email: email, password: password))
        .filterSuccessfulStatusCodes()
        .subscribe(onSuccess: { result in
            print(result)
            print("success to reset: \(result)")
            self.hud.dismiss(afterDelay: 1.0)
            self.resetPasswordSuccessAlert()
        }) { error in
            print("failed to reset: \(error)")
            self.hud.dismiss(afterDelay: 1.0)
            self.resetPasswordFailureAlert()
        }
        print("submit reset req: \(req)")
    }
}
