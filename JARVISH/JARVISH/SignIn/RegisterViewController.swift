//
//  RegisterViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/27.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

protocol RegisterViewControllerDelegate {
    func leaveRegister()
    func startRegister(name: String, email: String, pwd: String)
    func startFacebookRegister()
}

class RegisterViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nameTextField: CustomPaddingTextField!
    @IBOutlet weak var emailTextField: CustomPaddingTextField!
    @IBOutlet weak var passwordTextField: CustomPaddingTextField!
    @IBOutlet weak var confirmTextField: CustomPaddingTextField!
    
    var delegate: RegisterViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.backButton.setTitle(NSLocalizedString("action_back", comment: ""), for: UIControl.State.normal)
        
        self.nameTextField.layer.borderColor = UIColor.white.cgColor
        self.nameTextField.layer.borderWidth = 0.7
        self.nameTextField.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("input_name", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.nameTextField.delegate = self
        
        self.emailTextField.layer.borderColor = UIColor.white.cgColor
        self.emailTextField.layer.borderWidth = 0.7
        self.emailTextField.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("input_email", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.emailTextField.delegate = self
        
        self.passwordTextField.layer.borderColor = UIColor.white.cgColor
        self.passwordTextField.layer.borderWidth = 0.7
        self.passwordTextField.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("input_password", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.passwordTextField.delegate = self
        
        self.confirmTextField.layer.borderColor = UIColor.white.cgColor
        self.confirmTextField.layer.borderWidth = 0.7
        self.confirmTextField.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("confirm_password", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.confirmTextField.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EmailLoginViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickBack(_ sender: UIButton) {
        print("clickBack")
        self.clearInput()
        self.delegate?.leaveRegister()
    }
    
    
    @IBAction func clickRegister(_ sender: UIButton) {
        self.dismissKeyboard()
        if checkInput() {
            self.registerAlert()
        }
    }
    
    
    /*@IBAction func clickFacebookRegister(_ sender: UIButton) {
        self.delegate?.startFacebookRegister()
    }*/
    
    
    func clearInput() {
        self.nameTextField.text = ""
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
        self.confirmTextField.text = ""
    }
    
    
    private func checkInput() -> Bool {
        if self.nameTextField.text == nil ||
            self.nameTextField.text!.isEmpty {
            self.invalidInputAlert(title: NSLocalizedString("input_name_invalid", comment: ""))
            return false
        }
        if self.emailTextField.text == nil ||
            self.emailTextField.text!.isEmpty ||
        !Utility.isValidEmail(email: self.emailTextField.text!) {
            self.invalidInputAlert(title: NSLocalizedString("input_email_invalid", comment: ""))
            return false
        }
        if self.passwordTextField.text == nil ||
            self.passwordTextField.text!.isEmpty {
            self.invalidInputAlert(title: NSLocalizedString("input_password_invalid", comment: ""))
            return false
        }
        if self.confirmTextField.text == nil ||
            self.confirmTextField.text!.isEmpty ||        !self.confirmTextField.text!.elementsEqual(self.passwordTextField.text!) {
            self.invalidInputAlert(title: NSLocalizedString("input_confirm_password_invalid", comment: ""))
            return false
        }
        return true
    }
    
    
    private func invalidInputAlert(title: String) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("confirm")})
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func registerAlert() {
        let alert = UIAlertController(
            title: NSLocalizedString("action_warning", comment: ""),
            message: NSLocalizedString("sign_up_email_warning", comment: ""),
            preferredStyle: .alert)
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: {(alert: UIAlertAction!) in
            self.delegate?.startRegister(name: self.nameTextField.text!,
            email: self.emailTextField.text!,
            pwd: self.passwordTextField.text!)})
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
        alert.addAction(confirm)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
}


extension RegisterViewController: UITextFieldDelegate {
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
}
