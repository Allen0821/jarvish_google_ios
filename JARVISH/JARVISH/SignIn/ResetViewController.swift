//
//  ResetViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/31.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

protocol ResetViewControllerDelegate {
    func leaveResetPassword()
    func submitResetPassword(email: String, password: String, code: String)
}

class ResetViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var codeTextField: CustomPaddingTextField!
    @IBOutlet weak var pwdTextField: CustomPaddingTextField!
    @IBOutlet weak var confirmPwdTextFied: CustomPaddingTextField!
    
    var delegate: ResetViewControllerDelegate?
    var email: String = "allen.lin@jarvish.com"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.backButton.setTitle(NSLocalizedString("action_back", comment: ""), for: UIControl.State.normal)
        
        self.codeTextField.layer.borderColor = UIColor.white.cgColor
        self.codeTextField.layer.borderWidth = 0.7
        self.codeTextField.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("input_verification_code", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.codeTextField.delegate = self
        
        self.pwdTextField.layer.borderColor = UIColor.white.cgColor
        self.pwdTextField.layer.borderWidth = 0.7
        self.pwdTextField.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("input_password", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.pwdTextField.delegate = self
        
        self.confirmPwdTextFied.layer.borderColor = UIColor.white.cgColor
        self.confirmPwdTextFied.layer.borderWidth = 0.7
        self.confirmPwdTextFied.attributedPlaceholder = NSAttributedString(string:
        NSLocalizedString("confirm_password", comment: ""), attributes:
            [NSAttributedString.Key.foregroundColor:Utility.getPlaceholderColor()])
        self.confirmPwdTextFied.delegate = self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickBack(_ sender: UIButton) {
        self.dismissKeyboard()
        self.codeTextField.text = ""
        self.pwdTextField.text = ""
        self.confirmPwdTextFied.text = ""
        self.delegate?.leaveResetPassword()
    }
    
    
    @IBAction func clickSubmit(_ sender: UIButton) {
        self.dismissKeyboard()
        if checkInput() {
            print("eamil: \(String(describing: self.email))")
            self.delegate?.submitResetPassword(
                email: self.email,
                password: self.pwdTextField.text!,
                code: self.codeTextField.text!)
        }
    }
    
    
    
    private func checkInput() -> Bool {
        if self.codeTextField.text == nil ||
            self.codeTextField.text!.isEmpty {
            self.invalidInputAlert(title: NSLocalizedString("input_auth_code_invalid", comment: ""))
            return false
        }
        if self.pwdTextField.text == nil ||
            self.pwdTextField.text!.isEmpty {
            self.invalidInputAlert(title: NSLocalizedString("input_password_invalid", comment: ""))
            return false
        }
        if self.confirmPwdTextFied.text == nil ||
            self.confirmPwdTextFied.text!.isEmpty {
            self.invalidInputAlert(title: NSLocalizedString("input_confirm_password_invalid", comment: ""))
            return false
        }
        return true
    }
    
    
    private func invalidInputAlert(title: String) {
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("confirm")})
        alert.addAction(confirm)
        self.present(alert, animated: true, completion: nil)
    }
    
    func clearInput() {
        self.codeTextField.text = ""
        self.pwdTextField.text = ""
        self.confirmPwdTextFied.text = ""
    }
    
}


extension ResetViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("DidEndEditing \(textField.tag)")
        
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
   
}
