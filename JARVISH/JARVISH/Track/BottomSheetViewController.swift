//
//  BottomSheetViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/23.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import PullUpController

protocol BottomSheetViewControllerDelegate {
    func playTrack()
    func pauseTack()
}

class BottomSheetViewController: PullUpController {
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var depatureLabel: UILabel!
    @IBOutlet weak var arrivalLabel: UILabel!
    @IBOutlet weak var avgSpeedLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var isPlaying: Bool = true
    var delegate: BottomSheetViewControllerDelegate?
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = Utility.getTrackContentBgColor()
    }
    

    func presentBottomSheet(track: TrackLog, depature: Int, arrival: Int) {
        print("track \(track)")
        distanceLabel.attributedText = getDistanceString(distance: track.distance)
        durationLabel.text = Utility.getTrackDurtionString(duration: track.duration)
        depatureLabel.text = Utility.getDateTimeFormatString(timetamp: depature, format: "HH:mm")
        arrivalLabel.text = Utility.getDateTimeFormatString(timetamp: arrival, format: "HH:mm")
        avgSpeedLabel.attributedText = getSpeedString(speed: track.averageSpeed)
        timeLabel.text = showDateAndTime(timestamp: TimeInterval(track.timestamp))
    }
    
    
    private func showDateAndTime(timestamp: TimeInterval) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = DateFormatter.Style.medium
        formatter.timeStyle = DateFormatter.Style.medium
        return formatter.string(from: Date(timeIntervalSince1970: timestamp))
    }
    
    
    private func getDistanceString(distance: Double) -> NSMutableAttributedString {
        if UserDefaults.standard.integer(forKey: "app_units") == 0 {
            return Utility.getTrackDistanceKmAttributedString(distance: distance)
        } else {
            return Utility.getTrackDistanceMiAttributedString(distance: distance)
        }
    }
    
    
    private func getSpeedString(speed: Double) -> NSMutableAttributedString {
        if UserDefaults.standard.integer(forKey: "app_units") == 0 {
            return Utility.getTrackSpeedKmAttributedString(speed: speed)
        } else {
            return Utility.getTrackSpeedMiAttributedString(speed: speed)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func clickPlay(_ sender: UIButton) {
        if isPlaying {
            self.playButton.setImage(UIImage(systemName: "play.fill"), for: UIControl.State.normal)
            isPlaying = false
            self.delegate?.pauseTack()
        } else {
            self.playButton.setImage(UIImage(systemName: "pause.fill"), for: UIControl.State.normal)
            isPlaying = true
            self.delegate?.playTrack()
        }
    }
}
