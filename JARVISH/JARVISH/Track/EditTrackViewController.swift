//
//  EditTrackViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/17.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

protocol EditTrackViewControllerDelegate {
    func onUpdateTrack(track: TrackLog)
}

class EditTrackViewController: UIViewController {
    @IBOutlet weak var titleTextField: UITextField!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    var tracLog: TrackLog?
    var delegate: EditTrackViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleTextField.layer.borderColor = UIColor.white.cgColor
        titleTextField.layer.borderWidth = 0.5
        titleTextField.delegate = self
        print("tracLog: \(String(describing: tracLog))")
        titleTextField.text = tracLog?.title
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickCancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickConfirm(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        print("title \(String(describing: tracLog?.title))")
        Dao.shared.updateTrack(track: tracLog!)
        delegate?.onUpdateTrack(track: tracLog!)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TRACKCHANGED"), object: nil, userInfo: nil)
        self.dismiss(animated: true, completion: nil)
    }
}


extension EditTrackViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !(titleTextField.text!.isEmpty) {
            tracLog?.title = titleTextField.text!
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
