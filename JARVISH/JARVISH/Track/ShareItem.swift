//
//  ShareItem.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/11/13.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct ShareItem {
    var name: String
    var icon: String
    var index: Int
    
    init(name: String, icon: String, index: Int) {
        self.name = name
        self.icon = icon
        self.index = index
    }
}
