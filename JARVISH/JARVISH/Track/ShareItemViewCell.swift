//
//  ShareItemViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/11/13.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class ShareItemViewCell: UITableViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    var item: ShareItem! {
        didSet {
            if item.index == 1 {
                icon.image = UIImage(systemName: "ellipsis")
                icon.tintColor = Utility.getTintColor()
            } else {
                icon.image = UIImage(named: item.icon)
            }
            name.text = item.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
