//
//  ShareTrackViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/11/13.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import GoogleMaps
import FacebookShare

class ShareTrackViewController: UIViewController, GMSMapViewDelegate, SharingDelegate {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    @IBOutlet weak var topBar: UINavigationBar!
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var shareTableView: UITableView!
    
    var id: Int?
    var items: [ShareItem] = []
    let trackUtil = TrackUtil()
    
    var map: GMSMapView!
    var polyline: GMSPolyline?
 
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initMap()
        buildItems()
        shareTableView.delegate = self
        shareTableView.dataSource = self
        
    }
    
    private func initMap() {
        let width = UIScreen.main.bounds.size.width
        self.map = GMSMapView()
        //self.mapContainer.frame = CGRect(x: 0, y: 0, width: width, height: width)
        self.map.frame = CGRect(x: 0, y: 0, width: width, height: width)
        
        
        do {  // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style2", withExtension: "json") {
                self.map.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
              } else {
                NSLog("Unable to find style.json")
              }
            self.map.delegate = self
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        self.map.isMyLocationEnabled = false
        self.map.settings.setAllGesturesEnabled(false)
        //self.map.set
        //self.map.disa
        self.mapContainer.addSubview(self.map)
        print("Thread.isMainThread \(Thread.isMainThread)")
        self.map.isHidden = true
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        getGpsLogs()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func buildItems() {
        items.append(ShareItem(name: "Facebook", icon: "facebook", index: 0))
        items.append(ShareItem(name: NSLocalizedString("action_more", comment: ""), icon: "more", index: 1))
    }

    @IBAction func clickBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func getGpsLogs() {
        let gpsLogs = Dao.shared.queryGpsTracks(id: self.id!)
        //print("gpsLogs: \(gpsLogs)")
        if gpsLogs.count < 2 {
            return
        }
        buildMap(gpsLogs: gpsLogs)
    }
    
    
    private func buildMap(gpsLogs: [GpsLog]) {
        var bounds = GMSCoordinateBounds()
        let path = GMSMutablePath()
        var spans: [GMSStyleSpan] = []
        var step = 0
        
        
        gpsLogs.forEach { gps in
            let location = CLLocationCoordinate2D(latitude: gps.latitude, longitude: gps.longitude)
            //print("speed... \(gps.speed)")
            path.add(location)
            bounds = bounds.includingCoordinate(location)
            
            if step < gpsLogs.count - 1 {
            let span =
                GMSStrokeStyle.gradient(
                    from: trackUtil.getSpeedColor(speed: gpsLogs[step].speed),
                    to:  trackUtil.getSpeedColor(speed: gpsLogs[step + 1].speed))
                spans.append(GMSStyleSpan(style: span))
            }
            step+=1
        }
        
        self.polyline = GMSPolyline(path: path)
        self.polyline!.spans = spans
        self.polyline!.strokeWidth = 8.0
        let update = GMSCameraUpdate.fit(bounds, withPadding: 24.0)
        self.polyline!.map = self.map

        
        let start = trackUtil.createMapMarker(
            gps: gpsLogs[0],
            title: "",
            icon: UIImage(named: "start")!)
        start.map = self.map
        
        let end = trackUtil.createMapMarker(
            gps: gpsLogs[gpsLogs.count - 1],
            title: "",
            icon: UIImage(named: "end")!)
        end.map = self.map
        
        self.map.moveCamera(update)
        self.map.isHidden = false
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return true
    }
    
    
    func shareToMore(image: UIImage) {
        let imageShare = [image]
            let activityViewController = UIActivityViewController(activityItems: imageShare , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
    }
    
    func shareToFacebook(image: UIImage) {
        let photo = SharePhoto(image: image, userGenerated: true)
                let content = SharePhotoContent()
                content.photos = [photo]
        
                let dialog = self.dialog(withContent: content)

                // Recommended to validate before trying to display the dialog
                do {
                    try dialog.validate()
                } catch {
                    print("error: \(error)")
                }

                dialog.show()
    }
    
    func dialog(withContent content: SharingContent) -> ShareDialog {
            return ShareDialog(
                fromViewController: self,
                content: content,
                delegate: self
            )
    }
    
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        
    }
    
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        
    }
    
    func sharerDidCancel(_ sharer: Sharing) {
        
    }
}


extension ShareTrackViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShareItemViewCell", for: indexPath) as! ShareItemViewCell
        cell.item = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let renderer = UIGraphicsImageRenderer(bounds: self.map.bounds)
        let image = renderer.image { (UIGraphicsImageRendererContext) in
            self.map.drawHierarchy(in: self.map.bounds, afterScreenUpdates: true)
        }
        if indexPath.row == 0 {
            shareToFacebook(image: image)
        } else {
            shareToMore(image: image)
        }
    }
    
}
