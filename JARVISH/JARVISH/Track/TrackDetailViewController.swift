//
//  TrackDetailViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/21.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import JGProgressHUD


class TrackDetailViewController: UIViewController, BottomSheetViewControllerDelegate, GMSMapViewDelegate, EditTrackViewControllerDelegate {
   
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var weaIconImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
   
    
    var gMap: GMSMapView!
    var haveGpsLogs: Bool = false
    var haveBottomSheet: Bool = false
    
    var track: TrackLog!
    var gpsLogs: [GpsLog] = []
    var bottomSheetView: BottomSheetViewController!
    var timer: Timer?
    var step: Int = 0
    var isMoving: Bool = true
    var marker: GMSMarker?
    let zoomLevel: Float = 15.5
    //var tile: Double = 55.0
    var focus = true
    let trackUtil = TrackUtil()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        titleLabel.text = track.title
        
        self.locationButton.backgroundColor = Utility.getButtonBgColor()
        self.locationButton.layer.shadowColor = UIColor.black.cgColor
        self.locationButton.layer.shadowRadius = 2
        self.locationButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.locationButton.layer.shadowOpacity = 0.3
        // Do any additional setup after loading the view.
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: self.zoomLevel)
        self.gMap = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        do {  // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style2", withExtension: "json") {
                self.gMap.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
              } else {
                NSLog("Unable to find style.json")
              }
            self.gMap.delegate = self
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
                            
        //self.gMap.isMyLocationEnabled = true
        self.mapView.addSubview(self.gMap)
        self.gMap.isHidden = true
        self.weaIconImageView.isHidden = true
        self.temperatureLabel.isHidden = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !self.haveBottomSheet {
            self.bottomSheetView = self.storyboard?.instantiateViewController(withIdentifier: "BottomSheetViewController") as? BottomSheetViewController
            self.bottomSheetView.delegate = self
            addPullUpController(self.bottomSheetView, initialStickyPointOffset: 120.0, animated: true)
            self.haveBottomSheet = true
            self.getGpsLogs()
        } else {
            startPlayingTimer()
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
         if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
         }
        //self.gMap.clear()
    }
    
    
    private func invalidTrackAlert() {
        let alert = UIAlertController(title: NSLocalizedString("track_invalid_title", comment: ""), message: NSLocalizedString("track_invalid_content_open", comment: ""), preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let cancel = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
        })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getGpsLogs() {
        self.gpsLogs = Dao.shared.queryGpsTracks(id: track.gpsLogId)
        print("gpsLogs: \(self.gpsLogs)")
        if self.gpsLogs.count < 2 {
            invalidTrackAlert()
            return
        }
         
        self.haveGpsLogs = true
        self.bottomSheetView.presentBottomSheet(track: self.track, depature: self.gpsLogs[0].dateTime, arrival: self.gpsLogs[self.gpsLogs.count - 1].dateTime)
        print("weaIndex1: \(String(describing: self.track.weaIcon))")
        print("weaIndex2: \(String(describing: self.track.weaTemp))")
        print("weaIndex3: \(String(describing: self.track.weaDesc))")
        if self.track.weaDesc != nil && self.track.weaTemp != nil {
            let icon = "weather." + String(self.track.weaIcon!)
            self.weaIconImageView.image = UIImage(named: icon)
            self.temperatureLabel.text =  WeatherUtil.getTemperatureText(weaTemop: self.track.weaTemp!)
            self.weaIconImageView.isHidden = false
            self.temperatureLabel.isHidden = false
        }
        createMapMarker()
        drawTrackPolyline()
        self.gMap.animate(toViewingAngle: 55)
        self.gMap.isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            self.startPlayingTimer()
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickMore(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.overrideUserInterfaceStyle = .dark
        
        
        let action1 = UIAlertAction(title: NSLocalizedString("track_share_gpx", comment: ""), style: .default) {(_) in
            self.shareTrack()
        }
        let action2 = UIAlertAction(title: NSLocalizedString("track_export_gpx", comment: ""), style: .default) {(_) in
            self.exportTrack()
        }
        let action3 = UIAlertAction(title: NSLocalizedString("track_edit", comment: ""), style: .default) {(_) in
            self.editTrack()
        }
       
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in print("Cancel")})
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func exportTrack() {
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = NSLocalizedString("action_export", comment: "")
        hud.show(in: self.view)
        let gpxFile = ExportGpx.createGpxFile(track: track, gps: gpsLogs)
        hud.dismiss()
        var filesToShare = [Any]()
        filesToShare.append(gpxFile)
        let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    private func shareTrack() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareTrackViewController") as! ShareTrackViewController
        vc.id = self.track.gpsLogId
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    private func editTrack() {
        DispatchQueue.main.async() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditTrackViewController") as! EditTrackViewController
            vc.tracLog = self.track
            vc.delegate = self
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    func onUpdateTrack(track: TrackLog) {
        titleLabel.text = track.title
    }
       
    
    
    @IBAction func clickLocation(_ sender: UIButton) {
        if self.gpsLogs.count > 0 && self.step < self.gpsLogs.count {
            let location = CLLocationCoordinate2D(latitude: self.gpsLogs[self.step].latitude, longitude: self.gpsLogs[self.step].longitude)
            let center = GMSCameraUpdate.setTarget(location)
            self.gMap.animate(with: center)
            self.gMap.animate(toZoom: self.zoomLevel)
            self.focus = true
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
      print("mapView... \(gesture)")
        if gesture {
            self.focus = false
            print("mapView... \(self.zoomLevel)")
        }
    }

    
    private func drawTrackPolyline() {
        var bounds = GMSCoordinateBounds()
        let path = GMSMutablePath()
        var spans: [GMSStyleSpan] = []
        var step = 0
        
        self.gpsLogs.forEach { gps in
            let location = CLLocationCoordinate2D(latitude: gps.latitude, longitude: gps.longitude)            
            print("speed... \(gps.speed)")
            path.add(location)
            bounds = bounds.includingCoordinate(location)
            
            if step < self.gpsLogs.count - 1 {
            let span =
                GMSStrokeStyle.gradient(
                    from: trackUtil.getSpeedColor(speed: self.gpsLogs[step].speed),
                    to:  trackUtil.getSpeedColor(speed: self.gpsLogs[step + 1].speed))
                spans.append(GMSStyleSpan(style: span))
            }
            step+=1
        }
        
        let polyline = GMSPolyline(path: path)
        polyline.spans = spans
        polyline.strokeWidth = 8.0
        polyline.map = self.gMap
        self.gMap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 64))
    }
    
    
    private func createMapMarker() {
        /*let start = GMSMarker(position: CLLocationCoordinate2D(latitude: self.gpsLogs[0].latitude, longitude: self.gpsLogs[0].longitude))
        start.title = NSLocalizedString("track_departure", comment: "")
        start.icon = GMSMarker.markerImage(with: Utility.getTrackMarkerColor())
        start.map = self.gMap
        
        
        let destination = GMSMarker(position: CLLocationCoordinate2D(latitude: self.gpsLogs[self.gpsLogs.count - 1].latitude, longitude: self.gpsLogs[self.gpsLogs.count - 1].longitude))
        destination.title = NSLocalizedString("track_destination", comment: "")
        destination.icon = GMSMarker.markerImage(with: UIColor(red: 255.0/255.0, green: 99.0/255.0, blue: 71.0/255.0, alpha: 1.0))
        destination.map = self.gMap*/
        
        let start = trackUtil.createMapMarker(
            gps: self.gpsLogs[0],
            title: NSLocalizedString("track_departure", comment: ""),
            icon: UIImage(named: "start")!)
        start.map = self.gMap
        
        let end = trackUtil.createMapMarker(
            gps: self.gpsLogs[self.gpsLogs.count - 1],
            title: NSLocalizedString("track_destination", comment: ""),
            icon: UIImage(named: "end")!)
        end.map = self.gMap
        

        let uv = TrackMarkerView()
        uv.setHelmetImage(image: UIImage(named: track.helmet ?? "x01"))
        self.marker = GMSMarker()
        let markerView = UIImageView(frame: CGRect(x: 0, y: 0, width: 48, height: 64))
        markerView.addSubview(uv)
      
        
       
        self.marker!.iconView = markerView
        self.marker!.tracksViewChanges = false
        self.marker!.position = CLLocationCoordinate2D(latitude: self.gpsLogs[self.step].latitude, longitude: self.gpsLogs[self.step].longitude)
        self.marker!.map = self.gMap
    }
    
    
    private func startPlayingTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
        self.timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.updateStep), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateStep(step: Int) {
        if self.gMap.isHidden {
            return
        }
        
        if !isMoving {
            return
        }
        if self.step >= self.gpsLogs.count {
            self.step = 0
        }
        if self.marker == nil {
            self.marker = GMSMarker()
            self.marker!.position = CLLocationCoordinate2D(latitude: self.gpsLogs[self.step].latitude, longitude: self.gpsLogs[self.step].longitude)
             let img = UIImage(named: "trackitem")
            let markerView = UIImageView(image: img)
            self.marker!.iconView = markerView
            self.marker!.map = self.gMap
            //self.marker.appearAnimation = kGMSMarkerAnimationPop
        } else {
            self.marker!.position = CLLocationCoordinate2D(latitude: self.gpsLogs[self.step].latitude, longitude: self.gpsLogs[self.step].longitude)
        }
        if self.focus {
            var lastStep = 0
            if self.step == 0 {
                lastStep = self.gpsLogs.count - 1
            } else {
                lastStep = self.step - 1
            }
            
            let heading = GMSGeometryHeading(
                CLLocationCoordinate2D(latitude: self.gpsLogs[lastStep].latitude, longitude: self.gpsLogs[lastStep].longitude),
                CLLocationCoordinate2D(latitude: self.gpsLogs[self.step].latitude, longitude: self.gpsLogs[self.step].longitude))
            let location = CLLocationCoordinate2D(latitude: self.gpsLogs[self.step].latitude, longitude: self.gpsLogs[self.step].longitude)
            let center = GMSCameraUpdate.setTarget(location)
            self.gMap.animate(with: center)
            self.gMap.animate(toBearing: heading)
        }
        self.step = self.step + 1
    }
    
    
   
    func playTrack() {
        print("play track")
        self.isMoving = true
    }
    
    func pauseTack() {
        print("pause track")
        self.isMoving = false
    }
}


