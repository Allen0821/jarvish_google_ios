//
//  TrackItemViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/20.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class TrackItemViewCell: UITableViewCell {
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var deleteImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        itemView.layer.borderWidth = 0.5
        itemView.layer.borderColor = Utility.getTintColor().cgColor
    }

   
    var track: TrackLog! {
        didSet {
            dateLabel.text = Utility.getTrackDateString(timetamp: track.timestamp)
            titleLabel.text = track.title
            let content =
                getDistanceString(distance: track.distance) + " " +
                    getSpeedString(speed: track.averageSpeed)  + " " +
                        Utility.getTrackDurtionString(duration: track!.duration)
            contentLabel.text = content
        }
    }
    
    
    private func getDistanceString(distance: Double) -> String {
        if UserDefaults.standard.integer(forKey: "app_units") == 0 {
            return Utility.getTrackDistanceKmString(distance: distance)
        } else {
            return Utility.getTrackDistanceMiString(distance: distance)
        }
    }
    
    
    private func getSpeedString(speed: Double) -> String {
        if UserDefaults.standard.integer(forKey: "app_units") == 0 {
            return Utility.getTrackSpeedKmString(speed: speed)
        } else {
            return Utility.getTrackSpeedMiString(speed: speed)
        }
    }
}
