//
//  TrackMarkerView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/11/25.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class TrackMarkerView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var helmetImageView: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
 
    func setUp() {
        Bundle.main.loadNibNamed("TrackMarkerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = CGRect(x: 0, y: 0, width: 48, height: 64)
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        helmetImageView.layer.cornerRadius = helmetImageView.frame.width / 2
        helmetImageView.clipsToBounds = true
    }

    func setHelmetImage(image: UIImage?) {
        if let helmet = image {
            helmetImageView.image = helmet
        } else {
            helmetImageView.image = UIImage(named: "x01")
        }
    }
}
