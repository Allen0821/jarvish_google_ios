//
//  TrackUtil.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/8/19.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import GoogleMaps

class TrackUtil {
    
    func getSpeedColor(speed: Double) -> UIColor {
        let aaz = speed * 60 * 60 / 1000
        if aaz < 20.0 {
            return UIColor(red: 204/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        } else if (aaz >= 20.0 && aaz < 40.0) {
            return UIColor(red: 0.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        } else if (aaz >= 40.0 && aaz < 60.0) {
            return UIColor(red: 0.0/255.0, green: 255.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        } else if (aaz >= 60.0 && aaz < 80.0) {
            return UIColor(red: 204.0/255.0, green: 255.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        } else if (aaz >= 80.0 && aaz < 100.0) {
            return UIColor(red: 255.0/255.0, green: 204.0/255.0, blue: 102.0/255.0, alpha: 1.0)
        } else {
            return UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 103.0/255.0, alpha: 1.0)
        }
    }
    
    
    func createMapMarker(gps: GpsLog, title: String, icon: UIImage) -> GMSMarker {
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: gps.latitude, longitude: gps.longitude))
        marker.title = title
        marker.icon = icon
        marker.groundAnchor = .init(x: 0.5, y: 0.5)
        return marker
    }
  
    
    func gg(view: UIImageView) {
        view.layer.cornerRadius = view.layer.frame.width / 2
        view.clipsToBounds = true
    }
}
