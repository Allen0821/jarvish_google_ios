//
//  TrackViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/2/21.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import JGProgressHUD

class TrackViewController: UIViewController, UIDocumentPickerDelegate {
    @IBOutlet weak var topBar: UINavigationBar!
    @IBOutlet weak var trackTable: UITableView!
    @IBOutlet weak var imGpxButton: UIBarButtonItem!
    @IBOutlet weak var importButton: UIBarButtonItem!
    
    var refreshControl:UIRefreshControl!
    
    var tracks: [TrackLog] = []
    var deleteTag: Int?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        /*if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height

            let statusbarView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: statusBarHeight))
            statusbarView.backgroundColor = UIColor.black
            view.addSubview(statusbarView)
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor.black
        }*/
        self.trackTable.dataSource = self
        self.trackTable.delegate = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = .white
        self.trackTable.addSubview(self.refreshControl)
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: UIControl.Event.valueChanged)
        
        self.tracks = Dao.shared.queryTracks()
        self.tracks.sort(by: self.compareTrackLogDate)
        //let gpsLogs = Dao.shared.queryGpsTracks()
        print("tracks: \(self.tracks)")
    
        NotificationCenter.default.addObserver(self, selector: #selector(onTrackChanged), name: NSNotification.Name("TRACKCHANGED") , object: nil)
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func compareTrackLogDate(a: TrackLog, b: TrackLog) -> Bool {
        let date1 = Date(timeIntervalSince1970: TimeInterval(a.timestamp))
        let date2 = Date(timeIntervalSince1970: TimeInterval(b.timestamp))
        if (date1 >= date2) {
            print("TrackLog date -> date1 >= date2")
            return true
        } else {
            print("TrackLog date -> date1 < date2")
            return false
        }
    }
    

    @objc private func refreshData() {
        DispatchQueue.main.async {
            self.tracks.removeAll()
            self.tracks = Dao.shared.queryTracks()
            self.tracks.sort(by: self.compareTrackLogDate)
            self.trackTable.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc private func onTrackChanged() {
        print("onTrackChanged ######")
        refreshData();
    }
    
    /*override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }*/
    
    @objc private func deleteTrackAlert(_ sender: UITapGestureRecognizer) {
        self.deleteTag = sender.view!.tag
        print("delete track: \(self.tracks[self.deleteTag!].trackId)")
        let alert = UIAlertController(title: NSLocalizedString("action_delete_track_title", comment: ""), message: NSLocalizedString("action_delete_track_content", comment: ""), preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: {(alert: UIAlertAction!) in self.deleteTrack()})
        alert.addAction(confirm)
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    private func deleteTrack() {
        Dao.shared.deleteTrack(track: self.tracks[self.deleteTag!])
        self.tracks.remove(at: self.deleteTag!)
        self.trackTable.reloadData()
    }

    @IBAction func clickImport(_ sender: Any) {
        let importMenu = UIDocumentPickerViewController(documentTypes: ["public.item"], in: .import)
        //importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        importMenu.delegate = self
        /*if let popoverPresentationController = importMenu.popoverPresentationController {
            //popoverPresentationController.sourceView = sender
            // popoverPresentationController.sourceRect = sender.bounds
        }*/
        self.present(importMenu, animated: true, completion: nil)
    }
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print("vvv didPickDocumentsAt: \(String(describing: urls.first))")
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = NSLocalizedString("action_import", comment: "")
        hud.show(in: self.view)
        ImportGpx.readGpxFile(fileUrl: urls.first!, completion: { id in
            print("id: \(id)")
            hud.dismiss()
            self.refreshData()
        }) { error in
            print("error: \(error)")
            hud.dismiss()
        }
    }
}


extension TrackViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackItemViewCell", for: indexPath) as! TrackItemViewCell
        cell.track = self.tracks[indexPath.row]
        cell.deleteImageView.isUserInteractionEnabled = true
        cell.deleteImageView.tag = indexPath.row
        let deleteImageView = UITapGestureRecognizer(target: self, action: #selector(deleteTrackAlert))
        deleteImageView.numberOfTapsRequired = 1
        cell.deleteImageView.addGestureRecognizer(deleteImageView)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrackDetailViewController") as! TrackDetailViewController
            vc.track = self.tracks[indexPath.row]
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}
