//
//  AccountRowView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/15.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI
import MobileCoreServices

struct AccountRowView: View {
    var rawInfo: RawInfo
    @State var tag: Int? = nil
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 4.0) {
                Text(rawInfo.title)
                    .font(.footnote)
                    .fontWeight(.regular)
                    .foregroundColor(Color("colorText02"))
                    .multilineTextAlignment(.leading)
                Text(rawInfo.content)
                    .font(.body)
                    .fontWeight(.regular)
                    .multilineTextAlignment(.leading)
                    .lineLimit(1)
                    .onTapGesture(count: 2) {
                        if rawInfo.index == 0 {
                            print("copy")
                            UIPasteboard.general.string = rawInfo.content
                        }
                    }
            }
            .padding([.top, .bottom, .trailing], 8.0)
            Spacer()
            if rawInfo.type == 1 {
                Image(systemName: "chevron.right")
            }
        }
        .background(
           NavigationLink(
            destination: {
                VStack {
                    if rawInfo.index == 1 {
                        UpdateEmailView()
                    } else if rawInfo.index == 2 {
                        VerifyEmailView()
                    } else if rawInfo.index == 3 {
                        ElderLinkView()
                    } else {
                        EmptyView()
                    }
                }

            }(), tag: rawInfo.index, selection: $tag){
                EmptyView()
           }
           .disabled(rawInfo.type == 0)
           .hidden()
            
        )
        .onTapGesture(perform: {
            print("onTapGesture \(rawInfo.index)")
            if rawInfo.type == 1 {
                tag = rawInfo.index
            }
        })
    }
}

struct AccountRowView_Previews: PreviewProvider {
    static var previews: some View {
        let rawInfo = RawInfo.init(index: 0, title: "user id", content: "jakfoweow1231", type: 1)
        AccountRowView(rawInfo: rawInfo)
    }
}
