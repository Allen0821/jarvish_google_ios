//
//  AccountView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/15.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI
import FirebaseAuth


struct AccountView: View {
    @ObservedObject var viewModel = AccountViewModel()
    var dismiss: (() -> Void)!
    
    var body: some View {
        NavigationView {
            VStack {
                List(viewModel.rawInfos) { rawInfo in
                    AccountRowView(rawInfo: rawInfo)
                }
                
            }
            .listStyle(GroupedListStyle())
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(leading:
                                    Button(action: dismiss) {
                                           Image(systemName: "chevron.left")
                                       }
            )
            .navigationTitle("user_account")
        }
        .onReceive(NotificationCenter.default.publisher(for: Notification.Name(rawValue: "ACCOUNTUPDATED")), perform: { _ in
            viewModel.query()
        })
        .onAppear(perform: {
            viewModel.query()
        })
    }
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView()
    }
}
