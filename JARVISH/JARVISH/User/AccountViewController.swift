//
//  AccountViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/17.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

class AccountViewController: UIHostingController<AccountView> {
    init() {
        super.init(rootView: AccountView())
        rootView.dismiss = dismiss
    }
    
    @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func dismiss() {
        dismiss(animated: true, completion: nil)
    }
}
