//
//  ElderLinkPasswordView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/18.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI


struct ResetDoneView: View {
    @ObservedObject var viewModel: ElderLinkPasswordViewModel
    
    init(viewModel: ElderLinkPasswordViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack {
            Text("user_elder_reset_password_done_description")
                .font(.body)
                .fontWeight(.regular)
                .multilineTextAlignment(.leading)
            
            Button(action: {
                viewModel.done.toggle()
            }) {
                Spacer()
                Text(LocalizedStringKey("user_elder_back_to_link"))
                    .font(.body)
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                Spacer()
            }
            .padding(.vertical, 12.0)
            .background(Color("colorPrimary"))
            .cornerRadius(40.0)
            .padding(.top, 24.0)
            Spacer()
        }
        .padding(.horizontal, 32.0)
        .padding(.top, 32.0)
    }
}



struct ResetPasswordView: View {
    @ObservedObject var viewModel: ElderLinkPasswordViewModel
    
    init(viewModel: ElderLinkPasswordViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack {
            Text("user_elder_reset_password_description")
                .font(.body)
                .fontWeight(.regular)
                .multilineTextAlignment(.leading)
                
                
            HStack {
                TextField("user_elder_verification_code", text: $viewModel.code)
                    .lineLimit(1)
                    .padding(.all, 12.0)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 40)
                    .stroke(Color("colorText01"), lineWidth: 0.5))
            .padding(.top, 24.0)
            
            HStack {
                TextField("user_elder_new_password", text: $viewModel.password)
                    .textContentType(.password)
                    .keyboardType(.default)
                    .lineLimit(1)
                    .padding(.all, 12.0)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 40)
                    .stroke(Color("colorText01"), lineWidth: 0.5))
            .padding(.top, 4.0)
            
            HStack {
                TextField("user_elder_new_password_again", text: $viewModel.repassword)
                    .textContentType(.password)
                    .keyboardType(.default)
                    .lineLimit(1)
                    .padding(.all, 12.0)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 40)
                    .stroke(Color("colorText01"), lineWidth: 0.5))
            .padding(.top, 4.0)
            
            Button(action: {
                viewModel.requestResetPassword()
            }) {
                Spacer()
                Text(LocalizedStringKey("user_elder_submit_new_password"))
                    .font(.body)
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                Spacer()
            }
            .padding(.vertical, 12.0)
            .background(Color("colorPrimary"))
            .cornerRadius(40.0)
            .padding(.top, 12.0)
            Spacer()
        }
        .padding(.horizontal, 32.0)
        .padding(.top, 32.0)
    }
}


struct RequestCodeView: View {
    @ObservedObject var viewModel: ElderLinkPasswordViewModel
    
    init(viewModel: ElderLinkPasswordViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack {
            Text("user_elder_forgot_password_des")
                .font(.body)
                .fontWeight(.regular)
                .multilineTextAlignment(.leading)
                
                
            HStack {
                TextField("user_email_address", text: $viewModel.email)
                    .textContentType(/*@START_MENU_TOKEN@*/.emailAddress/*@END_MENU_TOKEN@*/)
                    .keyboardType(.emailAddress)
                    .lineLimit(1)
                    .padding(.all, 12.0)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 40)
                    .stroke(Color("colorText01"), lineWidth: 0.5))
            .padding(.top, 24.0)
            
            Button(action: {
                viewModel.requestVerificationCode()
            }) {
                Spacer()
                Text(LocalizedStringKey("user_elder_send_verification_code"))
                    .font(.body)
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                Spacer()
            }
            .padding(.vertical, 12.0)
            .background(Color("colorPrimary"))
            .cornerRadius(40.0)
            .padding(.top, 12.0)
            Spacer()
        }
        .padding(.horizontal, 32.0)
        .padding(.top, 32.0)
    }
}

struct ElderLinkPasswordView: View {
    @Environment(\.presentationMode) private var presentation
    @ObservedObject var viewModel = ElderLinkPasswordViewModel()
    
    var body: some View {
        ZStack {
            if viewModel.state == 0 {
                RequestCodeView(viewModel: viewModel)
            } else if viewModel.state == 1 {
                ResetPasswordView(viewModel: viewModel)
            } else {
                ResetDoneView(viewModel: viewModel)
            }
            
            if viewModel.isProgress {
                ProgressView()
            }
        }
        .disabled(viewModel.isProgress)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
        Button(action: {self.presentation.wrappedValue.dismiss()}) {
            Image(systemName: "chevron.left")
        }.disabled(viewModel.isProgress))
        .navigationTitle("user_elder_reset_password")
        .navigationBarTitleDisplayMode(.inline)
        .alert(isPresented: $viewModel.isShowAlert, content: {
            return Alert(title: Text(viewModel.elderLinkCode.rawValue))
        })
        .onChange(of: viewModel.done, perform: { value in
            if value {
                self.presentation.wrappedValue.dismiss()
            }
        })
    }
}

struct ElderLinkPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ElderLinkPasswordView()
    }
}
