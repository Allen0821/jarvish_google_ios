//
//  ElderLinkView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/16.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

struct ElderLinkView: View {
    @Environment(\.presentationMode) private var presentation
    @ObservedObject var viewModel = ElderLinkViewModel()
    
    
    var body: some View {
        ZStack {
            VStack {
                Text("user_link_elder_description")
                    .font(.body)
                    .fontWeight(.regular)
                    .multilineTextAlignment(.leading)
                    .padding(.horizontal, 8.0)
                
                HStack {
                    TextField("user_email_address", text: $viewModel.email)
                        .textContentType(/*@START_MENU_TOKEN@*/.emailAddress/*@END_MENU_TOKEN@*/)
                        .keyboardType(.emailAddress)
                        .lineLimit(1)
                        .padding(.all, 12.0)
                }
                .overlay(
                    RoundedRectangle(cornerRadius: 40)
                        .stroke(Color("colorText01"), lineWidth: 0.5))
                .padding(.top, 24.0)
                    
                
                HStack {
                    TextField("auth_user_password", text: $viewModel.password)
                        .lineLimit(1)
                        .padding(.all, 12.0)
                        
                }
                .overlay(
                    RoundedRectangle(cornerRadius: 40)
                        .stroke(Color("colorText01"), lineWidth: 0.5))
                
                HStack {
                    Spacer()
                    NavigationLink(destination: ElderLinkPasswordView()) {
                        Text("user_elder_forgot_password")
                            .font(.callout)
                            .fontWeight(.regular)
                            .foregroundColor(Color("colorSecondary"))
                            .padding(.trailing)
                                
                    }
                }
                
                Button(action: {
                    hideKeyboard()
                    viewModel.LoginWithEmail()
                }) {
                    Spacer()
                    Text(LocalizedStringKey("user_elder_sign_in"))
                        .font(.body)
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                    Spacer()
                }
                .padding(.vertical, 12.0)
                .background(Color("colorPrimary"))
                .cornerRadius(40.0)
                .padding(.top, 12.0)
                
                
                Button(action: {
                    hideKeyboard()
                    viewModel.facebookLogin()
                }) {
                    Spacer()
                    Text(LocalizedStringKey("auth_method_sign_in_with_facebook"))
                        .font(.body)
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                    Spacer()
                }
                .padding(.vertical, 12.0)
                .background(Color("colorFacebook"))
                .cornerRadius(40.0)
                .padding(.top, 24.0)
                
                Spacer()
                
            }
            .padding(.horizontal, 32.0)
            .padding(.top, 32.0)
            .navigationTitle("user_link_elder")
            .navigationBarTitleDisplayMode(.inline)
            
            if viewModel.isProgress {
                ProgressView()
            }
        }
        .disabled(viewModel.isProgress)
        .onTapGesture(perform: {
            hideKeyboard()
        })
        .alert(isPresented: $viewModel.isShowAlert, content: {
            return Alert(title: Text(viewModel.elderLinkCode.rawValue))
        })
        .alert(isPresented: $viewModel.isLinked) {
            Alert(
                title: Text("user_elder_link_Complected_title"),
                message: Text("user_elder_link_Complected_content"),
                dismissButton: .cancel(Text("action_confirm")) {
                    self.presentation.wrappedValue.dismiss()
                }
            )
        }
        
    }
}

struct ElderLinkView_Previews: PreviewProvider {
    static var previews: some View {
        ElderLinkView()
    }
}
