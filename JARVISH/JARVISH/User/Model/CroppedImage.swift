//
//  CroppedImage.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/24.
//  Copyright © 2021 jarvish. All rights reserved.
//

import UIKit

/// Crops a UIImage
/// - Parameters:
///   - image: the original image before processing.
///   - rect: the CGRect to which the image will be cropped.
/// - Returns: UIImage.
func croppedImage(from image: UIImage, croppedTo rect: CGRect) -> UIImage {

    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()

    let drawRect = CGRect(x: -rect.origin.x, y: -rect.origin.y, width: image.size.width, height: image.size.height)

    context?.clip(to: CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height))

    image.draw(in: drawRect)

    let subImage = UIGraphicsGetImageFromCurrentImageContext()

    UIGraphicsEndImageContext()
    return subImage!
}
