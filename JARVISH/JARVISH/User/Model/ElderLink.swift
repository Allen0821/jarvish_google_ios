//
//  ElderLink.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/18.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

enum ElderLinkCode: LocalizedStringKey {
    case NONE = "code_elder_none"
    case INVALID_EMAIL_ADDRESS = "code_elder_invalid_email_address"
    case INVALID_PASSWORD = "code_elder_invalid_password"
    case PASSWORD_NOT_MATCH = "code_elder_password_not_match"
    case SEND_VERIFICATION_CODE_FAILURE = "code_elder_send_verification_code_failure"
    case INVALID_VERIFICATION_CODE = "code_elder_invalid_verification_code"
    case RESET_PASSWORD_FAILURE = "code_elder_reset_password_failed"
    case LINK_FAILURE = "code_elder_link_elder_failure"
    case SIGN_IN_FAILURE = "code_elder_link_elder_sign_in_failure"
    
    var message: LocalizedStringKey {
        return rawValue
    }
    
}

class ElderLink {
    
    
    func linkFromEmailPassword(email: String, password: String, completion: @escaping (_ memberID: String?)->()) {
        JarvishProvider.rx.request(
            .loginWithEmail(email: email, password: password))
        .filterSuccessfulStatusCodes()
        .mapJSON()
        .subscribe(onSuccess: { result in
            print(result)
            if let session = Session(dictionary: result as! [String : Any]) {
                completion(session.memberId)
            } else {
                completion(nil)
            }
        }) { error in
            print("[Elder]: failed to signin with email: \(error)")
            completion(nil)
        }
    }
    
    
    func linkFromFacebook(token: String, completion: @escaping (_ memberID: String?)->()) {
        JarvishProvider.rx.request(
            .loginWithFacebook(token: token))
        .filterSuccessfulStatusCodes()
        .mapJSON()
        .subscribe(onSuccess: { result in
            if let session = Session(dictionary: result as! [String : Any]) {
                completion(session.memberId)
            } else {
                completion(nil)
            }
        }) { error in
            print("[Elder]: facebook login error \(error)")
            completion(nil)
        }
    }
    
    
    func requestVerificationCode(email: String, completion: @escaping (ElderLinkCode)->()) {
        JarvishProvider.rx.request(
            .forgetPassword(email: email))
        .filterSuccessfulStatusCodes()
        .subscribe(onSuccess: { result in
            completion(ElderLinkCode.NONE)
        }) { error in
            print("[Elder]: failed to request verification code: \(error)")
            completion(ElderLinkCode.SEND_VERIFICATION_CODE_FAILURE)
        }
    }
    
    
    func requestResetPassword(verifyCode: String, email: String, password: String, completion: @escaping (ElderLinkCode)->()) {
        let req = JarvishProvider.rx.request(
            .resetPassword(verificationCode: verifyCode, email: email, password: password))
        .filterSuccessfulStatusCodes()
        .subscribe(onSuccess: { result in
            completion(ElderLinkCode.NONE)
        }) { error in
            completion(ElderLinkCode.RESET_PASSWORD_FAILURE)
        }
    }
}
