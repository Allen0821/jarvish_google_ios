//
//  RawInfo.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/15.
//  Copyright © 2021 jarvish. All rights reserved.
//


struct RawInfo: Identifiable {
    var id: String = UUID().uuidString
    
    var index: Int
    var title: String
    var content: String
    var type: Int
    
    init(index: Int, title: String, content: String, type: Int) {
        self.index = index
        self.title = title
        self.content = content
        self.type = type
    }
}
