//
//  UserManager.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/17.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI
import FirebaseAuth

enum UserManagerCode: LocalizedStringKey {
    case CHANGE_EMAIL_OK = "code_change_email_none"
    case VERIFY_EMAIL_OK = "code_verify_email_ok"
    case VERIFY_EMAIL_FAILURE = "code_verify_email_failure"
    case UPDATE_PROFILE_OK = "code_user_profile_update_ok"
    case UPDATE_PROFILE_FAILURE = "code_user_profile_update_failure"
    case EMAIL_ALREADY_IN_USE = "code_change_email_user_in_use"
    case NEED_REAUTHENTICATION = "code_change_email_reauth"
    case CHANGE_EMAIL_FAILURE = "code_change_email_failure"
    
    var message: LocalizedStringKey {
        return rawValue
    }
}

class UserManager {
    let storeAccount = StoreAccount()
    let currentUser: User
    
    init() {
        self.currentUser = Auth.auth().currentUser!
    }
    
    func updateEmail(email: String, completion: @escaping (UserManagerCode)->()) {
        currentUser.updateEmail(to: email) { (error) in
            print("updateEmail: \(String(describing: error?.localizedDescription))")
            if error != nil {
                if let errorCode = AuthErrorCode(rawValue: error!._code) {
                    print("updateEmail errorCode: \(errorCode.rawValue)")
                    switch errorCode {
                    case .emailAlreadyInUse:
                        completion(UserManagerCode.EMAIL_ALREADY_IN_USE)
                        break
                    case .requiresRecentLogin:
                        completion(UserManagerCode.NEED_REAUTHENTICATION)
                        break
                    default:
                        completion(UserManagerCode.CHANGE_EMAIL_FAILURE)
                    }
                }
            } else {
                let data: [String: Any] = [
                        "email": email,
                        "emailVerified": false
                    ]
                self.storeAccount.updateDocument(documentID: self.currentUser.uid, data: data) { result in
                    completion(UserManagerCode.CHANGE_EMAIL_OK)
                }
            }
        }
    }
    
    func sendEmailVerification(completion: @escaping (Bool)->()) {
        currentUser.sendEmailVerification() { (error) in
            if let error = error {
                print("sendEmailVerification error: \(error)")
                completion(false)
            } else {
                completion(true)
            }
        }
    }
    
    func updateUserProfile(displayName: String, photoURL: URL?, completion: @escaping (Bool)->()) {
        let changeRequest = currentUser.createProfileChangeRequest()
        changeRequest.displayName = displayName
        if photoURL != nil {
            changeRequest.photoURL = photoURL
        }
        changeRequest.commitChanges { (error) in
            if let error = error {
                print("updateUserProfile error: \(error)")
                completion(false)
            } else {
                completion(true)
            }
        }
    }
}
