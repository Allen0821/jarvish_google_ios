//
//  UpdateEmailView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/17.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

struct UpdateEmailView: View {
    @Environment(\.presentationMode) private var presentation
    @ObservedObject var viewModel = UpdateEmailViewModel()
    
    var body: some View {
        ZStack {
            VStack {
                Text("user_change_email_address_description")
                    .font(.body)
                    .fontWeight(.regular)
                    .multilineTextAlignment(.leading)
                    .padding(.horizontal, 8.0)
                
                HStack {
                    Image(systemName: "envelope")
                        .resizable()
                        .frame(width: 20, height: 16)
                        .foregroundColor(.white)
                    
                    TextField("", text: $viewModel.email)
                        .textContentType(/*@START_MENU_TOKEN@*/.emailAddress/*@END_MENU_TOKEN@*/)
                        .keyboardType(.emailAddress)
                        .foregroundColor(.white)
                        .padding(.leading, 4.0)
                }
                .padding(.all, 12.0)
                .background(Color("colorTextFiledBG"))
                .cornerRadius(40.0)
                .padding(.top, 24.0)
                
                Button(action: {
                        hideKeyboard()
                        viewModel.updateEmail()}) {
                    Spacer()
                    Text(LocalizedStringKey("user_change_email_address_confirm"))
                        .font(.body)
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                    Spacer()
                }
                .disabled(viewModel.isSentDisabled)
                .padding(.vertical, 12.0)
                .background(buttonBackgroundColor())
                .cornerRadius(40.0)
                .padding(.top, 4.0)
                
                Spacer()
            }
            .padding(.horizontal, 32.0)
            .padding(.top, 32.0)
            
            if viewModel.isProgress {
                ProgressView()
            }
        }
        .disabled(viewModel.isProgress)
        .navigationTitle("user_change_email_address")
        .navigationBarTitleDisplayMode(.inline)
        .alert(isPresented: $viewModel.isShowAlert, content: {
            return Alert(title: Text(viewModel.userManagerCode.rawValue))
        })
        .onChange(of: viewModel.isEmailSent, perform: { value in
            if value {
                self.presentation.wrappedValue.dismiss()
            }
        })
    }
    
    func buttonBackgroundColor() -> Color {
        if viewModel.isSentDisabled {
            return Color.gray
        } else {
            return Color("colorPrimary")
        }
    }
}

struct UpdateEmailView_Previews: PreviewProvider {
    static var previews: some View {
        UpdateEmailView()
    }
}
