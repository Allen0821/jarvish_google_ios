//
//  VerifyEmailView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/17.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

struct VerifyEmailView: View {
    @Environment(\.presentationMode) private var presentation
    @ObservedObject var viewModel = VerifyEmailViewModel()
    
    var body: some View {
        ZStack {
            VStack {
                if !viewModel.sent {
                    Text("user_email_verification_description \(viewModel.email)")
                        .font(.body)
                        .fontWeight(.regular)
                        .multilineTextAlignment(.leading)
                    
                    Button(action: {
                        viewModel.sendVerifyEmail()
                    }) {
                        Spacer()
                        Text(LocalizedStringKey("user_email_verification_send"))
                            .font(.body)
                            .fontWeight(.bold)
                            .foregroundColor(Color.white)
                        Spacer()
                    }
                    .padding(.vertical, 12.0)
                    .background(Color("colorPrimary"))
                    .cornerRadius(40.0)
                    .padding(.top, 12.0)
                } else {
                    Text("user_email_verification_sent_description \(viewModel.email)")
                        .font(.body)
                        .fontWeight(.regular)
                        .multilineTextAlignment(.leading)
                    
                    Button(action: {
                        self.presentation.wrappedValue.dismiss()
                    }) {
                        Spacer()
                        Text(LocalizedStringKey("action_confirm"))
                            .font(.body)
                            .fontWeight(.bold)
                            .foregroundColor(Color.white)
                        Spacer()
                    }
                    .padding(.vertical, 12.0)
                    .background(Color("colorPrimary"))
                    .cornerRadius(40.0)
                    .padding(.top, 12.0)
                }
                Spacer()
            }
            .padding(.horizontal, 32.0)
            .padding(.top, 32.0)
            
            if viewModel.isProgress {
                ProgressView()
            }
        }
        .navigationTitle("user_email_verification")
        .navigationBarTitleDisplayMode(.inline)
        .alert(isPresented: $viewModel.isShowAlert, content: {
            return Alert(title: Text(UserManagerCode.UPDATE_PROFILE_OK.rawValue))
        })
    }
}

struct VerifyEmailView_Previews: PreviewProvider {
    static var previews: some View {
        VerifyEmailView()
    }
}
