//
//  CropAvatarView+Extensions.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/24.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

extension CropAvatarView {
   
    func loadImage() {
        guard let inputImage = viewModel.inputImage else { return }
        let w = inputImage.size.width
        let h = inputImage.size.height
        displayedImage = inputImage
        inputImageAspectRatio = w / h
        resetImageOriginAndScale()
    }
    
    func resetImageOriginAndScale() {
        print("reposition")
        let screenAspect: CGFloat = getAspect()

        withAnimation(.easeInOut){
            if inputImageAspectRatio >= screenAspect {
                displayW = UIScreen.main.bounds.width
                displayH = displayW / inputImageAspectRatio
            } else {
                displayH = UIScreen.main.bounds.height
                displayW = displayH * inputImageAspectRatio
            }
            currentAmount = 0
            zoomAmount = 1
            currentPosition = .zero
            newPosition = .zero
        }
    }
    
    private func getAspect() -> CGFloat {
        let screenAspectRatio = UIScreen.main.bounds.width / UIScreen.main.bounds.height
        return screenAspectRatio
    }
    
    func repositionImage() {
        let screenAspect: CGFloat = getAspect()
        let diameter = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        
        if screenAspect <= 1.0 {
            if inputImageAspectRatio > screenAspect {
                displayW = diameter * zoomAmount
                displayH = displayW / inputImageAspectRatio
            } else {
                displayH = UIScreen.main.bounds.height * zoomAmount
                displayW = displayH * inputImageAspectRatio
            }
        } else {
            if inputImageAspectRatio < screenAspect {
                displayH = diameter * zoomAmount
                displayW = displayH * inputImageAspectRatio
            } else {
                displayW = UIScreen.main.bounds.width * zoomAmount
                displayH = displayW / inputImageAspectRatio
            }
        }
        
        horizontalOffset = (displayW - diameter ) / 2
        verticalOffset = ( displayH - diameter) / 2

        ///Keep the user from zooming too far in. Adjust as required in your individual project.
        if zoomAmount > 4.0 {
                zoomAmount = 4.0
        }
  
        let adjust: CGFloat = 0.0

        if displayH >= diameter {
            if newPosition.height > verticalOffset {
                print("newPosition.height > verticalOffset")
                    newPosition = CGSize(width: newPosition.width, height: verticalOffset - adjust + inset)
                    currentPosition = CGSize(width: newPosition.width, height: verticalOffset - adjust + inset)
            }
            
            if newPosition.height < ( verticalOffset * -1) {
                print("newPosition.height < ( verticalOffset * -1)")
                    newPosition = CGSize(width: newPosition.width, height: ( verticalOffset * -1) - adjust - inset)
                    currentPosition = CGSize(width: newPosition.width, height: ( verticalOffset * -1) - adjust - inset)
            }
            
        } else {
            print("else: H")
                newPosition = CGSize(width: newPosition.width, height: 0)
                currentPosition = CGSize(width: newPosition.width, height: 0)
        }
        
        if displayW >= diameter {
            if newPosition.width > horizontalOffset {
                print("newPosition.width > horizontalOffset")
                    newPosition = CGSize(width: horizontalOffset + inset, height: newPosition.height)
                    currentPosition = CGSize(width: horizontalOffset + inset, height: currentPosition.height)
            }
            
            if newPosition.width < ( horizontalOffset * -1) {
                print("newPosition.width < ( horizontalOffset * -1)")
                    newPosition = CGSize(width: ( horizontalOffset * -1) - inset, height: newPosition.height)
                    currentPosition = CGSize(width: ( horizontalOffset * -1) - inset, height: currentPosition.height)

            }
            
        } else {
            print("else: W")
                newPosition = CGSize(width: 0, height: newPosition.height)
                currentPosition = CGSize(width: 0, height: newPosition.height)
        }

        if displayW < diameter - inset && displayH < diameter - inset {
            resetImageOriginAndScale()
        }
    }
    
    func processImage() {
        let scale = (viewModel.inputImage!.size.width) / displayW
        let originAdjustment = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        let diameter = ( originAdjustment - inset * 2 ) * scale
        
        let xPos = ( ( ( displayW - originAdjustment ) / 2 ) + inset + ( currentPosition.width * -1 ) ) * scale
        let yPos = ( ( ( displayH - originAdjustment ) / 2 ) + inset + ( currentPosition.height * -1 ) ) * scale
        
        viewModel.outputImage = croppedImage(from: viewModel.inputImage!, croppedTo: CGRect(x: xPos, y: yPos, width: diameter, height: diameter))
        viewModel.avatar = viewModel.outputImage
        viewModel.avatarChanged = true
    }
}
