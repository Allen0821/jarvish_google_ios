//
//  CropAvatarView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/23.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

struct CropAvatarView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var viewModel: ProfileEditViewModel
    
    @State var displayedImage: UIImage?
    @State var displayW: CGFloat = 0.0
    @State var displayH: CGFloat = 0.0
    @State var inputImageAspectRatio: CGFloat = 0.0
    
    @State var currentAmount: CGFloat = 0
    @State var zoomAmount: CGFloat = 1.0
    @State var currentPosition: CGSize = .zero
    @State var newPosition: CGSize = .zero
    @State var horizontalOffset: CGFloat = 0.0
    @State var verticalOffset: CGFloat = 0.0
    
    let inset: CGFloat = 10
    @State var originalZoom: CGFloat = 1.0
    
    
  
    var body: some View {
        ZStack {
            ZStack{
                Color.black.opacity(0.75)
                if displayedImage != nil {
                    Image(uiImage: displayedImage!)
                        .resizable()
                        .scaleEffect(zoomAmount + currentAmount)
                        .scaledToFill()
                        .aspectRatio(contentMode: .fit)
                        .offset(x: self.currentPosition.width, y: self.currentPosition.height)
                        .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                        .clipped()
                }
            }
            
            Rectangle()
                .fill(Color.black)
                .opacity(0.50)
                .mask(HoleShapeMask())
            
            Image("")
                .frame(width: UIScreen.main.bounds.width,
                       height: UIScreen.main.bounds.width)
                .border(Color.white, width: 1.5)
                .offset(x: 0, y: -2)

            VStack {
                Spacer()
                HStack {
                    Button(action: {
                        self.viewModel.viewMode = 0
                    }) {
                        Text("action_cancel")
                            .font(.body)
                            .fontWeight(.regular)
                            .foregroundColor(Color.white)
                    }
                    Spacer()
                    Button(action: {
                        viewModel.inputImage = displayedImage
                        processImage()
                        self.viewModel.viewMode = 0
                    }) {
                        Text("action_confirm")
                            .font(.body)
                            .fontWeight(.regular)
                            .foregroundColor(Color.white)
                    }
                }
            }
            .padding([.leading, .bottom, .trailing], 48.0)
            
        }
        .edgesIgnoringSafeArea(.all)
        .gesture(
            MagnificationGesture()
                .onChanged { amount in
                    self.currentAmount = amount - 1
                }
                .onEnded { amount in
                    self.zoomAmount += self.currentAmount
                    if zoomAmount > 4.0 {
                        withAnimation {
                            zoomAmount = 4.0
                        }
                    }
                    self.currentAmount = 0
                    withAnimation {
                        repositionImage()
                    }
                }
        )
        .simultaneousGesture(
            DragGesture()
                .onChanged { value in
                    self.currentPosition = CGSize(width: value.translation.width + self.newPosition.width, height: value.translation.height + self.newPosition.height)
                }
                .onEnded { value in
                    self.currentPosition = CGSize(width: value.translation.width + self.newPosition.width, height: value.translation.height + self.newPosition.height)
                    self.newPosition = self.currentPosition
                    withAnimation {
                        repositionImage()
                    }
                }
        )
        .simultaneousGesture(
            TapGesture(count: 2)
                .onEnded(  { resetImageOriginAndScale() } )
        )
        .navigationBarHidden(true)
        .onAppear(perform: {
            loadImage()
        })
    }
    
    
    func HoleShapeMask() -> some View {
        let rect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        var shape = Rectangle().path(in: rect)
        shape.addPath(Circle().path(in: rect))
        return shape.fill(style: FillStyle(eoFill: true))
    }
}

struct CropAvatarView_Previews: PreviewProvider {
    static var previews: some View {
        CropAvatarView(viewModel: ProfileEditViewModel())
    }
}
