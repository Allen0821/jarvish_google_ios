//
//  ProfileEditHostingViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/23.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

class ProfileEditHostingViewController: UIHostingController<ProfileEditView> {
    init(avatar: UIImage, name: String) {
        super.init(rootView: ProfileEditView())
        rootView.dismiss = dismiss
        rootView.avatar = avatar
        rootView.name = name
    }
    
    @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func dismiss() {
        dismiss(animated: true, completion: nil)
    }
}
