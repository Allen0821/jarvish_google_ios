//
//  ProfileEditView.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/23.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI

struct ImagePicker: UIViewControllerRepresentable {
    @ObservedObject var viewModel: ProfileEditViewModel
    

    init(viewModel: ProfileEditViewModel) {
        self.viewModel = viewModel
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePicker>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        if viewModel.source == 0 {
            picker.sourceType = .photoLibrary
        } else {
            picker.sourceType = .camera
        }
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        let parent: ImagePicker

        init(_ parent: ImagePicker) {
            self.parent = parent
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let uiImage = info[.originalImage] as? UIImage {
                    parent.viewModel.inputImage  = uiImage
                }
            parent.viewModel.viewMode = 2
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            parent.viewModel.viewMode = 0
        }
        
    }
}


struct ProfileEditView: View {
    @State private var showingImagePicker = false
    @State private var inputImage: UIImage?
    @ObservedObject var viewModel = ProfileEditViewModel()
   
    @State var selection: Int? = nil
    
    var dismiss: (() -> Void)!
    var avatar: UIImage!
    var name: String!
    
    //@State var viewMode = 0
    
    
    var body: some View {
        if viewModel.viewMode == 0 {
            NavigationView {
                ZStack {
                    VStack {
                        ZStack(alignment: .bottomTrailing) {
                            Image(uiImage: viewModel.avatar!)
                                .resizable()
                                .frame(width: 148, height: 148)
                                .scaledToFill()
                                .clipShape(Circle())
                                .overlay(Circle().stroke(Color("colorPrimary"), lineWidth: 1))
                            
                            Button(action: {
                                hideKeyboard()
                                self.showingImagePicker.toggle()
                            }) {
                                Image(systemName: "camera.circle.fill")
                                    .resizable()
                                    .frame(width: 32, height: 32)
                                    .foregroundColor(.white)
                                    .padding(.all, 4.0)
                                    .background(Color.black)
                                    .clipShape(Circle())
                            }.offset(x: -4, y: -4)
                        }
                        
                        HStack {
                            Text("user_name")
                                .font(.callout)
                                .fontWeight(.regular)
                                .foregroundColor(Color("colorText02"))
                                .padding(.top, 24.0)
                            Spacer()
                        }
                        TextField("", text: $viewModel.name)
                            .lineLimit(1)
                            .padding(.all, 8.0)
                            .background(
                                RoundedRectangle(cornerRadius: 10, style: .continuous)
                                    .stroke(Color("colorText01"), lineWidth: 0.5)
                                    )
                            .padding(.top, 8.0)
                        Spacer()
                    }
                    .padding(.horizontal, 32.0)
                    .padding(.top, 32.0)
                    .navigationBarTitleDisplayMode(.inline)
                    .navigationBarItems(leading:
                                            Button(action: {
                                                viewModel.isDiscard.toggle()
                                            }) {
                                                Text("user_discard_edit")
                                               }.disabled(viewModel.isProgress),
                                        trailing: Button(action: {
                                            hideKeyboard()
                                            viewModel.saveChange()
                                        }) {
                                            Text("user_save_edit")
                                        }
                                        .disabled(viewModel.isProgress)
                                        
                    )
                    .navigationTitle("user_edit_profile")
                    .onAppear(perform: {
                        if viewModel.queryProfile {
                            viewModel.avatar = avatar
                            viewModel.getStoreProfile()
                        }
                    })
                   
                
                    if viewModel.isProgress {
                        ProgressView()
                            
                    }
                }
            }
            .disabled(viewModel.isProgress)
            .alert(isPresented: $viewModel.isShowAlert, content: {
                return Alert(title: Text("code_user_profile_update_failure"))
            })
            .alert(isPresented: $viewModel.isDiscard){
                Alert(
                    title: Text("user_discard_edit_content"),
                    primaryButton: .destructive(Text("user_discard")) {
                        self.dismiss()
                    },
                    secondaryButton: .cancel()
                )
            }
            .onChange(of: viewModel.isUpdated, perform: { value in
                if value {
                    dismiss()
                }
            })
            .actionSheet(isPresented: self.$showingImagePicker) {
                ActionSheet(
                    title: Text("user_add_photo"),
                    message: nil,
                    buttons: [
                        .default(Text("user_add_photo_gallery")) {
                            viewModel.source = 0
                            viewModel.viewMode = 1
                        },
                        .default(Text("user_add_photo_camera")) { viewModel.source = 1
                            viewModel.viewMode = 1
                        },
                        .cancel()
                    ])
            }
        } else if viewModel.viewMode == 1 {
            ImagePicker(viewModel: viewModel)
        } else {
            CropAvatarView(viewModel: viewModel)
        }
    }
    
    
    func loadImage() {
        guard let inputImage = inputImage else { return }
        viewModel.avatar = inputImage
        self.selection = 1
    }
   
}

extension View {
    func hideKeyboard() {
        let resign = #selector(UIResponder.resignFirstResponder)
        UIApplication.shared.sendAction(resign, to: nil, from: nil, for: nil)
    }
}

struct ProfileEditView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileEditView()
    }
}
