//
//  AccountViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/15.
//  Copyright © 2021 jarvish. All rights reserved.
//

import SwiftUI
import FirebaseAuth
import Firebase

class AccountViewModel: ObservableObject {
    @Published var rawInfos = [RawInfo]()
    var currentUser: User?
    let storeAccount = StoreAccount()
    
    func query() {
        if let user = Auth.auth().currentUser {
            self.currentUser = user
            storeAccount.getDocument(documentID: self.currentUser!.uid) { account in
                self.rawInfos.removeAll()
                if let account = account {
                    let id = RawInfo.init(
                        index: 0,
                        title: NSLocalizedString("user_id", comment: ""),
                        content: self.currentUser!.uid,
                        type: 0)
                    self.rawInfos.append(id)
                    
                    /*var emailType = 0
                    for profile in self.currentUser!.providerData {
                        let providerID = profile.providerID
                        print("provider ID: \(providerID)")
                        if providerID.elementsEqual("facebook.com") ||
                            providerID.elementsEqual("apple.com") {
                            emailType = 1
                        }
                    }*/
                    
                    let email = RawInfo.init(
                        index: 1,
                        title: NSLocalizedString("user_email_address", comment: ""),
                        content: self.currentUser!.email ?? "",
                        type: 1)
                    self.rawInfos.append(email)
                    
                    
                    var isEmailVerified: String
                    var isEmailVerifiedType = 0
                    if self.currentUser!.isEmailVerified {
                        isEmailVerified = NSLocalizedString("user_email_verification_true", comment: "")
                    } else {
                        isEmailVerified = NSLocalizedString("user_email_verification_false", comment: "")
                        isEmailVerifiedType = 1
                    }
                    
                    if self.currentUser!.isEmailVerified != account.isEmailVerified {
                        self.updateEmailVerified(user: user)
                    }
                    
                    let verifyEmail = RawInfo.init(
                        index: 2,
                        title: NSLocalizedString("user_email_verification", comment: ""),
                        content: isEmailVerified,
                        type: isEmailVerifiedType)
                    self.rawInfos.append(verifyEmail)

                    
                    var elderContent: String = ""
                    var elderType: Int = 0
                    if account.isElder {
                        elderContent = account.elderId ?? ""
                        elderType = 0
                    } else {
                        elderContent = NSLocalizedString("user_not_elder_content", comment: "")
                        elderType = 1
                    }
                    let elder = RawInfo.init(
                        index: 3,
                        title: NSLocalizedString("user_elder", comment: ""),
                        content: elderContent,
                        type: elderType)
                    self.rawInfos.append(elder)
                    
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    let createString = formatter.string(from: account.created!.dateValue())
                    let created = RawInfo.init(
                        index: 4,
                        title: NSLocalizedString("user_created_date", comment: ""),
                        content: createString,
                        type: 0)
                    self.rawInfos.append(created)
                }
            }
        }
    }
    
    private func updateEmailVerified(user: User) {
        let storeAccount = StoreAccount()
        storeAccount.updateDocument(documentID: user.uid, data: ["emailVerified" : user.isEmailVerified]) { result in
            print("updateEmailVerified: \(result)")
        }
    }
    
}
