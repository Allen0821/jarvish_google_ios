//
//  ElderLinkPasswordViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/18.
//  Copyright © 2021 jarvish. All rights reserved.
//


class ElderLinkPasswordViewModel: ObservableObject {
    @Published var email: String = ""
    @Published var code: String = ""
    @Published var password: String = ""
    @Published var repassword: String = ""
    
    @Published var state: Int = 0
    @Published var isProgress: Bool = false
    @Published var isShowAlert: Bool = false
    var elderLinkCode: ElderLinkCode = ElderLinkCode.NONE
    @Published var done: Bool = false
    
    let elderLink = ElderLink()
    
    func requestVerificationCode() {
        self.isProgress = true
        elderLink.requestVerificationCode(email: self.email) { code in
            self.isProgress = false
            if code == ElderLinkCode.NONE {
                self.state = 1
            } else {
                self.elderLinkCode = code
                self.isShowAlert = true
            }
        }
    }
    
    
    func requestResetPassword() {
        if code.isEmpty || code.count != 8 {
            self.elderLinkCode = ElderLinkCode.INVALID_VERIFICATION_CODE
            self.isShowAlert = true
            return
        }
        
        if password.isEmpty {
            self.elderLinkCode = ElderLinkCode.INVALID_PASSWORD
            self.isShowAlert = true
            return
        }
        
        if !password.elementsEqual(repassword) {
            self.elderLinkCode = ElderLinkCode.PASSWORD_NOT_MATCH
            self.isShowAlert = true
            return
        }
        
        self.isProgress = true
        elderLink.requestResetPassword(
            verifyCode: self.code,
            email: self.email,
            password: self.password) { code in
            self.isProgress = false
            if code == ElderLinkCode.NONE {
                self.state = 2
            } else {
                self.elderLinkCode = code
                self.isShowAlert = true
            }
        }
    }
}
