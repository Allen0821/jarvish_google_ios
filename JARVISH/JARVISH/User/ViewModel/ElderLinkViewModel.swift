//
//  ElderLinkViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/18.
//  Copyright © 2021 jarvish. All rights reserved.
//
import FacebookLogin
import FacebookCore
import FirebaseAuth

class ElderLinkViewModel: ObservableObject {
    @Published var email: String = ""
    @Published var password: String = ""
    
    @Published var isLinked = false
    @Published var isProgress: Bool = false
    @Published var isShowAlert: Bool = false
    var elderLinkCode: ElderLinkCode = ElderLinkCode.NONE
    
    let elderLink = ElderLink()
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func LoginWithEmail() {
        
        if !isValidEmail(email: email) {
            self.elderLinkCode = ElderLinkCode.INVALID_EMAIL_ADDRESS
            self.isShowAlert = true
            return
        }
        
        if password.isEmpty {
            self.elderLinkCode = ElderLinkCode.INVALID_PASSWORD
            self.isShowAlert = true
            return
        }
        
        self.isProgress = true
        elderLink.linkFromEmailPassword(email: self.email, password: self.password) { id in
            if let id = id {
                self.updateStoreAccount(id: id) { updateResult in
                    self.isProgress = false
                    if updateResult {
                        self.isLinked = true
                    } else {
                        self.elderLinkCode = ElderLinkCode.LINK_FAILURE
                        self.isShowAlert = true
                    }
                }
            } else {
                self.isProgress = false
                self.elderLinkCode = ElderLinkCode.SIGN_IN_FAILURE
                self.isShowAlert = true
            }
        }
    }
    
    
    func facebookLogin() {
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: [.email, .publicProfile], viewController: nil) { result in
            switch result {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let granted, let declined, let token):
                print("Logged in! \(granted) \(declined) \(token)")
                self.isProgress = true
                self.elderLink.linkFromFacebook(token: token.tokenString) { id in
                    if let id = id {
                        self.updateStoreAccount(id: id) { updateResult in
                            self.isProgress = false
                            if updateResult {
                                self.isLinked = true
                            } else {
                                self.elderLinkCode = ElderLinkCode.LINK_FAILURE
                                self.isShowAlert = true
                            }
                        }
                    } else {
                        self.isProgress = false
                        self.elderLinkCode = ElderLinkCode.SIGN_IN_FAILURE
                        self.isShowAlert = true
                    }
                }
            }
            
        }
    }
    
    
    private func updateStoreAccount(id: String, completion: @escaping (Bool)->()) {
        let data: [String: Any] = [
                "elderId": id,
                "elder": true
            ]
        let storeAccount = StoreAccount()
        let user = Auth.auth().currentUser!
        storeAccount.updateDocument(documentID: user.uid, data: data) { result in
            if result {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "ACCOUNTUPDATED"), object: nil)
            }
            completion(result)
        }
    }
}
