//
//  ProfileEditViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/23.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseAuth
import SDWebImage

class ProfileEditViewModel: ObservableObject {
    let user = Auth.auth().currentUser!
    @Published var avatar = UIImage(named: "user")
    
    @Published var viewMode = 0
    
    var source = 0
    
    @Published var name: String = ""
    
    var inputImage: UIImage?
    @Published var outputImage = UIImage(named: "user")
    
    let storeProfile = StoreProfile()
    var userProfile: UserProfile?
    
    @Published var isProgress: Bool = false
    @Published var isShowAlert: Bool = false
    @Published var isUpdated: Bool = false
    @Published var isDiscard: Bool = false
    var avatarChanged = false
    var queryProfile = true
    
    func getStoreProfile() {
        self.name = self.user.displayName ?? ""
        //print("xerror \(String(describing: self.user.photoURL))")
        /*SDWebImageManager.shared.loadImage(
            with: self.user.photoURL,
            options: .highPriority,
            progress: nil) { (image, _, error, _, _, _) in
            print("xerror rrr")
            if let error = error {
                print("xerror \(error.localizedDescription)")
            } else {
                self.avatar = image
            }
        }*/
        storeProfile.getDocument(documentID: user.uid) { document in
            if let document = document {
                self.userProfile = document
                self.queryProfile = false
            }
        }
    }
    
    func saveChange() {
        isProgress = true
        if avatarChanged {
            uploadAvatar()
        } else {
            updateUser(name: name, avatar: nil)
        }
    }
    
    private func uploadAvatar() {
        if let jpeg = outputImage!.jpegData(compressionQuality: 0.8) {
            let fileURL = try! FileManager.default
                .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appendingPathComponent("avatar.jpg")

            do {
                try jpeg.write(to: fileURL, options: .atomic)
                let storage = FireStorage()
                storage.uploadAvatar(uid: user.uid, localFile: fileURL) { [self] url in
                    guard let avatarUrl = url else {
                        isProgress = false
                        return
                    }
                    self.updateUser(name: name, avatar: avatarUrl)
                }
            } catch {
                print(error)
                isProgress = false
            }
        }
    }
    
    private func updateUser(name: String, avatar: URL?) {
        let userManager = UserManager()
        userManager.updateUserProfile(
            displayName: name,
            photoURL: avatar) { result in
            if result {
                self.updateProfile(name: name, avatar: avatar)
            } else {
                self.isProgress = false
            }
        }
    }
    
    private func updateProfile(name: String, avatar: URL?) {
        print("update data x : \(name) \(String(describing: avatar))")
        
        let storeProfile = StoreProfile()
        self.userProfile?.name = name
        if avatar != nil {
            self.userProfile?.avatarUrl = avatar?.absoluteString
        }
        let data: [String : Any] = [
            "name" : self.userProfile?.name ?? "",
            "avatarUrl" : self.userProfile?.avatarUrl ?? ""
        ]
        
        print("update data: \(data)")
        storeProfile.updateDocument(documentID: user.uid, data: data) { result in
            if result {
                print("update completed")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "PROFILEUPDATED"), object: nil)
                self.isProgress = false
                self.isUpdated = true
            }
        }
    }
}
