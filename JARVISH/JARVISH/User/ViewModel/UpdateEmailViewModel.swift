//
//  UpdateEmailViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/17.
//  Copyright © 2021 jarvish. All rights reserved.
//

import FirebaseAuth

class UpdateEmailViewModel: ObservableObject {
    @Published var isSentDisabled: Bool = true
    @Published var isEmailSent = false
    @Published var isProgress: Bool = false
    @Published var isShowAlert: Bool = false
    var userManagerCode: UserManagerCode = UserManagerCode.CHANGE_EMAIL_OK
    
    @Published var email: String = "" {
        didSet {
            self.isSentDisabled = !isValidEmail(email: self.email)
            print("is  email valid? \(!self.isSentDisabled)")
        }
    }
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func updateEmail() {
        self.isProgress = true
        let userManager = UserManager()
        userManager.updateEmail(email: self.email) { code in
            self.isProgress = false
            if code == UserManagerCode.CHANGE_EMAIL_OK {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "ACCOUNTUPDATED"), object: nil)
                self.isEmailSent = true
            } else {
                self.isShowAlert = true
                self.userManagerCode = code
            }
        }
    }
    
    /*func updateEmail() {
        let data: [String: Any] = [
            "email": self.email
            ]
        let uid = Auth.auth().currentUser!.uid
        self.isProgress = true
        let storeAccount = StoreAccount()
        storeAccount.updateDocument(documentID: uid, data: data) { result in
            self.isProgress = false
            if result {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "ACCOUNTUPDATED"), object: nil)
                self.isEmailSent = true
            } else {
                self.isShowAlert = true
                self.userManagerCode = UserManagerCode.CHANGE_EMAIL_FAILURE
            }
        }
    }*/
}
