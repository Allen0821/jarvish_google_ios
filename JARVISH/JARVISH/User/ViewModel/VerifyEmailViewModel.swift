//
//  VerifyEmailViewModel.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2021/6/19.
//  Copyright © 2021 jarvish. All rights reserved.
//
import FirebaseAuth

class VerifyEmailViewModel: ObservableObject {
    @Published var sent: Bool = false
    @Published var isProgress: Bool = false
    @Published var isShowAlert: Bool = false
    
    let email: String = Auth.auth().currentUser?.email ?? ""
    
    
    func sendVerifyEmail() {
        isProgress = true
        let userManager = UserManager()
        userManager.sendEmailVerification() { result in
            self.isProgress = false
            if result {
                self.sent = true
            } else {
                self.isShowAlert = true
            }
        }
    }
}
