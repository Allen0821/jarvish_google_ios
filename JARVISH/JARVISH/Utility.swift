//
//  Utility.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/3/18.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import UIKit

public class Utility {
    
    static func getTintColor() -> UIColor {
        return UIColor(red: 61.0/255.0, green: 211.0/255.0, blue: 182.0/255.0, alpha: 1.0)
    }
    
    
    static func getPlaceholderColor() -> UIColor {
        return UIColor(red: 196.0/255.0, green: 198.0/255.0, blue: 206.0/255.0, alpha: 0.7)
    }
    
    
    static func getButtonBgColor() -> UIColor {
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.8)
    }
    
    
    static func getTrackItemBgColor() -> UIColor {
        return UIColor(red: 39.0/255.0, green: 39.0/255.0, blue: 39.0/255.0, alpha: 0.5)
    }
    
    static func getTrackContentBgColor() -> UIColor {
        return UIColor(red: 27.0/255.0, green: 54.0/255.0, blue: 63.0/255.0, alpha: 0.9)
    }
    
    
    static func getTrackMarkerColor() -> UIColor {
        return UIColor(red: 0.0/255.0, green: 191.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    
    static func detAlertTitleAttributed(title: String) -> NSAttributedString {
        let attributedString = NSAttributedString(string: title, attributes: [
            NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 22)
        ])
        return attributedString
    }
    
    
    static func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: email)
        return result
    }
    
    static func isValidSerialNumber(number: String?) -> Bool {
        print("validate emil: \(String(describing: number))")
        if (number == nil || number!.isEmpty || number!.count != 13) {
            return false
        }
        let num = Int(number!)
        if num == nil {
            return false
        }
        return true
    }
    
    
    static func compareDateTime(d1: Date, d2: Date) -> Int {
        let calendar = Calendar.current
        print("date1 \(d1)")
        print("date2 \(d2)")
        let components = calendar.dateComponents([.second], from: d1, to: d2)
        print("components \(components)")
        print("components \(String(describing: components.second))")
        if components.second == nil {
           return 1
        }
        return components.second!
    }
    
    static func getTrackDateString(timetamp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timetamp))
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    static func getDateTimeFormatString(timetamp: Int, format: String) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timetamp))
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    
    static func getTrackDistanceKmString(distance: Double) -> String {
        if distance < 1000 {
            return String(format: "%.0f", distance) + "m"
        }
        let km = distance / 1000
        return String(format: "%.2f", km) + "km"
    }
    
    
    static func getTrackDistanceMiString(distance: Double) -> String {
        let ft = distance * 3.28
        if ft < 1000 {
            return String(format: "%.0f", ft) + "ft"
        }
        let mi = distance / 1609.344
        return String(format: "%.2f", mi) + "mi"
    }
    
    static func getTrackDistanceKmAttributedString(distance: Double) -> NSMutableAttributedString {
        var d: String
        var u: String
        if distance < 1000 {
            d = String(format: "%.0f", distance)
            u = "m"
        } else {
            let km = distance / 1000
            d = String(format: "%.2f", km)
            u = "km"
        }
        let res = d + u
        let attributedString = NSMutableAttributedString(string: res)
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 12.0), range: NSMakeRange(d.count, u.count))
        return attributedString
    }
    
    
    static func getTrackDistanceMiAttributedString(distance: Double) -> NSMutableAttributedString {
        var d: String
        var u: String
        let ft = distance * 3.28
        if ft < 1000 {
            d = String(format: "%.0f", ft)
            u = "ft"
        } else {
            let mi = distance / 1609.344
            d = String(format: "%.2f", mi)
            u = "mi"
        }
        let res = d + u
        let attributedString = NSMutableAttributedString(string: res)
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 12.0), range: NSMakeRange(d.count, u.count))
        return attributedString
    }
    
    
    static func getTrackSpeedKmString(speed: Double) -> String {
        let km = speed * 60 * 60 / 1000
        return String(format: "%.2f", km) + "km/hr"
    }
    
    static func getTrackSpeedMiString(speed: Double) -> String {
        let mi = speed * 60 * 60 / 1609.344
        return String(format: "%.2f", mi) + "mi/hr"
    }
    
    
    static func getTrackSpeedKmAttributedString(speed: Double) -> NSMutableAttributedString {
        let km = speed * 60 * 60 / 1000
        let s = String(format: "%.2f", km)
        let u = "km/hr"
        let res = s + u
        let attributedString = NSMutableAttributedString(string: res)
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 12.0), range: NSMakeRange(s.count, u.count))
        return attributedString
    }
    
    
    static func getTrackSpeedMiAttributedString(speed: Double) -> NSMutableAttributedString {
        let mi = speed * 60 * 60 / 1609.344
        let s = String(format: "%.2f", mi)
        let u = "mi/hr"
        let res = s + u
        print("res: \(res) \(s.count) \(res.count)")
        let attributedString = NSMutableAttributedString(string: res)
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 12.0), range: NSMakeRange(s.count, u.count))
        return attributedString
    }
    
    
    static func getTrackDurtionString(duration: Int) -> String {
        let d = Double(duration)
        print("duration \(d)")
        let sec = d.truncatingRemainder(dividingBy: 60.0)
        let m = d / 60
        let min = m.truncatingRemainder(dividingBy: 60.0)
        var hr = d / 3600.0
        if hr < 0 {
           hr = 0
        }
        print("sec \(sec)")
        print("min \(min)")
        print("hr \(hr)")
        return String(format: "%02d:%02d:%02d", Int(hr), Int(min), Int(sec))
    }
    
    
    static func getTrackItemTitleString(timetamp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timetamp))
        let weekday = Calendar.current.component(.weekday, from: date)
        print("weekday: \(weekday)")
        let hr = Calendar.current.component(.hour, from: date)
        print("hr: \(hr)")
        
        var w: String
        switch weekday {
        case 1:
            w = NSLocalizedString("track_title_sunday", comment: "")
        case 2:
            w = NSLocalizedString("track_title_monday", comment: "")
        case 3:
            w = NSLocalizedString("track_title_tuesday", comment: "")
        case 4:
            w = NSLocalizedString("track_title_wednesday", comment: "")
        case 5:
            w = NSLocalizedString("track_title_thursday", comment: "")
        case 6:
            w = NSLocalizedString("track_title_friday", comment: "")
        default:
            w = NSLocalizedString("track_title_saturday", comment: "")
        }
        
        var h: String
        if hr >= 3 && hr < 6 {
            h = NSLocalizedString("track_title_3_6", comment: "")
        } else if hr >= 6 && hr < 8 {
            h = NSLocalizedString("track_title_6_8", comment: "")
        } else if hr >= 8 && hr < 11 {
            h = NSLocalizedString("track_title_8_11", comment: "")
        } else if hr >= 11 && hr < 13 {
            h = NSLocalizedString("track_title_11_13", comment: "")
        } else if hr >= 13 && hr < 17 {
            h = NSLocalizedString("track_title_13_17", comment: "")
        } else if hr >= 17 && hr < 19 {
            h = NSLocalizedString("track_title_17_19", comment: "")
        } else if hr >= 19 && hr < 23 {
            h = NSLocalizedString("track_title_19_23", comment: "")
        } else {
            h = NSLocalizedString("track_title_23_3", comment: "")
        }
        return w + " " + h
    }
    
    
    static func getVideoDateFormat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        guard let date = dateFormatter.date(from: dateString) else {
            return ""
        }
        let calendar = Calendar.current
        if calendar.isDateInYesterday(date) {
            return NSLocalizedString("helmet_yestoday", comment: "")
        } else if calendar.isDateInToday(date) {
            return NSLocalizedString("helmet_today", comment: "")
        } else if calendar.isDateInTomorrow(date) {
            return NSLocalizedString("video_tomorrow", comment: "")
        } else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 {
                if day >= -7 {
                    return String(format: NSLocalizedString("helmet_days_ago", comment: ""), -day)
                } else {
                    let f = DateFormatter()
                    f.locale = Locale.current
                    f.dateFormat = "MMM d yyyy"
                    return f.string(from: date)
                }
            } else {
                return String(format: NSLocalizedString("helmet_in_days", comment: ""), day)
                
            }
        }
    }
    
    
    static func getDateTimeFormat() -> String {
        let locale = NSLocale.preferredLanguages[0]
        if locale.contains("zh-Hant") {
            return "EE HH:mm, yyyy年M月d日"
        } else {
            return "EE HH:mm, MMMM d, yyyy"
        }
    }
    
    
}
