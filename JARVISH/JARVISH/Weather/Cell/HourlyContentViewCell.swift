//
//  HourlyContentViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/14.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class HourlyContentViewCell: UICollectionViewCell {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var precipLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    
    
    var hourly: Hourly! {
        didSet {
            timeLabel.text = getimeString(dateString: hourly.validTimeLocal!)
            precipLabel.text = String(hourly.precipChance!) + "%"
            let icon = "weather." + String(hourly.iconCode!)
            iconImageView.image = UIImage(named: icon)
            tempLabel.text = String(hourly.temperature!) + "°"
        }
    }
    
    
    private func getimeString(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZ"
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: dateObj!)
    }
    
}
