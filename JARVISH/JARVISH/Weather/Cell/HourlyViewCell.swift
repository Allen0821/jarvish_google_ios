//
//  HourlyViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/14.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class HourlyViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var hourlyCollectionView: UICollectionView!
    
    var width: CGFloat = 0
    var hourlies: [Hourly] = []
    
    
    var results: [Hourly]! {
        didSet {
            hourlies.removeAll()
            hourlies.append(contentsOf: results)
            hourlyCollectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print("XSX: Initialization hourlyCollectionView")
        hourlyCollectionView.delegate = self
        hourlyCollectionView.dataSource = self
        let screenwidth = UIScreen.main.bounds.width
        width = screenwidth / 5
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("XSX: numberOfItemsInSection")
        return hourlies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("XSX: HourlyContentViewCell")
        let cell = hourlyCollectionView.dequeueReusableCell(withReuseIdentifier: "HourlyContentViewCell", for: indexPath) as! HourlyContentViewCell
        cell.hourly = self.hourlies[indexPath.row]
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.width, height: 140)
    }
}

