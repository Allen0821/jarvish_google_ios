//
//  MoreInfoViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/13.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class MoreInfoViewCell: UITableViewCell {
    @IBOutlet weak var visibilityTitleLabel: UILabel!
    @IBOutlet weak var humidityTitleLabel: UILabel!
    @IBOutlet weak var ultravioletTitleLabel: UILabel!
    @IBOutlet weak var WspdTitleLabel: UILabel!
    @IBOutlet weak var WdirTitleLabel: UILabel!
    
    
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var ultravioletLabel: UILabel!
    @IBOutlet weak var WspdLabel: UILabel!
    @IBOutlet weak var WdirLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        visibilityTitleLabel.text = NSLocalizedString("weather_vis_index", comment: "")
        humidityTitleLabel.text = NSLocalizedString("weather_rh_index", comment: "")
        ultravioletTitleLabel.text = NSLocalizedString("weather_uv_index", comment: "")
        WspdTitleLabel.text = NSLocalizedString("weather_wind_speed", comment: "")
        WdirTitleLabel.text = NSLocalizedString("weather_wind_direction", comment: "")
    }
    
    
    var visibility: Double! {
        didSet {
            self.visibilityLabel.text = String(visibility) + WeatherUtil.getDistanceUnits()
        }
    }

    
    var humidity: Int! {
        didSet {
            self.humidityLabel.text = String(humidity) + "%"
        }
    }
    
    
    var ultraviolet: String! {
        didSet {
            self.ultravioletLabel.text = ultraviolet
        }
    }
    
    
    var wspd: Int! {
        didSet {
            self.WspdLabel.text = String(wspd) + WeatherUtil.getSpeedUnits()
        }
    }
    
    
    var wdir: String! {
        didSet {
            self.WdirLabel.text = WeatherUtil.getWindDirectionDes(wdirIndex: wdir)
        }
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
