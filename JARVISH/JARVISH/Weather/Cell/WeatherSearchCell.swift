//
//  WeatherSearchCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/9/10.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class WeatherSearchCell: UITableViewCell {
    @IBOutlet weak var placceImageView: UIImageView!
    @IBOutlet weak var deleteImageView: UIImageView!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    
    var wPlace: WPlace! {
        didSet {
            placeLabel.text = wPlace.name
            var location = ""
            let area: String = wPlace.area ?? ""
            let country: String = wPlace.country ?? ""
            if !area.isEmpty && !country.isEmpty {
                location = area + ", " + country
            } else if !area.isEmpty && country.isEmpty {
                location = area
            } else if area.isEmpty && !country.isEmpty {
                location = country
            }
           
            if wPlace.type == 0 {
                location = NSLocalizedString("weather_search_current_place", comment: "")
                placceImageView.image = UIImage(systemName: "location.fill")
                deleteImageView.isHidden = true
            } else {
                placceImageView.image = UIImage(systemName: "mappin.circle.fill")
                deleteImageView.isHidden = false
            }
            locationLabel.text = location
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

}

