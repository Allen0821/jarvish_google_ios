//
//  WeeklyContentViewCell.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/16.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit

class WeeklyContentViewCell: UITableViewCell {
    @IBOutlet weak var dayOfWeekLabel: UILabel!
    @IBOutlet weak var wxPhraseLabel: UILabel!
    @IBOutlet weak var precipChanceLabel: UILabel!
    @IBOutlet weak var wxIconImageView: UIImageView!
    @IBOutlet weak var temperatureMaxLabel: UILabel!
    @IBOutlet weak var temperatureMinLabel: UILabel!
    
    
    var weekly: Weekly! {
        didSet {
            dayOfWeekLabel.text = weekly.dayOfWeek
            wxPhraseLabel.text = weekly.weeklyPart![0].wxPhraseLong!
            precipChanceLabel.text = String(weekly.weeklyPart![0].precipChance!) + "%"
            let icon = "weather." + String(weekly.weeklyPart![0].iconCode!)
            wxIconImageView.image = UIImage(named: icon)
            temperatureMaxLabel.text = String(weekly.temperatureMax!) + "°"
            temperatureMinLabel.text = String(weekly.temperatureMin!) + "°"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
