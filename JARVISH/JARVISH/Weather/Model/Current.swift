//
//  Current.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/9.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct Current {
    var dayInd: String
    var expireTimeGmt: Int
    var feelsLike: Int
    var maxTemp: Int
    var minTemp: Int
    var obsName: String
    var precipHrly: Double
    var vis: Double
    var rh: Int
    var temp: Int
    var uvDesc: String
    var uvIndex: Int
    var wspd: Int
    var wdirCardinal: String
    var wxIcon: Int
    var wxPhrase: String
      
    var dictionary: [String: Any?] {
           return [
            "day_ind": dayInd,
            "expire_time_gmt": expireTimeGmt,
            "feels_like": feelsLike,
            "max_temp": maxTemp,
            "min_temp": minTemp,
            "obs_name": obsName,
            "precip_hrly": precipHrly,
            "vis": vis,
            "rh": rh,
            "temp": temp,
            "uv_desc": uvDesc,
            "uv_index": uvIndex,
            "wspd": wspd,
            "wdir_cardinal": wdirCardinal,
            "wx_icon": wxIcon,
            "wx_phrase": wxPhrase
           ]
       }
}


extension Current: Serializable {
    init?(dictionary: [String : Any?]) {
        let dayInd = dictionary["day_ind"] as? String ?? "D"
        let expireTimeGmt = dictionary["expire_time_gmt"] as? Int ?? 0
        let feelsLike = dictionary["feels_like"] as? Int ?? 0
        let maxTemp = dictionary["max_temp"] as? Int ?? 0
        let minTemp = dictionary["min_temp"] as? Int ?? 0
        let obsName = dictionary["obs_name"] as? String ?? ""
        let precipHrly = dictionary["precip_hrly"] as? Double ?? 0.0
        let vis = dictionary["vis"] as? Double ?? 0
        let rh = dictionary["rh"] as? Int ?? 0
        let temp = dictionary["temp"] as? Int ?? 0
        let uvDesc = dictionary["uv_desc"] as? String ?? ""
        let uvIndex = dictionary["uv_index"] as? Int ?? 0
        let wspd = dictionary["wspd"] as? Int ?? 0
        let wdirCardinal = dictionary["wdir_cardinal"] as? String ?? ""
        let wxIcon = dictionary["wx_icon"] as? Int ?? 44
        let wxPhrase = dictionary["wx_phrase"] as? String ?? ""
    
        
        self.init(
            dayInd: dayInd,
            expireTimeGmt: expireTimeGmt,
            feelsLike: feelsLike,
            maxTemp: maxTemp,
            minTemp: minTemp,
            obsName: obsName,
            precipHrly: precipHrly,
            vis: vis,
            rh: rh,
            temp: temp,
            uvDesc: uvDesc,
            uvIndex: uvIndex,
            wspd: wspd,
            wdirCardinal: wdirCardinal,
            wxIcon: wxIcon,
            wxPhrase: wxPhrase
        )
    }
}
