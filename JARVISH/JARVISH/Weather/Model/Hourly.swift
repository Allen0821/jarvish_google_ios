//
//  Hourly.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/15.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct Hourly {
    var iconCode: Int?
    var precipChance: Int?
    var temperature: Int?
    var validTimeLocal: String?
    var expirationTimeUtc: Int?
    
    var dictionary: [String: Any?] {
        return [
         "iconCode": iconCode,
         "precipChance": precipChance,
         "temperature": temperature,
         "validTimeLocal": validTimeLocal,
         "expirationTimeUtc": expirationTimeUtc
        ]
    }
}


extension Hourly: Serializable {
    init?(dictionary: [String : Any?]) {
        let iconCode = dictionary["iconCode"] as? Int ?? 44
        let precipChance = dictionary["precipChance"] as? Int ?? 0
        let temperature = dictionary["temperature"] as? Int ?? 0
        let validTimeLocal = dictionary["validTimeLocal"] as? String ?? ""
        let expirationTimeUtc = dictionary["expirationTimeUtc"] as? Int ?? 0
       
        self.init(
            iconCode: iconCode,
            precipChance: precipChance,
            temperature: temperature,
            validTimeLocal: validTimeLocal,
            expirationTimeUtc: expirationTimeUtc
        )
    }
}
