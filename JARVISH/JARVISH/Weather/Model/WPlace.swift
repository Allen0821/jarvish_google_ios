//
//  WPlace.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/9/10.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct WPlace: Codable {
    var id: String?
    var name: String?
    var area: String?
    var country: String?
    var lat: Double?
    var lnt: Double?
    var type: Int?
    
    var dictionary: [String: Any?] {
        return [
         "id": id,
         "name": name,
         "area": area,
         "country": country,
         "lat": lat,
         "lnt": lnt,
         "type": type
        ]
    }
}


extension WPlace: Serializable {
    init?(dictionary: [String : Any?]) {
        let id = dictionary["id"] as? String ?? ""
        let name = dictionary["name"] as? String ?? ""
        let area = dictionary["area"] as? String ?? ""
        let country = dictionary["country"] as? String ?? ""
        let lat = dictionary["lat"] as? Double ?? 0.0
        let lnt = dictionary["lnt"] as? Double ?? 0.0
        let type = dictionary["type"] as? Int ?? 0
       
        self.init(
            id: id,
            name: name,
            area: area,
            country: country,
            lat: lat,
            lnt: lnt,
            type: type
        )
    }
}
