//
//  Weekly.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/15.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct Weekly {
    var dayOfWeek: String?
    var expirationTimeUtc: Int?
    var temperatureMax: Int?
    var temperatureMin: Int?
    var weeklyPart: [WeeklyPart]?
    
    var dictionary: [String: Any?] {
        return [
         "dayOfWeek": dayOfWeek,
         "expirationTimeUtc": expirationTimeUtc,
         "temperatureMax": temperatureMax,
         "temperatureMin": temperatureMin,
         "weeklyPart": weeklyPart
        ]
    }
}


extension Weekly: Serializable {
    init?(dictionary: [String : Any?]) {
        let dayOfWeek = dictionary["dayOfWeek"] as? String ?? ""
        let expirationTimeUtc = dictionary["expirationTimeUtc"] as? Int ?? 0
        let temperatureMax = dictionary["temperatureMax"] as? Int ?? 0
        let temperatureMin = dictionary["temperatureMin"] as? Int ?? 0
        let weeklyPart = dictionary["weeklyPart"] as? [WeeklyPart]
       
        self.init(
            dayOfWeek: dayOfWeek,
            expirationTimeUtc: expirationTimeUtc,
            temperatureMax: temperatureMax,
            temperatureMin: temperatureMin,
            weeklyPart: weeklyPart
        )
    }
}
