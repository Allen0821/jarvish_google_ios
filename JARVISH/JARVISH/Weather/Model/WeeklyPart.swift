//
//  WeeklyPart.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/15.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation


struct WeeklyPart {
    var daypartName: String?
    var wxPhraseLong: String?
    var iconCode: Int?
    var temperature: Int?
    var precipChance: Int?
    
    var dictionary: [String: Any?] {
        return [
         "daypartName": daypartName,
         "wxPhraseLong": wxPhraseLong,
         "iconCode": iconCode,
         "temperature": temperature,
         "precipChance": precipChance
        ]
    }
}


extension WeeklyPart: Serializable {
    init?(dictionary: [String : Any?]) {
        let daypartName = dictionary["daypartName"] as? String ?? "D"
        let wxPhraseLong = dictionary["wxPhraseLong"] as? String ?? ""
        let iconCode = dictionary["iconCode"] as? Int ?? 44
        let temperature = dictionary["temperature"] as? Int ?? 0
        let precipChance = dictionary["precipChance"] as? Int ?? 0
       
        self.init(
            daypartName: daypartName,
            wxPhraseLong: wxPhraseLong,
            iconCode: iconCode,
            temperature: temperature,
            precipChance: precipChance
        )
    }
}

