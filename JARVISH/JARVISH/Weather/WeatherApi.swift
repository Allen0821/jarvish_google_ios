//
//  WeatherApi.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/9.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import Moya

let WeatherProvider = MoyaProvider<Weather>()


public enum Weather {
    case getCurrentConditions(latitude: Double, longitude: Double, units: String, language: String, apiKey: String)
    case get2DaysConditions(geocode: String, format: String, units: String, language: String, apiKey: String)
    case get7daysConditions(geocode: String, format: String, units: String, language: String, apiKey: String)
}



extension Weather: TargetType {
    public var baseURL: URL {
        return URL(string: "https://api.weather.com")!
    }
    
    public var path: String {
        switch self {
        case .getCurrentConditions(let latitude, let longitude,_,_,_):
            return "/v1/geocode/\(latitude)/\(longitude)/observations.json"
        case .get2DaysConditions(_,_,_,_,_):
            return "/v3/wx/forecast/hourly/2day"
        case .get7daysConditions(_,_,_,_,_):
            return "/v3/wx/forecast/daily/7day"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .getCurrentConditions(_,_,_,_,_):
            return .get
        case .get2DaysConditions(_,_,_,_,_):
            return .get
        case .get7daysConditions(_,_,_,_,_):
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .getCurrentConditions(_,_, let units, let language, let apiKey):
            return .requestParameters(parameters: [
                "language" : language,
                "units" : units,
                "apiKey" : apiKey],
                encoding: URLEncoding.default)
        case .get2DaysConditions(let geocode, let format, let units, let language, let apiKey):
            return .requestParameters(parameters: [
                "geocode": geocode,
                "format": format,
                "units" : units,
                "language" : language,
                "apiKey" : apiKey],
                encoding: URLEncoding.default)
        case .get7daysConditions(let geocode, let format, let units, let language, let apiKey):
            return .requestParameters(parameters: [
                "geocode": geocode,
                "format": format,
                "units" : units,
                "language" : language,
                "apiKey" : apiKey],
                encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        return [
            "Content-type": "application/json",
        ]
    }
}
