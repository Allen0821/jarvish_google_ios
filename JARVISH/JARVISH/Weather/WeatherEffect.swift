//
//  WeatherEffect.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/8/11.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation


class WeatherEffect {
    

    var effectView: UIView?
    //let emitterCell = CAEmitterCell()
    //let emitterLayer = CAEmitterLayer()
    var emitterView: UIView?
    
    init(view: UIView) {
        self.effectView = view
    }
    
    
    func clearEffect() {
        if emitterView != nil {
            emitterView?.removeFromSuperview()
        }
    }
    
    
    func setEffect(dayInx: String, weaInx: Int) {
        clearEffect()
        if (weaInx >= 3 && weaInx <= 12) ||
                weaInx == 35 ||
                (weaInx >= 37 && weaInx <= 40) ||
                weaInx == 45 ||
                weaInx == 47 {
            if dayInx == "D" {
                emitterView = setRainDayEffect()
            } else {
                emitterView = setRainNightEffect()
            }
        } else if weaInx >= 23 && weaInx <= 30 {
            if dayInx == "D" {
                emitterView = setCloudDayEffect()
            } else {
                emitterView = setCloudNightEffect()
            }
        } else if (weaInx >= 31 && weaInx <= 34) ||
                weaInx == 36 ||
                weaInx == 44 {
            if dayInx == "D" {
                emitterView = setSunDayEffect()
            } else {
                emitterView = setSunNightEffect()
            }
        } else {
            return
        }
        if emitterView != nil {
            print("xxxxxssss add")
            //emitterView?.backgroundColor = UIColor.red
            self.effectView!.addSubview(emitterView!)
        }
    }
    
    
    private func setSunDayEffect() -> UIView {
        let view = UIView()
        view.frame = self.effectView!.bounds
        let cell = CAEmitterCell()
        let emitterLayer = CAEmitterLayer()
        
        cell.contents = UIImage(named: "effect.sun.day")?.cgImage
        cell.birthRate = 0.1
        cell.lifetime = 10
        cell.scale = 0.25
        cell.velocity = 0.15
        cell.emissionLongitude = .pi * 1.32
        //emitterCell.alphaSpeed = -0.2;
        //emitterCell.alphaRange = 0.2;
        emitterLayer.emitterPosition = CGPoint(x: effectView!.bounds.width / 2 - 50, y: effectView!.bounds.height / 2 - 50)
        emitterLayer.emitterSize =
            CGSize(width: 1, height: 1)
        emitterLayer.renderMode = .unordered
        emitterLayer.emitterMode = .outline
        emitterLayer.emitterShape = .line
        emitterLayer.emitterCells = [cell]
        view.layer.addSublayer(emitterLayer)
        return view
    }
    
    
    private func setSunNightEffect() -> UIView {
        let view = UIView()
        view.frame = self.effectView!.bounds
        let cell = CAEmitterCell()
        let emitterLayer = CAEmitterLayer()
        
        cell.contents = UIImage(named: "effect.sun.night")?.cgImage
        cell.birthRate = 0.2
        cell.lifetime = 4
        cell.scale = 0.01
        cell.velocity = 250
        //emitterCell.yAcceleration = 50
        cell.alphaSpeed = -0.1;
        cell.alphaRange = 0.1;
        cell.emissionLongitude = .pi * 1.32
        emitterLayer.emitterPosition = CGPoint(x: effectView!.bounds.width, y: 0)
        emitterLayer.emitterSize =
            CGSize(width: 150, height: 50)
        emitterLayer.renderMode = .unordered
        emitterLayer.emitterMode = .outline
        emitterLayer.emitterShape = .line
        emitterLayer.emitterCells = [cell]
        view.layer.addSublayer(emitterLayer)
        return view
    }
    
    
    private func setRainDayEffect() -> UIView {
        let view = UIView()
        view.frame = self.effectView!.bounds
        let cell = CAEmitterCell()
        let emitterLayer = CAEmitterLayer()
        
        cell.contents = UIImage(named: "effect.rain.day")?.cgImage
        cell.birthRate = 50
        cell.lifetime = 30
        cell.scale = 0.5
        cell.velocity = 150
        cell.yAcceleration = 200
        cell.emissionLongitude = .pi
        emitterLayer.emitterPosition = CGPoint(x: effectView!.bounds.width / 2, y: 0)
        emitterLayer.emitterSize =
            CGSize(width: effectView!.bounds.width, height: 0)
        emitterLayer.renderMode = .oldestFirst
        emitterLayer.emitterMode = .outline
        emitterLayer.emitterShape = .line
        emitterLayer.emitterCells = [cell]
        view.layer.addSublayer(emitterLayer)
        return view
    }
    
    
    
    private func setRainNightEffect() -> UIView {
        let view = UIView()
        view.frame = self.effectView!.bounds
        let cell = CAEmitterCell()
        let emitterLayer = CAEmitterLayer()
        
        cell.contents = UIImage(named: "effect.rain.night")?.cgImage
        cell.birthRate = 25
        cell.lifetime = 30
        cell.scale = 0.35
        cell.velocity = 150
        cell.yAcceleration = 200
        cell.emissionLongitude = .pi
        emitterLayer.emitterPosition = CGPoint(x: effectView!.bounds.width / 2, y: 0)
        emitterLayer.emitterSize =
            CGSize(width: effectView!.bounds.width, height: 0)
        emitterLayer.renderMode = .oldestFirst
        emitterLayer.emitterMode = .outline
        emitterLayer.emitterShape = .line
        emitterLayer.emitterCells = [cell]
        view.layer.addSublayer(emitterLayer)
        return view
    }
    
    
    private func setCloudDayEffect() -> UIView {
        let view = UIView()
        view.frame = self.effectView!.bounds
        let cell = CAEmitterCell()
        let emitterLayer = CAEmitterLayer()
        
        cell.contents = UIImage(named: "effect.cloud.day")?.cgImage
        cell.birthRate = 0.02
        cell.lifetime = 75
        cell.scale = 0.1
        cell.velocity = 10
        cell.emissionLongitude = .pi / 2
        emitterLayer.emitterPosition =
            CGPoint(x: -50, y: effectView!.bounds.height / 2 - 25)
        emitterLayer.emitterSize =
            CGSize(width: 25, height: effectView!.bounds.height / 2 - 25)
        emitterLayer.renderMode = .unordered
        emitterLayer.emitterMode = .outline
        emitterLayer.emitterShape = .line
        emitterLayer.emitterCells = [cell]
        view.layer.addSublayer(emitterLayer)
        return view
    }
    
    
    private func setCloudNightEffect() -> UIView {
        let view = UIView()
        view.frame = self.effectView!.bounds
        let cell = CAEmitterCell()
        let emitterLayer = CAEmitterLayer()
        
        cell.contents = UIImage(named: "effect.cloud.night")?.cgImage
        cell.birthRate = 0.02
        cell.lifetime = 75
        cell.scale = 0.5
        cell.velocity = 1
        cell.emissionLongitude = 0
        cell.xAcceleration = 0.25
        emitterLayer.emitterPosition =
            CGPoint(x: -100, y: effectView!.bounds.height / 2 + 100)
        emitterLayer.emitterSize =
            CGSize(width: 0, height: effectView!.bounds.height / 2 + 100)
        emitterLayer.renderMode = .oldestFirst
        emitterLayer.emitterMode = .outline
        emitterLayer.emitterShape = .line
        emitterLayer.emitterCells = [cell]
        view.layer.addSublayer(emitterLayer)
        return view
    }
    
}
