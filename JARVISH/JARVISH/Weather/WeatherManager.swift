//
//  WeatherManager.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/9.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import Moya

class WeatherManager {
    private var units: String = "m"
    private var language: String = "en-US"
    private var format: String =  "json"
    private let apiKey: String = "15840a56051f40c5840a56051f80c57d"
    
    private static var sharedWeatherManager: WeatherManager = {
        let instance = WeatherManager()
        // setup code
        return instance
    }()
    
    private init() {
        print("WeatherManager init")
        let locale = NSLocale.preferredLanguages[0]
        if locale.contains("zh-Hant") {
            language = "zh-TW"
        } else {
            language = "en-US"
        }
        
        if UserDefaults.standard.integer(forKey: "app_units") == 0 {
            units = "m"
        } else {
            units = "e"
        }
    }
    
    
    class func shared() -> WeatherManager {
        return sharedWeatherManager
    }
    
    
    public func setUnits(index: Int) {
        if index == 0 {
            units = "m"
        } else {
            units = "e"
        }
    }
    
    
    public func getCurrentCondictions(latitude: Double, longitude: Double, completion: @escaping (_ current: Current)->(), failed: @escaping (_ error: String)->()) {
        WeatherProvider.rx.request(.getCurrentConditions(latitude: latitude, longitude: longitude, units: self.units, language: self.language, apiKey: self.apiKey))
        .filterSuccessfulStatusCodes()
        .debug()
        .mapJSON()
            .subscribe(onSuccess: { result in
                if let result = result as? [String: AnyObject] {
                    if let current = self.parseCurrentCondictions(result: result) {
                        print("get current: \(String(describing: current))")
                        completion(current)
                    } else {
                        failed("empty observation")
                    }
                }
            }) { error in
                print("get current condictions error: \(error)")
                failed(error.localizedDescription)
        }
    }
    
    
    private func parseCurrentCondictions(result: [String: AnyObject]) -> Current? {
        if let observation = result["observation"] as? [String: AnyObject] {
            let current: Current = Current.init(dictionary: observation)!
            return current
        }
        return nil
    }
    
    
    public func getHourlyCondictions(latitude: Double, longitude: Double, completion: @escaping (_ hourlies: [Hourly])->(), failed: @escaping (_ error: String)->()) {
        let geocode = String(latitude) + "," + String(longitude)
        WeatherProvider.rx.request(.get2DaysConditions(geocode: geocode, format: self.format, units: self.units, language: self.language, apiKey: self.apiKey))
        .filterSuccessfulStatusCodes()
        .debug()
        .mapJSON()
            .subscribe(onSuccess: { result in
                if let result = result as? [String: AnyObject] {
                    if let hourlies = self.parseHourlyCondictions(result: result) {
                        print("get hourlies: \(String(describing: hourlies))")
                        completion(hourlies)
                    } else {
                        failed("empty hourlies")
                    }
                }
            }) { error in
                print("get hourly condictions error: \(error)")
                failed(error.localizedDescription)
        }
    }
    
    
    private func parseHourlyCondictions(result: [String: AnyObject]) -> [Hourly]? {
        var hourlies: [Hourly] = []
        let iconCode = result["iconCode"] as? [Int]
        let precipChance = result["precipChance"] as? [Int]
        let temperature = result["temperature"] as? [Int]
        let validTimeLocal = result["validTimeLocal"] as? [String]
        let expirationTimeUtc = result["expirationTimeUtc"] as? [Int]
        for i in 0...23 {
            let hourly = Hourly.init(
                iconCode: iconCode![i],
                precipChance: precipChance![i],
                temperature: temperature![i],
                validTimeLocal: validTimeLocal![i],
                expirationTimeUtc: expirationTimeUtc![i])
            hourlies.append(hourly)
        }
        print("hourlies: \(String(describing: hourlies))")
        return hourlies
    }
    
    
    public func getWeeklyCondictions(latitude: Double, longitude: Double, completion: @escaping (_ weeklies: [Weekly])->(), failed: @escaping (_ error: String)->()) {
        let geocode = String(latitude) + "," + String(longitude)
        WeatherProvider.rx.request(.get7daysConditions(geocode: geocode, format: self.format, units: self.units, language: self.language, apiKey: self.apiKey))
        .filterSuccessfulStatusCodes()
        .debug()
        .mapJSON()
            .subscribe(onSuccess: { results in
                if let result = results as? [String: Any] {
                    //print("get weeklies result: \(String(describing: result))")
                    if let weeklies = self.parseWeeklyCondictions(result: result) {
                        completion(weeklies)
                    } else {
                        failed("empty weeklies")
                    }
                }
            }) { error in
                print("get weeklies condictions error: \(error)")
                failed(error.localizedDescription)
        }
    }
    
    
    private func parseWeeklyCondictions(result: [String: Any]) -> [Weekly]? {
        var weeklies: [Weekly] = []
        if let dayparts = result["daypart"] as? [Any] {
            let daypart = dayparts[0] as! [String : Any]
            print("get weeklies daypart: \(String(describing: daypart))")
            let daypartName = daypart["daypartName"] as? [String?]
            let iconCode = daypart["iconCode"] as? [Int?]
            let temperature = daypart["temperature"] as? [Int?]
            let precipChance = daypart["precipChance"] as? [Int?]
            let wxPhraseLong = daypart["wxPhraseLong"] as? [String?]
            
            let dayOfWeek = result["dayOfWeek"] as? [String]
            let expirationTimeUtc = result["expirationTimeUtc"] as? [Int?]
            let temperatureMax = result["temperatureMax"] as? [Int?]
            let temperatureMin = result["temperatureMin"] as? [Int?]
            
            print("get weeklies dayOfWeek: \(String(describing: dayOfWeek))")
            print("get weeklies daypartName: \(String(describing: daypartName))")
            
            for i in 1...7 {
                let dayPart = WeeklyPart.init(
                    daypartName: daypartName![i*2],
                    wxPhraseLong: wxPhraseLong![i*2],
                    iconCode: iconCode![i*2],
                    temperature: temperature![i*2],
                    precipChance: precipChance![i*2])
                
                let nightPart = WeeklyPart.init(
                daypartName: daypartName![i*2 + 1],
                wxPhraseLong: wxPhraseLong![i*2 + 1],
                iconCode: iconCode![i*2 + 1],
                temperature: temperature![i*2 + 1],
                precipChance: precipChance![i*2 + 1])
                
                var weeklyParts: [WeeklyPart] = []
                weeklyParts.append(dayPart)
                weeklyParts.append(nightPart)
                
                let weekly = Weekly.init(
                    dayOfWeek: dayOfWeek![i],
                    expirationTimeUtc: expirationTimeUtc![i],
                    temperatureMax: temperatureMax![i],
                    temperatureMin: temperatureMin![i],
                    weeklyPart: weeklyParts)
                
                weeklies.append(weekly)
            }
        }
        return weeklies
    }
    
}
