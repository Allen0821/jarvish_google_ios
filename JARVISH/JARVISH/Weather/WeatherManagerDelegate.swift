//
//  WeatherManagerDelegate.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/10.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation


protocol CurrentCondictionsDelegate {
    func onCurrentCondictions(current: Current)
    func onFailure(error: String)
}

protocol HourlyCondictionsDelegate {
    func onOtaDisconnectHelmet()
}


protocol WeeklyCondictionsDelegate {
    func onOtaDisconnectHelmet()
}
