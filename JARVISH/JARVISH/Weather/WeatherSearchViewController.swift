//
//  WeatherSearchViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/9/10.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import GooglePlaces

protocol WeatherSearchDelegate {
    func onSearchPlace(wplace: WPlace)
}

class WeatherSearchViewController: UIViewController {
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var placceTableView: UITableView!
    
    private var placesClient: GMSPlacesClient!
    
    private var wPlaces: [WPlace] = []
    private var prefWPlaces: [WPlace] = []
    private var lastSearch: String?
    var current: WPlace!
    
    var delegate: WeatherSearchDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchBar.layer.cornerRadius = 8
        placceTableView.delegate = self
        placceTableView.dataSource = self
        placceTableView.reloadData()
        
        searchTextField.attributedPlaceholder =
            NSAttributedString(string: NSLocalizedString("weather_search_place", comment: ""),
                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
       
       
        self.placceTableView.keyboardDismissMode = .onDrag
        
        placesClient = GMSPlacesClient.shared()
        readPreferPlace()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func readPreferPlace() {
        self.prefWPlaces.insert(self.current, at: 0)
        if let pref = WeatherUtil.getPreferLocation() {
            self.prefWPlaces.append(contentsOf: pref)
        }
        self.wPlaces.removeAll()
        self.wPlaces.append(contentsOf: self.prefWPlaces)
        self.placceTableView.reloadData()
    }
    
    
    private func searchPlace(text: String) {
        self.wPlaces.removeAll()
        self.placceTableView.reloadData()
        let token = GMSAutocompleteSessionToken.init()
        placesClient.findAutocompletePredictions(
            fromQuery: text,
            filter: nil,
            sessionToken: token) { (predictions, error) in
                if let error = error {
                  print("Autocomplete error: \(error)")
                  return
                }
                if let predictions = predictions {
                  for prediction in predictions {
                    print("wplace: \(prediction.attributedPrimaryText.string)")
                    self.fetchPlace(placeId: prediction.placeID)
                  }
                }
        }
        
        /*placesClient.f(
            text,
            bounds: nil,
            boundsMode: GMSAutocompleteBoundsMode.bias,
            filter: filter) { (predictions, error) in
            if let error = error {
              print("Autocomplete error: \(error)")
              return
            }
            if let predictions = predictions {
              for prediction in predictions {
                print("wplace: \(prediction.attributedFullText) xxxx  \((prediction.attributedPrimaryText)) xxxx  \(String(describing: (prediction.attributedSecondaryText)))")
              }
            }
        }*/
    }
  
    
    private func fetchPlace(placeId: String) {
        self.wPlaces.removeAll()
        let fields: GMSPlaceField = GMSPlaceField(rawValue:
            UInt(GMSPlaceField.placeID.rawValue) |
                UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.coordinate.rawValue) |
            UInt(GMSPlaceField.addressComponents.rawValue))!
        placesClient.fetchPlace(
            fromPlaceID: placeId,
            placeFields: fields,
            sessionToken: nil) { (place, error) in
                if let error = error {
                  print("wplace An error occurred: \(error.localizedDescription)")
                  return
                }
                if let place = place {
                    print("xxplace id is: \(String(describing: place.placeID)), \(String(describing: place.addressComponents))")
                    self.getWPlace(place: place)
                }
        }
    }
    
    
    private func getWPlace(place: GMSPlace) {
        var area1 = ""
        var area2 = ""
        var area3 = ""
        var area = ""
        var country = ""
        let addressComponents = place.addressComponents
        print("wplace: \(String(describing: addressComponents))")
        
        if addressComponents != nil {
            for addressComponent in addressComponents! {
                if addressComponent.types[0].elementsEqual("administrative_area_level_1") {
                    area1 = addressComponent.name
                }
                if addressComponent.types[0].elementsEqual("administrative_area_level_2") {
                    area2 = addressComponent.name
                }
                if addressComponent.types[0].elementsEqual("administrative_area_level_3") {
                    area3 = addressComponent.name
                }
                if addressComponent.types[0].elementsEqual("country") {
                    country = addressComponent.name
                }
            }
            if !area1.isEmpty {
                area = area1;
            } else if !area2.isEmpty {
                area = area2;
            } else if !area3.isEmpty {
                area = area3;
            }
        }
        print("xxplace gggg \(place)")
        let wplace = WPlace.init(
            id: place.placeID,
            name: place.name,
            area: area,
            country: country,
            lat: place.coordinate.latitude,
            lnt: place.coordinate.longitude,
            type: 2)
        self.wPlaces.append(wplace)
        self.placceTableView.reloadData()
    }
    
   
    @IBAction func onBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onClear(_ sender: UIButton) {
        self.searchTextField.text = ""
        self.wPlaces.removeAll()
        self.wPlaces.append(contentsOf: self.prefWPlaces)
        self.placceTableView.reloadData()
    }
  
    
    @IBAction func searchTextChanged(_ sender: UITextField) {
        print("wplace search: \(String(describing: sender.text))")
        print("wplace search count: \(String(describing: sender.text?.count))")
        
        if sender.text == nil  || sender.text!.count == 0 {
            self.wPlaces.removeAll()
            self.wPlaces.append(contentsOf: self.prefWPlaces)
            print("wplace search xxx: \(String(describing: self.wPlaces))")
            self.placceTableView.reloadData()
            return
        }
    
        /*if self.lastSearch != nil &&
            sender.text != nil &&
            !self.lastSearch!.elementsEqual(sender.text!) {
            searchPlace(text: sender.text!)
        }
        self.lastSearch = sender.text*/
        searchPlace(text: sender.text!)
    }
    
    
    @objc func deletePlace(sender: UITapGestureRecognizer) {
        let view = sender.view
        let tag = (view?.tag)!
        let wplace = self.wPlaces[tag]
        if wplace.id != nil && wplace.type == 1 {
            do {
                self.wPlaces.removeAll {$0.id!.elementsEqual(wplace.id!)}
                print("xxxaaa: \(self.wPlaces)")
                self.placceTableView.reloadData()
                self.removeFromPref(wPlace: wplace)
            } catch {
                print("xxxaaa: \(error)")
            }
        }
    }
    
}


extension WeatherSearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        wPlaces.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherSearchCell", for: indexPath) as! WeatherSearchCell
        cell.wPlace = self.wPlaces[indexPath.row]
        let delete = UITapGestureRecognizer(target: self, action: #selector(deletePlace))
        cell.deleteImageView.isUserInteractionEnabled = true
        cell.deleteImageView.tag = indexPath.row
        delete.numberOfTapsRequired = 1
        cell.deleteImageView.addGestureRecognizer(delete)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let wplace = wPlaces[indexPath.row]
        print("xxplace ssss \(wplace)")
        addToPref(wPlace: wplace)
        delegate?.onSearchPlace(wplace: wplace)
        self.dismiss(animated:true, completion: nil)
    }
    
    
    private func addToPref(wPlace: WPlace) {
        if wPlace.id != nil && wPlace.type == 2 {
            for place in self.prefWPlaces {
                if place.id != nil && place.id!.elementsEqual(wPlace.id!) {
                    return
                }
            }
            var place = wPlace
            place.type = 1
            self.prefWPlaces.append(place)
            var p = self.prefWPlaces
            p.remove(at: 0)
            print("xxxaaa: \(p)")
            WeatherUtil.setPreferLocation(wPlaces: p)
        }
    }
    
    
    private func removeFromPref(wPlace: WPlace) {
        if wPlace.id != nil && wPlace.type == 1  {
            self.prefWPlaces.removeAll {$0.id!.elementsEqual(wPlace.id!)}
            var p = self.prefWPlaces
            p.remove(at: 0)
            print("xxxaaa: \(p)")
            WeatherUtil.setPreferLocation(wPlaces: p)
        }
    }
    
}
