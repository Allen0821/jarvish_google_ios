//
//  WeatherUtil.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/16.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

class WeatherUtil {
    
    public static func getWeatherBackground(dayIndex: String, iconIndex: Int) -> String {
        var bgName = ""
        if dayIndex == "N" {
            bgName = "night";
        } else {
            bgName = "day";
        }

        if (iconIndex >= 3 && iconIndex <= 12) ||
                iconIndex == 35 ||
                (iconIndex >= 37 && iconIndex <= 40) ||
                iconIndex == 45 ||
                iconIndex == 47 {
            bgName = bgName + "rain";
        } else if iconIndex >= 23 && iconIndex <= 30 {
            bgName = bgName + "cloud";
        } else if (iconIndex >= 31 && iconIndex <= 34) ||
                iconIndex == 36 ||
                iconIndex == 44 {
            bgName = bgName + "sun";
        } else {
            bgName = bgName + "others";
        }
        return bgName
    }
    
    
    public static func getWindDirectionDes(wdirIndex: String) -> String {
        switch wdirIndex {
        case "N":
            return NSLocalizedString("weather_wdir_n", comment: "")
        case "NNE":
            return NSLocalizedString("weather_wdir_nne", comment: "")
        case "NE":
            return NSLocalizedString("weather_wdir_ne", comment: "")
        case "ENE":
            return NSLocalizedString("weather_wdir_ene", comment: "")
        case "E":
            return NSLocalizedString("weather_wdir_e", comment: "")
        case "ESE":
            return NSLocalizedString("weather_wdir_ese", comment: "")
        case "SE":
            return NSLocalizedString("weather_wdir_se", comment: "")
        case "SSE":
            return NSLocalizedString("weather_wdir_sse", comment: "")
        case "S":
            return NSLocalizedString("weather_wdir_s", comment: "")
        case "SSW":
            return NSLocalizedString("weather_wdir_ssw", comment: "")
        case "SW":
            return NSLocalizedString("weather_wdir_sw", comment: "")
        case "WSW":
            return NSLocalizedString("weather_wdir_wsw", comment: "")
        case "W":
            return NSLocalizedString("weather_wdir_w", comment: "")
        case "WNW":
            return NSLocalizedString("weather_wdir_wnw", comment: "")
        case "NW":
            return NSLocalizedString("weather_wdir_nw", comment: "")
        case "NNW":
            return NSLocalizedString("weather_wdir_nnw", comment: "")
        case "CALM":
            return NSLocalizedString("weather_wdir_calm", comment: "")
        default:
            return NSLocalizedString("weather_wdir_var", comment: "")
        }
    }
    
    
    public static func setPreferLocation(wPlaces: [WPlace]) {
        if let encoded = try? JSONEncoder().encode(wPlaces) {
            UserDefaults.standard.set(encoded, forKey: "prefWPlaces")
        }
    }
    
    public static func getPreferLocation() -> [WPlace]? {
        do {
            if let object = UserDefaults.standard.object(forKey: "prefWPlaces") {
                let wPlaces = try JSONDecoder().decode([WPlace].self, from: object as! Data)
                print("Retrieved wPlaces: \(wPlaces)")
                return wPlaces
            } else {
                return nil
            }
        } catch {
            print(error)
            return nil
        }
    }
    
    
    private static func getTemperatureUnits() -> String {
        if UserDefaults.standard.integer(forKey: "app_units") == 0 {
            return "°C"
        } else {
            return "°F"
        }
    }
    
    
    public static func getDistanceUnits() -> String {
        if UserDefaults.standard.integer(forKey: "app_units") == 0 {
            return "km"
        } else {
            return "mi"
        }
    }
    
    
    public static func getSpeedUnits() -> String {
        if UserDefaults.standard.integer(forKey: "app_units") == 0 {
            return "km/hr"
        } else {
            return "mi/hr"
        }
    }
    
    
    public static func getTemperatureText(tempture: Int) -> String {
        if getTemperatureUnits() == "°C" {
            return String(tempture) + "°C"
        } else {
            return String(tempture) + "°F"
        }
    }
    
    
    public static func getTemperatureText(weaTemop: String) -> String {
        let s = weaTemop.index(before: weaTemop.endIndex)
        let i = weaTemop.index(s, offsetBy: -1)
        let temp = String(weaTemop[..<i])
        print("weax: \(weaTemop) \(temp)")
        if getTemperatureUnits() == "°C" {
            if weaTemop.contains("°C") {
                return weaTemop
            } else {
                let f = (Int(temp)! - 32) * 5 / 9
                return String(f) + "°C"
            }
        } else {
            if weaTemop.contains("°F") {
                return weaTemop
            } else {
                let c = Int(temp)! * 9 / 5 + 32
                return String(c) + "°F"
            }
        }
    }
}
