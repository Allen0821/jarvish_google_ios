//
//  WeatherViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/7/13.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class WeatherViewController: UIViewController, CLLocationManagerDelegate, WeatherSearchDelegate {
    
    @IBOutlet weak var effectView: UIView!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var weatherBgImageView: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var contentTableView: UITableView!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet weak var feelLikeLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var wxPhraseLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var topInsert: CGFloat = 0
    var bottomInsert: CGFloat = 0
    private var currentWPlace: WPlace = WPlace.init()
    private var isCurrent = true
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var wxCurrent: Current?
    var hourlies: [Hourly] = []
    var weeklies: [Weekly] = []
    
    var weatherEffect: WeatherEffect?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        contentTableView.dataSource = self
        contentTableView.delegate = self
        
        self.placeLabel.text = ""
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        topInsert = UIApplication.shared.windows[0].safeAreaInsets.top
        bottomInsert = UIApplication.shared.windows[0].safeAreaInsets.bottom
        print("topInsert: \(topInsert) bottomInsert: \(bottomInsert)")
        self.topViewHeight.constant = topInsert + 44
       
        weatherEffect = WeatherEffect.init(view: effectView)
        
    }
    
    override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
             
            if let headerView = contentTableView.tableHeaderView {
                let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
                if height != headerView.frame.size.height {
                    let screenSize = UIScreen.main.bounds
                    let screenHeight = screenSize.height
                    print("screenHeight \(screenHeight)")
                    let height = screenHeight - (topInsert + 44) - 190 - bottomInsert
                    headerView.frame.size.height = height
                    contentTableView.tableHeaderView = headerView
                }
                
            }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.wxCurrent != nil {
            showCurrentWeather(current: self.wxCurrent!)
        }
        if self.currentLocation != nil {
            getPlace(coordinate: self.currentLocation!.coordinate)
        }
    }
    
    
    @IBAction func clickBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func clickSearch(_ sender: UIButton) {
        DispatchQueue.main.async(){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WeatherSearchViewController") as! WeatherSearchViewController
            vc.delegate = self
            vc.current = self.currentWPlace
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    
    private func showCurrentWeather(current: Current) {
        let icon = "weather." + String(current.wxIcon);
        self.weatherIconImageView.image = UIImage(named: icon)
        self.temperatureLabel.text = WeatherUtil.getTemperatureText(tempture: current.temp)
        let fl = NSLocalizedString("weather_feel_like", comment: "") +
            WeatherUtil.getTemperatureText(tempture: current.feelsLike)
        self.feelLikeLabel.text = fl
        self.wxPhraseLabel.text = current.wxPhrase
        self.timeLabel.text = getDateString()
        let bg = WeatherUtil.getWeatherBackground(dayIndex: current.dayInd, iconIndex: current.wxIcon)
        print("weather bg: \(bg)")
        self.weatherBgImageView.image = UIImage(named: bg)
        weatherEffect?.setEffect(dayInx: current.dayInd, weaInx: current.wxIcon)
    }
    
    private func getDateString() -> String {
        let formatter = DateFormatter()
        let locale = NSLocale.preferredLanguages[0]
        if locale.contains("zh-Hant") {
            formatter.dateFormat = "EE, M月d日"
        } else {
            formatter.dateFormat = "EE, MMMM d"
        }
        return formatter.string(from: Date())
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        self.currentLocation = location
        print("LocationW: \(String(describing: self.currentLocation))")
        
        if self.isCurrent {
            let currentTimeInterval = Date().timeIntervalSince1970
            
            if self.wxCurrent == nil ||
                TimeInterval(self.wxCurrent!.expireTimeGmt) <= currentTimeInterval {
                getHourlyCondictions(
                    latitude: (self.currentLocation?.coordinate.latitude)!,
                    longitude: (self.currentLocation?.coordinate.longitude)!)
                getPlace(coordinate: self.currentLocation!.coordinate)
            }
            
            
            if hourlies.count == 0 ||
                TimeInterval(self.hourlies[0].expirationTimeUtc!) <= currentTimeInterval {
                getHourlyCondictions(
                    latitude: (self.currentLocation?.coordinate.latitude)!,
                    longitude: (self.currentLocation?.coordinate.longitude)!)
            }
            
            if weeklies.count == 0 ||
                TimeInterval(self.weeklies[0].expirationTimeUtc!) <= currentTimeInterval {
                getWeeklyCondictions(
                    latitude: (self.currentLocation?.coordinate.latitude)!,
                    longitude: (self.currentLocation?.coordinate.longitude)!)
            }
        }
    }
    
    
    private func getCurrentCondictions(latitude: Double, longitude: Double) {
        WeatherManager.shared().getCurrentCondictions(
            latitude: latitude, longitude: longitude, completion: { current in
             self.wxCurrent = current
             self.showCurrentWeather(current: self.wxCurrent!)
             self.contentTableView.reloadData()
        }) { error in
            print("get current error \(error)")
        }
    }
    
    
    private func getHourlyCondictions(latitude: Double, longitude: Double) {
        WeatherManager.shared().getHourlyCondictions(
            latitude: latitude, longitude: longitude, completion: { hourlies in
                self.hourlies = hourlies
                self.contentTableView.reloadData()
        }) { error in
            print("get hourly error \(error)")
        }
    }
    
    
    private func getWeeklyCondictions(latitude: Double, longitude: Double) {
        WeatherManager.shared().getWeeklyCondictions(
            latitude: latitude, longitude: longitude, completion: { weeklies in
                self.weeklies = weeklies
                self.contentTableView.reloadData()
        }) { error in
            print("get weekly error \(error)")
        }
    }
    
    
    private func getPlace(coordinate: CLLocationCoordinate2D) {
        let geocoder: GMSGeocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
            if let address: GMSAddress = response?.firstResult() {
                if address.subLocality != nil {
                    self.currentWPlace.name = address.subLocality
                } else if address.locality != nil {
                    self.currentWPlace.name = address.locality
                } else {
                    self.currentWPlace.name = address.administrativeArea
                }
            } else {
                self.currentWPlace.name = ""
            }
            print("sssdd: \(self.currentWPlace.name)")
            self.placeLabel.text = self.currentWPlace.name
            self.currentWPlace.lat = coordinate.latitude
            self.currentWPlace.lnt = coordinate.longitude
            self.currentWPlace.type = 0
            self.currentWPlace.id = ""
        }
    }
    
    
    func onSearchPlace(wplace: WPlace) {
        if wplace.lat != nil && wplace.lat != nil {
            if wplace.type == 0 {
                self.isCurrent = true
            } else {
                self.isCurrent = false
            }
            self.placeLabel.text = wplace.name
            self.getCurrentCondictions(latitude: wplace.lat!, longitude: wplace.lnt!)
            self.getHourlyCondictions(latitude: wplace.lat!, longitude: wplace.lnt!)
            self.getWeeklyCondictions(latitude: wplace.lat!, longitude: wplace.lnt!)
        }
    }

}


extension WeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + self.weeklies.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInfoViewCell", for: indexPath) as! MoreInfoViewCell
            cell.visibility = wxCurrent?.vis
            cell.humidity = wxCurrent?.rh
            cell.ultraviolet = wxCurrent?.uvDesc
            cell.wspd = wxCurrent?.wspd
            cell.wdir = wxCurrent?.wdirCardinal
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HourlyViewCell", for: indexPath) as! HourlyViewCell
            cell.results = self.hourlies
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeeklyContentViewCell", for: indexPath) as! WeeklyContentViewCell
            cell.weekly = self.weeklies[indexPath.row - 2]
            return cell
        }
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //self.lastContentOffset = scrollView.contentOffset.y
        //print("@@@contentOffset.y \(scrollView.contentOffset.y)")
    }

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("###contentOffset.y \(scrollView.contentOffset.y)")
        var alpha = scrollView.contentOffset.y / 64
        if alpha > 1 {
            alpha = 1
        } else if alpha < 0 {
            alpha = 0
        }
        if alpha == 1 {
            placeLabel.textColor = Utility.getTintColor()
            backButton.tintColor = Utility.getTintColor()
            searchButton.tintColor = Utility.getTintColor()
        } else {
            placeLabel.textColor = .white
            backButton.tintColor = .white
            searchButton.tintColor = .white
        }
        self.topView.backgroundColor = UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: alpha)
    }
}
