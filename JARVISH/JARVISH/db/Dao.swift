//
//  Dao.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/10.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import FMDB

class Dao: NSObject {
    static let shared = Dao()
    
    var filePath: String = ""
    var database: FMDatabase!
    
    private override init() {
        super.init()
        print("init database")
        let fileURL = try! FileManager.default
           .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
           .appendingPathComponent("JARVISH.sqlite")
        self.database = FMDatabase(url: fileURL)
        
        if self.database.open() {
            print("open database")
            self.createTable()
            self.updateTable()
        } else {
            print("Unable to open database")
        }
    }
    
    
    deinit {
        print("deinit: \(self)")
    }
    
    
    private func createTable() {
        print("create table")
        let table = "create table track (trackId integer primary key autoincrement not null, trackType integer, title text, content text, cover text, distance double, duration integer, averageSpeed double, maxSpeed double, gpsLogId integer, timestamp interer, weaTemp text, weaIcon interer, weaDesc text, helmet text)"
        let tableg = "create table gpsTracks (gpsTrackId integer primary key, gpsTrack text)"
        
       
        do {
            try self.database!.executeUpdate(table, values: nil)
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        
        do {
            try self.database!.executeUpdate(tableg, values: nil)
        } catch {
            print("failed: \(error.localizedDescription)")
        }
    }
    
    
    private func updateTable() {
        if !self.database.columnExists("weaTemp", inTableWithName: "track") {
            let after1 = "ALTER TABLE track ADD COLUMN weaTemp text"
            let after2 = "ALTER TABLE track ADD COLUMN weaIcon interer"
            let after3 = "ALTER TABLE track ADD COLUMN weaDesc text"
            
            do {
                try self.database!.executeUpdate(after1, values: nil)
                try self.database!.executeUpdate(after2, values: nil)
                try self.database!.executeUpdate(after3, values: nil)
            } catch {
                print("failed to add weather to table: \(error.localizedDescription)")
            }
        } else {
            print("don't need to add weather")
        }
        
        if !self.database.columnExists("helmet", inTableWithName: "track") {
            let after1 = "ALTER TABLE track ADD COLUMN helmet text"
            
            do {
                try self.database!.executeUpdate(after1, values: nil)
            } catch {
                print("failed to add helmet to table: \(error.localizedDescription)")
            }
        } else {
            print("don't need to add helmet")
        }
        
    }
    
    
    func queryTracks() -> [TrackLog] {
        print("#############################")
        let sql = "SELECT * FROM track"
        var tracks: [TrackLog] = []
        do {
            let res = try self.database!.executeQuery(sql, values: nil)
            while res.next() {
                print("column count: \(res)")
                let track = TrackLog.init(
                    trackId: res.long(forColumn: "trackId"),
                    trackType: res.long(forColumn: "trackType"),
                    title: res.string(forColumn: "title")!,
                    content: res.string(forColumn: "content")!,
                    cover: res.string(forColumn: "cover")!,
                    distance: res.double(forColumn: "distance"),
                    duration: res.long(forColumn: "duration"),
                    averageSpeed: res.double(forColumn: "averageSpeed"),
                    maxSpeed: res.double(forColumn: "maxSpeed"),
                    gpsLogId: res.long(forColumn: "gpsLogId"),
                    timestamp: res.long(forColumn: "timestamp"),
                    weaTemp: res.string(forColumn: "weaTemp"),
                    weaIcon: res.long(forColumn: "weaIcon"),
                    weaDesc: res.string(forColumn: "weaDesc"),
                    helmet: res.string(forColumn: "helmet"))
                tracks.append(track)
            }
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        print("#############################")
        return tracks
    }
    
    
    func queryGpsTracks(id: Int) -> [GpsLog] {
        let sql = "SELECT * FROM gpsTracks where gpsTrackId = ?"
        var logs: [GpsLog] = []
        do {
            let res = try self.database!.executeQuery(sql, values: [id])
            //print("GpsTracks column columnCount: \(res.columnCount)")
            while res.next() {
                print("id \(String(describing: res.string(forColumn: "gpsTrackId")))")
                let data = res.string(forColumn: "gpsTrack")!.data(using: .utf8)!
                do {
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String] {
                            //print("jsonArray: \(jsonArray)")
                            jsonArray.forEach { item in
                                let gps = parseGpsLog(text: item)
                                logs.append(gps!)
                        }
                        return logs
                    }
                } catch let error as NSError {
                    print(error)
                }
            }
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        return logs
    }
    
    
    func parseGpsLog(text: String) -> GpsLog? {
        if let data = text.data(using: .utf8) {
            print("data: \(data)")
            do {
                if let item = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
                    let gps = GpsLog.init(dictionary: item)
                    //print("gps \(String(describing: gps))")
                    return gps!
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func insertGpsTracks(id: Int, data: String) {
        let sql = "insert into gpsTracks (gpsTrackId, gpsTrack) values (?,?)"
        do {
            try self.database!.executeUpdate(sql, values: [id, data])
        } catch {
            print("failed: \(error.localizedDescription)")
        }
    }
    
    
    func insertTrack(track: TrackLog) {
        let sql = "insert into track (trackType, title, content, cover, distance, duration, averageSpeed, maxSpeed, gpsLogId, timestamp, weaTemp, weaIcon, weaDesc, helmet) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        do {
            try self.database!.executeUpdate(sql, values: [track.trackType, track.title, track.content, track.cover, track.distance, track.duration, track.averageSpeed, track.maxSpeed, track.gpsLogId, track.timestamp, track.weaTemp ?? 0, track.weaIcon ?? -10, track.weaDesc ?? "", track.helmet ?? ""])
        } catch {
            print("failed: \(error.localizedDescription)")
        }
    }
    
    
    func deleteTrack(track: TrackLog) {
        let sql1 = "delete from track where trackId = ?"
        do {
            try self.database!.executeUpdate(sql1, values: [track.trackId])
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        let sql2 = "delete from gpsTracks where gpsTrackId = ?"
        do {
            try self.database!.executeUpdate(sql2, values: [track.gpsLogId])
        } catch {
            print("failed: \(error.localizedDescription)")
        }
    }
    
    
    func updateTrack(track: TrackLog) {
        let sql1 = "update track set title = ? where trackId = ?"
        do {
            try self.database!.executeUpdate(sql1, values: [track.title, track.trackId])
        } catch {
            print("failed: \(error.localizedDescription)")
        }
    }
}
