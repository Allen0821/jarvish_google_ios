//
//  GpsLog.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/10.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct GpsLog: Codable {
    var latitude: Double
    var longitude: Double
    var altitude: Double
    var speed: Double
    var dateTime: Int
    
    
    init(latitude: Double, longitude: Double, altitude: Double, speed: Double, dateTime: Int) {
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.speed = speed
        self.dateTime = dateTime
    }
    
    
    var dictionary: [String: Any] {
        return [
            "latitude": latitude,
            "longitude": longitude,
            "altitude": altitude,
            "speed": speed,
            "dateTime": dateTime
        ]
    }
}


extension GpsLog: Serializable {
        init?(dictionary: [String : Any?]) {
            let latitude = dictionary["latitude"] as! Double
            let longitude = dictionary["longitude"] as! Double
            let altitude = dictionary["altitude"] as! Double
            let speed = dictionary["speed"] as! Double
            let dateTime = dictionary["dateTime"] as! Int
            
            self.init(
                latitude: latitude,
                longitude: longitude,
                altitude: altitude,
                speed: speed,
                dateTime: dateTime
            )
        }
}
