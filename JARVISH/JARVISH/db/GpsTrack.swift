//
//  GpsTrack.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/14.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct GpsTrack {
    var gpsTrackId: Int
    var gpsTrack: [GpsLog]
    
    var dictionary: [String: Any] {
        return [
            "gpsTrackId": gpsTrackId,
            "gpsTrack": gpsTrack
        ]
    }
}


extension GpsTrack: Serializable {
        init?(dictionary: [String : Any?]) {
            let gpsTrackId = dictionary["gpsTrackId"] as! Int
            let gpsTrack = dictionary["gpsTrack"] as! [GpsLog]
            
            self.init(
                gpsTrackId: gpsTrackId,
                gpsTrack: gpsTrack
            )
        }
}
