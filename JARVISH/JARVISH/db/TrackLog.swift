//
//  TrackLog.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/14.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

struct TrackLog {
    var trackId: Int
    var trackType: Int
    var title: String
    var content: String
    var cover: String
    var distance: Double
    var duration: Int
    var averageSpeed: Double
    var maxSpeed: Double
    var gpsLogId: Int
    var timestamp: Int
    var weaTemp: String?
    var weaIcon: Int?
    var weaDesc: String?
    var helmet: String?
  
    var dictionary: [String: Any] {
        return [
            "trackId": trackId,
            "trackType": trackType,
            "title": title,
            "content": content,
            "cover": cover,
            "distance": distance,
            "duration": duration,
            "averageSpeed": averageSpeed,
            "maxSpeed": maxSpeed,
            "gpsLogId": gpsLogId,
            "timestamp": timestamp,
            "weaTemp": weaTemp ?? 0,
            "weaIcon": weaIcon ?? -10,
            "weaDesc": weaDesc ?? "",
            "helmet": helmet ?? "x01"
        ]
    }
}


extension TrackLog: Serializable {
        init?(dictionary: [String : Any?]) {
            let trackId = dictionary["trackId"] as! Int
            let trackType = dictionary["trackType"] as! Int
            let title = dictionary["title"] as! String
            let content = dictionary["content"] as! String
            let cover = dictionary["cover"] as! String
            let distance = dictionary["distance"] as! Double
            let duration = dictionary["duration"] as! Int
            let averageSpeed = dictionary["averageSpeed"] as! Double
            let maxSpeed = dictionary["maxSpeed"] as! Double
            let gpsLogId = dictionary["gpsLogId"] as! Int
            let timestamp = dictionary["timestamp"] as! Int
            let weaTemp = dictionary["weaTemp"] as! String
            let weaIcon = dictionary["weaIcon"] as! Int
            let weaDesc = dictionary["weaDesc"] as! String
            let helmet = dictionary["helmet"] as! String
            
            self.init(
                trackId: trackId,
                trackType: trackType,
                title: title,
                content: content,
                cover: cover,
                distance: distance,
                duration: duration,
                averageSpeed: averageSpeed,
                maxSpeed: maxSpeed,
                gpsLogId: gpsLogId,
                timestamp: timestamp,
                weaTemp: weaTemp,
                weaIcon: weaIcon,
                weaDesc: weaDesc,
                helmet: helmet
            )
        }
}

