//
//  ExportGpx.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/16.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import AEXML

class ExportGpx {
    
    static func createGpxFile(track: TrackLog, gps: [GpsLog]) -> NSURL {
        let docUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        print("createGpx doc \(docUrl)")
        let fileName = "jarvish_" + getTimeFormat() + ".gpx"
        let gpxFile = docUrl.appendingPathComponent(fileName)
        let gpxRequest = AEXMLDocument()
        let attributes = [
            "xmlns": "http://www.topografix.com/GPX/1/1",
            "xmlns:gpxx" : "http://www.jarvish.com/GpxExtensions/v3",
            "xmlns:gpxtpx" : "http://www.jarvish.com/xmlschemas/TrackPointExtension/v1",
            "xmlns:xsi" : "http://www.w3.org/2001/XMLSchema-instance",
            "creator" : "Jarvish",
            "version" : "1.1",
            "xsi:schemaLocation" : "http://www.jarvish.com/GPX/1/1 http://www.here.com/"]
        let gpxTag = gpxRequest.addChild(name: "gpx", attributes: attributes)
        let trkTag = gpxTag.addChild(name: "trk")
        trkTag.addChild(name: GpxPie.name, value: track.title)
        trkTag.addChild(name: GpxPie.distance, value: "\(track.distance)")
        trkTag.addChild(name: GpxPie.duration, value: "\(track.duration)")
        trkTag.addChild(name: GpxPie.averageSpeed, value: "\(track.averageSpeed)")
        trkTag.addChild(name: GpxPie.maximumSpeed, value: "\(track.maxSpeed)")
        trkTag.addChild(name: GpxPie.weatherTemperature, value: "\(track.weaTemp ?? "")")
        trkTag.addChild(name: GpxPie.weatherIcon, value: "\(track.weaIcon ?? -10)")
        trkTag.addChild(name: GpxPie.weatherDescription, value: "\(track.weaDesc ?? "")")
        trkTag.addChild(name: GpxPie.timestamp, value: getDateTimeFormat(timetamp: track.timestamp))
        trkTag.addChild(name: GpxPie.helmet, value: track.helmet)
        let trksegTag = trkTag.addChild(name: "trkseg")
        for log in gps {
            writeGps(element: trksegTag, log: log)
        }
        print(gpxRequest.xml)
        do {
            try gpxRequest.xml.write(to: gpxFile, atomically: false, encoding: .utf8)
        } catch {
            print("\(error.localizedDescription)")
        }
        return NSURL(fileURLWithPath: gpxFile.absoluteString)
    }
    
    
    private static func writeGps(element: AEXMLElement, log: GpsLog) {
        let attributes = ["lat" : "\(log.latitude)", "lon" : "\(log.longitude)"]
        let trkptTag = element.addChild(name: "trkpt", attributes: attributes)
        trkptTag.addChild(name: "ele", value: "\(log.altitude)")
        trkptTag.addChild(name: "time", value: getDateTimeFormat(timetamp: log.dateTime))
        trkptTag.addChild(name: "speed", value: "\(log.speed)")
    }
    
    
    
    public static func getDateTimeFormat(timetamp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timetamp))
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return dateFormatter.string(from: date)
    }
    
    
    private static func getTimeFormat() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
        return dateFormatter.string(from: date)
    }
}
