//
//  GpxPie.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/8/25.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation

class GpxPie {
    public static let name = "name"
    public static let distance = "distance"
    public static let duration = "duration"
    public static let averageSpeed = "averageSpeed"
    public static let maximumSpeed = "maximumSpeed"
    public static let weatherTemperature = "weatherTemperature"
    public static let weatherIcon = "weatherIcon"
    public static let weatherDescription = "weatherDescription"
    public static let timestamp = "timestamp"
    public static let helmet = "helmet"
   
}
