//
//  ImportGpx.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/6/17.
//  Copyright © 2020 jarvish. All rights reserved.
//

import Foundation
import AEXML

class ImportGpx {
    
    static func readGpxFile(fileUrl: URL, completion: @escaping (_ id: Int)->(), failed: @escaping (_ error: String)->()) {
        do {
            let data = try Data(contentsOf: fileUrl)
            var options = AEXMLOptions()
            options.parserSettings.shouldProcessNamespaces = true
            options.parserSettings.shouldReportNamespacePrefixes = true
            options.parserSettings.shouldResolveExternalEntities = true
            let xmlDoc = try AEXMLDocument(xml: data, options: options)
            print("vvv" +  xmlDoc.xml)
            for child in xmlDoc.root.children {
                print("vvv xmlDoc: " + child.name)
            }
            let title = xmlDoc.root["trk"][GpxPie.name].value ?? ""
            let distance = Double(xmlDoc.root["trk"][GpxPie.distance].value!) ?? 0.0
            let duration = Int(xmlDoc.root["trk"][GpxPie.duration].value!) ?? 0
            let averageSpeed = Double(xmlDoc.root["trk"][GpxPie.averageSpeed].value!) ?? 0.0
            let maxSpeed = Double(xmlDoc.root["trk"][GpxPie.maximumSpeed].value!) ?? 0.0
            let timestamp: Int = Int(convertToTimestamp(string: xmlDoc.root["trk"][GpxPie.timestamp].value!))
            let id = Int(Date().timeIntervalSince1970)
            let weatemp = xmlDoc.root["trk"][GpxPie.weatherTemperature].value ?? ""
            let weaIcon = Int(xmlDoc.root["trk"][GpxPie.weatherIcon].value ?? "-10")
            let weaDesc = xmlDoc.root["trk"][GpxPie.weatherDescription].value ?? ""
            let helmet = xmlDoc.root["trk"][GpxPie.helmet].value ?? ""
            let gps = readGps(gpsDoc: xmlDoc.root["trk"]["trkseg"].children)
            let track = TrackLog.init(trackId: 0, trackType: 0, title: title, content: "", cover: "", distance: distance, duration: duration, averageSpeed: averageSpeed, maxSpeed: maxSpeed, gpsLogId: id, timestamp: timestamp, weaTemp: weatemp, weaIcon: weaIcon, weaDesc: weaDesc, helmet: helmet)
            if storeToDb(id: id, track: track, gps: gps) {
                completion(id)
            } else {
                failed("syroe to db failed")
            }
        } catch {
            failed(error.localizedDescription)
        }
    }
    
    
    private static func storeToDb(id: Int, track: TrackLog, gps: [GpsLog]) -> Bool {
         do {
            print("vvv gps: \(gps)")
            var logs: [String] = []
            gps.forEach { gpsLog in
                do {
                    let log = try JSONEncoder().encode(gpsLog)
                    logs.append(String(data: log, encoding: .utf8)!)
                } catch {
                    print("failed: \(error.localizedDescription)")
                }
            }
            let json = try JSONSerialization.data(withJSONObject: logs, options: [])
            let data = String(data: json, encoding: .utf8)!
            Dao.shared.insertGpsTracks(id: id, data: data)
            Dao.shared.insertTrack(track: track)
            return true
         } catch {
            print("failed: \(error.localizedDescription)")
        }
        return false
    }
    
    
    
    private static func readGps(gpsDoc: [AEXMLElement]) -> [GpsLog] {
        var gpsLogs: [GpsLog] = []
        for doc in gpsDoc {
            let lat: Double = Double(doc.attributes["lat"]!) ?? 0.0
            let lon: Double = Double(doc.attributes["lon"]!) ?? 0.0
            let ele: Double = Double(doc["ele"].value!) ?? 0.0
            let speed: Double = Double(doc["speed"].value!) ?? 0.0
            let dateTime: Int = Int(convertToTimestamp(string: doc["time"].value!))
            
            let log = GpsLog.init(latitude: lat, longitude: lon, altitude: ele, speed: speed, dateTime: dateTime)
            gpsLogs.append(log)
        }
        return gpsLogs
    }
    
    
    private static func convertToTimestamp(string: String) -> TimeInterval {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss'Z'"
        let date = dfmatter.date(from: string)
        return date!.timeIntervalSince1970
    }
}
