//
//  JourneyViewController.swift
//  JARVISH
//
//  Created by Ｃhun-Yang Lin on 2020/4/6.
//  Copyright © 2020 jarvish. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit


class JourneyViewController: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var classView: UIView!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var classButton: UIButton!
    
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var trackButton: UIButton!
    @IBOutlet weak var hintLabel: CustomPaddingLabel!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    var gMap: GMSMapView!
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var path = GMSMutablePath()
    
    var isTracking: Bool = false
    var pauseTracking = false
    var gpsLogs: [GpsLog] = []
    var distance: Double = 0
    var duration: Int = 0
    var lastStep: Int = 0
    var averageSpeed: Double = 0
    var maxSpeed: Double = 0
    var lastLocation: CLLocation?
    var startTime: Date!
    let zoomLevel: Float = 17.5
    var focus = true
    var gPolyline: GMSPolyline?
    
    var current: Current?
    
    var lastSpeed: Double = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setClass()
        hintLabel.padding(4, 4, 8, 8)
        hintLabel.layer.cornerRadius = 8
        hintLabel.layer.masksToBounds = true
        hintLabel.isHidden = true
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: self.zoomLevel)
        self.gMap = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        do {  // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style2", withExtension: "json") {
                self.gMap.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
              } else {
                NSLog("Unable to find style.json")
              }
            self.gMap.delegate = self
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
                            
        self.gMap.isMyLocationEnabled = true
        self.mapView.addSubview(self.gMap)
        self.gMap.isHidden = true
        
        self.locationButton.backgroundColor = Utility.getButtonBgColor()
        self.locationButton.layer.shadowColor = UIColor.black.cgColor
        self.locationButton.layer.shadowRadius = 2
        self.locationButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.locationButton.layer.shadowOpacity = 0.3
        
      
        self.trackButton.layer.cornerRadius = self.trackButton.frame.size.width/2
        self.trackButton.clipsToBounds = true
        self.trackButton.layer.borderColor = UIColor.white.cgColor
        self.trackButton.layer.borderWidth = 2.0
        self.trackButton.layer.shadowColor = UIColor.black.cgColor
        self.trackButton.layer.shadowRadius = 2
        self.trackButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.trackButton.layer.shadowOpacity = 0.3
        
        self.mapButton.backgroundColor = Utility.getButtonBgColor()
        self.mapButton.layer.shadowColor = UIColor.black.cgColor
        self.mapButton.layer.shadowRadius = 2
        self.mapButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.mapButton.layer.shadowOpacity = 0.3
        addLongPressGesture()
        
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 5
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        let tapWeatherIcon = UITapGestureRecognizer(target: self, action: #selector(clickWeather))
        weatherIcon.isUserInteractionEnabled = true
        weatherIcon.addGestureRecognizer(tapWeatherIcon)
        
        let tapWeatherTemp = UITapGestureRecognizer(target: self, action: #selector(clickWeather))
        temperatureLabel.isUserInteractionEnabled = true
        temperatureLabel.addGestureRecognizer(tapWeatherTemp)

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.currentLocation != nil {
            getCurrentWeather(location: self.currentLocation!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("viewWillDisappear")
    }
    
    
    private func setClass() {
        if UserDefaults.standard.object(forKey: "firstClass") == nil {
            self.classView.isHidden = false
            classLabel.text = NSLocalizedString("class_first_moto_button_message", comment: "")
            trackButton.isEnabled = false
        } else {
            self.classView.isHidden = true
            self.trackButton.isEnabled = true
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
      print("mapView... \(gesture)")
        if gesture {
            self.focus = false
        }
    }

    
    
    func addLongPressGesture() {
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
        longPress.minimumPressDuration = 1.5
        self.trackButton.addGestureRecognizer(longPress)
    }


    @objc func clickWeather() {
        DispatchQueue.main.async(){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WeatherViewController") as! WeatherViewController
            vc.currentLocation = self.currentLocation
            vc.wxCurrent = self.current
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }


    
    @IBAction func clickClass(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "firstClass")
        classView.isHidden = true
        trackButton.isEnabled = true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func clickLocation(_ sender: UIButton) {
        if !self.gMap.isHidden {
            self.gMap.animate(toLocation: self.currentLocation!.coordinate)
            self.gMap.animate(toZoom: self.zoomLevel)
            self.focus = true
        }
    }
    
    @IBAction func clickOpenMap(_ sender: UIButton) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://")!, options: [:], completionHandler: nil)
        } else {
            let regionDistance:CLLocationDistance = 10000
            let coordinates = self.currentLocation?.coordinate
            let regionSpan = MKCoordinateRegion(center: coordinates!, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates!, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = ""
            mapItem.openInMaps(launchOptions: options)
        }
    }
    
    private func checkLocationStatus() {
        if #available(iOS 14.0, *) {
            print("[Location]: user auth 14 = \(locationManager.authorizationStatus)")
            if locationManager.authorizationStatus == .authorizedAlways {
                print("[Location]: always")
            } else {
                let alert = UIAlertController(
                    title: nil,
                    message: NSLocalizedString("set_location_always", comment: ""),
                    preferredStyle: .alert)
                let cancel = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: nil)
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func clickTrack(_ sender: UIButton) {
        if self.isTracking {
            if pauseTracking {
                checkLocationStatus()
                self.lastStep = Int(Date().timeIntervalSince1970)
                hintLabel.isHidden = false
                hintLabel.text = NSLocalizedString("track_tracking_hint", comment: "")
                pauseTracking = false
                self.trackButton.setImage(UIImage(named: "pause"), for: .normal)
            } else {
                hintLabel.isHidden = false
                hintLabel.text = NSLocalizedString("track_pause_hint", comment: "")
                pauseTracking = true
                self.trackButton.setImage(UIImage(named: "moto"), for: .normal)
            }
            print("pauseTracking \(pauseTracking)")
        } else {
            checkLocationStatus()
            hintLabel.isHidden = false
            hintLabel.text = NSLocalizedString("track_tracking_hint", comment: "")
            self.trackButton.setImage(UIImage(named: "pause"), for: .normal)
            self.isTracking = true
            self.distance = 0
            self.duration = 0
            self.lastStep = 0
            self.averageSpeed = 0
            self.maxSpeed = 0
            self.startTime = Date()
            self.focus = true
            pauseTracking = false
        }
    }
    
    private func trackLogVerification() -> Bool {
        if self.self.gpsLogs.count < 2 {
            return false
        }
        return true
    }
    
    private func invalidTrackAlert() {
        let alert = UIAlertController(title: NSLocalizedString("track_invalid_title", comment: ""), message: NSLocalizedString("track_invalid_content_store", comment: ""), preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let cancel = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
        })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    private func storeTrackAlert() {
        let alert = UIAlertController(title: NSLocalizedString("track_stop_title", comment: ""), message: NSLocalizedString("track_stop_content", comment: ""), preferredStyle: .alert)
        alert.overrideUserInterfaceStyle = .dark
        let confirm = UIAlertAction(title: NSLocalizedString("action_confirm", comment: ""), style: .default, handler: {(alert: UIAlertAction!) in
            if !self.trackLogVerification() {
                self.invalidTrackAlert()
            } else {
                self.storeGpslogs()
            }
            self.trackButton.setImage(UIImage(named: "moto"), for: .normal)
            self.isTracking = false
            self.gMap.clear()
            self.trackButton.tintColor = Utility.getTintColor()
            self.self.gpsLogs.removeAll()
            self.hintLabel.isHidden = true
        })
        alert.addAction(confirm)
        let cancel = UIAlertAction(title: NSLocalizedString("action_cancel", comment: ""), style: .cancel, handler: {(alert: UIAlertAction!) in
        })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @objc func longPress(gesture: UILongPressGestureRecognizer) {
        if self.isTracking {
            storeTrackAlert()
        }
    }
    
    
    private func storeGpslogs() {
        var logs: [String] = []
        self.gpsLogs.forEach { gpsLog in
            do {
                let log = try JSONEncoder().encode(gpsLog)
                logs.append(String(data: log, encoding: .utf8)!)
            } catch {
                print("failed: \(error.localizedDescription)")
            }
        }
        do {
            let json = try JSONSerialization.data(withJSONObject: logs, options: [])
            let data = String(data: json, encoding: .utf8)!
            let id = Int(Date().timeIntervalSince1970)
            Dao.shared.insertGpsTracks(id: id, data: data)
            storeTrack(gpsLogId: id)
        } catch {
            print("failed: \(error.localizedDescription)")
        }
        
        
    }
    
    
    private func storeTrack(gpsLogId: Int) {
        let d1 = (self.gpsLogs[self.gpsLogs.count - 1].dateTime -
            self.gpsLogs[0].dateTime)
        var d: Int
        if d1 > self.duration {
            d = self.duration
        } else {
            d = d1
        }
        let averageSpeed = distance / Double(duration)
        let title = Utility.getTrackItemTitleString(timetamp: Int(self.startTime.timeIntervalSince1970))
        /*print("weaIndex1: \(String(describing: self.current!.wxIcon))")
               print("weaIndex2: \(String(describing: self.current!.temp))")
               print("weaIndex3: \(String(describing: self.current!.wxPhrase))")*/
        let temp =  WeatherUtil.getTemperatureText(tempture: self.current!.temp)
        let helmet = UserDefaults.standard.string(forKey: "YOUR_HELMET")
        let track = TrackLog.init(trackId: 0, trackType: 0, title: title, content: "", cover: "", distance: self.distance, duration: d, averageSpeed: averageSpeed, maxSpeed: self.maxSpeed, gpsLogId: gpsLogId, timestamp: Int(Date().timeIntervalSince1970), weaTemp: temp, weaIcon: self.current!.wxIcon, weaDesc: self.current!.wxPhrase, helmet: helmet)
        Dao.shared.insertTrack(track: track)
        clearPolyline()
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TRACKCHANGED"), object: nil, userInfo: nil)
    }
    
    
    private func clearPolyline() {
        self.gMap.clear()
        self.gpsLogs.removeAll()
        path.removeAllCoordinates()
    }
    
}


extension JourneyViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        //print("[Location]: current \(location)")
        self.currentLocation = location
        

        if self.gMap.isHidden {
            self.gMap.isHidden = false
            self.gMap.animate(toLocation: location.coordinate)
            self.gMap.animate(toZoom: self.zoomLevel)
        } else {
            if isTracking && !pauseTracking {
                
                if !self.filterLocation(location: location) {
                    return
                }
                
                path.add(location.coordinate)
                let polyline = GMSPolyline(path: self.path)
                polyline.strokeWidth = 8.0
                polyline.strokeColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.8)
                self.gPolyline = polyline
                self.gPolyline?.map = self.gMap
                let dateTime: Int = Int(Date().timeIntervalSince1970)
                if (self.lastStep != 0) {
                    duration = duration + (dateTime - self.lastStep);
                }
                self.lastStep = dateTime
                //print("getDuration: \(self.lastStep) \(duration)")
                let gps = GpsLog.init(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude, altitude: location.altitude, speed: location.speed, dateTime: dateTime)
                //print("gps: \(gps)")
                self.gpsLogs.append(gps)
                if self.lastLocation != nil {
                    self.distance = self.distance + self.currentLocation!.distance(from: self.lastLocation!)
                }
                //print("speed: \(String(describing: self.currentLocation?.speed))")
                if self.currentLocation!.speed > self.maxSpeed {
                   self.maxSpeed = self.currentLocation!.speed
                }
                self.lastLocation = self.currentLocation
                if self.focus {
                    self.gMap.animate(toLocation: location.coordinate)
                }
            }
        }
        
        if self.current == nil ||
            TimeInterval(self.current!.expireTimeGmt) <= Date().timeIntervalSince1970 {
            getCurrentWeather(location: self.currentLocation!)
        }
        
    }
  
    
    private func filterLocation(location: CLLocation) -> Bool {
        //print("filter speed: \(location.speed)")
     
        if self.gpsLogs.count == 0 {
            return true
        }
        
        if location.speed == -1 || (location.speed == 0 && self.lastSpeed == 0) {
            return false
        }
        self.lastSpeed = location.speed
        
        if self.lastLocation != nil && location.distance(from: self.lastLocation!) > 5 {
            return true
        }
        
        return false
    }
    
    
    private func getCurrentWeather(location: CLLocation) {
        WeatherManager.shared().getCurrentCondictions(
            latitude: location.coordinate.latitude,
            longitude: location.coordinate.longitude, completion: { current in
                self.current = current
                let icon = "weather." + String(self.current!.wxIcon);
                self.weatherIcon.image = UIImage(named: icon)
                self.temperatureLabel.text = WeatherUtil.getTemperatureText(tempture: self.current!.temp)
        }) { error in
            
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("[Location]: access was restricted.")
            mapView.isHidden = false
        case .denied:
            print("[Location]: User denied access to location.")
            mapView.isHidden = true
        case .notDetermined:
            print("[Location]: status not determined.")
        case .authorizedAlways:
            print("[Location]: always")
            mapView.isHidden = false
        case .authorizedWhenInUse:
          print("[Location]: in use")
            mapView.isHidden = false
        @unknown default:
            fatalError()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error)")
    }
}
